<?php
/*
	Template Name: Store Finder
*/

// Only adding the "entry-content" post class on non-woocommerce pages to avoid CSS conflicts
$post_class = (nm_is_woocommerce_page()) ? '' : 'entry-content';

get_header(); ?>
    <div class="nm-row">
        <div class="col-xs-12">
            <?php if (have_posts()): while (have_posts()) : the_post(); ?>

                <div id="post-<?php the_ID(); ?>" <?php post_class($post_class); ?>>
                    <?php the_content(); ?>
                </div>

                <?php
            endwhile;
            else :
                ?>

                <div>
                    <h2><?php esc_html_e('Sorry, nothing to display.', 'nm-framework'); ?></h2>
                </div>

            <?php endif; ?>
        </div>
    </div>

    <div class="nm-row">
        <div class="col-xs-12">
            <?php while (have_posts()) : the_post(); ?>
            <?php endwhile; ?>
            <?php
            $pins = [];
            function aasort($array, $key)
            {
                $sorter = array();
                $ret = array();
                reset($array);
                foreach ($array as $ii => $va) {
                    $sorter[] = $va[$key];
                }
                asort($sorter);
                foreach ($sorter as $ii => $va) {
                    $ret[] = $array[$ii];
                }
                return $array = $ret;
            }

            $re = '/([0-9]*).*/';
            if (have_rows('addresses', 'option')):
                $k = 0;
                while (have_rows('addresses', 'option')) : the_row();
                    $pins[$k] = [
                        'shop_name' => get_sub_field('shop_name'),
                        'shop_address' => get_sub_field('shop_address'),
                        'shop_phone' => get_sub_field('shop_phone')
                    ];
                    $str = str_replace(["\n", "\r", ""], "", strip_tags($pins[$k]['shop_address']));
                    preg_match($re, $str, $matches);
                    $pins[$k]['index'] = $matches[1];
                    $k++;
                endwhile;
                $pins = aasort($pins, 'index');
            endif;
            ?>
            <div class="col-xs-12 map-all">
                <div class="search-map">
                    <div class="map-list">
                        <div class="search-country">
                            <form action="" class="nm-row form-search-country">
                                <div class="col-xs-12 col-sm-12 input-block">
                                    <input id="search_input" class="input-search" type="text" placeholder="To search the store, enter the name of the city...">
                                    <div id="results"></div>
                                </div>
                                <button id="search" class="col-xs-12 col-sm-12 button-search">Search</button>
                            </form>
                        </div>
                        <div class="map-list-inner">
                            <?php foreach ($pins as $key => $pin) : ?>
                                <div class="list-city-market" onclick="openInfowindow(<?=$key?>)">
                                    <div class="market-name"><?= $pin['shop_name']; ?></div>
                                    <div class="market-address">
                                        <i class="material-icons material-icons-search">location_on</i>
                                        <?= $pin['shop_address']; ?>
                                    </div>
                                    <div class="market-phone">
                                        <i class="material-icons material-icons-search">phone</i>
                                        <?= $pin['shop_phone']; ?>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <div class="map" id="map"></div>
                </div>
            </div>
        </div>
    </div>
    <script>
        var map, geocoder, prev_infowindow =false;
        var infowindow = [], marker = [];
        function openInfowindow(id){
            google.maps.event.trigger(marker[id], 'click');
        }
        function initMap() {
            var uluru = {lat: 52.5648988, lng: 13.3983221};
            map = new google.maps.Map(document.getElementById('map'), {
                zoom: 11,
                center: uluru
            });
            var image = new google.maps.MarkerImage("<?=get_template_directory_uri()?>/img/map-marker.png", new google.maps.Size(36, 44), new google.maps.Point(0, 0), new google.maps.Point(18, 44));
            <?php
            if (have_rows('addresses', 'option')):
            $k = 0;
            while (have_rows('addresses', 'option')) : the_row();
            $location = get_sub_field('shop_map');
            $address = get_sub_field('shop_address');
            ?>
            marker[<?=$k?>] = new google.maps.Marker({
                position: {lat: <?=$location['lat']?>, lng: <?=$location['lng']?>},
                icon: image,
                map: map
            });
            infowindow[<?=$k?>] = new google.maps.InfoWindow({
                content: '<?=str_replace("\n", "", strip_tags($address))?>'
            });
            google.maps.event.addListener(marker[<?=$k?>], 'click', function () {
                if( prev_infowindow ) {
                    prev_infowindow.close();
                }

                prev_infowindow = infowindow[<?=$k?>];
                infowindow[<?=$k?>].open(map, marker[<?=$k?>]);
            });
            <?php $k++;
            endwhile;
            endif;
            ?>
            geocoder = new google.maps.Geocoder();
        }
        (function ($) {
            var results;

            function searchTmpl(id, name) {
                return "<div data-id=\"" + id + "\">" + name + "</div>"
            }

            function searchAddress(address, success) {
                geocoder.geocode({'address': address}, function (r, s) {
                    results = r;
                    $("#results").removeClass('hidden').text('');
                    if (s == google.maps.GeocoderStatus.OK) {
                        results.forEach(function (i, n) {
                            $("#results").append(searchTmpl(n, i.formatted_address));
                        });
                        if (success != undefined) {
                            success();
                        }
                    } else {
                        console.log('Something went wrong.');
                    }
                });
            }

            function setMapCenter(id) {
                map.setOptions({
                    center: results[id].geometry.location,
                    zoom: 12
                });
            }

            $('#search').click(function (e) {
                e.preventDefault();
                searchAddress(this.value, setMapCenter(0));
            });
            $('#search_input').keyup(function (e) {
                if (this.value.length >= 3) {
                    searchAddress(this.value);
                } else {
                    $("#results").text('');
                }
            });
            $("#results").on('click', 'div', function () {
                id = $(this).data('id');
                $("#results").addClass('hidden');
                setMapCenter(id);
            });
        })(jQuery);
    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyALw8v3rBzndGUB0D5QRlf1gnSSf44H40k&libraries=places&callback=initMap">
    </script>

<?php get_footer(); ?>