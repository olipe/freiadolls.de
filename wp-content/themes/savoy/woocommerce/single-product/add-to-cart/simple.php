<?php
/**
 * Simple product add to cart
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/add-to-cart/simple.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see        https://docs.woocommerce.com/document/template-structure/
 * @author        WooThemes
 * @package    WooCommerce/Templates
 * @version     3.0.0
 * NM: Modified */
if (!defined('ABSPATH')) {
    exit;
}

global $product;

if (!$product->is_purchasable()) {
    return;
}

echo wc_get_stock_html($product);

if ($product->is_in_stock()) : ?>

    <?php do_action('woocommerce_before_add_to_cart_form'); ?>
    <form class="cart" method="post" enctype='multipart/form-data'>
        <?php
        /**
         * @since 2.1.0.
         */
        do_action('woocommerce_before_add_to_cart_button');

        /**
         * @since 3.0.0.
         */
        do_action('woocommerce_before_add_to_cart_quantity');

        woocommerce_quantity_input(array(
            'min_value' => apply_filters('woocommerce_quantity_input_min', $product->get_min_purchase_quantity(), $product),
            'max_value' => apply_filters('woocommerce_quantity_input_max', $product->get_max_purchase_quantity(), $product),
            'input_value' => isset($_POST['quantity']) ? wc_stock_amount($_POST['quantity']) : $product->get_min_purchase_quantity(),
        ));

        /**
         * @since 3.0.0.
         */
        do_action('woocommerce_after_add_to_cart_quantity');
        ?>

        <button type="submit" name="add-to-cart" value="<?php echo esc_attr($product->get_id()); ?>" class="nm-simple-add-to-cart-button single_add_to_cart_button button alt"><?php echo esc_html($product->single_add_to_cart_text()); ?></button>
        <?php
//        $_pf = new WC_Product_Factory();
//        $_product = $_pf->get_product(14193);
        ?>

        <?php
        /**
         * @since 2.1.0.
         */
        do_action('woocommerce_after_add_to_cart_button');
        ?>
    </form>
<?php /*
    <div class="product-form__certificate">
        <?php
            $certificate_id = 14193; //certificate
            $_pf = new WC_Product_Factory();
            $_certificate = $_pf->get_product(14193);
            $certificatePrice = $_certificate->get_price();
        ?>
        <span class="get_cert"><?php echo esc_html($product->single_add_to_cart_text()) ?></span>
        <div class="form__certificate">
            <form class="certificate-form <?=ICL_LANGUAGE_CODE?>">
                <p class="form-text"><? _e('Ich habe solange auf dich gewartet. Jetzt sind wir Freunde fürs Leben. Willkommen!', 'freiadolls');?></p>
                <div class="main-form-div">
                    <div class="form-div">
                        <p><?php _e('Name:', 'nm-framework') ?></p>
                        <p><?php _e('Date of birth:', 'nm-framework') ?></p>
                        <p><?php _e('Place of birth:', 'nm-framework') ?></p>
                        <p><?php _e('Height:', 'nm-framework') ?></p>
                        <p><?php _e('For:', 'nm-framework') ?></p>
                    </div>
                    <div class="form-div">
                        <div class="input-block">
                            <input type="text" name="name">
                            <span class="error-span"></span>
                        </div>
                        <div class="input-block">
                            <input type="text" name="date" id="datepicker">
                            <span class="error-span"></span>
                        </div>
                        <div class="input-block">
                            <input type="text" name="place">
                            <span class="error-span"></span>
                        </div>
                        <div class="input-block">
                            <input type="text" name="height" disabled>
                            <span class="error-span"></span>
                        </div>
                        <div class="input-block">
                            <input type="text" name="for">
                            <span class="error-span"></span>
                        </div>
                    </div>
                    <input type="hidden" name="language" value="<?=ICL_LANGUAGE_CODE?>">
                    <button type="submit" class="get_cert-button"><?php _e('Bookable for only', 'freiadolls')?> +<?php echo $certificatePrice ?> EUR</button>
                    <button type="submit" name="add-to-cart" value="<?php echo esc_attr($product->get_id()); ?>"
                            class="nm-simple-add-to-cart-button single_add_to_cart_button button alt"><?php
                         _e('Next without a birth certificate', 'freiadolls');
                    ?></button>
                    <span class="close-form-certificate"></span>
                </div>
            </form>
        </div>
    </div>
    */
?>

    <?php do_action('woocommerce_after_add_to_cart_form'); ?>

<?php endif; ?>
