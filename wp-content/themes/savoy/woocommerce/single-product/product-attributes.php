<?php
/**
 * Product attributes
 *
 * Used by list_attributes() in the products class.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-attributes.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see        https://docs.woocommerce.com/document/template-structure/
 * @author        WooThemes
 * @package    WooCommerce/Templates
 * @version     3.1.0
 */

if (!defined('ABSPATH')) {
    exit;
}
?>
<div class="flex-attributes">
    <table class="shop_attributes">
        <?php if ($display_dimensions && $product->has_weight()) : ?>
            <tr>
                <th><?php _e('Weight', 'woocommerce') ?></th>
                <td class="product_weight"><?php echo esc_html(wc_format_weight($product->get_weight())); ?></td>
            </tr>
        <?php endif; ?>

        <?php if ($display_dimensions && $product->has_dimensions()) : ?>
            <tr>
                <th><?php _e('Dimensions', 'woocommerce') ?></th>
                <td class="product_dimensions"><?php echo esc_html(wc_format_dimensions($product->get_dimensions(false))); ?></td>
            </tr>
        <?php endif; ?>

        <?php foreach ($attributes as $attribute) : ?>
            <tr>
                <th><?php echo wc_attribute_label($attribute->get_name()); ?></th>
                <td><?php
                    $values = array();

                    if ($attribute->is_taxonomy()) {
                        $attribute_taxonomy = $attribute->get_taxonomy_object();
                        $attribute_values = wc_get_product_terms($product->get_id(), $attribute->get_name(), array('fields' => 'all'));

                        foreach ($attribute_values as $attribute_value) {
                            $value_name = esc_html($attribute_value->name);

                            if ($attribute_taxonomy->attribute_public) {
                                $values[] = '<a href="' . esc_url(get_term_link($attribute_value->term_id, $attribute->get_name())) . '" rel="tag">' . $value_name . '</a>';
                            } else {
                                $values[] = $value_name;
                            }
                        }
                    } else {
                        $values = $attribute->get_options();

                        foreach ($values as &$value) {
                            $value = make_clickable(esc_html($value));
                        }
                    }

                    echo apply_filters('woocommerce_attribute', wpautop(wptexturize(implode(', ', $values))), $attribute, $values);
                    ?></td>
            </tr>
        <?php endforeach; ?>
    </table>
    <div class="four-blocks">
        <div class="one-block">
            <img src="<?= get_template_directory_uri() ?>/img/1.png" alt="">
            <div class="text-for-one-block">
                <div class="text-title-one-block">
                    <?php _e('ECO FRIENDLY', 'nm-framework') ?>
                </div>
                <p><?php _e('made from natural materials', 'nm-framework') ?></p>
            </div>
        </div>
        <div class="one-block">
            <img src="<?= get_template_directory_uri() ?>/img/2.png" alt="">
            <div class="text-for-one-block">
                <div class="text-title-one-block">
                    <?php _e('NICE TO TOUCH', 'nm-framework') ?>
                </div>
                <p><?php _e('your child will love to pick up, just for the feel of holding them close', 'nm-framework') ?></p>
            </div>
        </div>
        <div class="one-block">
            <img src="<?= get_template_directory_uri() ?>/img/3.png" alt="">
            <div class="text-for-one-block">
                <div class="text-title-one-block">
                    <?php _e('HANDMADE', 'nm-framework') ?>
                </div>
                <p><?php _e('Every toy is unique, with its own character and charm', 'nm-framework') ?></p>
            </div>
        </div>
        <div class="one-block">
            <img src="<?= get_template_directory_uri() ?>/img/4.png" alt="">
            <div class="text-for-one-block">
                <div class="text-title-one-block">
                    <?php _e('SAFE', 'nm-framework') ?>
                </div>
                <p><?php _e('comply with safe the Standard EN 71', 'nm-framework') ?></p>
            </div>
        </div>
    </div>
</div>