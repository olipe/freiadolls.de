<?php
/**
 * Description tab
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/tabs/description.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 NM: Modified */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post,$wpdb;

$langpost = [];
$post_id = 0;
$result = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}postmeta WHERE meta_key= 'attach_to_product' AND meta_value = ".$post->ID);
if($result){
    foreach ($result as $value) {
        $langpost[] = $value->post_id;
    }
    if(!empty($langpost)){
        $result = $wpdb->get_results("SELECT element_id FROM {$wpdb->prefix}icl_translations WHERE element_id IN (".implode(',',$langpost).")");
        if(!empty($result)){
            $post_id = $result[0]->element_id;
        }
    }
}

if($post_id){
    setup_postdata( get_post($post_id) );
?>
    <div class="nm-page-wrap">
        <div class="nm-page-wrap-inner">
            <div class="nm-page-full">
                <div class="entry-content">
<?php  the_content() ?>
                </div>
            </div>
        </div>
    </div>
<?php
    wp_reset_postdata();
} else {
    the_content();
}
