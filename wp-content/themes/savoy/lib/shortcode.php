<?php
add_action( 'init', 'create_post_type' );
function create_post_type()
{
    register_post_type( 'product_description',
        array(
            'labels' => array(
                'name'             => __( 'Products Description' ),
                'singular_name'    => __( 'Product Description Page' ),
                'add_new_item'     => __( 'Add Product Description' ),
                'edit_item'        => __( 'Edit Product Description' ),
                'new_item'         => __( 'New Product Description' ),
                'all_items'        => __( 'All Product Description' ),
            ),
            'menu_position'   => 4,
            'menu_icon'       => 'dashicons-awards',
            'supports'        => array( 'title', 'page-attributes', 'thumbnail', 'editor' ),
            'rewrite'         => array('slug' => 'product_description', 'with_front' => false),
            'hierarchical'    => true,
            'show_ui'         => true,
            'query_var'       => true,
            'capability_type' => 'post',
            'public'          => true,
            'has_archive'     => false,
        )
    );
}
/*
function product_description_function() {
    $projects = get_field_object('blocks', 'option');
    $text = '';
    if(IsSet($projects['value'])){
        foreach($projects['value'] as $row){
            $text.=$row['elements'];
        }
    }
        return $text;
}

add_shortcode( 'product_description', 'product_description_function' );

function product_title_function() {
    global $wp_query;
    return $wp_query->queried_object->post_title;
}

add_shortcode( 'product_title', 'product_title_function' );

function product_imageID_function() {
    global $wp_query;
    return 9982;
}

add_shortcode( 'product_image', 'product_imageID_function' );
*/

function product_slider_category_function($atts = '') {
    global $post;
    $args = array(
        'orderby' => 'date',
        'order' => 'DESC',
        'product_cat' => 'startseite',
        'return' => 'ids',
    );
    $products = get_field_object('goods','option');
    echo'<div class="nm-product-slider col-3" data-slides-to-show="3" data-slides-to-scroll="3">';
        echo'<div class="woocommerce columns-3 ">';
    woocommerce_product_loop_start();
    foreach ($products['value'] as $value) {
        $post = $value['goods_on_the_main'];
        setup_postdata( $post );
        wc_get_template_part( 'content', 'product' );
    }
    woocommerce_product_loop_end();
        echo'</div>';
    echo'</div>';
    echo'<div id="nm-page-includes" class="products product-slider shop_categories " style="display:none;">&nbsp;</div>';
    wp_reset_postdata();
}

add_shortcode( 'product_slider_category', 'product_slider_category_function' );