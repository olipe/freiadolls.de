<?php
if (function_exists('acf_add_options_page')) {

    acf_add_options_page(array(
        'page_title' => 'Theme General Settings',
        'menu_title' => 'Store Settings',
        'menu_slug' => 'theme-general-settings',
        'capability' => 'edit_posts',
        'redirect' => false
    ));

    acf_add_options_sub_page(array(
        'page_title' => 'Theme Settings',
        'menu_title' => 'Popup Text',
        'parent_slug' => 'theme-general-settings',
    ));

    acf_add_options_sub_page(array(
        'page_title' => 'Theme Settings',
        'menu_title' => 'Product Description',
        'parent_slug' => 'theme-general-settings',
    ));

    acf_add_options_sub_page(array(
        'page_title' => 'Theme Settings',
        'menu_title' => 'Shop addresses',
        'parent_slug' => 'theme-general-settings',
    ));
}
function my_acf_init()
{

    acf_update_setting('google_api_key', 'AIzaSyALw8v3rBzndGUB0D5QRlf1gnSSf44H40k');
}

add_action('acf/init', 'my_acf_init');