jQuery(document).ready(function ($) {
    function getDate() {
        var today = new Date();
        // console.log(today);
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }
        return mm + '.' + dd + '.' + yyyy;
    }

    $(document).ready(function () {
        $("#datepicker").datepicker($.datepicker.regional["de"]);
        $('.get_cert').click(function () {
            $('.form__certificate').addClass('opened');
            $('.form__certificate input[name=name]').val($('.product .summary .product_title').text());
            $('.form__certificate input[name=date]').val(getDate());
            // console.log($('.shop_attributes tr:eq(0) td').text());
            $('.form__certificate input[name=height]').val($('.shop_attributes tr:eq(0) td').text());
        });
        $('.close-form-certificate').click(function () {
            $('.form__certificate').removeClass('opened');
        });

        var err = {
            'en' : 'Field is required',
            'de' : 'Feld ist erforderlich'
        };

        $('.certificate-form').submit(function (e) {
            e.preventDefault();
            var form = $(this),
                lang = $(this).find('input[name=language]').val(),
                ok = true,
                data = {'action' : 'add_certificate'};
            form.find('input').each(function () {
                if ( $(this).val() == '' ) {
                    $(this).parent().addClass('error').find('span').text(err[lang]);
                    ok = false;
                } else data[$(this).attr('name')] = $(this).val();
            });
            if (ok) {
                $.post(url, data, function(response) {
                    // console.log('Got this from the server: ' + response);
                    $('.single_add_to_cart_button').click();
                });
            }
            return false;
        });

        $('.input-block input').on('focus click', function () {
            $(this).parent().removeClass('error');
        });
    });

});
