<?php
global $nm_theme_options, $nm_globals;

// Ubermenu
if (function_exists('ubermenu')) {
    $ubermenu = true;
    $ubermenu_wrap_open = '<div class="nm-ubermenu-wrap clear">';
    $ubermenu_wrap_close = '</div>';
} else {
    $ubermenu = false;
    $ubermenu_wrap_open = $ubermenu_wrap_close = '';
}

// Layout class
$header_class = $nm_globals['header_layout'];

// Scroll class
$header_scroll_class = apply_filters('nm_header_on_scroll_class', 'resize-on-scroll'); // Note: Use "static-on-scroll" class to prevent header top/bottom spacing from resizing on-scroll
$header_class .= (strlen($header_scroll_class) > 0) ? ' ' . $header_scroll_class : '';

// Alternative logo class
$header_class .= ($nm_theme_options['alt_logo_config'] != '0') ? ' ' . $nm_theme_options['alt_logo_config'] : '';
?>

<script>
    var url = '<?= admin_url('/admin-ajax.php') ?>';
</script>

<!-- header -->
<header id="nm-header" class="nm-header <?php echo esc_attr($header_class); ?> clear">
    <div class="nm-line">
        <div class="nm-row header-row">
            <div class="col-sm-6 col-xs-12">
                <p><?php _e('KOSTENLOSER VERSAND AB', 'nm-framework') ?> <?php the_field('kostenloser_versand', 'option') ?></p>
            </div>
            <div class="col-sm-6 col-xs-12">
                <ul class="nm-menu pull-right">
                    <?php

                    if (nm_woocommerce_activated() && $nm_theme_options['menu_login']) :
                        ?>
                        <li class="nm-menu-account menu-item">
                            <i class="material-icons">person</i>
                            <?php echo nm_get_myaccount_link(true); ?>
                        </li>
                        <?php
                    endif;

                    if ($nm_globals['wishlist_enabled']) : ?>
                        <li class="nm-menu-wishlist menu-item">
                            <i class="material-icons">favorite</i>
                            <?php
                            $menuParameters = array(
                                'theme_location' => 'right-menu',
                                'container' => false,
                                'echo' => false,
                                'fallback_cb' => false,
                                'items_wrap' => '%3$s',
                                'depth' => 0
                            );
                            echo strip_tags(wp_nav_menu( $menuParameters ), '<a>' );
                            ?>
                        </li>
                    <?php endif;

                    if ($nm_globals['cart_link']) :

                        $cart_menu_class = ($nm_theme_options['menu_cart_icon']) ? 'has-icon' : 'no-icon';
                        $cart_url = ($nm_globals['cart_panel']) ? '#' : WC()->cart->get_cart_url();
                        ?>
                        <li class="nm-menu-cart menu-item <?php echo esc_attr($cart_menu_class); ?>">
                            <i class="material-icons">shopping_cart</i>
                            <a href="<?php echo esc_url($cart_url); ?>" id="nm-menu-cart-btn">
                                <?php echo nm_get_cart_title(); ?>
                                <?php echo nm_get_cart_contents_count(); ?>
                            </a>
                        </li>
                        <?php
                    endif;

                    if ($nm_globals['shop_search_header']) :
                        ?>
                        <li class="nm-menu-search menu-item"><a href="#" id="nm-menu-search-btn"><i
                                        class="nm-font nm-font-search-alt flip"></i></a></li>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </div>
    <div class="nm-header-inner">
        <div class="nm-header-row nm-row">
            <div class="nm-header-logo col-xs-12 col-sm-2">
                <?php
                // Header part: Logo
                get_header('part-logo');
                ?>
            </div>
            <div class="nm-header-col col-xs-12 col-sm-10">
                <div class="nm-row">
                    <div class="col-sm-12 col-xs-12">
                        <?php echo $ubermenu_wrap_open; ?>
                    </div>
                </div>

                <?php if ($ubermenu) : ?>
                    <?php ubermenu('main', array('theme_location' => 'main-menu')); ?>
                <?php else : ?>
                    <nav class="nm-main-menu center-block">
                        <ul id="nm-main-menu-ul" class="nm-menu">
                            <?php
                            wp_nav_menu(array(
                                'theme_location' => 'main-menu',
                                'container' => false,
                                'fallback_cb' => false,
                                'items_wrap' => '%3$s'
                            ));
                            ?>
                        </ul>
                        <?php do_action('wcml_currency_switcher', array('format' => '%code%')); ?>
                    </nav>
                <?php endif; ?>

                <?php echo $ubermenu_wrap_close; ?>
            </div>
        </div>
    </div>

    <?php
    // Shop search-form
    if ($nm_globals['shop_search_header']) {
        //wc_get_template( 'product-searchform_nm.php' );
        get_template_part('woocommerce/product', 'searchform_nm'); // Note: Don't use "wc_get_template()" here in case default checkout is enabled
    }
    ?>

</header>
<!-- /header -->
                    