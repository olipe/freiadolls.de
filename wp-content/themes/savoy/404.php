<?php get_header(); ?>

<div class="nm-row">
    <div class="col-xs-12">
        <div class="nm-page-not-found">
            <h2><?php esc_html_e( 'Oops, page not found.', 'nm-framework' ); ?></h2>
            <div class="nm-page-not-found-image"><img src="<?=get_template_directory_uri()?>/img/bouquet.png" alt="404"></div>
            <p><?php esc_html_e( 'It looks like nothing was found at this location. Click the link below to return home.', 'nm-framework' ); ?></p>
            <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php esc_html_e( 'Homepage &nbsp;&nbsp;&rarr;', 'nm-framework' ); ?></a>
        </div>
    </div>
</div>

<?php get_footer(); ?>
