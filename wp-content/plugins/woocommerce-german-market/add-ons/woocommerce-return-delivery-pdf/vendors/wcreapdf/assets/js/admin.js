jQuery( document ).ready( function( $ ) {

    $( '#woocomerce_wcreapdf_wgm_image_upload_button' ).click( function() {
	
        tb_show( '', 'media-upload.php?type=image&amp;TB_iframe=true' );
        return false;
		
    });

    window.send_to_editor = function( html ) {
        
        imgurl = $( 'img', html ).attr( 'src' );
        
        if ( typeof imgurl == "undefined" ) {
            imgurl = $( html ).attr( 'src' );
        }

        if ( ! $( '#woocomerce_wcreapdf_wgm_pdf_logo_url' ).length ) {
            $( '#woocomerce_wcreapdf_wgm_pdf_logo_url_delivery' ).val( imgurl );
        } else {
            $( '#woocomerce_wcreapdf_wgm_pdf_logo_url' ).val( imgurl );
        }

        
        tb_remove();
    }
	
	$( '#woocomerce_wcreapdf_wgm_image_remove_button' ).click( function() {
		
         if ( ! $( '#woocomerce_wcreapdf_wgm_pdf_logo_url' ).length ) {
            $( '#woocomerce_wcreapdf_wgm_pdf_logo_url_delivery' ).val( '' );
        } else {
            $( '#woocomerce_wcreapdf_wgm_pdf_logo_url' ).val( '' );
        }

    });
	
});
