<?php

/**
 * Class WCEVC_Calculations
 */
class WCEVC_Calculations {

	/**
	 * @var null|WCEVC_Calculations
	 */
	private static $instance = NULL;

	/**
	 * Private clone method to prevent cloning of the instance of the
	 * *Singleton* instance.
	 *
	 * @return void
	 */
	private function __clone() { }

	/**
	 * @return WCEVC_Calculations
	 */
	public static function get_instance() {

		if ( self::$instance === NULL ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	/**
	 * @return  WCEVC_Calculations
	 */
	private function __construct() { 
		add_filter( 'woocommerce_adjust_non_base_location_prices', '__return_false' );
	}

	/**
	 * Returns if the given cart_items contain at least one product for vatmoss-calcualations.
	 *
	 * @param array $cart_items		the cart-items of $cart->get_cart().
	 * @return boolean true|false   true, if the cart has at least one vatmoss-product, false if no vatmoss-product was found.
	 */
	public function has_vatmoss_products( $cart_items ) {
		$has_vatmoss_products = false;

		foreach ( $cart_items as $item ) {
			$product = $item[ 'data' ];
			if ( $this->is_product_vatmoss_eligible( $product ) ) {
				$has_vatmoss_products = true;
				break;
			}
		}

		return $has_vatmoss_products;
	}

	/**
	 *  Check if ...
	 *      - the product is downloadable
	 *      - the selected customer country is inside the EU.
	 *      - the "shop base tax rate" !== "product tax rate"
	 *
	 * This method requires the option 'wcevc_enabled_wgm' set to 'downloadable'.
	 *
	 * @param WC_Product $product
	 *
	 * @return bool
	 */
	public static function is_product_vatmoss_eligible( $product ) {

		if ( ! $product->is_downloadable() ) {
			return false;
		}

		$wc = WC();
		if ( empty( $wc->customer ) ) {
			return false;
		}

		$eu_countries       = $wc->countries->get_european_union_countries();
		$customer_country   = $wc->customer->get_billing_country();
		if ( ! in_array( $customer_country, $eu_countries ) ) {
			return false;
		}

		$tax_rates           = WC_Tax::get_rates( $product->get_tax_class() );
		$shop_base_tax_rates = WC_Tax::get_base_tax_rates( $product->get_tax_class() );
		if ( $tax_rates === $shop_base_tax_rates ) {
			return false;
		}

		return true;
	}

}