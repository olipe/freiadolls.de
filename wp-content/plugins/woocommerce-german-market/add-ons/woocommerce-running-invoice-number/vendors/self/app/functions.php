<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! class_exists( 'WP_WC_Running_Invoice_Number_Functions' ) ) {
	
	/**
	* core functions
	*
	* @WP_WC_Running_Invoice_Number_Functions
	* @version 1.0
	* @category	Class
	*/
	class WP_WC_Running_Invoice_Number_Functions {
		
		/**
		 * @var string
		 * @access private
		 */
		private $prefix;
		
		/**
		 * @var string
		 * @access private
		 */
		private $suffix;
		
		/**
		 * @var integer
		 * @access private
		 */
		private $next_running_number;
		
		/**
		 * @var string
		 * @access private
		 */
		private $invoice_number;
		
		/**
		 * @var int
		 * @access private
		 */
		private $invoice_date;
		
		
		/**
		* constructor
		*
		* @since 0.0.1
		* @access public
		* @param WC_Order
		* @return void
		*/	
		public function __construct( $order ) {
			
			// maybe reset invoice number
			self::reset_number();

			// which options do we have to load? invoice number or refund numbers
			$load_refund_options = false;

			if ( is_a( $order, 'WC_Order_Refund' ) ) {
				if ( get_option( 'wp_wc_running_invoice_refund_separation', 'on' ) == 'on' ) {
					$load_refund_options = true;
				}
			}

			if ( $load_refund_options ) {
				
				$this->prefix				= get_option( 'wp_wc_running_invoice_number_prefix_refund' );
				$this->suffix				= get_option( 'wp_wc_running_invoice_number_suffix_refund' );
				$this->number_of_digits		= absint( get_option( 'wp_wc_running_invoice_number_digits_refund' ) );
				$next_running_invoice_number = ( is_multisite() && get_site_option( 'wp_wc_running_invoice_number_multisite_global', 'no' ) == 'yes' ) ? get_site_option( 'wp_wc_running_invoice_number_next_refund', 1 ) : get_option( 'wp_wc_running_invoice_number_next_refund', 1 );
			
			} else {
				
				$this->prefix				= get_option( 'wp_wc_running_invoice_number_prefix' );
				$this->suffix				= get_option( 'wp_wc_running_invoice_number_suffix' );
				$this->number_of_digits		= absint( get_option( 'wp_wc_running_invoice_number_digits' ) );
				$next_running_invoice_number = ( is_multisite() && get_site_option( 'wp_wc_running_invoice_number_multisite_global', 'no' ) == 'yes' ) ? get_site_option( 'wp_wc_running_invoice_number_next', 1 ) : get_option( 'wp_wc_running_invoice_number_next', 1 );

			}

			// invoice date
			if ( ! is_a( $order, 'WC_Order_Refund' ) ) {
				$post_meta_date	= get_post_meta( $order->get_id(), '_wp_wc_running_invoice_number_date', true );
				if ( $post_meta_date == '' ) {
					$this->invoice_date		= time();
					update_post_meta( $order->get_id(), '_wp_wc_running_invoice_number_date', $this->invoice_date );
				} else {
					$this->invoice_date		= $post_meta_date;
				}
			} else {
				$this->invoice_date = $order->get_date_created()->format( 'Y-m-d' );
			}

			// Change Placeholdes
			$placeholder_date_time = new DateTime();
			$search 		= array( '{{year}}', '{{year-2}}', '{{month}}', '{{day}}' );
			$replace 		= array( $placeholder_date_time->format( 'Y' ), $placeholder_date_time->format( 'y' ), $placeholder_date_time->format( 'm' ), $placeholder_date_time->format( 'd' ) );
			$this->suffix 	= str_replace( $search, $replace, $this->suffix );
			$this->prefix 	= str_replace( $search, $replace, $this->prefix ); 

			$this->next_running_number	= absint( $next_running_invoice_number );
					
			$post_meta_number	= get_post_meta( $order->get_id(), '_wp_wc_running_invoice_number', true );

			// Filter
			$this->suffix 	= apply_filters( 'wp_wc_invoice_number_before_construct_suffix', $this->suffix, $order, $this );
			$this->prefix 	= apply_filters( 'wp_wc_invoice_number_before_construct_prefix', $this->prefix, $order, $this );
			
			// invoice number
			if ( $post_meta_number == '' ) {
				$this->invoice_number	= $this->prefix . str_pad( $this->next_running_number, $this->number_of_digits, '0', STR_PAD_LEFT ) . $this->suffix;
				update_post_meta( $order->get_id(), '_wp_wc_running_invoice_number', $this->invoice_number );
				
				if ( $load_refund_options ) {
					if ( is_multisite() && get_option( 'wp_wc_running_invoice_number_multisite_global', 'no' ) == 'yes' ) {
						update_site_option( 'wp_wc_running_invoice_number_next_refund', ( $this->next_running_number + 1 ) );
					} else {
						update_option( 'wp_wc_running_invoice_number_next_refund', ( $this->next_running_number + 1 ) );
					}
				} else {
					if ( is_multisite() && get_option( 'wp_wc_running_invoice_number_multisite_global', 'no' ) == 'yes' ) {
						update_site_option( 'wp_wc_running_invoice_number_next', ( $this->next_running_number + 1 ) );
					} else {
						update_option( 'wp_wc_running_invoice_number_next', ( $this->next_running_number + 1 ) );
					}
				}
				
			} else {
				$this->invoice_number	= $post_meta_number;
			}

			do_action( 'wp_wc_invoice_number_after_construct', $this, $order );
			
		}

		/**
		* get invoice number
		*
		* @since 0.0.1
		* @access public
		* @return void
		*/	
		public function get_invoice_number() {
			return $this->invoice_number;	
		}
		
		/**
		* get formated invoice date
		*
		* @since 0.0.1
		* @access public
		* @return void
		*/	
		public function get_invoice_date() {
			global $wp_locale;
			return date_i18n( get_option( 'date_format' ), ( intval( $this->invoice_date ) == 0 ) ? time() : $this->invoice_date );	// some error handling, in very few cases ajax-request on shop-order $this->invoice_date is still 0 
		}
		
		/**
		* get invoice timestamp
		*
		* @since 0.0.1
		* @access public
		* @return void
		*/	
		public function get_invoice_timestamp() {
			return ( intval( $this->invoice_date ) == 0 ) ? time() : $this->invoice_date;	// some error handling, in very few cases ajax-request on shop-order $this->invoice_date is still 0 
		}
		
		/**
		* constructor
		*
		* @since 0.0.1
		* @static
		* @access public
		* @return void
		*/
		public static function static_construct( $order ) {
			$object	= new WP_WC_Running_Invoice_Number_Functions( $order );
		}
		
		/**
		* constructor with $order_id
		*
		* @since 0.0.1
		* @static
		* @access public
		* @return void
		*/
		public static function static_construct_by_order_id( $order_id ) {
			$order 	= wc_get_order( $order_id );
			$object	= new WP_WC_Running_Invoice_Number_Functions( $order );
		}

		/**
		* Reset Invoice Number monthly / annually
		*
		* @since GM 3.2
		* @static
		* @access public
		* @return void
		*/
		public static function reset_number() {

			$reset_option = get_option( 'wp_wc_running_invoice_number_reset_interval', 'off' );
			$reset_option = apply_filters( 'wp_wc_running_invoice_number_reset_option', $reset_option );
			
			if ( $reset_option != 'off' ) {

				$now = new DateTime();

				$last_date_string = get_option( '_wp_wc_runninv_invoice_reset_last_date', $now->format( 'Y-m-d' ) );
				$last_date_date_time = new DateTime( $last_date_string );

				$format = 'Y-m';
				
				if ( $reset_option == 'annually' ) {
					$format = 'Y';
				} else if ( $reset_option == 'monthly' ) {
					$format = 'Y-m';
				} else if ( $reset_option == 'daily' ) {
					$format = 'Y-m-d';
				} else {
					$format = apply_filters( 'wp_wc_running_invoice_number_reset_format', 'Y-m-d', $reset_option );
				}

				if ( $now->format( $format ) != $last_date_date_time->format( $format ) ) {

					if ( is_multisite() && get_option( 'wp_wc_running_invoice_number_multisite_global', 'no' ) == 'yes' ) {
						update_site_option( 'wp_wc_running_invoice_number_next', 1 );
						update_site_option( 'wp_wc_running_invoice_number_next_refund', 1 );
					} else {
						update_option( 'wp_wc_running_invoice_number_next', 1 );
						update_option( 'wp_wc_running_invoice_number_next_refund', 1 );
					}

				}

				update_option( '_wp_wc_runninv_invoice_reset_last_date', $now->format( 'Y-m-d' ) );

			}

		}
		
	} // end class
	
} // end if

?>