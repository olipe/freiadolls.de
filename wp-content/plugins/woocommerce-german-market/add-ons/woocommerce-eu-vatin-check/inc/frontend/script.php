<?php
/**
 * Feature Name: Script Functions
 * Version:      1.0
 * Author:       MarketPress
 * Author URI:   http://marketpress.com/
 */

/**
 * Enqueue the scripts.
 *
 * @wp-hook wp_enqueue_scripts
 * @return  void
 */
function wcvat_wp_enqueue_scripts() {

	$scripts = wcvat_get_scripts();

	foreach ( $scripts as $handle => $script ) {

		// load script
		wp_enqueue_script(
			$handle,
			$script[ 'src' ],
			$script[ 'deps' ],
			$script[ 'version' ],
			$script[ 'in_footer' ]
		);

		// check localize
		if ( ! empty( $script[ 'localize' ] ) )
			foreach ( $script[ 'localize' ] as $localize_id => $vars )
				wp_localize_script( $handle, $localize_id, $vars );
	}
}

/**
 * Returning our Scripts
 *
 * @return  array
 */
function wcvat_get_scripts(){

	$scripts = array();
	$suffix = wcvat_get_script_suffix();
	$base_location = wc_get_base_location();

	// adding the main-js
	$scripts[ 'wcvat-js' ] = array(
		'src'       => wcvat_get_asset_directory_url( 'js' ) . 'frontend' . $suffix . '.js',
		'deps'      => array( 'jquery' ),
		'version'   => '3.5.1',
		'in_footer' => TRUE,
		'localize'  => array(
			'wcvat_script_vars' 	=> array(
				'ajaxurl' 			=> admin_url( 'admin-ajax.php' ),
				'error_badge' 		=> '<span class="error-badge">' . __( 'The VATIN is not valid!', 'woocommerce-german-market' ) . '</span>',
				'correct_badge' 	=> '<span class="correct-badge">&nbsp;</span>',
				'spinner' 			=> '<span class="spinner-badge">' . __( 'Validating ...', 'woocommerce-german-market' ) . '</span>',
				'base_country'		=> $base_location[ 'country' ],
				'base_country_hide' => apply_filters( 'wcvat_hide_vat_field_billing_country_equal_shop_country', true ),
			),
		),
	);

	return apply_filters( 'wcvat_get_scripts', $scripts );
}
