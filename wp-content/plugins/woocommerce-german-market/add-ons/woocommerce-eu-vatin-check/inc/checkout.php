<?php
/**
 * Feature Name: Options Page
 * Version:      1.0
 * Author:       MarketPress
 * Author URI:   http://marketpress.com
 */

/**
 * Adds the VAT Field to the billing adress
 *
 * @param array $address_fields
 * @return array
 */
function wcvat_woocommerce_billing_fields( $address_fields ) {

	$vat_field = array(
		'billing_vat' => array(
			'label' => get_option( 'vat_options_label', __( 'VAT Number', 'woocommerce-german-market' ) ),
			'class' => array( 'form-row-wide' )
		),
	);

	$address_fields = wcvat_array_insert( $address_fields, 'billing_address_1', $vat_field );

	return $address_fields;
}

/**
 * Shows the VAT in order received
 *
 * @wp-hook	woocommerce_order_details_after_customer_details
 * @param	object $order
 * @return	void
 */
function wcvat_order_details_after_customer_details( $order ) {

	$vat = get_post_meta( $order->get_id(), 'billing_vat', TRUE );
	if ( $vat )
		echo '<tr><th>' . get_option( 'vat_options_label', __( 'VAT Number', 'woocommerce-german-market' ) ) . '</th><td data-title="' . get_option( 'vat_options_label', __( 'VAT Number', 'woocommerce-german-market' ) ) . '">' . $vat . '</td></tr>';
}

/**
 * Validates the user input and
 * loads the VAT Validator to
 * check it.
 *
 * @return void
 */
function wcvat_woocommerce_after_checkout_validation() {

	if ( isset( $_POST[ 'billing_vat' ] ) && $_POST[ 'billing_vat' ] != '' ) {

		// set the input
		$input = array( strtoupper( substr( $_POST[ 'billing_vat' ], 0, 2 ) ), strtoupper( substr( $_POST[ 'billing_vat' ], 2 ) ) );

		// set country
		$billing_country = isset( $_POST[ 'billing_country' ] ) ? $_POST[ 'billing_country' ] : '';

		// Validate the input
		if ( ! class_exists( 'WC_VAT_Validator' ) ) {
			require_once 'class-wc-vat-validator.php';
		}

		$validator = new WC_VAT_Validator( $input, $billing_country );

		if ( ! $validator->is_valid() ) {
			if ( $validator->has_errors() ) {
				if ( $validator->get_last_error_code() != '200' ) {
					wc_add_notice( $validator->get_last_error_messages(), 'error' );
				} else {
					wc_add_notice( __( 'Please enter a valid VAT Identification Number registered in a country of the EU.',
					                   'woocommerce-german-market' ), 'error' );
				}
			}
		}
	}
}

/**
 * Adds the VAT Number to the E-Mails
 *
 * @wp-hook	woocommerce_email_order_meta_keys
 * @param	array $keys
 * @return	array $keys
 */
function wcvat_custom_checkout_field_order_meta_keys( $keys ) {
	$keys[ get_option( 'vat_options_label', __( 'VAT Number', 'woocommerce-german-market' ) ) ] = 'billing_vat';
	return $keys;
}

/**
 * Notice: "Tax free intracommunity delivery" in emails
 *
 * @wp-hook	woocommerce_email_after_order_table
 * @param	WC_Order $order
 * @return	void
 */
function wcvat_woocommerce_email_after_order_table( $order ) { 
	
	$notice = '';
	$eu_countries = array(
		'ET', 'AT', 'BE', 'BG', 'CY', 'CZ', 'DE', 'DK', 'EE', 'GR', 'ES', 'FI', 'FR', 'GB', 'HR', 'HU', 'IE', 'IT', 'LT', 'LU', 'LV', 'MT', 'NL', 'PL', 'RO', 'SE', 'SI', 'SK'
	);

	if ( get_post_meta( $order->get_id(), 'billing_vat', true ) != '' ) {

		if ( $order->get_billing_country() != WC()->countries->get_base_country() ) {
			
			if ( in_array( $order->get_billing_country(), $eu_countries ) ) {
				$notice = get_option( 'vat_options_notice', __( 'Tax free intracommunity delivery', 'woocommerce-german-market' ) );
			}

		}

	}

	if ( ! in_array( $order->get_billing_country(), $eu_countries ) ) {
		$notice = apply_filters( 'wcvat_woocommerce_vat_notice_not_eu', get_option( 'vat_options_non_eu_notice', __( 'Tax-exempt export delivery', 'woocommerce-german-market' ) ), $order );
	}

	if ( $notice != '' ) {
		echo apply_filters( 'wcvat_woocommerce_email_after_order_table', '<br class="wcvat-br"/><p><b>' . $notice . '</b></p>', $order );
	} else {
		echo '<br />';
	}
}

/**
 * Notice: "Tax free intracommunity delivery" in my-account
 *
 * @wp-hook	woocommerce_order_details_after_order_table
 * @param	WC_Order $order
 * @return	void
 */
function wcvat_woocommerce_order_details_after_order_table( $order ) {
	
	$notice = '';
	$eu_countries = array(
		'ET', 'AT', 'BE', 'BG', 'CY', 'CZ', 'DE', 'DK', 'EE', 'GR', 'ES', 'FI', 'FR', 'GB', 'HR', 'HU', 'IE', 'IT', 'LT', 'LU', 'LV', 'MT', 'NL', 'PL', 'RO', 'SE', 'SI', 'SK'
	);

	if ( get_post_meta( $order->get_id(), 'billing_vat', true ) != '' ) {

		if ( $order->get_billing_country() != WC()->countries->get_base_country() ) {
			
			if ( in_array( $order->get_billing_country(), $eu_countries ) ) {
				$notice = get_option( 'vat_options_notice', __( 'Tax free intracommunity delivery', 'woocommerce-german-market' ) );
			}

		}

	}

	if ( ! in_array( $order->get_billing_country(), $eu_countries ) ) {
		$notice = apply_filters( 'wcvat_woocommerce_vat_notice_not_eu', get_option( 'vat_options_non_eu_notice', __( 'Tax-exempt export delivery', 'woocommerce-german-market' ) ), $order );
	}

	if ( $notice != '' ) {
		echo apply_filters( 'wcvat_woocommerce_order_details_after_order_table', '<p>' . $notice . '</p>', $order );
	}

}

/**
 * Display the VAT Field in the Backend
 *
 * @param object $order
 *
 * @return void
 */
function wcvat_woocommerce_admin_order_data_after_billing_address( $order ) {
	
	if ( get_post_meta( $order->get_id(), 'billing_vat', TRUE ) != '' ) {
		echo '<p style="width: 100%; float: left;"><strong>' . get_option( 'vat_options_label', __( 'VAT Number', 'woocommerce-german-market' ) ) . ':</strong><br />' . get_post_meta( $order->get_id(), 'billing_vat', TRUE ) . '</p>';
	}

}

/**
 * Save the VAT Number at the order
 *
 * @param int $order_id
 *
 * @return void
 */
function wcvat_woocommerce_checkout_update_order_meta( $order_id ) {
	
	if ( ! empty( $_POST[ 'billing_vat' ] ) ) {
		$vat = sanitize_text_field( $_POST[ 'billing_vat' ] );
		update_post_meta( $order_id, 'billing_vat', $vat );
	}
}

/**
 * AJAX callback to check the VAT
 *
 * @wp-hook	wp_ajax_wcvat_check_vat, wp_ajax_nopriv_wcvat_check_vat
 * @return	void
 */
function wcvat_check_vat() {

	// get the billing vat
	$billing_vat = $_REQUEST[ 'vat' ];
	$raw_billing_vat = strtoupper( substr( $billing_vat, 0, 2 ) ) . strtoupper( substr( $billing_vat, 2 ) );
	$billing_vat = array( strtoupper( substr( $billing_vat, 0, 2 ) ), strtoupper( substr( $billing_vat, 2 ) ) );
	$billing_country = $_REQUEST[ 'country' ];

	$response = array( 'success' => '', 'data' => '' );

	// No need to validate if field is empty
	// The following block code does not work as expected, cause setting the sessions takes too long
	// see wcvat_woocommerce_before_calculate_totals, first comments, the first code block in this functions fixes the problem
	if ( trim( $_REQUEST[ 'vat' ] ) == '' ) {
		
		$response[ 'success' ] = FALSE;
		$response[ 'data' ]    = __( 'Field is empty.', 'woocommerce-german-market' );

		// add taxes
		WGM_Session::add( 'eu_vatin_check_exempt', false );
		WGM_Session::remove( 'eu_vatin_check_billing_vat' );

		if ( is_user_logged_in() ) {
			$current_user = wp_get_current_user();
			delete_user_meta( $current_user->ID, 'billing_vat' );
		}

		WC()->customer->set_is_vat_exempt( false );

		echo json_encode( $response );
		exit;
	}

	// validate the billing_vat
	if ( ! class_exists( 'WC_VAT_Validator' ) ) {
		require_once 'class-wc-vat-validator.php';
	}

	$validator = new WC_VAT_Validator( $billing_vat, $billing_country );

	if ( $validator->is_valid() === FALSE ) {

		// add taxes
		WGM_Session::add( 'eu_vatin_check_exempt', false );
		WGM_Session::remove( 'eu_vatin_check_billing_vat' );

		if ( is_user_logged_in() ) {
			$current_user = wp_get_current_user();
			delete_user_meta( $current_user->ID, 'billing_vat' );
		}

		$response[ 'success' ] = FALSE;

	} else {
		
		if ( $billing_country == WC()->countries->get_base_country() ) {

			// add taxes
			WGM_Session::add( 'eu_vatin_check_exempt', false );
			WGM_Session::remove( 'eu_vatin_check_billing_vat' );

			if ( is_user_logged_in() ) {
				$current_user = wp_get_current_user();
				delete_user_meta( $current_user->ID, 'billing_vat' );
			}

		} else {

			// remove taxes
			WGM_Session::add( 'eu_vatin_check_exempt', true );
			WGM_Session::add( 'eu_vatin_check_billing_vat', $billing_vat );

			if ( is_user_logged_in() ) {
				$current_user = wp_get_current_user();
				update_user_meta( $current_user->ID, 'billing_vat', $raw_billing_vat );
			}

		}

		// output response
		$response[ 'success' ] = TRUE;
	}

	echo json_encode( $response );
	exit;
}

/**
 * Check VAT exempt with WGM_Session Class
 *
 * @wp-hook	woocommerce_before_calculate_totals
 * @return	void
 */
function wcvat_woocommerce_before_calculate_totals() {
	
	// if billing vat is empty => not vat exempted
	// in most cases when switching the country to base country
	// the session variable is set to slow, so we need this check
	if ( isset( $_REQUEST[ 'post_data' ] ) ) {
		
		parse_str( $_REQUEST[ 'post_data' ], $post_data );
		
		$billing_vat_is_empty = true;

		if ( isset( $post_data[ 'billing_vat' ] ) ) {

			if ( $post_data[ 'billing_vat' ] != '' ) {
				$billing_vat_is_empty = false;
			}

		}

		if ( $billing_vat_is_empty ) {
			WC()->customer->set_is_vat_exempt( false );
			WGM_Session::add( 'eu_vatin_check_exempt', false );
			WGM_Session::remove( 'eu_vatin_check_billing_vat' );
			return;
		}
		
	}

	if ( WGM_Session::get( 'eu_vatin_check_exempt' ) ) {
			WC()->customer->set_is_vat_exempt( true );
	}

}
