<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * SEPA Direct Debit Gateway
 *
 * Provides a SEPA Direct Debit.
 *
 * @class 		WGM_Gateway_Sepa_Direct_Debit
 * @extends		WC_Payment_Gateway
 * @version		1.0
 */
class WGM_Gateway_Sepa_Direct_Debit extends WC_Payment_Gateway {

	public static $instances = 0;
    /**
     * Init Payment Gateway
     */
    function __construct() {
		
		self::$instances++;

		$this->id           = 'german_market_sepa_direct_debit';
		$this->method_title = __( 'SEPA Direct Debit', 'woocommerce-german-market' );
		$this->has_fields   = true;

		// Load the settings
		$this->init_form_fields();
		$this->init_settings();

		// Get settings
		$this->enabled 			  	= $this->get_option( 'enabled', 'no' );
		$this->title              	= $this->get_option( 'title' );
		$this->description        	= $this->get_option( 'description' );
		$this->instructions       	= $this->get_option( 'instructions' );
		$this->enable_for_methods 	= $this->get_option( 'enable_for_methods', array() );
	    $this->order_status 	  	= $this->get_option( 'order_status', 'processing' );
	    $this->data_store 		  	= $this->get_option( 'data_storing', 'raw' );
	    $this->mask_option 		  	= $this->get_option( 'iban_mask', 3 );
	    $this->mask_symbol 	 	  	= $this->get_option( 'iban_mask_symbol', '*' );
	    $this->mandate_reference  	= $this->get_option( 'mandate_reference', __( 'MANDATE', 'woocommerce-german-market' ) . '{order-id}' );
	    $this->email_subject 	  	= $this->get_option( 'sepa_mandate_email_subject', __( 'SEPA Direct Debit Mandate', 'woocommerce-german-market' ) );
	    $this->email_heading 	  	= $this->get_option( 'sepa_mandate_email_heading', __( 'SEPA Direct Debit Mandate', 'woocommerce-german-market' ) );
	    $this->email_type 	  	  	= $this->get_option( 'sepa_mandate_email_type', 'html' );
	    $this->email_admin		  	= $this->get_option( 'sepa_mandate_email_recipient', 'customer_and_admin' );
	    $this->email_pdf		  	= $this->get_option( 'sepa_mandate_email_pdf', 'no' );
	    $this->email_admin_mask   	= $this->get_option( 'sepa_mandate_email_admin_mask_iban', 'off' );
	    $this->email_admin_address	= $this->get_option( 'sepa_mandate_email_address', get_option( 'admin_email' ) );
	    $this->pain_format 		  	= $this->get_option( 'pain_format', 'pain.008.002.02' );

	    // process when order has been made
	    add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
	    
	    // show instructions on settings page
	    add_action( 'woocommerce_thankyou_german_market_sepa_direct_debit', array( $this, 'thankyou' ) );

	    // show data in emails
	    add_action( 'woocommerce_email_customer_details', array( $this, 'email_customer_details' ), 20, 3 );
	   	
	   	// admin fields
	   	add_filter( 'woocommerce_admin_billing_fields', array( $this, 'admin_billing_fields' ) );

	    // save data into order and user
		add_action( 'woocommerce_checkout_update_order_meta', array( $this, 'update_order_meta' ), 5, 3 );

		// save data in backend
		add_action( 'woocommerce_before_order_object_save', array( $this, 'sava_admin_data' ), 10, 2 );

		// bulk sepa download
		add_action( 'admin_footer', array( $this, 'bulk_admin_footer' ) );
		add_action( 'load-edit.php', array( $this, 'bulk_action' ) );

		// sepa xml download errors
		if ( get_option( '_german_market_sepa_xml_error', '' ) != '' ) {
			add_action( 'admin_notices', array( $this, 'admin_xml_error_notice' ) );
		}

		add_action( 'woocommerce_admin_order_actions', array( $this, 'admin_order_actions' ), 10, 2 );

	}

	/**
	* Mask IBAN
	*
	* @param String $iban
	* @return String
	**/
	private function mask_iban( $iban ) {
		
		if ( intval( $this->mask_option ) > 0 ) {

			$string_length 		= strlen( $iban );
			$show_length 		= intval( $this->mask_option );
			$mask_length 		= $string_length - $show_length;

			$not_masked_start 	= -1 * $show_length;
			$iban_not_masked	= substr( $iban, $not_masked_start, $show_length );

			$masked_string 		= str_repeat( $this->mask_symbol, $mask_length );

			$iban = $masked_string . $iban_not_masked;

		}

		return $iban;
	}

	/**
	* Show Sepa Data in plain emails
	*
	* @param WC_Order $order
	* @return void
	**/
	public function email_customer_details_plain_text( $order ) {

		echo "\n" . strtoupper( apply_filters( 'german_market_sepa_email_headline', __( 'SEPA Direct Debit Data', 'woocommerce-german-market' ) ) ) . "\n\n";
							
		$email_fields = array();

		$sepa_fields = self::get_payment_fields();

		foreach ( $sepa_fields as $key => $sepa_field ) {

			if ( $this->encryption_possible_on() ) {
				$value = $this->decrypt( $order->get_meta( '_german_market_sepa_' . esc_attr( $key ) ), $order );
			} else {
				$value = $order->get_meta( '_german_market_sepa_' . esc_attr( $key ) );
			}

			if ( $key == 'iban' ) {
				$value = $this->mask_iban( $value );
			}

			$email_fields[ $sepa_field[ 'label' ] ] = $value;

		}

		$email_fields = apply_filters( 'german_market_sepa_email_fields', $email_fields );

		foreach ( $email_fields as $label => $value ) {
			echo preg_replace( '#<br\s*/?>#i', "\n", $label . ': ' . $value ) . "\n";
		}

	}

	/**
	* Show Sepa Data in Emails
	*
	* @wp-hook woocommerce_email_customer_details
	* @param WC_Order $order
	* @param Boolean $sent_to_admin
	* @param Boolean $plain_text
	* @return void
	**/
    public function email_customer_details( $order, $sent_to_admin, $plain_text ) {

    	if ( $order->get_payment_method() != 'german_market_sepa_direct_debit' ) {
    		return;
    	}
    	
    	$sepa_fields = self::get_payment_fields();
   		
   		// return if no sepa data is available
   		if ( empty( $sepa_fields ) ) {
   			return;
   		}

   		// return if no sepa data is available
    	foreach ( $sepa_fields as $key => $sepa_field ) {
    		$value = $order->get_meta( '_german_market_sepa_' . esc_attr( $key ) );
    		if ( $value == '' ) {
    			return;
    		}
    	}

    	if ( $plain_text ) {
    		$this->email_customer_details_plain_text( $order );
    		return;
    	}

    	$text_align = is_rtl() ? 'right' : 'left';

		?><table id="sepa-direct-debit" cellspacing="0" cellpadding="0" style="width: 100%; vertical-align: top;" border="0">
			<tr>
				<td class="td" style="text-align:<?php echo $text_align; ?>; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;" valign="top" width="50%">
					<h3><?php echo apply_filters( 'german_market_sepa_email_headline', __( 'SEPA Direct Debit Data', 'woocommerce-german-market' ) ); ?></h3>

					<p class="text">
						
						<?php
							
							$email_fields = array();

				    		foreach ( $sepa_fields as $key => $sepa_field ) {

				    			if ( $this->encryption_possible_on() ) {
				    				$value = $this->decrypt( $order->get_meta( '_german_market_sepa_' . esc_attr( $key ) ), $order );
				    			} else {
				    				$value = $order->get_meta( '_german_market_sepa_' . esc_attr( $key ) );
				    			}

				    			if ( $key == 'iban' ) {
									$value = $this->mask_iban( $value );
								}

				    			$email_fields[ $sepa_field[ 'label' ] ] = $value;

				    		}

				    		$email_fields = apply_filters( 'german_market_sepa_email_fields', $email_fields );

				    		foreach ( $email_fields as $label => $value ) {

				    			echo $label . ': ' . $value . '<br />';

				    		}

						?>

					</p>
				</td>
			</tr>
		</table>

		<?php

    }

    /**
	* Save Data in Backend
	*
	* @wp-hook woocommerce_before_order_object_save
	* @return void
	**/
    public function sava_admin_data( $order, $data_store ) {

    	if ( is_admin() && is_a( $order, 'WC_Order' ) ) {

    		if ( $this->encryption_possible_on() ) {

    			$sepa_fields = self::get_payment_fields();
    			foreach ( $sepa_fields as $key => $sepa_field ) {

    				$post_key = '_german_market_sepa_' . esc_attr( $key );
					
					if ( isset( $_POST[ $post_key ] ) ) {
						$value = $this->encrypt( $_POST[ $post_key ], $order );
						$order->update_meta_data( $post_key, $value );
					}

    			}

    			if ( isset( $_POST[ '_german_market_sepa_mandate_reference' ] ) ) {
	    			$value = $this->encrypt( $_POST[ '_german_market_sepa_mandate_reference' ], $order );
	    			$order->update_meta_data( '_german_market_sepa_mandate_reference', $value );
	    		}

    			$order->save_meta_data();

    		}

    	}
    	
    }

    /**
	* Admin Billing Fields
	*
	* @wp-hook woocommerce_admin_billing_fields
	* @param Array $fields
	* @return Array
	**/
    public function admin_billing_fields( $fields ) {

    	global $post;
    	$order = wc_get_order( $post->ID );

    	if ( $order->get_payment_method() == 'german_market_sepa_direct_debit' ) {

    		$sepa_fields = self::get_payment_fields();

    		foreach ( $sepa_fields as $key => $sepa_field ) {

    			if ( $this->encryption_possible_on() ) {
    				$value = $this->decrypt( $order->get_meta( '_german_market_sepa_' . esc_attr( $key ) ), $order );
    			} else {
    				$value = $order->get_meta( '_german_market_sepa_' . esc_attr( $key ) );
    			}

    			$fields[ 'german-market-sepa-' . esc_attr( $key ) ] = array(
    				'label' => $sepa_field[ 'label' ],
		    		'id'  	=> '_german_market_sepa_' . esc_attr( $key ),
		    		'value' => $value,
					'show'  => apply_filters( 'german_market_sepa_admin_fields_show', false, $key ),
    			);

    		}

    		if ( $this->encryption_possible_on() ) {
				$value = $this->decrypt( $order->get_meta( '_german_market_sepa_mandate_reference' ), $order );
			} else {
				$value = $order->get_meta( '_german_market_sepa_mandate_reference' );
			}

    		$fields[ 'german-market-mandate-reference' ] = array(
    			'label' => __( 'Mandate Reference', 'woocommerce-german-market' ),
	    		'id'  	=> '_german_market_sepa_mandate_reference',
	    		'value' => $value,
				'show'  => apply_filters( 'german_market_sepa_admin_fields_show', false, 'mandate-reference' ),
    		);

    	}

    	return $fields;

    }

    /**
	* Returns empty Array if encyription is possible
	*
	* @return Array
	**/
    private function encryption_possible() {
    	
    	$errors = array();

    	if ( ! function_exists( 'random_bytes' ) ) {
    		$errors[] = __( 'Encryption is not possible because the function <code>random_bytes</codes> is not available on your server. Please contact your admin / webhoster.', 'woocommerce-german-market' );
    	}

    	if ( ! function_exists( 'openssl_encrypt' ) ) {
    		$errors[] = __( 'Encryption is not possible because the function <code>openssl_encrypt</codes> is not available on your server. Please contact your admin / webhoster.', 'woocommerce-german-market' );
    	}

    	if ( ! function_exists( 'openssl_decrypt' ) ) {
    		$errors[] = __( 'Encryption is not possible because the function <code>openssl_decrypt</codes> is not available on your server. Please contact your admin / webhoster.', 'woocommerce-german-market' );
    	}

    	if ( ! defined( 'GERMAN_MARKET_SEPA_ENCRYPTION' ) ) {
    		$errors[] = __( "To enable encryption, please copy the following line into your wp_config.php file:<br /><code>define( 'GERMAN_MARKET_SEPA_ENCRYPTION', 'Your Passhprase');</code><br />Replace 'Your Passphrase' with an indivudual passphrase. This passhrase should never be changed or removed. Save the passphrase locally to not forget it.", 'woocommerce-german-market' );
    	}

    	return $errors;
    }

    /**
	* Returns true if encryption is possible and enabled
	*
	* @return boolean
	**/
    private function encryption_possible_on() {
    	return empty( $this->encryption_possible() ) && $this->data_store == 'encryption';
    }

    /**
	* Encrypt string
	*
	* @param String $string
	* @param WC_Order $order
	* @return String
	**/
    private function encrypt( $string, $order ) {

    	$iv 			= apply_filters( 'german_market_sepa_encryption_iv', substr( $order->get_cart_hash(), 0, 16 ), $order );
    	$method 		= apply_filters( 'german_market_sepa_encryption_method', 'aes-256-ctr', $order );

    	return openssl_encrypt( $string, $method, GERMAN_MARKET_SEPA_ENCRYPTION, 0, $iv );

    }

    /**
	* Derypt string
	*
	* @param String $string
	* @param WC_Order $order
	* @return String
	**/
    private function decrypt( $string, $order ) {

    	$iv 			= apply_filters( 'german_market_sepa_encryption_iv', substr( $order->get_cart_hash(), 0, 16 ), $order );
    	$method 		= apply_filters( 'german_market_sepa_encryption_method', 'aes-256-ctr', $order );

    	return openssl_decrypt( $string, $method, GERMAN_MARKET_SEPA_ENCRYPTION, 0, $iv );

    }

    /**
	* Save meta data
	*
	* @wp-hook woocommerce_checkout_update_order_meta
	* @param Integer $order_id
	* @param Array $posted
	* @return void
	**/
	public function update_order_meta( $order_id, $posted ) {

		$order = wc_get_order( $order_id );

		if ( $order->get_payment_method() == 'german_market_sepa_direct_debit' ) {

			$sepa_fields = WGM_Gateway_Sepa_Direct_Debit::get_payment_fields();

			foreach ( $sepa_fields as $key => $field ) {

				$field_key = 'german-market-sepa-' . esc_attr( $key );
				
				if ( isset( $_POST[ $field_key ] ) ) {
					
					if ( $this->encryption_possible_on() ) {
						$value = $this->encrypt( $_POST[ $field_key ], $order );
					} else {
						$value = $_POST[ $field_key ];
					}

					$order->update_meta_data( '_' . str_replace( '-', '_', $field_key ), $value );
				}
				
			}

			// mandate reference
			$mandate_value = $this->build_mandate_reference( $order );
			if ( $this->encryption_possible_on() ) {
				$mandate_value = $this->encrypt( $mandate_value, $order );
			}

			$order->update_meta_data( '_german_market_sepa_mandate_reference', $mandate_value );

			$order->save_meta_data();

		}

	}

	private function build_mandate_reference( $order ) {

		$placeholders = apply_filters( 'gm_sepa_direct_debit_mandate_ref_placeholders', array(
			'{order-id}' => $order->get_id(),
		) );

		if ( class_exists( 'WP_WC_Running_Invoice_Number_Functions' ) ) {
			$running_invoice_number = new WP_WC_Running_Invoice_Number_Functions( $order );	
			$placeholders[ '{invoice-number}' ] = $running_invoice_number->get_invoice_number();
		}

		$mandate_reference = $this->mandate_reference;

		foreach ( $placeholders as $search => $replace ) {
			$mandate_reference = str_replace( $search, $replace, $mandate_reference );
		}

		return $mandate_reference;

	}

	/**
	 * Admin Panel Options
	 * - Options for bits like 'title' and availability on a country-by-country basis
	 *
	 * @access public
	 * @return void
	 */
	function admin_options() {
		
		// German Market styles
		?>
		<h3><?php _e( 'SEPA Direct Debit', 'woocommerce-german-market' ); ?></h3>
		<p><?php _e( 'Allows payments by SEPA direct debit.', 'woocommerce-german-market' ); 
		echo WGM_Ui::get_video_layer( 'https://s3.eu-central-1.amazonaws.com/marketpress-videos/german-market/sepa.mp4' ); ?></p>
		<table class="form-table">
			<?php $this->generate_settings_html(); ?>
		</table> <?php
	}


	/**
	 * Initialise Gateway Settings Form Fields
	 *
	 * @access public
	 * @return void
	 */
	function init_form_fields() {
		global $woocommerce;

		if ( is_admin() ) {

			// Init
			$possible_statuses = array(
				'pending'		=> __( 'Pending Payment', 'woocommerce-german-market' ),
				'processing'	=> __( 'Processing', 'woocommerce-german-market' ),
				'on-hold'		=> __( 'On Hold', 'woocommerce-german-market' ),
				'completed'		=> __( 'Completed', 'woocommerce-german-market' ),
			);

			$shipping_methods = array();

			foreach ( $woocommerce->shipping->load_shipping_methods() as $method ) {
				$shipping_methods[ $method->id ] = $method->get_method_title();
			}

			$data_store_options = array(
				'raw' => __( 'Save raw data in orders (unencrypted)', 'woocommerce-german-market' )
			);

			if ( empty( $this->encryption_possible() ) ) {
				
				$data_store_options[ 'encryption' ] = __( 'Sava data encrypted', 'woocommerce-german-market' );
				$data_store_info = '';
				
				if ( $this->get_option( 'data_storing', 'raw' ) == 'encryption' ) {
					$data_store_info = __( 'If you deactivate encryption, all the user sepa data that has alrady been saved encrypted will not be readable.', 'woocommerce-german-market' );
				}

			} else {
				$erros = $this->encryption_possible();
				$data_store_info = implode( $erros, '<br /><br />' );
			}

			// mandate reference info
			$mandate_info = apply_filters( 'gm_sepa_direct_debit_mandate_ref_placeholders_text', __( 'Enter the format of the mandate reference ID. You can (you should) use <code>{order-id}</code> as a placeholder. You can add a prefix and / or a suffix.', 'woocommerce-german-market' ) );

			if ( class_exists( 'WP_WC_Running_Invoice_Number_Functions' ) ) {
				$mandate_info = apply_filters( 'gm_sepa_direct_debit_mandate_ref_placeholders_text', __( 'Enter the format of the mandate reference ID. You can (you should) use <code>{order-id}</code> or <code>{invoice-number}</code> as a placeholder. You can add a prefix and / or a suffix.', 'woocommerce-german-market' ) );
			}

			if ( get_option( 'gm_gross_shipping_costs_and_fees', 'off' ) == 'off' ) {
				$fee_notice = sprintf( __( 'Collect an extra service fee for "SEPA Direct Debit" payments. Enter amount in %s excluding tax.', 'woocommerce-german-market' ), esc_attr( get_option( 'woocommerce_currency' ) ) );
			} else {
				$fee_notice = sprintf( __( 'Collect an extra service fee for "SEPA Direct Debit" payments. Enter amount in %s including tax.', 'woocommerce-german-market' ), esc_attr( get_option( 'woocommerce_currency' ) ) );
			}

			// Set form fields
			$this->form_fields = array(
				'enabled' => array(
					'title' => __( 'Enable SEPA Direct Debit', 'woocommerce-german-market' ),
					'label' => __( 'Enable SEPA Direct Debit', 'woocommerce-german-market' ),
					'type' => 'checkbox',
					'description' => '',
					'default' => 'no'
				),
				'title' => array(
					'title' => __( 'Title', 'woocommerce-german-market' ),
					'type' => 'text',
					'description' => __( 'Payment method description that the customer will see on your checkout.', 'woocommerce-german-market' ),
					'default' => __( 'SEPA Direct Debit', 'woocommerce-german-market' ),
					'desc_tip'      => true,
				),
				'description' => array(
					'title' => __( 'Description', 'woocommerce-german-market' ),
					'type' => 'textarea',
					'description' => __( 'Payment method description that the customer will see on your website in order.', 'woocommerce-german-market' ),
					'default' => '',
				),
				
				'instructions' => array(
					'title' => __( 'Instructions', 'woocommerce-german-market' ),
					'type' => 'textarea',
					'description' => __( 'Payment method description that the customer will see on your website and in the order emails.', 'woocommerce-german-market' ),
					'default' => __( 'The amount will be automatically debited from your account via SEPA direct debit.', 'woocommerce-german-market' )
				),

				'creditor_information' => array(
					'title' => __( 'Creditor Information', 'woocommerce-german-market' ),
					'type' => 'text',
					'desc_tip' => __( 'Your company information.', 'woocommerce-german-market' ),
					'default' => '',
				),

				'creditor_account_holder' => array(
					'title' => __( 'Account Holder', 'woocommerce-german-market' ),
					'type' => 'text',
					'desc_tip' => __( 'Name of your Account Holder.', 'woocommerce-german-market' ),
					'default' => '',
				),

				'iban' => array(
					'title' => __( 'IBAN', 'woocommerce-german-market' ),
					'type' => 'text',
					'desc_tip' => __( 'Your bank IBAN.', 'woocommerce-german-market' ),
					'default' => '',
				),

				'bic' => array(
					'title' => __( 'BIC', 'woocommerce-german-market' ),
					'type' => 'text',
					'desc_tip' => __( 'Your bank BIC.', 'woocommerce-german-market' ),
					'default' => '',
				),

				'creditor_identifier' => array(
					'title' => __( 'Creditor Identifier Number', 'woocommerce-german-market' ),
					'type' => 'text',
					'desc_tip' => __( 'Your Creditor Identifier Number.', 'woocommerce-german-market' ),
					'default' => '',
				),

				'direct_debit_mandate' => array(
					'title' => __( 'Direct Debit Mandate', 'woocommerce-german-market' ),
					'type' => 'textarea',
					'css' => 'min-height: 300px;',
					'desc_tip' => __( 'This text will be used as preview direct debit mandate and as email template text.', 'woocommerce-german-market' ),
					'description' => __( 'You can use the following placeholders:', 'woocommerce-german-market' ) . ' <code>[creditor_information], [creditor_identifier], [creditor_account_holder], [creditor_iban], [creditor_bic], [mandate_id], [street], [city], [postcode], [country], [date], [account_holder], [account_iban], [account_bic]</code>',
					'default' => __( '[creditor_information]
Creditor Identifier: [creditor_identifier]
Mandate Referene Number: [mandate_id].
<h4>SEPA - Direct Debit Mandate</h4>
I hereby authorize the payee to collect a payment from my account by direct debit. At the same time, I instruct my credit institution to settle the direct debit drawn by the payee on my account.
Note: I can request the refund of the debited amount within eight weeks, beginning with the debiting date. The conditions of my credit credit institution are applied here.

<strong>Payer:</strong>
Account Holder: [account_holder]
Street: [street]
Postcode: [postcode]
City: [city]
Country: [country]
IBAN: [account_iban]
BIC: [account_bic]

[city], [date], [account_holder]

This letter has been created automatically and is valid without signature.

<hr />
Please note: The deadline for the advance information of the SEPA direct debit will be reduced to one day.', 'woocommerce-german-market' ),
				),

				'checkbox_confirmation' => array(
					'title' => __( 'Checkbox Confirmation', 'woocommerce-german-market' ),
					'type' => 'select',
					'options' => array(
						'activated'   => __( 'Activated', 'woocommerce-german-market' ),
						'deactivated' => __( 'Deactivated', 'woocommerce-german-market' ),
					),
					'desc_tip' => __( 'Activate the checkbox confirmation during checkout and the preview of the direct debit mandate.', 'woocommerce-german-market' ),
					'default' => 'activated',
				),

				'checkbox_confirmation_text' => array(
					'title' => __( 'Checkbox Text', 'woocommerce-german-market' ),
					'type' => 'text',
					'desc_tip' => __( 'Choose the label for the checkbox text. You can use [link]sepa direct debit mandate[/link] as a placeholder.', 'woocommerce-german-market' ),
					'default' => __( 'I agree to the [link]sepa direct debit mandate[/link].', 'woocommerce-german-market' ),
				),

				'mandate_reference' => array(
					'title'			=> __( 'Mandate Reference', 'woocommerce-german-market' ),
					'type'			=> 'text',
					'default'		=> __( 'MANDATE', 'woocommerce-german-market' ) . '{order-id}',
					'description'	=> $mandate_info,
				),

				'iban_mask' => array(
					'title'			=> __( 'IBAN Masking', 'woocommerce-german-market' ),
					'type'			=> 'select',
					'options'		=> apply_filters( 'gm_sepa_direct_debit_masking', array(
						0	=> __( 'Don\'t mask IBAN', 'woocommerce-german-market' ),
						1 	=> __( 'Only show last digit', 'woocommerce-german-market' ),
						2   => __( 'Only show last two digitis', 'woocommerce-german-market' ),
						3	=> __( 'Only show last three digitis', 'woocommerce-german-market' ),
						4	=> __ ('Only show last four digits', 'woocommerce-german-market' ),
						5	=> __( 'Only show last five digits', 'woocommerce-german-market' )
					) ),
					'default'		=> 3,
					'desc_tip'		=> __( 'You can mask the IBAN in emails.', 'woocommerce-german-market' ),
				),

				'iban_mask_symbol' => array(
					'title'			=> __( 'IBAN Masking Symbol', 'woocommerce-german-market' ),
					'type'			=> 'text',
					'default'		=> '*',
					'custom_attributes' => array(
						'maxlength' => 1
					),
					'css'			=> 'width: 30px;',
					'desc_tip'		=> __( 'If you mask the IBAN, you can choose the symbol used to mask the digits.', 'woocommerce-german-market' ),
				),

				'data_storing' => array(
					'title'			=> __( 'Data Storing in Orders', 'woocommerce-german-market' ),
					'type'			=> 'select',
					'default'		=> 'raw',
					'options' 		=> $data_store_options, 
					'desc_tip'		=> __( 'How to save the additional data in orders.', 'woocommerce-german-market' ),
					'description'	=> $data_store_info
				),

				'order_status' => array(
					'title'			=> __( 'Order Status', 'woocommerce-german-market' ),
					'type'			=> 'select',
					'default'		=> 'completed',
					'options'		=> $possible_statuses,
					'desc_tip'		=> __( 'Choose the order status of the customer\'s order after the customer finished the order process. We recommend to the set the option "Processing".', 'woocommerce-german-market' ),
				),

				'woocommerce_german_market_sepa_direct_debit_fee' => array(
					'title' 		=> __( 'Service Fee', 'woocommerce-german-market' ),
					'type' 			=> 'text',
					'css'  			=> 'width:50px;',
					/* translators: %s = default currency, e.g. EUR */
					'desc_tip' 		=> $fee_notice,
					'default' 		=> '',
					'description'	=> __( '<span style="color: #f00;">Attention!</span> These service fees are no longer permissible since 01.07.2017, see this <a href="https://www.bmjv.de/SharedDocs/Artikel/DE/2017/06022017_BT_Zahlungsdiensterichtlinie.html" target="_blank">article of the BMJV</a>. We recommend not to set up a service fee. This function will be excluded in the next German Market Update.', 'woocommerce-german-market' ),
				),

				'enable_for_methods' => array(
					'title' 		=> __( 'Enable for shipping methods', 'woocommerce-german-market' ),
					'type' 			=> 'multiselect',
					'class'			=> 'chosen_select',
					'css'			=> 'width: 450px;',
					'default' 		=> '',
					'description' 	=> __( 'If "SEPA Direct Debit" is only available for certain methods, set it up here. Leave blank to enable for all methods.', 'woocommerce-german-market' ),
					'options'		=> $shipping_methods,
					'desc_tip'      => true,
				),

				'sepa_mandate_email_title' => array(
					'title' 		=> __( 'Sepa Mandate Email', 'woocommerce-german-market' ),
					'type' 			=> 'title',
					'description'	=> WGM_Ui::get_video_layer( 'https://s3.eu-central-1.amazonaws.com/marketpress-videos/german-market/archivierung-sepa-lastschriftmandate.mp4' ) . '<br />' . __( 'This email is sent after the cusomer has finished the order process.', 'woocommerce-german-market' )
				),

				'sepa_mandate_email_subject' => array(
					'title'			=> __( 'Email Subject', 'woocommerce-german-market' ),
					'type'			=> 'text',
					'default'		=> __( 'SEPA Direct Debit Mandate', 'woocommerce-german-market' ),
				),

				'sepa_mandate_email_heading' => array(
					'title'			=> __( 'Email Heading', 'woocommerce-german-market' ),
					'type'			=> 'text',
					'default'		=> __( 'SEPA Direct Debit Mandate', 'woocommerce-german-market' ),
				),

				'sepa_mandate_email_type' => array(
					'title'			=> __( 'Email Type', 'woocommerce-german-market' ),
					'type'			=> 'select',
					'default'		=> 'html',
					'options'		=> array(
						'html'	=> __( 'HTML', 'woocommerce-german-market' ),
						'plain' => __( 'Plain Text', 'woocommerce-german-market' )
					)
				),

				'sepa_mandate_email_recipient' => array(
					'title' => __( 'Direct Debit Mandat Email Recipients', 'woocommerce-german-market' ),
					'type' => 'select',
					'options' => array(
						'customer'   		 => __( 'Send Email only to customer', 'woocommerce-german-market' ),
						'customer_and_admin' => __( 'Send Email to customer and admin', 'woocommerce-german-market' ),
					),
					'desc_tip' => __( 'The email with the Direct Debit Mandat can be send to the admin, too.', 'woocommerce-german-market' ),
					'default' => 'customer_and_admin',
				),

				'sepa_mandate_email_pdf' => array(
					'title' => __( 'PDF Attachement for Direct Debit Mandat Email', 'woocommerce-german-market' ),
					'type' => 'select',
					'options' => array(
						'no'   		 		 => __( 'No PDF attachement', 'woocommerce-german-market' ),
						'admin' 			 => __( 'PDF attachment in admin email', 'woocommerce-german-market' ),
						'customer' 			 => __( 'PDF attachment in customer email', 'woocommerce-german-market' ),
						'admin_and_customer' => __( 'PDF attachment in admin email and customer email', 'woocommerce-german-market' ),
					),
					'desc_tip' => __( 'A PDF file with the Dircet Debit Mandat can be attached additionally in the Direct Debit Mandat email.', 'woocommerce-german-market' ),
					'default' => 'no',
				),

				'sepa_mandate_email_admin_mask_iban' => array(
					'title'			=> __( 'IBAN Masking for admin', 'woocommerce-german-market' ),
					'type'			=> 'select',
					'options'		=> apply_filters( 'gm_sepa_direct_debit_masking', array(
						'off'	=> __( 'Don\'t mask IBAN', 'woocommerce-german-market' ),
						'on' 	=> __( 'Mask IBAN', 'woocommerce-german-market' ),
					) ),
					'default'		=> 'off',
					'desc_tip'		=> __( 'Probably, you do not want the iban to be masked when information is send to the admin. E.g. when use this email for archiving.', 'woocommerce-german-market' ),
				),

				'sepa_mandate_email_address' => array(
					'title'			=> __( 'Admin Email Address', 'woocommerce-german-market' ),
					'type'			=> 'text',
					'default'		=> get_option( 'admin_email' ),
				),

				'XML Export' => array(
					'title' 		=> __( 'XML Export', 'woocommerce-german-market' ),
					'type' 			=> 'title',
				),

				'pain_format' => array(
					'title'			=> __( 'Pain Format', 'woocommerce-german-market' ),
					'type'			=> 'text',
					'default'		=> 'pain.008.002.02',
					'description'	=> __( 'You have to enter the XML Export Pain format that your banks needs. Maybe your bank needs the format pain.001.002.03, pain.001.001.03, pain.008.002.02 or pain.008.001.02. For further information, please ask your credit institution.', 'woocommerce-german-market' )
				),

		   );

		}
	}

	/**
	 * Check If The Gateway Is Available For Use
	 *
	 * @access public
	 * @return bool
	 */
	function is_available() {
		
		global $woocommerce;

		if ( ! empty( $this->enable_for_methods ) ) {

			if ( is_wc_endpoint_url( get_option( 'woocommerce_checkout_pay_endpoint' ) ) ) {

                $order_id = absint( get_query_var( 'order-pay' ) );
				$order = new WC_Order( $order_id );

				if ( ! $order->get_shipping_method() )
					return false;

				$chosen_method = $order->get_shipping_method();

			} elseif ( empty( $woocommerce->session->chosen_shipping_methods ) ) {
				return false;
			} else {
				$chosen_method = $woocommerce->session->chosen_shipping_methods;
			}

			$found = false;

			if ( is_array( $chosen_method ) && count( $chosen_method ) == 1 ) {
				$chosen_method = $chosen_method[ 0 ];
			}

			foreach ( $this->enable_for_methods as $method_id ) {
				if ( ( is_string( $chosen_method ) && strpos( $chosen_method, $method_id ) === 0 ) ||
					 ( is_array( $chosen_method ) && in_array( $method_id, $chosen_method ) ) ) {
					$found = true;
					break;
				}
			}

			if ( ! $found )
				return false;
		}

		return parent::is_available();
	}


	/**
	 * Process the payment and return the result
	 *
	 * @access public
	 * @param int $order_id
	 * @return array
	 */
	function process_payment ( $order_id ) {
		
		global $woocommerce;

		$order = new WC_Order( $order_id );

		if ( is_wc_endpoint_url( 'order-pay' ) ) {
			$this->update_order_meta( $order_id, array() );
		}

		// Set oder status
		$order->update_status( $this->order_status, __( 'SEPA Direct Debit', 'woocommerce-german-market' ) );

		// Reduce stock levels
		wc_reduce_stock_levels( $order->get_id() );

		// Remove cart
		$woocommerce->cart->empty_cart();

		// Send sepa mandate email
		if ( apply_filters( 'gm_sepa_send_sepa_email', true ) ) {
			
			$sepa_mail = include( untrailingslashit( Woocommerce_German_Market::$plugin_path ) . DIRECTORY_SEPARATOR . 'inc' . DIRECTORY_SEPARATOR . 'WGM_Email_Sepa.php' );

			$args = array(

	            'street'    			=> $order->get_billing_address_1(),
	            'city'      			=> $order->get_billing_city(),
	            'zip'       			=> $order->get_billing_postcode(),
	            'country'   			=> $order->get_billing_country(),
	            'holder'    			=> $order->get_meta( '_german_market_sepa_holder' ),
	            'iban'      			=> $order->get_meta( '_german_market_sepa_iban' ),
	            'bic'      				=> $order->get_meta( '_german_market_sepa_bic' ),
	            'mandate_id'			=> $order->get_meta( '_german_market_sepa_mandate_reference' ),
	            'date'					=> date_i18n( get_option( 'date_format' ), strtotime( $order->get_date_created() ) ),

	            'email_subject' 		=> $this->email_subject,
	            'email_heading' 		=> $this->email_heading,
	            'email_type'			=> $this->email_type,
	            'email_admin'			=> $this->email_admin,
	            'email_pdf'				=> $this->email_pdf,
	            'email_admin_iban_mask' => $this->email_admin_mask,
	            'email_admin_address'	=> $this->email_admin_address,

	        );

			if ( $this->encryption_possible_on() ) {
				$args[ 'holder' ] 		= $this->decrypt( $args[ 'holder' ], $order );
				$args[ 'iban' ] 		= $this->decrypt( $args[ 'iban' ], $order );
				$args[ 'bic' ] 			= $this->decrypt( $args[ 'bic' ], $order );
				$args[ 'mandate_id' ] 	= $this->decrypt( $args[ 'mandate_id' ], $order );
			}

			$args[ 'unmasked_iban' ] = $args[ 'iban' ];
			$args[ 'iban' ] = $this->mask_iban( $args[ 'iban' ] );

			$sepa_mail->set_args( $args );

	        $sepa_mail->trigger( $order_id );

	    }

		// Return thankyou redirect
		return array(
			'result' 	=> 'success',
			'redirect'	=> $order->get_checkout_order_received_url()
		);
	}


	/**
	 * Output for the order received page.
	 *
	 * @access public
	 * @return void
	 */
	function thankyou() {
		echo $this->instructions != '' ? wpautop( $this->instructions ) : '';
	}

	/**
	* Get Payment Fields
	* @return Array
	**/
	public static function get_payment_fields(){

		return  apply_filters( 'gm_sepa_fields_in_checkout', array(

			'holder'			=> array(
				'label'			=> __( 'Account Holder', 'woocommerce-german-market' ),
				'required'		=> true,
				'autocomplete'	=> 'off',
				'placeholder'	=> '',
				'value'			=> ''
			),

			'iban'			=> array(
				'label'			=> __( 'IBAN', 'woocommerce-german-market' ),
				'required'		=> true,
				'autocomplete'	=> 'off',
				'placeholder'	=> '',
				'value'			=> ''
			),

			'bic'			=> array(
				'label'			=> __( 'BIC/SWIFT', 'woocommerce-german-market' ),
				'required'		=> true,
				'autocomplete'	=> 'off',
				'placeholder'	=> '',
				'value'			=> '',
				'p-style'		=> apply_filters( 'gm_sepa_fields_in_checkout_bic_p_style', 'margin: 0 0 1.5em;' ) // looks better in theme twentyseventeen
			),

		) );
	}
	/**
	 * Additonal Checkout fields
	 *
	 * @return void
	 */
	public function payment_fields() {

		$description =  apply_filters( 'gm_sepa_description_in_checkout', $this->description );

		if ( trim( $description != '' ) ) {
			echo '<p class="german-market-sepa-description">' . $description . '</p>';
		}

		$sepa_fields_in_checkout = self::get_payment_fields();

		if ( empty(  $sepa_fields_in_checkout ) ) {
			return;
		}
		
		?>
		<fieldset id="german-market-sepa-checkout-fields">
			
			<?php

				$defaults = array(
					'label'			=> '',
					'value' 		=> '',
					'required'		=> true,
					'placeholder'	=> '',
					'value'			=> '',
					'autocomplete'	=> 'off',
					'p-style'		=> '',
				);

				foreach ( $sepa_fields_in_checkout as $key => $field ) {

					$field = wp_parse_args( $field, $defaults );

					$p_style = $field[ 'p-style' ] != '' ? ' style="'. $field[ 'p-style' ] . '"'  : '';

					?><p class="form-row form-row-wide"<?php echo $p_style;?>><?php
					
						if ( $field[ 'label' ] != '' ) {
							?><label for="german-market-sepa-<?php echo esc_attr( $key );?>"><?php echo $field[ 'label' ]; ?> <abbr class="required" title="<?php echo __( 'required', 'woocommerce-german-market' ); ?>">*</abbr></label><?php
						}

						if ( is_wc_endpoint_url( 'order-pay' ) ) {
							if ( isset( $_REQUEST[ 'german-market-sepa-' . $key ] ) ) {
								$field[ 'value' ] = $_REQUEST[ 'german-market-sepa-' . $key ];
							}
						}

						?>
					
						<input 	type="text" 
								id="german-market-sepa-<?php echo esc_attr( $key );?>" 
								name="german-market-sepa-<?php echo esc_attr( $key );?>" 
								value="<?php echo esc_attr( $field[ 'value' ]); ?>"
								autocomplete="<?php echo esc_attr( $field[ 'autocomplete' ] ); ?>" 
								placeholder="<?php echo esc_attr( $field[ 'placeholder' ] ); ?>"

						/>
					
					</p><?php

				}

				// support for manual order confirmation
				if ( is_wc_endpoint_url( 'order-pay' ) ) {
					
					$order_id = wc_get_order_id_by_order_key( $_REQUEST[ 'key' ] );
					$the_order = wc_get_order( $order_id );

					?>
					<input type="hidden" name="billing_address_1" 	value="<?php echo $the_order->get_billing_address_1(); ?>" />
					<input type="hidden" name="billing_postcode" 		value="<?php echo $the_order->get_billing_postcode(); ?>" />
					<input type="hidden" name="billing_city" 			value="<?php echo $the_order->get_billing_city(); ?>" />
					<input type="hidden" name="billing_country" 		value="<?php echo $the_order->get_billing_country(); ?>" />
					<?php

				}

			?>
			
		</fieldset>
		<?php

	}

	/**
	* Validate Additional Fields if second checkout page is enabled
	*
	* @wp-hook gm_checkout_validation called in inc/WGM_Sepa_Direct_debit
	* @param Integer $error_count
	* @return Integer
	**/
	public static function validate_required_fields( $error_count = 0 ) { 

		if ( isset( $_REQUEST[ 'payment_method' ] ) && $_REQUEST[ 'payment_method' ] == 'german_market_sepa_direct_debit' ) {

			$sepa_fields_in_checkout = self::get_payment_fields();

			foreach ( $sepa_fields_in_checkout as $key => $field ) { 

				if ( isset( $field[ 'required' ] ) && $field[ 'required' ] ) {

					$request_key 	= 'german-market-sepa-' . esc_attr( $key );
					$entered_value 	= $_REQUEST[ $request_key ];

					if ( trim( $entered_value ) == '' ) {

						wc_add_notice( sprintf( __( '<strong>%s</strong> is a required field.', 'woocommerce-german-market' ), $field[ 'label' ] ), 'error' );
						$error_count++;
					}

				}
			}

			// validate iban
			if ( isset( $_REQUEST[ 'german-market-sepa-iban' ] ) && trim( $_REQUEST[ 'german-market-sepa-iban' ] ) != '' ) {

				// include library for iban validation
				include_once( untrailingslashit( Woocommerce_German_Market::$plugin_path ) . DIRECTORY_SEPARATOR . 'inc' . DIRECTORY_SEPARATOR . 'php-iban' . DIRECTORY_SEPARATOR . 'oophp-iban.php' );

				$validator = new IBAN( trim( $_REQUEST[ 'german-market-sepa-iban' ] ) );
				$validation = $validator->Verify();

				if ( ! $validation ) {
					wc_add_notice( apply_filters( 'gm_sepa_direct_debit_iban_validation_error_notice', __( 'Please enter a valid IBAN.', 'woocommerce-german-market' ) ), 'error' );
					$error_count++;
				}

			}

			// validate bic
			if ( isset( $_REQUEST[ 'german-market-sepa-bic' ] ) && trim( $_REQUEST[ 'german-market-sepa-bic' ] ) != '' ) {
				
				if ( ! preg_match( '/^([a-zA-Z]){4}([a-zA-Z]){2}([0-9a-zA-Z]){2}([0-9a-zA-Z]{3})?$/', $_REQUEST[ 'german-market-sepa-bic' ] ) ) {
					wc_add_notice( apply_filters( 'gm_sepa_direct_debit_bic_validation_error_notice', __( 'Please enter a valid BIC', 'woocommerce-german-market' ) ), 'error' );
					$error_count++;
				}
			}

			if ( is_wc_endpoint_url( 'order-pay' ) ) {
				if ( ! isset( $_POST[ 'woocommerce_checkout_update_totals' ] ) && isset( $_POST[ 'payment_method' ] ) && $_POST[ 'payment_method' ] == 'german_market_sepa_direct_debit' && empty( $_POST[ 'gm-sepa-direct-debit-checkbox' ] ) ) {
					$notice_text = apply_filters( 'gm_sepa_direct_debit_checkbox_validation_notce', __( 'You have to agree to the sepa direct debit mandate.', 'woocommerce-german-market' ) );
					wc_add_notice( $notice_text, 'error' );
					$error_count ++;
				}
			}
		
		}

		return $error_count;

	}

	/**
	* Validate Additional Fields if second checkout page is disabeld
	**/
	public function validate_fields() { 

		if ( get_option( 'woocommerce_de_secondcheckout', 'off' ) == 'off' ) {
			$error_count = self::validate_required_fields( 0 );
		}

	}

	/**
	* Add checkbox
	*
	* @wp-hook woocommerce_de_review_order_after_submit called in inc/WGM_Sepa_Direct_debit
	* @param Integer $error_count
	* @return Integer
	**/
	public static function checkout_field_checkbox( $review_order ) {

		$rtn_before = apply_filters( 'gm_sepa_checkout_field_checkbox', false );
		if ( $rtn_before ) {
			return $review_order;
		}

		$confirmation_text = get_option( 'woocommerce_german_market_sepa_direct_debit_checkbox_confirmation_text', __( 'I agree to the [link]sepa direct debit mandate[/link].', 'woocommerce-german-market' ) );
		$confirmation_text = str_replace( 
			array( '[link]', '[/link]' ),
			array( '<a href="#" id="gm-sepa-mandate-preview" style="cursor: pointer;">', '</a>' ),
			$confirmation_text
		);

		$show_checkbox_style = '';
		$class = '';
		$confirm_order_page_enabled = get_option( 'woocommerce_de_secondcheckout', 'off' ) == 'on';

		if ( $confirm_order_page_enabled ) {

			$payment_method = WGM_Session::get( 'payment_method', 'first_checkout_post_array' );
			if ( $payment_method != 'german_market_sepa_direct_debit' ) {
				$show_checkbox_style = 'style="display:none;"';
			}

		} else {

			$show_checkbox_style = 'style="display:none;"';
			$class = ' gm-sepa-direct-debit-second-checkout-disabled';
		}

		// support for manual order confirmation
		if ( is_wc_endpoint_url( 'order-pay' ) ) {
			$show_checkbox_style = 'style="display:none;"';
			$class = ' gm-sepa-direct-debit-order-pay';
		}

		$review_order .= sprintf(
			'<p class="form-row terms' . $class . '" ' . $show_checkbox_style . '>
				<input type="checkbox" class="input-checkbox" name="gm-sepa-direct-debit-checkbox" id="gm-sepa-direct-debit-checkbox" />
				<label for="gm-sepa-direct-debit-checkbox" class="checkbox">%s</label>
				<div id="gm-sepa-mandate-preview-text" style="display: none;"></div>
			</p>',
			$confirmation_text
		);
		
		return $review_order;
	}

	/**
	* Validate checkbox
	*
	* @wp-hook gm_checkout_validation_fields called in inc/WGM_Sepa_Direct_debit
	* @param Integer $error_count
	* @return Integer
	**/
	public static function checkout_field_checkbox_validation( $error_count = 0 ) {

		$rtn_before = apply_filters( 'gm_sepa_checkout_field_checkbox', false );
		if ( $rtn_before ) {
			return $error_count;
		}

		if ( ! isset( $_POST[ 'woocommerce_checkout_update_totals' ] ) && isset( $_POST[ 'payment_method' ] ) && $_POST[ 'payment_method' ] == 'german_market_sepa_direct_debit' && empty( $_POST[ 'gm-sepa-direct-debit-checkbox' ] ) ) {
			$notice_text = apply_filters( 'gm_sepa_direct_debit_checkbox_validation_notce', __( 'You have to agree to the sepa direct debit mandate.', 'woocommerce-german-market' ) );
			wc_add_notice( $notice_text, 'error' );
			$error_count ++;
		}

		return $error_count;

	}

	/**
	* add bulk action download 
	*
	* @access public
	* @hook admin_footer
	* @return void
	*/
	public function bulk_admin_footer() {
		
		global $post_type;

		if ( 'shop_order' == $post_type ) {
			?>
			<script type="text/javascript">
			jQuery(function() {
				jQuery('<option>').val('gm_download_sepa_xml').text('<?php _e( 'Downloads SEPA XML', 'woocommerce-german-market' )?>').appendTo('select[name="action"]');
				jQuery('<option>').val('gm_download_sepa_xml').text('<?php _e( 'Downloads SEPA XML', 'woocommerce-german-market' )?>').appendTo('select[name="action2"]');
			});
			</script>
			<?php
		}
	}

	/**
	* Do bulk action sepa xml
	*
	* @access public
	* @hook load-edit.php
	* @return void
	*/
	public function bulk_action() {

		$wp_list_table = _get_list_table( 'WP_Posts_List_Table' );
		$action        = $wp_list_table->current_action();

		// return if it's not the sepa xml action
		if ( $action != 'gm_download_sepa_xml' ) {
			return;
		}

		// return if no orders are checked
		if ( ! isset( $_REQUEST[ 'post' ] ) ) {
			return;
		}

		$post_ids = array_map( 'absint', (array) $_REQUEST[ 'post' ] );

		// return if no order is checked
		if ( empty( $post_ids ) ) {
			return;
		}

		$sepa_orders = array();

		foreach ( $post_ids as $post_id ) {

			$order = wc_get_order( $post_id );
			if ( $order->get_payment_method() == 'german_market_sepa_direct_debit' ) {
    			$sepa_orders[] = $order;
    		}

		}

		if ( empty( $sepa_orders ) ) {
			return;
		}

		$sdd_settings = get_option( 'woocommerce_german_market_sepa_direct_debit_settings' );

		$message_id = apply_filters( 'german_market_sepa_xml_message_id', $sdd_settings[ 'bic' ] . '00' . date( 'YmdHis', time() ) );
		$payment_id = apply_filters( 'german_market_sepa_xml_payment_id', 'PAY-ID-' . date( 'YmdHis', time() ) );

		$direct_debit = Digitick\Sepa\TransferFile\Factory\TransferFileFacadeFactory::createDirectDebit( $message_id, $sdd_settings[ 'creditor_account_holder' ], $this->pain_format );

		$direct_debit->addPaymentInfo( $payment_id, array(
			    'id'                    => $payment_id,
			    'creditorName'          => $sdd_settings[ 'creditor_account_holder' ],
			    'creditorAccountIBAN'   => preg_replace('/\s+/', '', $sdd_settings[ 'iban' ] ),
			    'creditorAgentBIC'      => preg_replace('/\s+/', '', $sdd_settings[ 'bic' ] ),
			    'seqType'               => Digitick\Sepa\PaymentInformation::S_ONEOFF,
			    'creditorId'            => preg_replace('/\s+/', '', $sdd_settings[ 'creditor_identifier' ] ),
			)
		);

		foreach ( $sepa_orders as $sepa_order ) {

			$order_sepa_fields = array();
			$sepa_fields = self::get_payment_fields();

    		foreach ( $sepa_fields as $key => $sepa_field ) {

    			if ( $this->encryption_possible_on() ) {
    				$value = $this->decrypt( $sepa_order->get_meta( '_german_market_sepa_' . esc_attr( $key ) ), $sepa_order );
    			} else {
    				$value = $sepa_order->get_meta( '_german_market_sepa_' . esc_attr( $key ) );
    			}

    			$order_sepa_fields[ $key ] = $value;

    		}

    		$order_sepa_fields[ 'mandate_id' ] = $sepa_order->get_meta( '_german_market_sepa_mandate_reference' );
    		if ( $this->encryption_possible_on() ) {
    			$order_sepa_fields[ 'mandate_id' ] = $this->decrypt( $order_sepa_fields[ 'mandate_id' ], $sepa_order );
    		}

			$direct_debit->addTransfer( $payment_id, array(
				    'amount'                => $sepa_order->get_total(),
				    'debtorIban'            => preg_replace('/\s+/', '', $order_sepa_fields[ 'iban' ] ),
				    'debtorBic'             => preg_replace('/\s+/', '', $order_sepa_fields[ 'bic' ] ), 
				    'debtorName'            => $order_sepa_fields[ 'holder' ],
				    'debtorMandate'         => $order_sepa_fields[ 'mandate_id' ],
				    'debtorMandateSignDate' => date_i18n( 'd.m.Y', strtotime( $sepa_order->get_date_created() ) ),
				    'remittanceInformation' => apply_filters( 'german_market_sepa_xml_remittance_information', sprintf( __( 'Order %s', 'woocommerce-germanized' ), $sepa_order->get_order_number() ), $sepa_order ),
				)
			);
		}
		
		$filename = apply_filters( 'german_market_sepa_xml_filename', 'sepa-xml-export-' . date( 'Y-m-d-H-i' ) . '-' . $payment_id . '.xml' );

		// Output and Error Handling
		$xml = false;
		$error = false;

		try {

			$xml =  $direct_debit->asXML();

		} catch ( Exception $e ) {

			$error = $e->getMessage();

		}

		if ( $error ) {

			update_option( '_german_market_sepa_xml_error', $error );

		} else {

			header( 'Content-Description: File Transfer' );
			header( 'Content-Disposition: attachment; filename=' . $filename );
			header( 'Content-Type: text/xml; charset=' . get_option( 'blog_charset' ), true );
			header( 'Cache-Control: no-cache, no-store, must-revalidate' ); 
			header( 'Pragma: no-cache' );
			header( 'Expires: 0' );

			echo $xml;

			exit();

		}

	}

	/**
	* Show XML Download Errors
	*
	* @access public
	* @hook admin_notices
	* @return void
	*/
	public function admin_xml_error_notice() {

		$class = 'notice notice-error';

		$link = get_admin_url() . 'admin.php?page=wc-settings&tab=checkout&section=german_market_sepa_direct_debit';

		$message = sprintf( __( 'An error occurred during SEPA XML Download.<br />Please, check whether you have entered and saved all the SEPA options about the creditor (Account Holder, IBAN, BIC, Creditor Identifier Number, Pain Format) here: <a href="%s">SEPA Settings</a>.<br />If you still get errors after this, please contact <a href="https://marketpress.de/hilfe/" target="_blank">MarketPress Support</a> and convey this error message: %s', 'woocommerce-german-market' ), $link, get_option( '_german_market_sepa_xml_error', 'No Message' ) );

		printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), $message ); 

		delete_option( '_german_market_sepa_xml_error' );

	}

	/**
	* adds a small download button to the admin page for orders
	*
	* @since 0.0.1
	* @access public
	* @static 
	* @hook woocommerce_admin_order_actions
	* @arguments $actions, $theOrder
	* @return $actions
	*/	
	public static function admin_order_actions( $actions, $order ) {
		
		if ( $order->get_payment_method() == 'german_market_sepa_direct_debit' ) {
			
			$args = array(

				'action'				=> 'german_market_download_sepa_mandate',
				'order_id'				=> $order->get_id(),

	            'street'    			=> $order->get_billing_address_1(),
	            'city'      			=> $order->get_billing_city(),
	            'zip'       			=> $order->get_billing_postcode(),
	            'country'   			=> $order->get_billing_country(),
	            'holder'    			=> $order->get_meta( '_german_market_sepa_holder' ),
	            'iban'      			=> $order->get_meta( '_german_market_sepa_iban' ),
	            'bic'      				=> $order->get_meta( '_german_market_sepa_bic' ),
	            'mandate_id'			=> $order->get_meta( '_german_market_sepa_mandate_reference' ),
	            'date'					=> date_i18n( get_option( 'date_format' ), strtotime( $order->get_date_created() ) ),

	            'email_admin_iban_mask' => $this->email_admin_mask,

	        );

	        if ( $this->encryption_possible_on() ) {
				$args[ 'holder' ] 		= $this->decrypt( $args[ 'holder' ], $order );
				$args[ 'iban' ] 		= $this->decrypt( $args[ 'iban' ], $order );
				$args[ 'bic' ] 			= $this->decrypt( $args[ 'bic' ], $order );
				$args[ 'mandate_id' ] 	= $this->decrypt( $args[ 'mandate_id' ], $order );
			}

			$args[ 'unmasked_iban' ] = $args[ 'iban' ];
			$args[ 'iban' ] = $this->mask_iban( $args[ 'iban' ] );

			$query = http_build_query( $args );

			// sepa_mandate pdf button
			$create_pdf = array( 
				'url' 		=>	wp_nonce_url( admin_url( 'admin-ajax.php?' . $query ), 'german-market-sepa-mandate' ), 
				'name' 		=> __( 'Download Sepa Mandate', 'woocommerce-german-market' ),
				'action' 	=> 'sepa_mandate'
			);

			$actions[ 'sepa_mandate' ] = $create_pdf;

		}
	
		return $actions;
	}

}
