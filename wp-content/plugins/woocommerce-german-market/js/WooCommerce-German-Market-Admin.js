jQuery.noConflict();

( function( $ ) {

	woocommerce_de = {

		init: function () {

			this.scale_unit_hint();
            this.show_and_hide_panels();

            $('input#_digital, input.variable_is_downloadable').change(function(){
                woocommerce_de.show_and_hide_panels();
            });

            $( document ).on( 'woocommerce_variations_loaded', function() {
               $( '#variable_product_options' ).on( 'change', 'input.variable_is_downloadable, input.variable_is_digital', function () {
                    var variation_div = $( this ).closest( '.woocommerce_variable_attributes' );
                    woocommerce_de.show_and_hide_variable_requirements( variation_div );
                } );
            });

            this.video();

            this.update_screen();

            this.email_attachments();

            jQuery( '#de_shop_emails_file_attachments_nr' ).change( function() {
                woocommerce_de.email_attachments();
            });

            this.upload_attachments();
            this.remove_attachments();
            this.ui_tooltips();

		},

        ui_tooltips: function() {


            var tiptip_args = {
                'attribute': 'data-tip',
                'fadeIn': 50,
                'fadeOut': 50,
                'delay': 200
            };

            
            $( '.german-market-main-menu .tips, .german-market-main-menu .help_tip, .german-market-main-menu .woocommerce-help-tip' ).tipTip( tiptip_args );

        },

        upload_attachments: function() {

            jQuery( '.de_shop_emails_file_attachments_upload_button' ).click( function() {

                var this_id      = ( jQuery(this).attr( 'id' ) );
                var formfield_id = this_id.replace( 'de_shop_emails_file_attachments_upload_button_', 'de_shop_emails_file_attachment_' );
                
                window.send_to_editor = function( html ) {
                    var attachment = jQuery( 'a', html );
                    var href = jQuery( attachment ).find( 'a' ).attr( 'href' );

                    if ( typeof href == "undefined" ) {
                      href = $( html ).attr( 'href' );
                    }
                    
                    jQuery( '#' + formfield_id ).val( href );
                    tb_remove();
                }
            
            tb_show( '', 'media-upload.php?TB_iframe=true' );
            return false;
                
            });

        },

        remove_attachments: function() {

            jQuery( '.de_shop_emails_file_attachments_remove_button' ).click( function() {

                var this_id = ( jQuery(this).attr( 'id' ) );
                var formfield_id = this_id.replace( 'de_shop_emails_file_attachments_remove_button_', 'de_shop_emails_file_attachment_' );
                jQuery( '#' + formfield_id ).val( '' );

            });

        },

        email_attachments: function() {

            if ( jQuery( '#de_shop_emails_file_attachments_nr' ).length ) {

                var number_of_attachments = jQuery( '#de_shop_emails_file_attachments_nr' ).val();

                jQuery( '.de_shop_emails_file_attachment' ).each( function() {

                    jQuery( this ).parents( 'tr' ).hide();

                });

                for ( var i = 1; i <= number_of_attachments; i++ ) {

                    var id_of_the_element = '#de_shop_emails_file_attachment_' + i;
                    jQuery( id_of_the_element ).parents( 'tr' ).show();

                }
            }

        },

        show_and_hide_variable_requirements: function( variation_div ) {
            
            var digital_checkbox = $( variation_div ).find( '.variable_is_digital' );
            var downloadable_checkbox = $( variation_div ).find( '.variable_is_downloadable' );
            var is_variable_digital = $( digital_checkbox ).prop( 'checked' );
            var is_variable_downloadable = $( downloadable_checkbox ).prop( 'checked' );
            var is_variable_digital_or_downloadable = is_variable_digital || is_variable_downloadable;

            if ( is_variable_digital_or_downloadable ) {
                $( variation_div ).find( '.show_if_variation_downloadable_or_digital' ).show();
            } else {
                $( variation_div ).find( '.show_if_variation_downloadable_or_digital' ).hide();
            }

        },

        show_and_hide_panels: function() {
            var is_digital      = $('input#_digital:checked').size();
            var is_variable_downloadable = $('input.variable_is_downloadable:checked').size();

            $('.show_if_digital').hide();
            $('.show_if_variation_is_downloadable').hide();

            if( is_digital ) {
                $('.show_if_digital').show();
            }

            if( is_variable_downloadable ) {
                $('.show_if_variation_is_downloadable').show();
            }


            $('input#_manage_stock').change();
        },

		scale_unit_hint: function () {

			if ( $( '#woocommerce_attributes .toolbar' ).length > 1)
				$( '#woocommerce_attributes .toolbar' )
					.last()
					.append( woocommerce_product_attributes_msg );
		},

        update_screen: function() {

            $( '#update-plugins-table' ).ready( function() {
                $( this ).find( 'strong' ).each ( function() {
                   if ( $( this ).html() == 'German Market' ) {
                        $( this ).next( 'a' ).hide();
                        return false;
                    }
                });
            });

        },

        video: function() {

            $( '.wgm-video-wrapper a.open' ).click( function(){

                var video_url = $( this ).parent().find( 'span' ).html();
                var video_outer = $( this ).parent().find( '.videoouter' );
                var video_markup = '<video autoplay><scource src="' + video_url + '" type="video/mp4"></scource></video>';
                var video_div = $( this ).parent().find( '.video' );
                var video_close = $( this ).parent().find( '.close' );

                if ( $( video_div ).html() == '' ) {
                    
                    var video = $('<video />', {
                        id: 'video' + video_url,
                        src: video_url,
                        type: 'video/mp4',
                        controls: true,
                        autoplay: true,
                    });
                    
                    video.appendTo( $( video_div ) );
    
                }
                
                $(video_outer).show();
                $( this ).hide();
                $( video_close ).show();
                $( this ).parent().addClass( 'wgm-video-isShown' );
                $( video_div ).show();             

            } );

            $( '.wgm-video-wrapper a.close' ).click( function(){
               
                var video_open = $( this ).parent().parent().parent().find( '.open' );
                var video_div = $( this ).parent().find( '.video' );
                var video_outer = $( this ).parent().parent().parent().find( '.videoouter' );
                
                $( video_outer ).hide();
                $( this ).hide();
                $( video_open ).show();
                $( this ).parent().removeClass( 'wgm-video-isShown' );
                $( this ).parent().find( 'video' ).get(0).pause();
                $( video_div ).hide();  
                    
            });

            $( '.videoouter' ).click( function() {
                
                var element = $( event.target );
                
                if ( $( element ).hasClass( 'videoouter' ) ) {
                    
                    var video_open = $( this ).parent().parent().parent().find( '.open' );
                    var video_div = $( this ).parent().find( '.video' );
                    var video_outer = $( this ).parent().parent().parent().find( '.videoouter' );
                    
                    $( video_outer ).hide();
                    $( this ).hide();
                    $( video_open ).show();
                    $( this ).parent().removeClass( 'wgm-video-isShown' );
                    $( this ).parent().find( 'video' ).get(0).pause();
                    $( video_div ).hide();  
                }

            });
            
            $( '.german-market-left-menu .mobile-menu-outer' ).click( function(){
                $( '.german-market-left-menu ul,.mobile-icon' ).toggleClass( 'open' );
            });

            $( '.add-on-switcher' ).click( function() {
                $( this ).toggleClass( 'on', 'off' );
            });

        }
	};

	$( document ).ready( function( $ ) { 
        woocommerce_de.init();
    } );

} )( jQuery );
