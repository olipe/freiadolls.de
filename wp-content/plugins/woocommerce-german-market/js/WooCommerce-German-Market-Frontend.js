jQuery.noConflict();

(
	function( $ ) {

		var woocommerce_de = {

			init: function() {
				//this.setupAjax();
				this.remove_totals();
				this.register_payment_update();
				this.on_update_variation();
				this.sepa_direct_debit();
			},

			// not in use any more, but still exists for compatibility check reasons
			setupAjax: function() {
				if ( typeof wgm_wpml_ajax_language !== 'undefined' ) {
					$.ajaxSetup( { data: { 'lang': wgm_wpml_ajax_language } } );
				}
			},

			remove_totals: function() {

				if ( woocommerce_remove_updated_totals == 1 ) {
					$( '.woocommerce_message' ).remove();
				}
			},

			register_payment_update: function() {
				$( document.body ).on( 'change', 'input[name="payment_method"]', function() {
					$( 'body' ).trigger( 'update_checkout' );
				} );
			},

			on_update_variation: function() {
				var product = $( 'body.single-product' ), price = $( '.legacy-itemprop-offers' );
				product.on( 'found_variation', '.variations_form', function() {
					price.slideUp();
				} );

				product.on( 'reset_data', '.variations_form', function() {
					price.slideDown();
				} );

			},

			sepa_direct_debit_show_preview: function() {

				var data = {
					'holder' 	: $( '[name="german-market-sepa-holder"]' ).val(),
					'iban' 		: $( '[name="german-market-sepa-iban"]' ).val(),
					'bic' 		: $( '[name="german-market-sepa-bic"]' ).val(),
					'street' 	: $( '[name="billing_address_1"]' ).val(),
					'zip'		: $( '[name="billing_postcode"]' ).val(),
					'city'		: $( '[name="billing_city"]' ).val(),
					'country'	: $( '[name="billing_country"]' ).val(),
				};

				var show = true;

				for ( key in data ) {
					if ( data[ key ] !== undefined && data[ key ].trim() == '' ) {
						show = false;
						break;
					}
				}

				return show;

			},

			sepa_direct_debit_show_preview_do: function() {

				var do_it = woocommerce_de.sepa_direct_debit_show_preview();

				if ( do_it ) {

					$( '.gm-sepa-direct-debit-second-checkout-disabled' ).show();
					$( '.gm-sepa-direct-debit-order-pay' ).show();

				} else {

					$( '.gm-sepa-direct-debit-second-checkout-disabled' ).hide();
					$( '.gm-sepa-direct-debit-order-pay' ).hide();
					$( '#gm-sepa-mandate-preview-text' ).slideUp();

				}

			},

			sepa_direct_debit: function() {

				$( document.body ).on( 'click', '#gm-sepa-mandate-preview', function( e ){

					e.preventDefault();

					var data = {
						'action'	: 'gm_sepa_direct_debit_mandate_preview',
						'holder' 	: $( '[name="german-market-sepa-holder"]' ).val(),
						'iban' 		: $( '[name="german-market-sepa-iban"]' ).val(),
						'bic' 		: $( '[name="german-market-sepa-bic"]' ).val(),
						'street' 	: $( '[name="billing_address_1"]' ).val(),
						'zip'		: $( '[name="billing_postcode"]' ).val(),
						'city'		: $( '[name="billing_city"]' ).val(),
						'country'	: $( '[name="billing_country"]' ).val(),
						'nonce'		: sepa_ajax_object.nonce
					};

					jQuery.post( sepa_ajax_object.ajax_url, data, function( response ) {
						$( '#gm-sepa-mandate-preview-text' ).html( response );
						$( '#gm-sepa-mandate-preview-text' ).slideDown();

						$( '#gm-sepa-mandate-preview-text .close' ).click( function(){
							$( '#gm-sepa-mandate-preview-text' ).slideUp();
						});
					});

				});


				$( '#gm-sepa-mandate-preview' ).ready( function(){

					if ( ! $( '#gm-sepa-mandate-preview' ).length ) {
						return;
					}

					var fields = {
						0 : '[name="german-market-sepa-holder"]',
						1 : '[name="german-market-sepa-iban"]' ,
						2 : '[name="german-market-sepa-bic"]',
						3 : '[name="billing_address_1"]',
						4 : '[name="billing_postcode"]',
						5 : '[name="billing_city"]',
						6 : '[name="billing_country"]',
					};

					for ( key in fields ) {
						$( document.body ).on( 'change keyup', fields[ key ], woocommerce_de.sepa_direct_debit_show_preview_do );
					}
				
				});

				$( document.body ).on( 'change', 'input[name="payment_method"]', function() {
					if ( $( this ).val() == 'german_market_sepa_direct_debit' ) {
						$( '.gm-sepa-direct-debit-second-checkout-disabled' ).show();
					} else {
						$( '.gm-sepa-direct-debit-second-checkout-disabled' ).hide();
					}
				});

				$( '.gm-sepa-direct-debit-second-checkout-disabled' ).ready( function() {
					if ( $( '#payment_method_german_market_sepa_direct_debit' ).is(':checked' ) ) {
						$( '.gm-sepa-direct-debit-second-checkout-disabled' ).show();
					}
				});

			}
		};

		$( document ).ready( function( $ ) {
			woocommerce_de.init();
		} );

	}
)( jQuery );