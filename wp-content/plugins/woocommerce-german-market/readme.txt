=== German Market ===
Contributors: MarketPress
Requires at least: 4.8+
Tested up to: 4.8+
Stable tag: 3.5.1


== Description ==
German Market macht deinen WooCommerce-Shop fit für den deutschsprachigen Markt und für die gesamte EU. Inklusive aller rechtlich relevanter Inhalte, integrierter Rechnungserstellung, Anbindung an dein Buchhaltungssystem, Bearbeitung von Stornos und vielem mehr. Einfach verkaufen - mit German Market.

= Features =
<https://marketpress.de/german-market-funktionen>


= Hinweise =
*WICHTIG - HAFTUNGSAUSSCHLUSS*
Dieses Plugin trägt wesentlich dazu bei, ihren WooCommerce Shop rechtssicher betreiben zu können. Es ist durch einen Rechtsanwalt geprüft worden, der ebenfalls die rechtlichen Mustertexte wie AGB und die Widerrufsbelehrung erstellt hat. Sie entsprechen den rechtlichen Anforderungen eines typischen, an Verbraucher gerichteten Onlineshops. Jedoch kann die rechtliche Sicherheit eines Onlineshops nur im Einzelfall geprüft und bestätigt werden. Daher sind die rechtlichen Muster nur als Vorlagen zu verstehen, deren abschließende rechtliche Prüfung und ggf. Anpassung Ihnen obliegt. Falls Sie eine Prüfung Ihres Shops wünschen, können Sie sich an Rechtsanwalt Thomas Schwenke, LL.M. <http://rechtsanwalt-schwenke.de/service/kontakt/> wenden.

== Installation ==
= Requirements =
* WordPress 4.8+*
* PHP 5.6+*
* WooCommerce 3.2+*

= Installation =
 * Installieren Sie zuerst WooCommerce
 * Installieren Sie die Standardseiten für WooCommerce (Folgen Sie dazu der Installationsroutine von Wooocommerce)
 * Benutzen Sie den installer im Backend, oder

1. Entpacken sie das zip-Archiv
2. Laden sie es in das `/wp-content/plugins/` Verzeichnis ihrer WordPress Installation auf ihrem Webserver
3. Aktivieren Sie das Plugin über das 'Plugins' Menü in WordPress und drücken Sie aktivieren
4. Folgen Sie den Anweisungen des Installationsbildschirms

== Other Notes ==
= Acknowledgements =
Thanks Mike Jolley (http://mikejolley.com/) for supporting us with the WooCommerce core.

= Licence =
 GPL Version 3

= Languages =
- English (en_US) (default)
- German (de_DE)

== Changelog ==
= 3.5.1 =
- Erweitert: Es ist nun möglich den Link zu Datenschutz bei "Hier erfährst du mehr zu den Versandkosten, Zahlungsweisen und unserer Widerrufsbelehrung." einzufügen
- Erweitert: Die Details vor der Bestellübersicht, wie z.B. die Zahlungsanweisungen, lassen sich nun mit Filter ausstellen
- Erweitert: Für die Versandadresse gibt es jetzt bei WooCommerce -> German Market -> Rechnungs PDF -> Rechnungsinhalt nun die Option Lieferadresse
- Erweitert: Mehr Filter um die Checkout-Checkboxen zu manipulieren
- Erweitert: Rechnungs-PDF Add-On: Neuer Filter vor der Generierung der PDF hinzugefügt, um Generierung der PDF zu unterbinden
- Erweitert: Filter für Lieferzeitangabe, wenn das Produkt "Out of stock" (Nicht auf Lager) ist
- Erweitert: Lieferschein im Briefumschlag mit Fenster nun nutzbar 
- Erweitert: Filter, um "zzgl. Versand"-Link ohne target=_blank zu gestalten
- Erweitert: EU USTID-Nr. Check Add-On: Filter, um das Verstecken und Löschen des VAT ID Feldes zu verhindern, wenn Kundenrechnungsland und Shopbasisland übereinstimmen
- Erweitert: Filter, Lieferzeiten bei variablen Produkten, um die Ausgabe Lieferzeit des Elternproduktes zu ermöglichen
- Verbessert: Kompatibilität Woocomerce Composite Products Plugin
- Verbessert: Kompatibilität Woocomerce Klarna
- Verbessert: Kompatibilität Polylang
- Verbessert: Kompatibilität WPML
- Verbessert: Kompatibilität Theme Avada Checkout
- Verbessert: Kompatibilität Theme Envision
- Verbessert: Kompatibilität Theme Fluent
- Verbessert: Lexoffice Add-On: Bessere Fehlermeldung, wenn Bestellungen mit einem Gesamtbetrag von 0 Euro gesendet werden
- Verbessert: Double Opt-In für Kundenregistrierung
- Fix: Steuerberechnung bei Kleinst-Cent-Beträgen
- Fix: lexoffice Add-On: Bei kleineren Beträgen kam es zu Fehlern in der Rundung
- Fix: EU Mehrwertsteuer Add-On: Berechnung im Checkout wegen geänderten Klasseneingenschaften
- Fix: Kleinere Bugfixes
- Update: Sprachdateien


= 3.5 (Releasepost: https://marketpress.de/2017/german-market-sepa-archive-double-opt-in) =
- Feature: Double Opt-In für Kundenregistrierung
- Feature: Archivierung von SEPA-Mandaten
- Erweitert: Kauf auf Rechnung - Verfügbarkeit im Checkout optional erst ab 3. Bestellung
- Erweitert: Bulkdownload für Liefer- und Rücksendescheine
- Erweitert: Der Hinweistext für Kleinunternehmerregelung lässt sich nun im Backend anpassen
- Erweitert: Das Lexoffice Add-On wurde um einen Filter erweitert, damit auch Belege an lexoffice gesendet werden können, obwohl die Bestellung in WooCommerce noch nicht als “fertiggestellt” markiert wurde
- Verbessert: Option “German Market Checkout Template erzwingen” greift nun auch auf das Template “checkout/terms”
- Verbessert: Kompatibilität mit WooCommerce Subscriptions und Rechnungsnummer Add-On
- Verbessert: Kompatibilität WPML
- Verbessert: Kompatibilität Polylang
- Verbessert: Kompatibilität Theme Avada
- Verbessert: Kompatibilität Theme Superba
- Verbessert: Kompatibilität Theme WooDance
- Update: Sprachdateien
- Fix: Im Liefer- & Rücksendeschein wurden HTML Tags im Produktnamen ausgegeben
- Fix: Einheit (user unit = cm) bei body{ width: ... cm } fehlte
- Fix: EU VAT ID CHECK: Wenn die Klasse SoapClient nicht vorhanden ist, gibt es einen FATAL ERROR
- Fix: Kleinere weitere Bugfixes.


= 3.4.2 =
- Enhancement: Invoice PDF: Switch off the shipping address by filter
- Improved: Show shipping information to non-EU countries” for variable products in the wrong place
- Improved: Itemprop Markup removed
- Fix: WGM Template::attribute _in_product_name_order - make third parameter optional with default to false, because some themes only apply the filter with 2 instead of 3 parameters
- Fix: Invoice PDF: syntax error, unexpected ‘wp_wc_invoice_pdf_page_numbers’ (T_STRING) in PhpEvaluator.php (55): eval () ‘d code on line 5
- Fix: Small other bugfixes


= 3.4.1 =
- Improved: EU VAT Number Check: Handling of the VAT ID input field during checkout and being tax exempt when logging in
- Fix: Error when login fails on 1st checkout, avoid possibility to login on 2nd checkout page
- Fix: Small other bugfixes


= 3.4 =
- Feature: IT Recht Kanzlei API
- Feature: Protected Shops API
- Feature: 1&1 Online Buchhaltung API
- Enhancement: EU VAT Number Check - Optional text for non-EU countries (Tax-exempt export delivery)
- Enhancement: Refund Number is editable
- Enhancement: Invoice PDF & Subcriptions: PDFs can be attached in emails of the Plugin "Subscriptions"
- Improved: Price per unit - better output and markup.
- Improved: Compatibility with WPML
- Improved: Purchase on account + cash on delivery + SEPA - notice at fee
- Improved: Small improvements to the lexoffice API
- Improved: Compatibility with Product CSV Importer
- Update: DOMPDF Version
- Fix: Incompatibility: Add-On “EU USTID Check” and “WooCommerce Amazon Pay Gateway”
- Fix: Invoice PDF - PDF Design is displayed incorrectly when the invoice is downloaded a second time
- Fix: Small other bugfixes


= 3.3.2 =
- Improved: Price per unit – better output and markup
- Improved: Compatibility with WPML
- Fix: Incompatibility: Add-On “EU USTID Check” and “WooCommerce Amazon Pay Gateway”
- Fix: VAT for variations of a variable product, PHP Warning
- Fix: Fatal error Uncaught TypeError: argument 2 passed to WGM_Fee :: add_fee_to_order_tax_totals () must be an instance of WC_Order
- Fix: “Digital Item Notice” was not displayed in the checkout without 2nd Checkout
- Fix: Invoice PDF – PDF Design is displayed incorrectly when the invoice is downloaded a second time
- Fix: Small other bugfixes


= 3.3.1 =
- Enhancement: Filter for email heading in Confirm Order email
- Enhancement: Filter for CC in emails
- Enhancement: SEPA Filter to prevent email sending to customer
- Enhancement: Added check whether $order is a refund in our hooks
- Improved: More notices for SEPA gateway
- Improved: Compatibility with WooCommerce Subscriptions
- Updated: Translations
- Fix: No schema.org data at related products
- Fix: “Learn more about shipping costs, payment methods and our revocation policy.” Incorrect/broken HTML markup
- Fix: GM UI Tooltips 
- Fix: Wrong Itemprop in variable products
- Fix: EU Vat Checkout: Do not force parameter type
- Fix: Email Attachements: If these are activated but disabled by admins, the legal texts are also not attached to customer emails (thanks for your order) if an admin e-mail is sent at the same time
- Fix: SEPA XML Export: Wrong totals
- Fix: Small other bugfixes


= 3.3 =
- Feature: SEPA Direct Debit Mandate
- Feature: Billbee Add-On - Link to the order in Billbee
- Enhancement Improved German Market Checkout themes compatibility
- Enhancement: Cart Note: “You can use [link-shipping][/link-shipping], [link-payment][/link-payment]. [link-revocation][/link-revocation] to add the specific links.”
Is now customizable on the backend.
- Enhancement: lexoffice - In the case of the WooCommerce guests that shall be created in lexoffice, the e-mail address of the WooCommerce guest is used to check whether this is already present in lexoffice. If so, this lexoffice contact is used.
- Enhancement: lexoffice - Problems with guest users, if not as a collection in lexoffice
- Enhancement: Lexoffice - Better error output for debugging
Lexoffice: Better error output for debugging
- Enhancement: more hooks for loop -> product output
- Enhancement: Running Invoice Number - Filter for prefix and suffix before generation
- Improved: Hide other shipping methods at free shipping
- Improved: Compatibility with WooCommerce Composite Products
- Improved: Compatibility with WPML
- Improved: Compatibility with WordPress Theme “The Retailer”
- Updated: WooCommerce Templates
- Updated: Translations
- Fix: Incorrect output of the delivery time string, if this contains a comma as soon as the order is completed and the quantity of the line item in the purchase order is 1
- Fix: Purchase On Acccount - Order Status “On Hold” does not work
- Fix: FIC - alc. Is always stored in the order item when the order is completed, even if no value has been set
- Fix: Disable shipping notifications, does not store for variations of variable products, so it does not work in the frontend
- Fix: small Bugfixes lexoffice tax calculation
- Fix: small Bugfixes in Invoice and Refund PDF/Template
- Fix: order confirmation mail - thick dashes in the order table
- Fix: small other Bugfixes


= 3.2.3 =
- Fix: SevDesk FATAL ERROR at SKU (determine product first) from order item
- Fix: Shipping costs, taxes .. if the option is not an array, comes a PHP warning
- Fix: Due date, PHP ERROR by WC Changeover of time functions
- Fix: WooCommerce 3.0.1 compatibility
- Fix: small other Bugfixes


= 3.2.2 =
- Enhancement: Invoice Number - Double-digit year
- Enhancement: Landscape for Invoice PDF
- Enhancement: Daily reset of the invoice number
- Enhancement: Filter for FIC Frontend
- Enhancement: sevDesk - more filters to always mark the order as "open"
- Enhancement: Filter for additional text in the shopping basket "Here you will find .."
- Enhancement: More filters for Order Totals
- Enhancement: Action after invoice number creation
- Enhancement: Filter for delivery time string 
- Enhancement: Filter for the backend no delivery time or (in each case xy Euro)
- Enhancement: Filter before and after PDF Template
- Enhancement: More filters at create-pdf.php
- Enhancement: New filter for PDF e-mail sending
- Enhancement: (In each case xy Euro) show only if quantity> 1
- Update: Translations
- Improved: Manual order
- Improved: Compatibility with WP All Import
- Improved: Compatibility with WPML
- Improved: Compatibility with Polylang
- Fix: After Quick Edit + Bulk Edit, product type returns to Digital
- Fix: Manual order acceptance - no shop admin mail
- Fix: EU VAT - Adjust tax rates
- Fix: Images in returns and delivery notes: http - https problem
- Fix: sevDesk - Incorrect tax and tax rates for small amounts due to incorrect rounding
- Fix: sevDesk: If the shipping is not taxed, false information to sevDesk
- Fix: WooCommerce 3.0 compatibility
- Fix: small UI Bugfixes
- Fix: small other Bugfixes


= 3.2.1 =
- Enhancement: Additional text: Tax free intracommunity delivery
- Enhancement: New filter in WGM_Shipping to manage rounding problems
- Update: Translations
- Fix: Legal text templates
- Fix: No invoice-pdf-download-button on thank-page for not logged in users
- Fix: Order button for payment method PayPal
- Fix: Checkout: At the first page call, "Send Order" is displayed
- Fix: Delivery note - The recipient and the sender are incorrect
- Fix: Static attachments can not be added
- Fix: In the refunds are payment instructions
- Fix: UST ID examination for the Baseland
- Fix: Error with manual order if no payment method is selected
- Fix: Filter to optionally not check if all variants of a variable product has the same delivery time
- Fix: Change of payment method with deactivated 2nd CO page has errors
- Fix: Grouped product - No display of the price range in the shop overview page
- Fix: Notice in the backend when saving orders with charges without taxes
- Fix: small other Bugfixes


= 3.2 =
- Feature: FIC Add-On
- Feature: VATIN Check Add-On
- Feature: Purchase On Acccount
- Feature: Manual order confirmation
- Feature: Due date for payments
- Enhancement: static mail attachments to the backend
- Enhancement: Invoice number - Resetting number at the end of the year and month
- Enhancement: Display of the price per unit display in the checkout
- Enhancement: The lexoffice API to create contacts
- Enhancement: Hide the legal texts in the mails 
- Enhancement: Order confirmation for digital products
- Enhancement: Hide the delivery times
- Enhancement: Customizable order buttons and checkboxes
- Enhancement: Disable the 2nd checkout page
- Enhancement: BCC/CC for the shopadmin of the customer mails
- Enhancement: Deactivation of the telephone number as mandatory field
- Enhancement: Invoice PDF - Variables for the greeting text
- Improved: Compatibility with Visual Composer
- Improved: Compatibility with amazon Payments
- Improved: Compatibility with WPML
- Improved: Compatibility with FooEvents for WooCommerce
- Improved: Compatibility with FooEvents PDF Tickets
- Improved: Compatibility with Polylang
- Improved: Tax calculation for fees
- Improved: MarketPress Auto-Updater
- Improved: Debug as option for the invoice PDF add-on
- Update: Legal text templates
- Update: Translations
- Fix: small Bugfixes

= 3.1.1 =
- Update: Translations
- Fix: PDF E-mail attachments were output blank
- Fix: Error in the order overview Backend when the products are clicked

= 3.1 =
- New German Market UI
- Feature: lexoffice API – For sending voucher data
- Enhancement: Invoice PDF - Bulk download of invoices and refunds for shopadmin
- Enhancement: Invoice PDF - Extra field for Refund number, Refund ID, Refund date, Order number, Invoice number & Payment method
- Enhancement: Invoice PDF - Option Net Prices
- Enhancement: Invoice PDF - Delete button for delete saved invoice PDF content to allow regeneration of the invoice PDF content
- Enhancement: Invoice PDF -If the Small Trading Exemption is activated, an additional text is automatically displayed in the invoice 
- Improved: Compatibility with Enfold Theme
- Improved: Compatibility with WooCommerce Subscriptions
- Improved: Invoice PDF - Display Order notes from the customer and for the customer
- Update: Translations
- Fix: small Bugfixes

= 3.0.2 =
- Feature: Avoid Free Items In Cart Option
- Improved: sevDesk: Filter, so the voucher is not marked as "open", even if it is considered paid in woocommerce
- Improved: Compatibility with WooCommerce Subscriptions
- Improved & Fix: Additional PDFs
- Fix: Backend Order overview page - Display errors in the order overview after product deletion
- Fix: Second-Checkout Template - fixed small bug with checkbox an back button
- Fix: DOMPDF did not load his autoloader, which caused a 500 error
- Fix: bug with Adobe Acrobat DC Pro

= 3.0.1 =
- Fix: Cash on Delivery display shipping method
- Fix: Invoice PDF Custom Google Fonts
- Fix: Display Tax if the tax settings are set to exclusive
- Fix: Removed coupon field from 2nd checkout page
- Fix: Invoice PDF Section text after the billing content now saves correctly html
- Fix: small Bugfixes

= 3.0 =
- Feature: Invoice PDF - Invoice PDF as an attachment to customer emails 
- Feature: Invoice Number - Running Invoice Number to orders
- Feature: Return Delivery Note PDF - Retoure PDF as an attachment to customer emails
- Feature: Refunds – Refund system for customer credits and refunds
- Feature: sevDesk API – For sending voucher data
- Feature:  EU VAT Checkout - Display fixed gross prices and dynamically recalculate taxes (VAT) included in those prices. EU tax rates can be configured in and will be retrieved from the default WooCommerce tax table. 
- Feature: Email attachments for Shop Admin Emails
- Feature: Display of the order time in the order overview and refund overview
- Feature: Translation Legal texts in English
- Feature: Delivery times linked to stock

- Improved: Default Delivery time for variants 
- Improved: Sale Label can be now deactivated in the product loop pages and product single pages.
- Improved: Customizable in second checkout + email attachments 

- Fix: Cash on Delivery endless loop fixed
- Fix: The class shop_table_responsive added in cart.php. 

= 2.6.10 =
- Improved: Compatibility with EU VAT Number
- Update: MarketPress Auto-Updater
- Change: small revised translations

= 2.6.9 =
- Fix: hidden fields in second checkout
- Fix: checkboxes of Order-Pay Page
- Fix: display delivery time for variation
- Fix: display option delivery address by digital + downloadable proucts
- Removed action `add_product_short_desc_to_order_title_mail`
- Renamed: „Place binding order“ Button
- Change: small revised translations

= 2.6.8 =
- Fix: Problem with themes that use buffer ("ob_get_clean")
- Fix: VAT in variation
- Fix: Show Delivery Times Option
- Improved: Parameters for Digital Products

= 2.6.7 =
- Add: Add option to hide sale labels in product loops
- Add: Improved support for grouped products
- Add: Support product embeds

- Fix: Add some checks to delivery time output
- Fix: Improve split-tax calculation when digital products are involved
- Fix: Improve fee tax calculation when no split-tax is used
- Fix: Enforce order-comments to be UTF-8 encoded in PHP < 5.4

- Change: Fix German Translation of Email Footer Setting string
- Change: Display full country name during checkout
- Change: Tidy up WGM_Template code and deprecate some functions
- Change: Remove unnecessary calls to get_plugin_data()
- Change: Tidy up main class and separate admin/frontend-related code
- Change: Always show delivery times on product-single view
- Change: Get rid of cart-totals.php template
- Change: add span tag to sale labels
- Change: Do not show (possibly false) tax rates on variable product summary
- Change: Product summary/price markup is nowin line with WooCommerce default

= 2.6.6 =
- Fix: Prevent using front-end-specific WooCommerce functions from the back-end. This has led to a number of problems in combination with some 3rd party plugins
- Fix: Variation price was not displayed if all variations cost the same
- Fix: Global delivery time setting was not being used
- Fix: Changed JS behaviour when selecting variations so that general information is not hidden earlier than necessary
- Fix: Correctly show tax information during checkout for all tax_display_total/tax_display_cart combinations

= 2.6.5 =
- Fix: Formatting in plaintext and HTML emails
- Fix: Prevent a couple of potential notices
- Fix: Minor markup improvements for 'Small Trading Exemption'(KUR) output
- Fix: 'Small Trading Exemption'(KUR) notice now takes shop location into account
- Fix: Code cleanup and various minor enhancements
- Fix: Correct an issue with second checkout when using SSL
- Fix: Correctly display fee taxes with 'split tax' disabled
- Fix: Prevent potential errors when no 'measuring_unit' attributes are present on PPU settings
- Fix: Fix tax strings not respecting 'Tax based on' setting, fixing various oddities with tax strings
- Fix: Add missing default settings for 'Small Trading Exemption'(KUR) Feature
- Fix: Show thankyou-notice on Cash-on-delivery

- Change: Move 'Small Trading Exemption'(KUR) notice below order total
- Change: Move several product settings to general product options (and variations)
- Change: Fees now get updated when changing payment gateways during checkout
- Change: Add formal language pack support
- Change: Refunded items now show correct tax strings in refund emails
- Change: Update terms checkboxes to match WC2.5 layout

- Add: Introduce configurable 'Sale Labels' to product sale prices
- Add: Display individual tax/shipping info on product variations


= 2.6.4 =
- fixed escaped content attributes (WC 2.4.9 security fix)
- fixed if the option text-mail activated, the legal texts are now properly added
- fixed if the option text-mail activated, the order confirmation will be sent correctly
- fixed broken link revocation in checkout.
- fixed fatal error on reorder button

= 2.6.3 =
- fixed missing variations
- fixed missing KUR in totals for reviewed and mailed orders
- fixes unnecessary spaces removed
- fixes doubled description in second-checkout2 page

= 2.6.2 =
- fixed warning in confirmation mail
- fixed grammatical errors and translation errors corrected
- fixed disappearing attributes after installation
- fixed missing tax for the shipping costs
- fixed deprecated function args
- fixed incomplete declaration of $tax_total_string
- fixed updated text template terms (en+at)
- fixed added missing WooCommerce hooks in templates second-checkout2.php, email-order-items.php and plain/email-order-items.php

- added options for order confirmation mail
- added only check visable variations if they are digital

= 2.6.1 =
- updated UI message on second checkout page to be styled as an informational notice instead of success in order to prevent customers from misreading it and not finalizing their order
- fixed a bug where split tax calculation would not be applied to additional fees
- fixed a bug where a “1” would be displayed instead of the tax label
- fixed a bug where taxes would not appear when tax total display was set to “single”
- fixed a bug where taxes would be displayed as “0,00” at the order-received endpoint
- localized a previously hard-coded text string in a JavaScript file

= 2.6 =
- **Changed default language for text strings from German to English. [Learn more …](http://make.marketpress.com/woocommerce-german-market/269)**
- Ridded the world of improper use of the term “Imprint” outside print context. Props [@_andywashere](https://twitter.com/_andywashere/status/591146826335526913).
- Fixed rate binary output on first checkout page
- Shipping options product tab is now no longer available for virtual products
- Added new product type "digital"
- Added an order confirmation mail
- Reworked WGM sessions
- Split tax calculation is now done before coupons
- Added a filter for split tax deactivation
- Fixed structured data for product single view
- Fixed qoute usage in order comments
- WGM product tabs are using now woocommerce class to be only visable when applicable
- Rearranged element order for variable products
- Added an option for a default tax label
- Shipping cost links now open in new tabs
- fixed inconsistent options
- Added price per unit to product variation view
- Fixed shipping costs for digital products
- Fixed missing revocation policy text in mails when using paypal
- Changed default plugin language form german to english
- Added prerequisites for digital and downloadable variations
- Fixed shipping times for virtual products
- Normalized all tax strings
- Normalized WGM options
- Completely reworked translation

= 2.5.3 =
- Change Autoupdater to support inline plugin update

= 2.5.2 =
- Fixed doubled totals in cart for WooCommerce 2.3.8

= 2.5.1 =
- Fixed missing subtotals on second checkout page

= 2.5 =
- Added exclude tax notice to cart and checkout totals
- Added spacing for shipping method tax string
- Added compatibility with WooCommerce Print Invoice and other PDF Invoice plugins
- Added filter for product overview order
- Added tax calculation for digital products when using the cash on delivery gateway
- Added filters for link attributes in checkbox labels during second checkout
- Added filters for disclaimer labels during checkout
- Added coupons to second checkout
- Updated WooCommerce templates
- Updated translation
- Removed doubled requirements tab in product settings
- Removed internal item meta fields admin order panel
- Removed filters `wgm_checkout_revocation_checkbox_text` and `wgm_checkout_revocation_digital_checkbox_text` in favor of new filters `wgm_checkout_revocation_label_text` and `wgm_checkout_revocation_digital_label_text`

= 2.4.13 =
- Moved “Next” button on checkout page below terms rows
- Updated cart templates
- Fixed a bug where old orders coundn’t be displayed or accessed in the back-end
- Removed duplicated button and shipping calculator on the cart page

= 2.4.12 =

- Improved the MarketPress update feature to be able broadcast an update notice to the WordPress backend.
- Fixed button text during checkout when PayPal was selected as payment gateway

= 2.4.11 =

- Fixed a visual bug with duplicated items in cart
- Removed CSS class `second-checkout-button-container` in favor of `wc-proceed-to-checkout wgm-proceed-to-checkout`
- Removed no longer needed CSS declarations in frontend.css
- Removed no longer needed JS function call (left function in place for legacy)
- Updated button CSS classes to match default WooCommerce CSS classes
- Updated cart-totals.php template to make use of `wc_cart_totals_coupon_label()` and `wc_cart_totals_taxes_total_html()`
- Added body class `woocommerce` to be applied to second checkout page in order to match default WC button styles
- Added CSS classes `wgm-place-order`, `wgm-go-back-button` and `checkout-button` to second-checkout2.php template

= 2.4.10 =
- WGM now uses the Woocommerce Geolocate feature to determine the displayed tax rates if the option is enabled
- Added "Cash on delivery" for virtual products to the cash on delivery gateway
- Updated Autoupdater
- Changed from WC_Customer to WC_Order to retrive customer location in orders
- Removed selectable shipping methods in second checkout and only display chonsen one
- Added second checkout to woocommerce exclude from caching transient
- Removed shipping methods from checkout when the cart only contains digital products.
- Updated translation
- Fixed typos

= 2.4.9 =
- Fixed deprecated function calls from cart->tax to WC_Tax
- Fixed a typo in WGM_Settings
- Removed old and no longer needed review-order.php template

= 2.4.8 =
 - Fixed an issue with the Amazon Payments gateway
 - Fixed an issue where tax was worngly calculated when using a coupon
 - Fixed an issue where shipping costs for variable products in small business regulation shops would not be displayed
 - Fixed an issue where with excluding tax option active the price would not be displayed
 - Fixed an issue where taxes would be worngly calculated when a reduced digital products was in cart
 - Fixed an issue where no further text could be inserted before or after the woocommerce_de_check shortcode
 - Fixed a division by zero
 - Fixed typos
 - Changed button text on first checkout page when using paypal to "next" from "pay with paypal"
 - Added one decimal place to tax rate display
 - Updated translation
 - Added a wraping div to mails

= 2.4.7 =
- added new filters `wgm_get_split_tax_html` and `wgm_get_excl_incl_tax_string`
- improved support for coupons during split tax calculations for shipping costs and fees
- fixed a bug during in cart view where shipping costs would display net values before switching to gross values when checked
- fixed a bug during in cart view where taxes for shipping costs would be duplicated when added to total amount
- fixed a bug during order editing in the back-end where WGM’s calculations for split taxes on shipping costs and fees would not be applied

= 2.4.6 =
- added a missing argument to WGM_Template::checkout_readonly_field()
- added displaying of fee taxes for COD on thank-you page
- added legal notice for store owners to text templates
- improved formating of text templates, fixed corrupted HTML tag
- improved tax calculation for shipping fees and COD
- fixed a PHP notice at order-pay endpoint
- fixed a bug that resulted in zero values for taxes in invoice e-mails
- fixed obsolete displaying of fee taxes when small business option was enabled
- fixed a bug that resulted in falsified tax calculations for checkout totals
- fixed a bug that resulted in missing tax columns for orders in the back-end

= 2.4.5 =
- Fixed an fatal error when sending invoices from the backend
- Fixed wrong tax display for cash on delivery
- Fixed undefined offset notice for additional services tax calculation

= 2.4.4 =
- fixed wrong use of woo translation string
- fixed cod gateway and fee display
- show variation price in oderlisting
- added paypal return URL fix
- added new calculation for shipping and COD costs
- removed unnecessary expression
- replaced all , with . for wc_price in price per unit output
- updated translation

= 2.4.3 =
- Fixed notice in cart
- Fixed wording and punctuation
- Added Compability for Woo 2.2
- Removed strict warnings
- Updated updater
- Removed notice in mail
- Added target _blank to disclaimer line links

= 2.4.2 =
- Added filter for checkout checbox lables and texts
- Remmoved double shipping cost notice for variations with the same price
- Fixed digital notice in outgoing mails and paypal

= 2.4.1 =
- Added a Filter for HTML digital notice and the digital keyword itself respectively
- Added prerequists for virtual variable products
- Added missing CSS class prefix
- Replaced old "woocommerce_order_item_title" with new hook
- Changed COD Gateway to use the Woocommerce fees api
- Fixed worngly used hook "woocommerce_order_actions" to "woocommerce_order_actions_start"
- Fixed digital notice HTML output for all completed order tasks (e.g. Paypal payments etc)
- Fixed doubled displayed shipping costs notice and missing tax notice on variable products
- Fixed various typos
- Fixed some internal filters
- Fixed an issue where the updater would show an update notice for an old version of the plugin
- Fixed an Issue where the wrong cancelation policy notice would be shown at checkout
- Fixed an display issue with virtual product prerequists
- Fixed multiple display of shipping costs on product page
- Fixed estimate cart option
- Fixed an undefined variable error
- Fixed strict standard notices
- Fixed missing digital notice in cart widget
- Fixed various php notices
- Fixed diliverytime display with virutal products
- Fixed false prositive digital products in cart
- Fixed dislay for digital variation prerequists in checkout
- Fixed supress shipping product option
- Cleand up old code
- Replaced old deprecated functions with new ones
- Updated translation

= 2.4 =
- made software compliant with new german online sales rights (13.6.2014)
- added new text templates
- updated old text templates
- upadted translation
- removed some options
- updated mail
- added new product field for requirements for digital products
- tax and delivery notice is now mandatory
- new customer acknowledgement for digital products
- some cleanup

= 2.3.6 =
- info release, no fixes or features

= 2.3.5 =
- added a body class for second checkout
- fixed doubled ids in second checkout
- added css class for place order button in second checkout
- minor cleanups
- fixed wrongly displayed deliveryadress in second checkout
- translated licence key messgae
- minor html fixes

= 2.3.4 =
- fixed an serious javascript issue

= 2.3.3 =
- fixed various issues with WPML and WGM ajax calls

= 2.3.2 =
- Added support for WPML
- Added compability for WooCommerce 2.1.6
- added css class for variation price infos
- added various filter and actions for manipulating wgm's markup output
- added css class for loding animation
- fixed variation price display with identical prices
- fixed various typos
- fixed various translations
- fixed broken markup
- hide default price on variation single product page when variation price is loaded
- fixed static asset loading
- fixed free shipping label display to use user entered instead of default label
- fixed tax display with empty values in conjunction with small business regulation
- fixed doulbe tax loading with variation products
- fixed cash on delivery display on certain pages
- fixed missing row in cart
- fixed various issues with coupons
- fixed a typo in a setting id
- fixed various issues with checkout validation
- fixed wgm's default tax rates
- fixed missing shortcode in english installations
- updated documentation
- fixed wgm cash on delivery gateway
- added html markup to checkout readonly fields
- removed default woo tax notices
- added english translation for tax rate import

= 2.3.1 =
- fixed variation frontend javascript
- added plugin version function
- fixed body classes
- updated woocommerce templates
- fixed typo in cart template
- updated deprecrated parameter in checkout template
- fixed filter for WGM_Template::add_mwst_rate_to_product_item
- fixed typos ins translation
- fixed price per unit currency smybol usage
- fixed wrong option for price per unit
- changed additional vat string for better context
- fixed tax display for shipping with no costs
- various minor compatibility fixes

= 2.3 =
- updated hooks and functions for WooCommerce 2.1
- added filter for unexpected extra EU costs
- added filter for product short desc
- added filter for payment method in mails
- added filter for disclaimer
- added filter for small business regulation text
- added filter for small business regulation review text
- added filter for for after cart totals
- fixed typos
- implemented new template loadding system (templates can now be overwritten like woo templates)
- fixed check if woocommerce is active
- cleaned up css
- updated woocommerce templates
- fixed shipping calculation in second checkout
- fixed some installation errors
- added option for dual shipping methods
- fixed a warning in checkout with some particular vat options
- adjusted html output
- fixed an issue with taxes in product variations
- fixed some minor stability issues
- fixed some translation errors

= 2.2.6 =
- Added warning message

= 2.2.5 =
- Added Filter for small buissiness regulation texts
- Remove hardcoded 'approx' from delivery times
- Removed no deliverytime text field
- Added filter for default deliverytimes
- Extends Checkout SSL to all WGM Checkout sites
- Removed Deliverytimes from quickedit
- Changed deliverytimes from indecies to term id
- fixed price per unit display
- updated translation
- fixed some woocommrece incompabilities
- updated settings desctiptions
- some minor display fixes
- fixed some PHP5.4 warnings
- fixed some issues with wrong delivery times
- fixed an error which would not display price per unit attributs in dropdown menus
- fixed a missing currency symbol in mails
- updated translation
- fixed an superfluous whitespace in small bussiness regulations text filter
- removed wrong post count form deliverytime editor
- moved price per unit display under actual price
- added price per unit filter and before and after actions

= 2.2.4 =
- tax does not anymore depend on country when sending a mail manually through the backend

= 2.2.3 =
- fixed a bug where delivery times would not be set correctly after update
- product short desc on second checkout now hides properly
- fixed translation
- replaced a depricated jquery function .live with .on
- fixed deliverytimes quick edit
- fixed strict error message
- fixed wrongly stacked html
- tax rates are matching the country you selected on checkout
- removed debug output
- added css class for product option panales
- "approx" deos not display before "not available at the moment"
- fixed wrong tax calculation when using a coupon

= 2.2.1 =
- fixed wrong delivery times after upgrade
- fixed translation
- removed deliverytimes from quickedit

= 2.2 =
- Added small business regulation according to §19 UStG
- Fixed a bug where VAT didn't show up in cart
- Added compability for new woocommerce releases
- added dliverytime editor
- fixed some style issues
- fixed plugin activation notice
- removed style recommendation box on settings page
- CSS and JS only now only loading when needed
- fixed colspan in some tables
- fixed free shipping when free shipping amount is reached
- updated deliverytime wording
- updated leagal templates
- updated translation
- fixed wrongly rounded VAT displays
- fixed some issues with WPML
- fixed mails to not allways include legal stuff in the footer
- VAT can now be entered with . or ,
- fixed VAT percent displayed
- fixed typos
- fixed some minor display issues
- added support for a wider range of themes
- fixed a warning occurring when doing a search

= 2.1.6 =
- sperated price per unit html from data
- added some html elements for styling to second checkout
- updated updater

= 2.1.5 =
- templates clean up
- added function Woocommerce_German_Market::is_wgm_checkout() to check if the called site is the second checkout page
- fixed some minor issues
- moved inline css to css file
- fixed some css issues

= 2.1.4 =
- Fixed VAT Display in cart

= 2.1.3 =
- Added filter into cart template
- Fixed sorting order of delivery time terms

= 2.1.2 =
- changed array_map function for translated terms into static function

= 2.1.1 =
- Fixed fatal error in PHP 5.2

= 2.1 =
- Fixed a conflict with WPML
- Added product short descriptions to the product in checkout and order listing
- Fixed an issue where the VAT dind't display properly
- Fixed a conflict with Role Based Prices
- Added an option to display a hint text for extra costs when shipping in non eu contries
- Shippingcosts no longer displayed when the limit for free shipping is reached
- Removed activation.css from Frontend
- Added option for 'Free Shipping'
- Show terms in default selection
- Fixed various translation issues
- Added a Deliverytime Editor
- Fixed some typos
- Fixed various display issues
- Fixed various behaviour issues
- Fixed various javascript issues

= 2.0.2 =
- Fixed array to string conversion
- Added filter for colspan in cart
- Removed superfluous td tag
- added filter for additional tax strings
- changed prices tag method in additional tax display
- Fixed COD Payment gateway

= 2.0.1 =
- Fixed array to string conversion
- Fixed js payment gateway selection
- Fixed bodyclasses for new checkoutpages

= 2.0 =
- Added Support for Woocommerce 2.0+
- Refactoring, splitted codebase into seperate classes
- Removed no longer needed functions
- Added documentation to all methods
- Added various hooks for thrid party developers
- Added Class Autoloaded
- Changed Textdomain loading
- Updated Templates
- Improved Template loading
- Improved Cash on delivery Gateway
- Improved second Checkout page
- Fixed verious loading issues
- Replaced Emailtemplates with a hook
- Updated Translation
- Updated all woocommerce API calls
- Fixed various issues regarding the english version
- Fixed an issue where the terms page was not assined correctly
- Fixed an issue where the error notice when woocommerce is not installed didn't disapear after a reload
- Fixed an issue with the shipping costs
- Fixed an issue where the revoction page had no content
- Fixed an issue where the tax didn't got correctly assigned by the installtion routine
- Fixed an issue where taxes dindt got displayed properly
- Fixes various display issues
- Fixed an issue where paged coulnd't be saved in the en_US version
- Fixed an issue where the plugin didnt deactivated it self when the wrong version of woocommerce was instlled
- Fixed various corrupted or broken options
- Fixed price per unit handling and display
- Fixed various typos
- Fixed various minor things

= 1.1.4 =
- JS only listens on radio buttons on first checkout page
- All numbers are now formatted properly
- Exclusive tax in cart are now displayed properly
- Removed tax from items order table for cleaner view
- Custom delivery strings now obey the "delivery time in overview" option
- Fixed variation prices in mails

= 1.1.3 =
- Fixed PHP 5.4 Warnings
- Checkout Page dropdown now gets dispayed and saved into the database
- Fixed Shipping text position
- Added a new distinctive css class for the buy button
- Added a hint fild in the options which gets displayed on the last checkout page
- Some CSS fixes
- Some rearangements on the last checkout page
- Updated translation

= 1.1.2 =
- Added English localization
- Added missing english translations
- Conditions can now be turned off
- Updated depricated functions
- Add fresh styled Setup-Message
- Add english versions of page templates like imprint, terms, Method of payment ...
- Support english for sample page localizazion and Messages
- Updated Mailtemplates
- Fixed wrong Shippingcost calculations
- Notices displayed on the cart page can now be turned off
- Fixed Spelling mistakes
- Fixed some display errors on the checkout page

= 1.1.1 =
- Fixed several issues in autoupdater
- Fixed issue with facebook plugins
- separate wording for "Widerruf" checkbox
- delivery time: rename "day" to "business day"
- new template loading mechanism
- new default mail templates
- enhanced cart
- changed some translations
- fixed numberformarts in various areas
- some internal changes
- taxes now displayed properly on every listing
- various typo fixes

= 1.1 =
- placed next button in the right order
- fixed baseprice display
- fixed tax display
- fixed autoupdater

= 1.0.10 =
- include shipping in second checkout
- extended price per unit meassure
- new custom review order template with taxes
- license code dosn't show up in frontend anymore when defined in the config
- added filter for second checkout buy button text
- fixed mail bug
- fixed notice

= 1.0.9 =
- Terms page has now the correct content
- Cash on delivery is now properly displayed in mails
- delivery cost link is now longer splitted into 2 parts

= 1.0.8 =
- New option for displaying delivery costs seperatly in product overview and detailview
- Removed install notice if WGM was previously installed

= 1.0.7 =
- CSS Fixes
- TOS and Widerrufsbelehrung on first checkout are now links only
- Deliverytime and Shipping costs are now displayed properly
- Removed no longer needed files
- Refactored roduct display
- Base prices are now properly displayed in the product overview
- Autoupdater: use site_option instead of get_option
- Move shippingcosts and tax rate closer to pricetag

= 1.0.6 =
- Updated Auto Updater (pro-only)

= 1.0.5 =
- Moved Paymentmethod above the product list on the checkout page

= 1.0.4 =
- Fixed update process

= 1.0.3 =
- Fixed delivery adress on checkout page
- price incl tax on single product page is now unter the title
- Fixed bug which caused problems for the plugin update mechanism
- small changes on the checkout page for the "Button-Lösung"
- added changelog

= 1.0.2 =
- Changes for the "Button-Lösung"
- Short description under the product on the checkout page
- various CSS fixes
- spelling fixes

= 1.0.1 =
- compatibility changes for Woocommerce 1.6.1
- localization
- various CSS fixes

= 1.0  =
- Inital Release
