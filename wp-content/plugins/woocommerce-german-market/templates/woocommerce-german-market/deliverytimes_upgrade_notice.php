<div class="updated woocommerce-de-message warning">
    <div class="logo"></div>
	<h4><?php _e( 'Heads up! WooCommerce German Market will no longer auto-prefix delivery times.', 'woocommerce-german-market' ); ?></h4>

	<h5>
        <?php
            printf(
            	__( 'In prior versions every delivery time entry automatically got prepended with the addition “approx.”. That automation is no longer in effect. To keep your store interface legally compliant, please add the prefix via the <a href="%s">Delivery Times</a> editor.', 'woocommerce-german-market' ),
				admin_url( 'edit-tags.php?taxonomy=product_delivery_times&post_type=product' )
            );
        ?>
    </h5>

	<br class="clear" />
	<br/>

	<form action="<?php admin_url( 'admin.php' ); ?>" method="post">
		<input type="submit" class="button" name="woocommerce_de_upgrade_deliverytimes" value="<?php __( 'Dismiss this notice.' ); ?>" />
	</form>

	</form>
</div>
