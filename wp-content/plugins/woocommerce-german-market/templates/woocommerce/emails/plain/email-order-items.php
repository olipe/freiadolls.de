<?php
/**
 * Email Order Items (plain)
 *
 * @author		WooThemes
 * @package		WooCommerce/Templates/Emails/Plain
 * @version		2.1.2
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $woocommerce;

foreach ( $items as $item_id => $item ) :
	$_product     = apply_filters( 'woocommerce_order_item_product', $order->get_product_from_item( $item ), $item );

	if ( apply_filters( 'woocommerce_order_item_visible', true, $item ) ) {

		// Title
		echo apply_filters( 'woocommerce_order_item_name', $item->get_name(), $item, false );
		do_action( 'wgm_email_after_item_name', $item );

		// SKU
		if ( $show_sku && $_product->get_sku() ) {
			echo ' (#' . $_product->get_sku() . ')';
		}

		do_action( 'woocommerce_order_item_meta_start', $item_id, $item, $order, $plain_text );
		
		// Variation
		echo strip_tags( wc_display_item_meta( $item, array(
			'before'    => "\n- ",
			'separator' => "\n- ",
			'after'     => "",
			'echo'      => false,
			'autop'     => false,
		) ) );

		// Quantity
		echo "\n" . sprintf( __( 'Quantity: %s', 'woocommerce' ), apply_filters( 'woocommerce_email_order_item_quantity', $item['qty'], $item ) );

		// Cost
		echo "\n" . sprintf( __( 'Cost: %s', 'woocommerce-german-market' ), $order->get_formatted_line_subtotal( $item ) );

		// Download URLs
		if ( $show_download_links && $_product->exists() && $_product->is_downloadable() ) {
			$download_files = $order->get_item_downloads( $item );
			$i              = 0;

			foreach ( $download_files as $download_id => $file ) {
				$i++;

				if ( count( $download_files ) > 1 ) {
					$prefix = sprintf( __( 'Download %d', 'woocommerce-german-market' ), $i );
				} elseif ( $i == 1 ) {
					$prefix = __( 'Download', 'woocommerce-german-market' );
				}

				echo "\n" . $prefix . '(' . esc_html( $file['name'] ) . '): ' . esc_url( $file['download_url'] );
			}
		}

		do_action( 'woocommerce_order_item_meta_end', $item_id, $item, $order ,$plain_text );

	}
	// Note
	if ( $show_purchase_note && $purchase_note = $_product->get_purchase_note() ) {
		echo "\n" . $purchase_note;
	}

	echo "\n\n";

endforeach;
