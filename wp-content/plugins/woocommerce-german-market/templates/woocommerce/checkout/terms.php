<?php
/**
 * Checkout terms and conditions checkbox
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.5.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


if ( get_option( 'woocommerce_de_secondcheckout', 'off' ) == 'off' ) {
	$current_action = current_filter();
	if ( $current_action != apply_filters( 'gm_checkout_terms_filter_name_return_check', 'woocommerce_de_add_review_order' ) ) {
		return;
	}
}

$terms_and_condition_text_default = __( 'I have read and accept the [link]terms & conditions[/link]', 'woocommerce-german-market' );
$terms_and_condition_text = get_option( 'woocommerce_de_checkbox_text_terms_and_conditions', $terms_and_condition_text_default );
$terms_and_condition_text = str_replace( '[link]', '<a href="%s" target="_blank">', $terms_and_condition_text );
$terms_and_condition_text = str_replace( '[/link]', '</a>', $terms_and_condition_text );

if ( wc_get_page_id( 'terms' ) > 0 && apply_filters( 'woocommerce_checkout_show_terms', true ) ) : ?>
	<?php do_action( 'woocommerce_checkout_before_terms_and_conditions' ); ?>
	<p class="form-row terms wc-terms-and-conditions">
		<input type="checkbox" class="input-checkbox" name="terms" <?php checked( apply_filters( 'woocommerce_terms_is_checked_default', isset( $_POST['terms'] ) ), true ); ?> id="terms" />
		<label for="terms" class="checkbox"><?php printf( $terms_and_condition_text, esc_url( wc_get_page_permalink( 'terms' ) ) ); ?> <span class="required">*</span></label>
		<input type="hidden" name="terms-field" value="1" />
	</p>
	<?php do_action( 'woocommerce_checkout_after_terms_and_conditions' ); ?>
<?php endif; ?>
