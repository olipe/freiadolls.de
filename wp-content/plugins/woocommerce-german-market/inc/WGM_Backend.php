<?php
/**
 * Backend Functions
 *
 * @author MarketPress
 * @since WGM 3.0
 */
Class WGM_Backend {


	/**
	 * Add time to shop-order column 'Date'
	 *
	 * @access public
	 * @static
	 * @wphook post_date_column_time
	 * @param String $h_time
	 * @param WP_Post $post
	 * @return void
	 */
	public static function post_date_column_time( $column ) {

		global $post, $the_order;

		if ( empty( $the_order ) || $the_order->get_id() !== $post->ID ) {
			$the_order = wc_get_order( $post->ID );
		}

		switch ( $column ) {
			
			case 'order_date' :

			// add a filter and return false if you don't want to show the time
			if ( apply_filters( 'woocommerce_de_post_date_column_time_add_time', true ) ) {
				echo apply_filters( 'woocommerce_de_post_date_column_time_wc_3', ' ' . $the_order->get_date_created()->format( 'H:i' ) );
			}

			break;

		}

	}

	/**
	 * Add support link to plugin row meta
	 *
	 * @access public
	 * @static
	 * @wphook plugin_row_meta
	 * @param Array $links
	 * @param String $file
	 * @return void
	 */
	public static function plugin_row_meta( $links, $file ) {

		if ( strpos( $file, 'WooCommerce-German-Market.php' ) !== false ) {
			
			$links[ 'documentation' ] 	= sprintf( '<a href="%s" target="_blank">%s</a>', 'https://marketpress.de/documentation/german-market/', __( 'Documentation', 'woocommerce-german-market' ) );
			$links[ 'videos' ] 			= sprintf( '<a href="%s" target="_blank">%s</a>', 'https://www.youtube.com/playlist?list=PLnf1BSfzpccGlCI6bQdjfbQE42CoKACe7/', __( 'Tutorial videos', 'woocommerce-german-market' ) );
			
			$locale = get_locale();
			$support_link = str_replace( 'de', '', $locale ) != $locale ? 'https://marketpress.de/hilfe/' : 'https://marketpress.com/help/';
			$links[ 'support' ] = sprintf( '<a href="%s" target="_blank">%s</a>', $support_link, __( 'Support', 'woocommerce-german-market' ) );

		}
	
		return $links;
	}

}
