<?php

/**
 * Class with Template Snippet functions, Template Helper Functions
 * Output filtering Funktions of WooCommerce hooks
 *
 * @author jj, ap
 */
class WGM_Template {

	static protected $button_html;

	/**
	 * Overloading Woocommerce with German Market templates
	 * @param string $template
	 * @param string $template_name
	 * @param string $template_path
	 * @access public
	 * @static
	 * @author ap
	 * @return string the template
	 */
	public static function add_woocommerce_de_templates( $template, $template_name, $template_path ){

		$path = dirname( __FILE__ ) . DIRECTORY_SEPARATOR . '../templates' . DIRECTORY_SEPARATOR . '/woocommerce' . DIRECTORY_SEPARATOR;
		$orignal_template = $template;

		// Only load our templates if they are nonexistent in the theme
		if( file_exists( $path . $template_name ) && ! locate_template( array( WC()->template_path() . $template_name ) ) ) {
			$template = $path . $template_name;
		}

		if ( $template_name == 'checkout/form-checkout.php' && get_option( 'gm_force_checkout_template', 'off' ) == 'on' ) {
			$template = $path . $template_name;
		}

		if ( $template_name == 'checkout/terms.php' && get_option( 'gm_force_checkout_template', 'off' ) == 'on' ) {
			$template = $path . $template_name;
		}

		// cart/cart.php template
		if ( $template_name == 'cart/cart.php' ) {
			
			if ( locate_template( array( WC()->template_path() . $template_name ) ) ) {
				
				// template exists in theme
				
				if ( apply_filters( 'gm_cart_template_in_theme_show_taxes', true ) ) {

					// add filter to show taxes in cart in theme template
					if ( ! has_filter( 'woocommerce_cart_item_subtotal', array( 'WGM_Template', 'add_mwst_rate_to_product_item' ) ) ) {
						add_filter( 'woocommerce_cart_item_subtotal', array( __CLASS__, 'show_taxes_in_cart_theme_template' ), 10, 3 );
					}

				} else if ( apply_filters( 'gm_cart_template_use_gm_template', false ) ) {

					// use german market template
					$template = $path . $template_name;

				}
			
			}

			if ( apply_filters( 'gm_cart_template_force_woocommerce_template', false ) ) {
				return $orignal_template;
			}

		}

		return $template;
	}

	/**
	* Add Taxes in cart/cart.php if not german market template is used
	*
	* @wp-hook woocommerce_cart_item_subtotal
	* @param String $subtotal
	* @param Array $cart_item
	* @param String $cart_item_key
	* @return String
	**/
	public static function show_taxes_in_cart_theme_template( $subtotal, $cart_item, $cart_item_key ) {

		$_product     	= apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );

		if ( ! $_product->is_taxable() ){
			return $subtotal;
		}

		$_tax = new WC_Tax();

		if( ! is_object( WC()->customer ) ){
			$order = wc_get_order( get_the_ID() );
			$addr = $order->get_address();
			$country = $addr[ 'country' ];
			$state = $addr[ 'state' ];

		} else {
			list( $country, $state, $postcode, $city ) = WC()->customer->get_taxable_address();
		}

		$t = $_tax->find_rates( array(
			'country' 	=>  $country,
			'state' 	=> $state,
			'tax_class' => $_product->get_tax_class()
		) );

		// Setup.
		$tax                = array_shift( $t );
		$tax_display        = get_option('woocommerce_tax_display_cart');
		$tax_label          = apply_filters( 'wgm_translate_tax_label', $tax[ 'label' ] );
		$tax_amount         = wc_price( $cart_item[ 'line_subtotal_tax' ] );
		$tax_decimals       = WGM_Helper::get_decimal_length( $tax[ 'rate' ] );
		$tax_rate_formatted = number_format_i18n( (float)$tax[ 'rate' ], $tax_decimals );

		$tax_string = WGM_Tax::get_excl_incl_tax_string( $tax_label, $tax_display, $tax_rate_formatted, $tax_amount );


		return apply_filters( 'gm_cart_template_in_theme_show_taxes_markup', $subtotal . '<br class="wgm-break"/><span class="wgm-tax">' . $tax_string . '</span>', $subtotal, $tax_string );
	}

	/**
	 * Overloading WGM Teplates with templates form the theme if existent
	 * @param string $template_name  - tempalte name
	 * @param string $template_path  - path the templates in theme folder
	 * @param string $default_path  - path to templates in plugin folder
	 * @return mixed found template
	 * @author ap
	 * @since 2.3
	 */
	public static function locate_template( $template_name, $template_path = '', $default_path = '' ) {
		if ( !$template_path ) $template_path = 'woocommerce-german-market' . DIRECTORY_SEPARATOR;
		if ( !$default_path ) $default_path = untrailingslashit( Woocommerce_German_Market::$plugin_path ) . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . 'woocommerce-german-market' . DIRECTORY_SEPARATOR;

		$template = locate_template(
			array(
				trailingslashit( $template_path ) . $template_name,
				$template_name
			)
		);

		if ( ! $template )
			$template = $default_path . $template_name;

		return apply_filters('wgm_locate_template', $template, $template_name, $template_path);
	}

	/**
	 * @param string $template_name template name
	 * @param array $args variables for template scope
	 * @author ap
	 * @since 2.3
	 */
	public static function load_template( $template_name, array $args = array() ) {
		$tmpl = WGM_Template::locate_template( $template_name );

		extract( $args );
		include $tmpl;
	}

	public static function add_mwst_rate_to_product_item_init(){
		add_filter( 'woocommerce_cart_item_subtotal', array( 'WGM_Template', 'add_mwst_rate_to_product_item' ), 10 ,3 );
	}

	/**
	 * adds german vat tax rate to every product
	 * @since 1.1.5beta
	 * @access public
	 * @hook woocommerce_checkout_item_subtotal
	 * @author ap
	 * @param float $amount
	 * @param array $item
	 * @param int $item_id
	 * @return string
	 */
	public static function add_mwst_rate_to_product_item( $amount, $item, $item_id ) {

		$_product = wc_get_product( $item['variation_id'] ? $item['variation_id'] : $item['product_id'] );

		if( ! $_product->is_taxable() ){
			return $amount;
		}

		$_tax = new WC_Tax();

		if( ! is_object( WC()->customer ) ){
			$order = wc_get_order( get_the_ID() );
			$addr = $order->get_address();
			$country = $addr[ 'country' ];
			$state = $addr[ 'state' ];

		} else {
			list( $country, $state, $postcode, $city ) = WC()->customer->get_taxable_address();
		}

		$t = $_tax->find_rates( array(
			'country' 	=>  $country,
			'state' 	=> $state,
			'tax_class' => $_product->get_tax_class()
		) );

		// Setup.
		$tax                = array_shift( $t );
		$tax_display        = get_option('woocommerce_tax_display_cart');
		$tax_label          = apply_filters( 'wgm_translate_tax_label', $tax[ 'label' ] );
		$tax_amount         = wc_price( $item[ 'line_subtotal_tax' ] );
		$tax_decimals       = WGM_Helper::get_decimal_length( $tax[ 'rate' ] );
		$tax_rate_formatted = number_format_i18n( (float)$tax[ 'rate' ], $tax_decimals );

		/**
		 * Filter legacy
		 */
		if ( has_filter( 'wgm_additional_tax_notice' ) || has_filter( 'wgm_format_vat_output' ) ) {
			$incl_excl = '';
			if( $tax_display == 'excl' || get_option('woocommerce_tax_display_cart', '' ) == '' ){
				$incl_excl = $tax[ 'label' ] . ' ' . __( 'to be added', 'woocommerce-german-market' );
			}
			elseif( $tax_display ){
				$incl_excl = $tax[ 'label' ] . ' ' . __( 'included', 'woocommerce-german-market' );
			}

			$decimal_length = WGM_Helper::get_decimal_length( $tax[ 'rate' ] );
			$formatted_rate = number_format_i18n( (float)$tax[ 'rate' ], $decimal_length );

			$tax_string = apply_filters(
				'wgm_format_vat_output',
				sprintf( '%1$s %2$s (%3$s%%)', $incl_excl, wc_price( $item[ 'line_subtotal_tax' ] ), $formatted_rate ),
				$item[ 'line_tax' ],
				$tax[ 'rate' ],
				$incl_excl
			);

			$item = sprintf(
				'%s <span class="product-tax"> %s </span>',
				$amount,
				$tax_string
			);

			$item = apply_filters(
				'wgm_additional_tax_notice',
				$item,
				$amount,
				$tax,
				$incl_excl
			);

		} else {

			/**
			 * v2.6+
			 */

			$tax_string = WGM_Tax::get_excl_incl_tax_string( $tax_label, $tax_display, $tax_rate_formatted, $tax_amount );

			$item = sprintf(
				'%s <span class="product-tax"> %s </span>',
				$amount,
				$tax_string
			);

			$item = apply_filters(
				'wgm_checkout_add_tax_to_product_item',
				$item,
				$amount,
				$tax,
				$tax_display,
				$tax_label,
				$tax_amount,
				$tax_decimals,
				$tax_rate_formatted
			);
		}

		/**
		 * Add tax line.
		 */
		return $item;
	}


	/**
	 * adds german mwst tax rate to every product in line in order-details.php
	 * @since	1.1.5beta
	 * @access	public
	 * @author ap
	 * @hook 	woocommerce_checkout_item_subtotal
	 * @param float $subtotal
	 * @param array $item
	 * @param WC_Abstract_Order $order_obj
	 * @return string
	 */
	public static function add_mwst_rate_to_product_order_item( $subtotal, $item, $order_obj ) {

		// Little hack for WGM_Email (see WGM_Email::email_de_footer)

		if ( ! defined( 'WGM_MAIL' ) ) {
			define( 'WGM_MAIL', TRUE );
		}

		$_product = $order_obj->get_product_from_item( $item );

		if ( empty( $_product ) || ! $_product->is_taxable() ) {
			return $subtotal;
		}

		// get tax
 		$tax = array(
 			'label' => '',
 			'rate' => 0.0
  		);

 		if ( ( $order_obj->get_line_tax( $item ) > 0.0 ) || ( $item->get_subtotal_tax() > 0.0 ) ) {
 			if ( isset( $item[ 'taxes' ] ) && ( ! empty( $item[ 'taxes' ] ) ) ) {
 				$taxes = $item[ 'taxes' ];
 				$first_tax_element = array_shift( $taxes );
	 			$rate_id = key( $first_tax_element );
	 			$t =  WC_Tax::_get_tax_rate( $rate_id );
	 			// convert to find rates format 
	 			$tax = array(
	 				'label' => $t[ 'tax_rate_name' ],
	 				'rate' => $t[ 'tax_rate' ]
				);
 			}
 			
		}

		$tax_sum = $item[ 'taxes' ][ 'subtotal' ];
		$incl_excl = '';

		/**
		 * Filter legacy
		 */
		if ( has_filter( 'wgm_additional_tax_notice' ) ) {

			if ( get_option( 'woocommerce_tax_display_cart' ) == 'excl'
			     || get_option( 'woocommerce_tax_display_cart', '' ) == ''
			) {
				$incl_excl = $tax[ 'label' ] . ' ' . __( 'to be added', 'woocommerce-german-market' );
			} elseif ( get_option( 'woocommerce_tax_display_cart' ) == 'incl' ) {
				$subtotal  = $item[ 'line_subtotal' ] + array_sum( $tax_sum );
				$subtotal  = wc_price( $subtotal );
				$incl_excl = $tax[ 'label' ] . ' ' . __( 'included', 'woocommerce-german-market' );
			}

			$decimal_length = WGM_Helper::get_decimal_length( $tax[ 'rate' ] );
			$formatted_rate = number_format_i18n( (float) $tax[ 'rate' ], $decimal_length );

			$tax_string = apply_filters(
					'wgm_format_vat_output',
					sprintf( '%1$s (%2$s%%)', wc_price( array_sum( $tax_sum ) ), $formatted_rate ),
					array_sum( $tax_sum ),
					$tax[ 'rate' ],
					$incl_excl
			);

			$template = '%s <span class="product-tax"> %s %s </span>';

			$item = sprintf( $template, $subtotal, $tax_string, $incl_excl );
			$item = apply_filters( 'wgm_additional_tax_notice', $item, $subtotal, $tax, $incl_excl );

		} else {

			/**
			 * v2.6+
			 */
			$amount             = wc_price( $subtotal );
			$tax_label          = apply_filters( 'wgm_translate_tax_label', $tax[ 'label' ] );
			$tax_amount         = wc_price( array_sum( $tax_sum ) );
			$tax_display        = get_option( 'woocommerce_tax_display_cart' );
			$tax_decimals       = WGM_Helper::get_decimal_length( $tax[ 'rate' ] );
			$tax_rate_formatted = number_format_i18n( (float) $tax[ 'rate' ], $tax_decimals );

			$tax_string = '';
			if ($tax[ 'rate' ] > 0) {
				 $tax_string = WGM_Tax::get_excl_incl_tax_string( $tax_label, $tax_display, $tax_rate_formatted, $tax_amount );
			} else {
				$tax_string = apply_filters( 'wgm_zero_tax_rate_message', '', 'product_tax_line' );
			} 

			$item = sprintf(
					'%s <span class="product-tax"> %s </span>',
					$subtotal,
					$tax_string
			);

			$item = apply_filters(
					'wgm_checkout_add_tax_to_order_product_item',
					$item,
					$subtotal,
					$tax,
					$tax_display,
					$tax_label,
					$tax_amount,
					$tax_decimals,
					$tax_rate_formatted
			);
		}

		return $item;
	}

	/**
	 * adds mwst to variation price
	 * @uthor jj, ap
	 * @access public
	 * @static
	 * @return void
	 */
	public static function add_mwst_rate_to_variation_product_price( $variation ) {

		ob_start();
		do_action( 'wgm_before_tax_display_variation' );
		WGM_Tax::text_including_tax( $variation );
		do_action( 'wgm_after_tax_display_variation' );
		$variation_tax = ob_get_clean();

		ob_start();
		if ( ! $variation->is_virtual() ) :
			do_action( 'wgm_before_variation_shipping_fee' );
			echo WGM_Shipping::shipping_page_link( $variation );
			do_action( 'wgm_after_variation_shipping_fee' );
		endif;
		$variation_shipping = ob_get_clean();

		$variation_ppu = WGM_Price_Per_Unit::get_price_per_unit_string( $variation );

		ob_start();
		WGM_Template::add_digital_product_prerequisits( $variation );
		$prerequists = ob_get_clean();

		$output_html = '<div class="wgm-single-variation-wrap">';
		$output_html .= $variation_tax . $variation_ppu . $variation_shipping . $prerequists;
		$output_html .= '</div>' . "\n";

		$output = apply_filters(
			'wgm_after_price_variation_single',
			$output_html,
			$variation_tax,
			$variation_ppu,
			$variation_shipping
		);

		echo $output;
	}

	/**
	* add filter for mwst rate to the label on cart
	*
	* @access public
	* @static
	* @return void
	* @uses add_filter
	*/
	public static function add_mwst_rate_to_cart_totals(){

		// add filter to enable rate at the mwst label
		add_filter( 'woocommerce_rate_label', array( 'WGM_Template', 'add_rate_to_label' ), 10, 2 );
	}


	/**
	* remove filter for mwst rate to the label on cart
	*
	* @access public
	* @static
	* @return void
	* @uses add_filter
	*/
	public static function remove_mwst_rate_from_cart_totals(){

		// remove filter from: this->add_mwst_rate_to_cart_totals
		remove_filter( 'woocommerce_rate_label', array( 'WGM_Template', 'add_rate_to_label' ), 10 );
	}


	/**
	 * Adds tax rate (percentage) to tax label (tax rate name) in cart and checkout.
	 *
	 * @wp-hook woocommerce_rate_label
	 *
	 * @param   string $rate_name
	 * @param   string $key
	 *
	 * @return  string $new_rate_name
	 */
	public static function add_rate_to_label( $rate_name, $key ) {
		global $wpdb;

		$sql    = "SELECT tax_rate FROM " .  $wpdb->prefix . "woocommerce_tax_rates WHERE tax_rate_id = %s";
		$query  = $wpdb->prepare( $sql, $key );
		$rate   = $wpdb->get_var( $query );

		$new_rate_name  = $rate_name;

		if ( ! empty( $rate ) ) {
			$decimal_length = WGM_Helper::get_decimal_length( $rate );
			$rate           = number_format_i18n( (float)$rate, $decimal_length );
			$new_rate_name  = sprintf( '%s (%s%%)', $new_rate_name, $rate );
		}

		return $new_rate_name;
	}



	/**
	* print tax hint after prices in loop
	*
	* @uses globals $product, remove_action
	* @access public
	* @hook woocommerce_after_shop_loop_item_title
	* @static
	* @author jj, ap
	* @return void
	*/
	public static function woocommerce_de_price_with_tax_hint_loop() {
		
		global $product;
		
		remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price' );
		
		if ( has_action( 'custom_german_market_woocommerce_de_price_with_tax_hint_loop' ) ) {
			do_action( 'custom_german_market_woocommerce_de_price_with_tax_hint_loop', $product );
		}

		if ( is_a( $product, 'WC_Product_Grouped' ) ) {
			add_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price' );
			return;
		}

		$classes = ' ' . apply_filters( 'wgm_loop_price_class', '' );
		
		if ( $product->get_price_html() ) : 
			
				// close </a> tag in loop before german market product summary, otherwise you will get <a> tag in <a> tag
				if ( apply_filters( 'wgm_close_a_tag_before_wgm_product_summary_in_loop', true ) ) {
					?></a><?php
				}
				
				do_action( 'wgm_before_wgm_product_summary_in_loop', $product );
					
				?>

				<p class="price <?php echo $classes; ?>">

				<?php

				echo self::get_wgm_product_summary();
				
				?></p><?php

				do_action( 'wgm_after_wgm_product_summary_in_loop', $product );

				?>

		<?php endif;

		
	}

	public static function get_wgm_product_summary( $product = NULL ) {

		if ( is_null( $product ) || ! ( $product ) ) {
			$product = wc_get_product();

			// WC 2.7 beta 2
			if ( ! $product ) {
				$product = wc_get_product( get_the_ID() );
			}

		}

		$single = is_single();
		$hook   = ( $single ) ? 'single' : 'loop';

		// for related products, use 'loop'
		global $woocommerce_loop;
		if ( isset( $woocommerce_loop[ 'name' ] ) && $woocommerce_loop[ 'name' ] == 'related') {
			$hook = 'loop';
		}

		$output_parts = array();
		$output_parts = apply_filters( 'wgm_product_summary_parts', $output_parts, $product, $hook );
		$output_parts = WGM_Shipping::add_shipping_part( $output_parts, $product ); // this class inits the filter with less parameters, so call it manually
		$output_html  = implode( $output_parts );

		//TODO: Remove the filter used in this method and the method itself. Deprecated as of 2.6.7
		$output_html = self::__deprecated_filter_after_price_output( $output_html, $hook, $output_parts );

		$output_html = apply_filters( 'wgm_product_summary_html', $output_html, $output_parts, $product, $hook );

		return $output_html;
	}

	public static function add_product_summary_price_part( $parts, WC_Product $product, $hook ) {
		//if ( $product instanceof WC_Product_Grouped ) {
		//	return;
		//}
		ob_start();

		do_action( 'wgm_before_' . $hook . '_price' );
		echo $product->get_price_html();
		do_action( 'wgm_after_' . $hook . '_price' );

		if ( $hook === 'single' && $product->get_type() != 'variable' ) {
			
			if ( apply_filters( 'gm_show_itemprop', false ) ) { ?>

				<meta itemprop="price" content="<?php echo esc_attr( $product->get_price() ); ?>" />
				<meta itemprop="priceCurrency" content="<?php echo esc_attr( get_woocommerce_currency() ); ?>" />
				<link itemprop="availability" href="http://schema.org/<?php echo $product->is_in_stock() ? 'InStock' : 'OutOfStock'; ?>" />

			<?php

			}
		}

		$parts[ 'price' ] = ob_get_clean();

		return $parts;
	}

	/**
	 * @param $html
	 * @param $hook
	 * @param $parts
	 *
	 * @deprecated as of v.2.6.7
	 * @return mixed|null|void
	 */
	public static function __deprecated_filter_after_price_output( $html, $hook, $parts ) {

		$price    = ( isset( $parts[ 'price' ] ) ) ? $parts[ 'price' ] : '';
		$tax      = ( isset( $parts[ 'tax' ] ) ) ? $parts[ 'tax' ] : '';
		$ppu      = ( isset( $parts[ 'ppu' ] ) ) ? $parts[ 'ppu' ] : '';
		$shipping = ( isset( $parts[ 'shipping' ] ) ) ? $parts[ 'shipping' ] : '';
		if ( has_filter( 'wgm_after_price_output_' . $hook ) ) {
			_doing_it_wrong( 'wgm_after_price_output_' . $hook,
			                 'This Filter is deprecated. Please use "wgm_product_summary_html" instead', 'WGM 2.6.7' );
		}

		return apply_filters( 'wgm_after_price_output_' . $hook, $html, $price, $tax,
		                      $ppu, $shipping );

	}



	/**
	*  print tax hint after prices in single
	*
	* @author jj, ap
	* @hook woocommerce_single_product_summary
	* @uses remove_action, get_post_meta, get_the_ID, get_woocommerce_currency_symbol
	* @access public
	* @static
	* @return void
	*/
	public static function woocommerce_de_price_with_tax_hint_single() {
		global $product;
		if ( $product instanceof WC_Product_Grouped ) {
			return;
		}
		remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price' );

		$classes = ' ' . apply_filters( 'wgm_single_price_class', '' );
		do_action( 'wgm_before_single_price' );
		?>
		
		
		<?php if ( apply_filters( 'gm_show_itemprop', false ) ) { ?>

				<div class="legacy-itemprop-offers" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
		
		<?php } else { ?>

				<div class="legacy-itemprop-offers">

		<?php } ?>

			<p class="price <?php echo $classes; ?>">

				<?php echo self::get_wgm_product_summary(); ?>

			</p>
		</div>

		<?php

		if ( apply_filters( 'gm_compatibility_is_variable_wgm_template', true, $product ) ) {
		
			if ( is_a( 'WC_Product_Variable', $product ) ) {
				WGM_Template::add_digital_product_prerequisits( $product );
			}
			
		}

	}
	

	/**
	 * Adds taxes and other German Market product data to grouped products view
	 *
	 * @wp-hook woocommerce_get_stock_html (since 3.5.1)
	 * @param String $html
	 * @param WC_Prodduct $product
	 * @return String
	 */
	public static function add_grouped_product_info( $html, $product ) {

		$tax = self::get_wgm_product_summary( $product );
		return $html . $tax;
	}

	/**
	 * Adds price filter to grouped product view.
	 *
	 */
	public static function init_grouped_product_adaptions() {

		remove_filter( 'wgm_product_summary_parts', array( 'WGM_Template', 'add_product_summary_price_part' ), 0 );

		add_filter( 'woocommerce_get_stock_html', array('WGM_Template','add_grouped_product_info'), 10, 2 );

	}
	/**
	 * Output the Shippingform
	 * @access public
	 * @static
	 * @author jj, ap
	 * @return string the Shipping Form
	 */
	public static function second_checkout_form_shipping() {

		$woocommerce_checkout = WC()->checkout();

		if( WGM_Session::get('ship_to_different_address', 'first_checkout_post_array') != '1' ) return;

		if ( WGM_Template::should_be_shipping_to_shippingadress() ) :

			echo '<h3>' . __( 'Shipping Address', 'woocommerce-german-market' ) . '</h3>';

			echo'<table class="review_order_shipping">';
			$hidden_fields = array();

			foreach ( $woocommerce_checkout->checkout_fields[ 'shipping' ] as $key => $field ) :
				$out = WGM_Template::checkout_readonly_field( $key, $field );
				if ( is_array( $out ) ) {
					echo $out[0];
					$hidden_fields[] = $out[1];
				}
			endforeach;

			echo'</table>';
		endif;
	}


	/**
	 * @access public
	 * @static
	 * @author ap
	 * @since 2.3.5
	 */
	public static function shipping_address_check(){
		WGM_Session::add('ship_to_different_address', WC()->checkout()->get_value( 'ship_to_different_address' ), 'first_checkout_post_array');
	}

	/**
	 * @access public
	 * @static
	 * @author ap
	 * @return bool
	 */
	public static function should_be_shipping_to_shippingadress() {

		global $woocommerce;

		if ( $woocommerce->cart->needs_shipping() && ! WGM_Helper::ship_to_billing() || get_option( 'woocommerce_require_shipping_address' ) == 'yes' ) {
			return TRUE;
		}

		return FALSE;
	}

	/**
	* Output the billing information form
	* @access public
	* @static
	* @author jj, ap
	* @return string The billing information
	*/
	public static function second_checkout_form_billing() {

		// Get checkout object
		$checkout = WC()->checkout();

		if ( WGM_Helper::ship_to_billing() ) {
			echo '<h3>'. __( 'Billing &amp; Shipping', 'woocommerce-german-market' ). '</h3>';
		} else {
			echo '<h3>'. __( 'Billing Address', 'woocommerce-german-market' ).'</h3>';
		}

		echo '<table class="review_order_billing">';
		$hidden_fields = array();

		// Billing Details
		foreach ( $checkout->checkout_fields[ 'billing' ]  as $key => $field ) {
			$out = WGM_Template::checkout_readonly_field( $key, $field );
			if ( is_array( $out ) ) {
				echo $out[ 0 ];
				$hidden_fields[] = $out[ 1 ];
			}
		}

		echo '</table>';

		// print the hidden fields
		echo implode( '', $hidden_fields );
	}

	/**
	* print hidden fields for given post array
	* determined by given field array
	*
	* @param array $post_array
	* @param array $fields_array
	* @static
	* @author jj
	* @return void
	*/
	public static function print_hidden_fields( $post_array, $fields_array ) {

		// Why does this function take 2 args when it is used only once and fed with
		// print_hidden_fields( $arr, array_keys( $arr ) ) ?
		// Why not take 1 array and iterate with foreach( $arr as $k => $v) ?
		foreach ( $fields_array as $field ) {
			if ( ! is_array( $post_array[ $field ] ) ) {
				echo '<input type="hidden" name="' . $field . '" value="' . $post_array[ $field ] . '" />';
			} else {
				self::array_to_input( $post_array[ $field ], $field );
			}
		}

	}

	/**
	 * create hidden input element for associative array
	 *
	 * @link https://gist.github.com/eric1234/5802030
	 *
	 * @param array  $array  associative array containing values
	 * @param string $prefix name attribute of input element
	 */

	public static function array_to_input($array, $prefix='') {
		if( (bool)count(array_filter(array_keys($array), 'is_string')) ) {
			foreach($array as $key => $value) {
				if( empty($prefix) ) {
					$name = $key;
				} else {
					$name = $prefix.'['.$key.']';
				}
				if( is_array($value) ) {
					self::array_to_input($value, $name);
				} else { ?>
					<input type="hidden" value="<?php echo $value ?>" name="<?php echo $name?>">
				<?php }
			}
		} else {
			$count=0;
			foreach($array as $item) {
				if( is_array($item) ) {
					self::array_to_input($item, $prefix.'['.$count.']');
				} else { ?>
					<input type="hidden" name="<?php echo $prefix ?>[]" value="<?php echo $item ?>">
				<?php }
				$count++;
			}
		}
	}


	/**
	* Change the button text, if on checkout page
	*
	* @param string $button_text
	* @static
	* @author jj, ap
	* @return string
	* @hook woocommerce_order_button_text
	*/
	public static function change_order_button_text( $button_text ) {

		// @todo do not touch button, when on pay for order page
		// @todo when refreshing payments,  session is expired, because cart is empty, see woocommerce-ajax.php

		// browser back button may not work, try this fix:
		$check_id = get_option( 'woocommerce_check_page_id' );
		if ( function_exists( 'icl_object_id' ) ) {
			$check_id = icl_object_id( $check_id );
		}
		$is_confirm_and_place_order_page = get_the_ID() == $check_id;

		if ( WGM_Session::is_set( 'woocommerce_de_in_first_checkout' ) || ( ! $is_confirm_and_place_order_page ) ) {
			
			if ( get_option( 'woocommerce_de_secondcheckout', 'off' ) == 'on' ) {
				$button_text = _x( 'Proceed', '1st checkout button text', 'woocommerce-german-market' );
			} else {
				$button_text = get_option( 'woocommerce_de_order_button_text', __( 'Place binding order', 'woocommerce-german-market' ) );
				$button_text = apply_filters( 'woocommerce_de_buy_button_text', $button_text );
 			}

		} else {

			$button_text = get_option( 'woocommerce_de_order_button_text', __( 'Place binding order', 'woocommerce-german-market' ) );
			$button_text = apply_filters( 'woocommerce_de_buy_button_text', $button_text );

		}

		return $button_text;
	}

	public static function add_cart_estimate_notice() {

		$cart_tax                 = WC()->cart->get_cart_tax();
		$show_estimate_disclaimer = ( get_option( 'woocommerce_de_estimate_cart', 'on' ) === 'on' );
		if ( $cart_tax && $show_estimate_disclaimer ) {
			?>
			<p class="wc-cart-shipping-notice">
				<small><?php

					if (
						WC()->customer->is_customer_outside_base()
						&& ! WC()->customer->has_calculated_shipping()
					) {

						$country_estimated_for = WC()->countries->estimated_for_prefix();
						// Textdomain "woocommerce" left intentionally, otherwise would have to translate country names.
						$country_estimated_for .= __( WC()->countries->countries[ WC()->countries->get_base_country() ],
						                              'woocommerce' );

						printf(
						/* translators: %s = assumed billing country */
							__( 'Note: Shipping and taxes are estimated (taxes estimated for %s) and will be updated during checkout based on your billing and shipping information.',
							    'woocommerce-german-market' ),
							$country_estimated_for
						);

					} else {

						_e( 'Note: Shipping and taxes are estimated and will be updated during checkout based on your billing and shipping information.',
						    'woocommerce-german-market' );

					}

					?></small>
			</p>
			<?php
		}

	}

	/**
	* Adds the Shipping Time to the Product title
	*
	* @param string $item_name
	* @param array $item
	* @static
	* @author jj, ap
	* @return string
	* @hook woocommerce_order_product_title
	*/
	public static function add_delivery_time_to_product_title( $item_name, $item ) {

		if ( ! isset( $item[ 'deliverytime' ] ) ) {

			// since v.3.1.2, don't return yet, so dilvery time will be also added to manually added items
			if ( apply_filters( 'add_delivery_time_to_product_title_no_deliverytime', false, $item ) ) { 
				return $item_name;
			}
			
		}

		if ( (int) $item[ 'variation_id' ] ) {
			$product = wc_get_product( $item[ 'variation_id' ] );
		} else {
			$product = wc_get_product( $item[ 'product_id' ] );
		}
		$shipping_time = self::get_deliverytime_string( $product );

		// @todo string fragments need to be brought into 1 whole string
		$shipping_time_output = ', ' . __( 'delivery time:', 'woocommerce-german-market' ) . ' ' . $shipping_time;
		$shipping_time_output = apply_filters( 'wgm_shipping_time_product_string', $shipping_time_output,
		                                       $shipping_time, $item );

		// if product is out of stock, don't show delivery time
		if ( ! self::show_delivery_time_if_product_is_not_in_stock( $product ) ) {
			$shipping_time_output = '';
		}

		if ( ! isset( $item[ 'price' ] ) ) {
			$price = ( $item[ 'line_subtotal' ] + $item[ 'line_subtotal_tax' ] ) / $item[ 'qty' ];
		} else {
			$price = $item[ 'price' ];
		}

		// 3rd party plugins that sets taxes to zero
		if ( ! ( $item[ 'line_subtotal_tax' ] > 0 ) ) {
			$price = ( $item[ 'line_subtotal' ] + $item[ 'line_subtotal_tax' ] ) / $item[ 'qty' ];
		}

		$each_string = __( 'each', 'woocommerce-german-market' ) . ' ' . wc_price( $price );
		if ( intval( $item[ 'qty' ] == 1 ) && apply_filters( 'gm_dont_show_each_string_if_qty_is_one', true ) ) {
			$each_string = '';
			$shipping_time_output = trim( substr( $shipping_time_output, 1 ) );
		}

		if ( get_option( 'woocommerce_de_show_delivery_time_order_summary', 'on' ) == 'on' ) {
			$return = $item_name . apply_filters( 'woocommerce_de_additional_item_string',  ' (' . $each_string . $shipping_time_output . ') ' );
		} else {
			if ( $each_string != '' ) {
				$return = $item_name . apply_filters( 'woocommerce_de_additional_item_string', ' (' . $each_string . ') ' );
			} else {
				$return = $item_name;
			}
		}

		// strip tags to avoid backend display errors, because return value is placed in title attributes and contains html markutp
		if ( is_admin() && function_exists( 'get_current_screen' ) ) {
			$screen = get_current_screen();
			if ( is_object( $screen )  && isset( $screen->id ) && ( $screen->id == 'edit-shop_order' ) ) {
				$return = strip_tags( $return );
			}
		}

		return apply_filters( 'woocommerce_de_add_delivery_time_to_product_title', $return, $item_name, $item );
	}

	/**
	 * Add product info to order item
	 * @param int $order_id
	 * @param int $item_id
	 * @param WC_Product $product
	 * @param int $qty
	 * @param array $args
	 *
	 * @return void
	 * @wp-hook woocommerce_order_add_product
	 * @author ap
	 * @since 2.4.13
	 */
	public static function add_deliverytime_to_order_item( $item_id, $item, $order_id ){
		
		if ( isset( $item[ 'variation_id' ] ) && intval( $item[ 'variation_id' ] ) > 0 ) {
			$id = $item[ 'variation_id' ];
		} else if ( isset( $item[ 'product_id' ] ) && intval( $item[ 'product_id' ] ) > 0 ) {
			$id = $item[ 'product_id' ];
		} else {
			return;
		}

		$product = wc_get_product( $id );

		$shipping = self::get_term_id_from_product_meta('_lieferzeit',$product);
		wc_add_order_item_meta( $item_id, '_deliverytime', $shipping );
		wc_add_order_item_meta( $item_id, '_price', $product->get_price() );
	}


	/**
	* add fax to billing fields
	*
	* @access public
	* @static
	* @author jj
	* @param array	 $billing_fields
	* @return array
	* @hook woocommerce_billing_fields
	*/
	public static function billing_fields( $billing_fields ) {

		$billing_fields[ 'billing_fax' ] = array(
			'type'=> 'phone',
			'name'=>'fax',
			'label' => __( 'Fax', 'woocommerce-german-market' ),
			'required' => false,
			'class' => array( 'form-row-last' )
		);

		return WGM_Template::set_field_type( $billing_fields, 'state', 'required' );
	}



	/**
	* add fax number to shipping fields
	* @access public
	* @static
	* @author jj
	* @param array $shipping_fields
	* @return array
	* @hook woocommerce_shipping_fields
	*/
	public static function shipping_fields( $shipping_fields ) {

		$shipping_fields[ 'shipping_fax' ] = array(
			'type'=> 'phone',
			'name'=>'fax',
			'label' => __( 'Fax', 'woocommerce-german-market' ),
			'required' => false,
			'class' => array( 'form-row-last' )
		);

		return WGM_Template::set_field_type( $shipping_fields, 'state', 'required' ) ;
	}


	/**
	* show the delivery time in overview
	* @access public
	* @static
	* @author ap
	* @return void
	* @return mixed
	* @hook woocommerce_after_shop_loop_item
	*/
	public static function woocommerce_de_after_shop_loop_item() {

		// if this function is defined in template, use it (like in the original woocommerce)
		if ( ! function_exists( 'woocommerce_de_after_shop_loop_item' ) ) {
			WGM_Template::add_template_loop_shop();
		} else {
			woocommerce_de_after_shop_loop_item();
		}
	}

	/**
	* Remove Checkbox for terms in checkout page (not pay for order page)
	* @access public
	* @static
	* @hook woocommerce_checkout_show_terms
	* @param Boolean $bool
	* @return Boolean
	*/
	public static function remove_terms_from_checkout_page( $bool ) {
		
		if ( ! is_wc_endpoint_url( 'order-pay' ) ) {
			$bool = false;
		}

		return $bool;
	}


	/**
	* Interupt checkout process after validation, to use the checkout site again, to fullfill
	* the german second checkout verify obligation
	* Since 3.0 this is only to redirect to second checkout page
	*
	* @access public
	* @static
	* @return void
	* @param array $posted $_POST array at hook position
	* @hook woocommerce_after_checkout_validation
	*/
	public static function do_de_checkout_after_validation ( $posted, $errors ) {

		if ( WGM_Session::is_set('woocommerce_de_in_first_checkout') ) {

			// validation for 1st checkout
			$error_count = 0;
			if ( isset ( $errors->errors ) ) {
				$error_count = count( $errors->errors );
			}

			$error_count = apply_filters( 'gm_checkout_validation_first_checkout', $error_count );

			if ( $error_count != 0 ) {
				return;
			}

			// reset woocommerce_de_in_first_checkout
			WGM_Session::remove('woocommerce_de_in_first_checkout');

			// save the $_POST variables into session, to save them during redirect
			$_POST[ 'order_comments' ] = htmlentities( stripcslashes( $_POST[ 'order_comments' ] ),
			                                           ENT_COMPAT | ENT_HTML401, 'UTF-8' );

			WGM_Session::add( 'first_checkout_post_array', $_POST );

			foreach ( $_POST as $field => $item ) {
				WGM_Session::add( $field, $item, 'first_checkout_post_array' );
			}

			if ( is_ajax() ) {
					echo json_encode( array(
								'result'   => 'success',
								'redirect' => WGM_Helper::get_check_url()
								) );
					exit;
			} else {
				wp_safe_redirect( WGM_Helper::get_check_url() );
				exit;
			}

		} else {
			
			// validation
			$has_virtual = false;
			$cart = WC()->cart->get_cart();
			$dcount = 0;
			foreach( $cart as $item ){

				$id_to_check_is_digital = empty( $item[ 'variation_id' ] ) ? $item['product_id'] : $item[ 'variation_id' ];

				if ( WGM_Helper::is_digital( $id_to_check_is_digital ) ) {
					$has_virtual = true;
					$dcount++;
				}

			}

			$only_digital = false;
			if( $dcount == count( $cart ) ) {
				$only_digital = true;
			}

			WGM_Session::add('only_digital', $only_digital, 'WGM_CHECKOUT');
			WGM_Session::add('has_digital', $has_virtual, 'WGM_CHECKOUT');

			$error_count = wc_notice_count();

			// check terms and conditions
			remove_filter( 'woocommerce_checkout_show_terms', array( 'WGM_Template', 'remove_terms_from_checkout_page' ) );
			if ( wc_get_page_id( 'terms' ) > 0 && apply_filters( 'woocommerce_checkout_show_terms', true ) ) {
				if ( ! isset( $_POST[ 'terms' ] ) ) {
					wc_add_notice( __( 'You must accept our Terms &amp; Conditions.', 'woocommerce' ), 'error' );
					if ( ! isset( $_REQUEST[ 'woocommerce_checkout_update_totals' ] ) ) {
						$error_count++;
					}
				}
			}
			add_filter( 'woocommerce_checkout_show_terms', array( 'WGM_Template', 'remove_terms_from_checkout_page' ) );

			// check widerruf
			if ( ! $only_digital ) {
				if ( !isset( $_POST[ 'woocommerce_checkout_update_totals' ] ) && empty( $_POST[ 'widerruf' ] ) && get_option( 'woocommerce_widerruf_page_id' ) > 0 && get_option( WGM_Helper::get_wgm_option( 'woocommerce_de_show_Widerrufsbelehrung' ) ) == 'on' ) {
					$notice_text = apply_filters( 'wgm_checkout_validation_revocation_notice', __( 'Please confirm the revocation policy.', 'woocommerce-german-market' ) );
					wc_add_notice( $notice_text, 'error' );
					$error_count ++;
				}
			}

			if ( $has_virtual && get_option( WGM_Helper::get_wgm_option( 'wgm_display_digital_revocation' ) ) != 'off'  ) {

				if ( !isset( $_POST[ 'woocommerce_checkout_update_totals' ] ) && empty( $_POST[ 'widerruf-digital' ] ) ) {
					$notice_text = apply_filters( 'wgm_checkout_validation_digital_revocation_notice', __( 'Please confirm the revocation policy for digital content.', 'woocommerce-german-market' ) );
					wc_add_notice( $notice_text, 'error' );
					$error_count ++;
				}

				if ( !isset( $_POST[ 'woocommerce_checkout_update_totals' ] ) && empty( $_POST[ 'widerruf-digital-acknowledgement' ] ) ) {
					$notice_text = apply_filters( 'wgm_checkout_validation_revocation_acknowledgement_notice', __( 'Please confirm the waiver for your rights of revocation regarding digital content.', 'woocommerce-german-market' ) );
					wc_add_notice( $notice_text, 'error' );
					$error_count ++;
				}
			}

			$error_count = apply_filters( 'gm_checkout_validation_fields_second_checkout', $error_count );

			if ( $error_count != 0 ) {

				// Redirect (stay) on second checkout page (confirm and place order)
				wp_safe_redirect( get_permalink( get_option( WGM_Helper::get_wgm_option( 'check' ) ) ) );
				exit();

			} else {
			
				if ( WGM_Session::is_set( 'first_checkout_post_array' ) ) {
					WGM_Session::remove( 'first_checkout_post_array' );
				}
			}
		}
	}

	/**
	* Checkout Validation without 2nd Checkout page
	*
	* @access public
	* @since GM v3.2
	* @static
	* @return void
	* @param array $posted $_POST array at hook position
	* @hook woocommerce_after_checkout_validation
	*/
	public static function checkout_after_validation_without_sec_checkout( $posted ) {

		// validation
		$has_virtual = false;
		$cart = WC()->cart->get_cart();
		$dcount = 0;
		foreach( $cart as $item ){

			$id_to_check_is_digital = empty( $item[ 'variation_id' ] ) ? $item['product_id'] : $item[ 'variation_id' ];

			if ( WGM_Helper::is_digital( $id_to_check_is_digital ) ) {
				$has_virtual = true;
				$dcount++;
			}

		}

		$only_digital = false;
		if( $dcount == count( $cart ) ) {
			$only_digital = true;
		}

		// check widerruf
		if( ! $only_digital ) {
			if ( !isset( $_POST[ 'woocommerce_checkout_update_totals' ] ) && empty( $_POST[ 'widerruf' ] ) && get_option( 'woocommerce_widerruf_page_id' ) > 0 && get_option( WGM_Helper::get_wgm_option( 'woocommerce_de_show_Widerrufsbelehrung' ) ) == 'on' ) {
				$notice_text = apply_filters( 'wgm_checkout_validation_revocation_notice', __( 'Please confirm the revocation policy.', 'woocommerce-german-market' ) );
				
				if ( apply_filters( 'gm_show_revocation_validation_withous_sec_checkout', true  ) ) {
					wc_add_notice( $notice_text, 'error' );
				}
				
			}
		}

		if( $has_virtual && get_option( WGM_Helper::get_wgm_option( 'wgm_display_digital_revocation' ) ) != 'off'  ) {

			if ( !isset( $_POST[ 'woocommerce_checkout_update_totals' ] ) && empty( $_POST[ 'widerruf-digital' ] ) ) {
				$notice_text = apply_filters( 'wgm_checkout_validation_digital_revocation_notice', __( 'Please confirm the revocation policy for digital content.', 'woocommerce-german-market' ) );
				wc_add_notice( $notice_text, 'error' );
			}

			if ( !isset( $_POST[ 'woocommerce_checkout_update_totals' ] ) && empty( $_POST[ 'widerruf-digital-acknowledgement' ] ) ) {
				$notice_text = apply_filters( 'wgm_checkout_validation_revocation_acknowledgement_notice', __( 'Please confirm the waiver for your rights of revocation regarding digital content.', 'woocommerce-german-market' ) );
				wc_add_notice( $notice_text, 'error' );
			}
		}

		$more_errors = apply_filters( 'gm_checkout_validation_fields', 0 );

	}

	/**
	* Validation for "pay order" page
	* Checks whether user agreed to revocation policy / revocation policy for digital products
	* See WooCommerce method WC_Form_Handler::pay_action
	* See WGM method WGM_Template::do_de_checkout_after_validation
	*	
	* @access public
	* @static
	* @return void
	* @hook wp
	*/
	public static function pay_order_validation_of_revocation_policy() {

		global $wp;

		if ( isset( $_POST['woocommerce_pay'] ) && isset( $_POST['_wpnonce'] ) && wp_verify_nonce( $_POST['_wpnonce'], 'woocommerce-pay' ) ) {

			ob_start();

			// Pay for existing order
			$order_key  = $_GET['key'];
			$order_id   = absint( $wp->query_vars['order-pay'] );
			$order      = wc_get_order( $order_id );
			$notices 	= 0;

			if ( $order->get_id() == $order_id && $order->get_order_key() == $order_key && $order->needs_payment() ) {

				// Preperation
				$has_virtual = false;
				$cart = $order->get_items();
				$dcount = 0;

				foreach ( $cart as $item ) {
					
					$id_to_check_is_digital = empty( $item[ 'variation_id' ] ) ? $item['product_id'] : $item[ 'variation_id' ];

					if ( WGM_Helper::is_digital( $id_to_check_is_digital ) ) {
						$has_virtual = true;
						$dcount++;
					}

				}

				$only_digital = false;
				if ( $dcount == count( $cart ) ) {
					$only_digital = true;
				}

				// Terms
				if ( ! empty( $_POST['terms-field'] ) && empty( $_POST['terms'] ) ) {
					$notices++;
				}

				if ( ! $only_digital ) {
					if ( empty( $_POST[ 'widerruf' ] ) && get_option( 'woocommerce_widerruf_page_id' ) > 0 && get_option( WGM_Helper::get_wgm_option( 'woocommerce_de_show_Widerrufsbelehrung' ) ) == 'on' ) {
						$notice_text = apply_filters( 'wgm_checkout_validation_revocation_notice', __( 'Please confirm the revocation policy.', 'woocommerce-german-market' ) );
						wc_add_notice( $notice_text, 'error' );
						$notices++;
					}
				}

				if ( $has_virtual && get_option( WGM_Helper::get_wgm_option( 'wgm_display_digital_revocation' ) ) != 'off'  ) {

					if ( empty( $_POST[ 'widerruf-digital' ] ) ) {
						$notice_text = apply_filters( 'wgm_checkout_validation_digital_revocation_notice', __( 'Please confirm the revocation policy for digital content.', 'woocommerce-german-market' ) );
						wc_add_notice( $notice_text, 'error' );
						$notices++;
					}

					if ( empty( $_POST[ 'widerruf-digital-acknowledgement' ] ) ) {
						$notice_text = apply_filters( 'wgm_checkout_validation_revocation_acknowledgement_notice', __( 'Please confirm the waiver for your rights of revocation regarding digital content.', 'woocommerce-german-market' ) );
						wc_add_notice( $notice_text, 'error' );
						$notices++;
					}
				}

			}

			if ( $notices > 0 ) {
				wp_safe_redirect( wp_get_referer() );
			}

		}

	}

	/**
	 * Template function for disclaimer label + link.
	 * Defaults to revocation disclaimer ('widerruf').
	 *
	 * @access public
	 * @static
	 * @author glueckpress
	 * @param string $text (default: '')
	 * @param array $link (default: array())
	 * @return string
	 */
	public static function review_order_label_template( $text = '', array $link = array(), $digital = false ) {

		if ( ! $digital ) {
			$default_text = __( 'I have read and accept the [link]Revocation Policy[/link]', 'woocommerce-german-market' );
			$text = get_option( 'woocommerce_de_checkbox_text_revocation', $default_text );
		} else {
			$default_text = __( 'I have read and accept the [link]Revocation Policy for digital content[/link]', 'woocommerce-german-market' );
			$text = get_option( 'woocommerce_de_checkbox_text_revocation_digital', $default_text );
		} 

		$text = str_replace( '[link]', '<a href="%1$s" %2$s>', $text );
		$text = str_replace( '[/link]', '</a>', $text );

		$url = get_permalink( get_option( WGM_Helper::get_wgm_option( 'widerruf' ) ) );
		if ( function_exists( 'icl_object_id' ) ) {
			$url = get_permalink( icl_object_id( get_option( WGM_Helper::get_wgm_option( 'widerruf' ) ) ) );
		}

		// default link
		$default_link = array(
			'atts'   => 'target="_blank"',
			'id'     => get_option( WGM_Helper::get_wgm_option( 'widerruf' ) ),
			'option' => WGM_Helper::get_wgm_option( 'widerruf' ),
			'text'   => __( 'Revocation Policy', 'woocommerce-german-market' ),
			'url'    => $url,
		);

		$link = wp_parse_args( $link, $default_link );

		// template
		$label_text = sprintf(
			$text,
			esc_url( $link[ 'url' ] ),
			esc_attr( $link[ 'atts' ] )
		);

		return $label_text;
	}


	/**
	* add the german disclaimer to checkout
	* @access public
	* @static
	* @author ap
	* @return string review order
	* @hook woocommerce_review_order_before_submit
	*/
	public static function add_review_order() {

		do_action( 'woocommerce_de_add_review_order' );

		$has_virtual = FALSE;
		$dcount      = 0;

		// Is that the order pay page?
		if ( is_wc_endpoint_url( 'order-pay' ) ) {
			
			global $wp;
			$order_key  = $_GET['key'];
			$order_id   = absint( $wp->query_vars['order-pay'] );
			$order      = wc_get_order( $order_id );
			$cart 		= $order->get_items();	

		} else {
			$cart = WC()->cart->get_cart();
		}

		foreach ( $cart as $item ) {

			if ( empty( $item[ 'variation_id' ] ) ) {
				$product = wc_get_product( $item[ 'product_id' ] );
			} else {
				$product = wc_get_product( $item[ 'variation_id' ] );
			}

			if ( WGM_Helper::is_digital( $product->get_id() ) ) {
				$has_virtual = TRUE;
				$dcount ++;
			}
		}

		$only_digital = FALSE;
		if ( $dcount == count( $cart ) ) {
			$only_digital = TRUE;
		}

		// Kick off review order
		$review_order = '';

		/**
		 * Disclaimer: revocation
		 */

		// revocation: link template (needs to be passed to filter)
		$revocation_link = array(
			'atts'   => 'target="_blank"',
			'id'     => get_option( WGM_Helper::get_wgm_option( 'widerruf' ) ),
			'option' => WGM_Helper::get_wgm_option( 'widerruf' ),
			'text'   => __( 'Revocation Policy', 'woocommerce-german-market' ),
			'url'    => get_permalink( get_option( WGM_Helper::get_wgm_option( 'widerruf' ) ) ),
		);

		// revocation: label template
		$revocation_label_text = self::review_order_label_template();

		if ( ! $only_digital ) {

			if ( get_option( WGM_Helper::get_wgm_option( 'woocommerce_de_show_Widerrufsbelehrung' ) ) == 'on' ) {

				$checked = isset( $_POST[ 'widerruf' ] ) ? checked( $_POST[ 'widerruf' ], 'on', FALSE ) : '';

				// filterable label
				$review_order = apply_filters( 'wgm_checkout_revocation_markup', sprintf(
					
					'<p class="form-row terms">
						<input type="checkbox" class="input-checkbox" %s name="widerruf" id="widerruf" />
						<label for="widerruf" class="checkbox">%s</label>
					</p>',
					
					$checked,
					
					apply_filters(
						'wgm_checkout_revocation_label_text',
						$revocation_label_text,
						$revocation_link
					)
				), $checked, $revocation_label_text, $revocation_link );
			}
		}

		/**
		 * Disclaimer: revocation for digital products
		 */

		$url = get_permalink( get_option( WGM_Helper::get_wgm_option( 'widerruf_fuer_digitale_medien' ) ) );
		if ( function_exists( 'icl_object_id' ) ) {
			$url = get_permalink( icl_object_id( get_option( WGM_Helper::get_wgm_option( 'widerruf_fuer_digitale_medien' ) ) ) );
		}

		// revocation for digital produtcs: link template
		$revocation_digital_link = array(
			'atts'   => 'target="_blank"',
			'id'     => get_option( WGM_Helper::get_wgm_option( 'widerruf_fuer_digitale_medien' ) ),
			'option' => WGM_Helper::get_wgm_option( 'widerruf_fuer_digitale_medien' ),
			'text'   => __( 'Revocation policy for <span class=\"wgm-virtual-notice\">digital</span> content',
			                'woocommerce-german-market' ),
			'url'    => $url,
		);

		// revocation for digital produtcs: label template
		$revocation_digital_label_text = self::review_order_label_template( '', $revocation_digital_link, true );

		if ( $has_virtual ) {

			if ( get_option( WGM_Helper::get_wgm_option( 'wgm_display_digital_revocation' ) ) != 'off' ) {
				
				$checked = isset( $_POST[ 'widerruf-digital' ] ) ? checked( $_POST[ 'widerruf-digital' ], 'on', FALSE ) : '';

				// filterable label
				$review_order .= sprintf(
					'<p class="form-row terms">
						<input type="checkbox" class="input-checkbox" %s name="widerruf-digital" id="widerruf-digital" />
						<label for="widerruf-digital" class="checkbox">%s</label>
					</p>',
					$checked,
					apply_filters(
						'wgm_checkout_revocation_digital_label_text',
						$revocation_digital_label_text,
						$revocation_digital_link
					)
				);

				$checked = isset( $_POST[ 'widerruf-digital-acknowledgement' ] ) ? checked( $_POST[ 'widerruf-digital-acknowledgement' ], 'on', FALSE ) : '';

				// revocation for digital produtcs: special acknowledgement re:start of delivery
				$digital_content_text_default = __( 'For digital content: You explicitly agree that we continue with the execution of our contract before expiration of the revocation period. You hereby also declare you are aware of the fact that you lose your right of revocation with this agreement.', 'woocommerce-german-market' );
				$digital_content_text = get_option( 'woocommerce_de_checkbox_text_digital_content', $digital_content_text_default );

				$review_order .= sprintf(
					'<p class="form-row terms">
					<input type="checkbox" class="input-checkbox" %s name="widerruf-digital-acknowledgement" id="widerruf-digital-acknowledgement" />
					<label for="widerruf-digital-acknowledgement" class="checkbox">%s</label>
				</p>',
					$checked,
					apply_filters(
						'wgm_checkout_digital_revocation_text',
						$digital_content_text
					)
				);
				
				if ( is_wc_endpoint_url( 'order-pay' ) ) {
					add_action( 'after_woocommerce_pay', array( 'WGM_Template', 'digital_items_notice' ) );
				} else {
					add_action( 'woocommerce_de_review_order_after_submit', array( 'WGM_Template', 'add_digital_items_notice' ), 1, 100 );
				}
			}
		}

		echo apply_filters( 'woocommerce_de_review_order_after_submit', $review_order );

	}

	/**
	* Get digitial item notice
	*
	* @return String
	**/
	public static function get_digital_item_notice() {
		$default_text = __( 'Notice: Digital content are products not being delivered on any physical medium (e.g. software downloads, e-books etc.).', 'woocommerce-german-market' );
		$text = get_option( 'woocommerce_de_checkbox_text_digital_content_notice', $default_text );

		return sprintf(
			'<span class="wgm-digital-checkout-notice">%s</span>',
			$text
		);
	}

	/**
	* Echo digitial item notice
	*
	* @return void
	**/
	public static function digital_items_notice() {

		echo self::get_digital_item_notice();

	}

	/**
	* Add digital item notice after checkboxes in checkout
	*
	* @wp-hook woocommerce_de_review_order_after_submit
	* @param String $review_order
	* @return String
	**/
	public static function add_digital_items_notice( $review_order ) {
		return $review_order .= self::get_digital_item_notice();
	}

	/**
	* add the hidden field and set the woocommerce_de_in_first_checkout
	* @access public
	* @static
	* @author ap
	* @hook woocommerce_review_order_before_submit
	*/
	public static function add_wgm_checkout_session() {
		// set update_totals, to generate the second checkout site
		if ( ! WGM_Session::is_set( 'woocommerce_de_in_first_checkout' ) ) {
			WGM_Session::add( 'woocommerce_de_in_first_checkout', true );
		}
	}

	/**
	 * Adds the selected sale label to the sale price
	 *
	 * @wp-hook woocommerce_format_sale_price
	 *
	 * @param String $html
	 * @param String $regular_price
	 * @param String $sale_price
	 * @param $product
	 *
	 * @return string
	 */
	public static function add_sale_label_to_price( $html, $regular_price, $sale_price, $product = false ) {

		if ( ! $product ) {
			global $product;
		}
		
		if ( is_archive() && 'on' != get_option( WGM_Helper::get_wgm_option( 'woocommerce_de_show_sale_label_overview' ) ) ) {
			return $html;
		}

		if ( is_product() && get_option( 'woocommerce_de_show_sale_label_product_page' ) != 'on' ) {
			return $html;
		}

		$term_id = self::get_term_id_from_product_meta( '_sale_label', $product );
		if ( $term_id == - 2 ) {
			return $html;
		}
		if ( (int) $term_id == - 1 || empty( $term_id ) ) {
			$term_id = get_option( WGM_Helper::get_wgm_option( 'global_sale_label' ) );
		}
		$label_term = get_term( $term_id, 'product_sale_labels' );
		if ( is_wp_error( $label_term ) || ! isset( $label_term ) ) {
			$label_string = '';
		} else {
			$label_string = $label_term->name;
		}

		return '<span class="wgm-sale-label">' . $label_string . '</span> ' . $html;
	}

	/**
	* Show shipping time and shipping costs
	* @access public
	* @static
	* @author jj, ap
	 * @param WC_Product $product
	* @return void
	* @hook woocommerce_single_product_summary
	*/
	public static function add_template_loop_shop( $product = NULL ) {

		// Init
		$label_string = self::get_deliverytime_string( $product );
		$lieferzeit_output = '';
		
		// If the product is a product variation, check if each variation has the same delivery time
		// => if not, do not display delivery time!
		// Add "add_filter( 'woocommerce_de_use_delivery_time_of_variable_product', '__return_true' );" to your functions.php to use the delivery time of the variable product (parent product)
		if ( is_a( $product, 'WC_Product_Variable' ) ) {

			if ( ! apply_filters( 'woocommerce_de_use_delivery_time_of_variable_product', false ) ) {

				if ( apply_filters( 'woocommerce_de_avoid_check_same_delivery_time', false ) ) {
					return;
				}

				$only_show_parent = apply_filters( 'woocommerce_de_avoid_check_same_delivery_time_show_parent', false );
				$delivery_times_of_variations = array(); // Array to put in all different delivery times
				
				if ( ! $only_show_parent ) {
				
					// Init variables for check
					$all_variations_have_the_same_delivery_time = true;
					$variations = $product->get_available_variations();
					
					// Loop all variations and add delivery time string to the "check array" $delivery_times_of_variations
					foreach ( $variations as $variation ) {
						$v_product = wc_get_product( $variation[ 'variation_id' ] );
						$v_delivery_time = self::get_deliverytime_string( $v_product );
						if ( ! in_array( $v_delivery_time, $delivery_times_of_variations ) ) {
							$delivery_times_of_variations[] = $v_delivery_time;
						}
					}

				}

				// If we don't have different delivery times => Show delivery time
				if ( ( count( $delivery_times_of_variations ) == 1 ) || $only_show_parent ) {
					
					if ( ! $only_show_parent ) {
						$label_string = array_shift( $delivery_times_of_variations );
					}
					
					$lieferzeit_output = apply_filters( 'wgm_deliverytime_loop', __( 'Delivery Time:', 'woocommerce-german-market' ) . ' ' . $label_string, $label_string );
				}

			} else {

				// use delivery time of partent product (the variable product)
				$lieferzeit_output = apply_filters( 'wgm_deliverytime_loop', __( 'Delivery Time:', 'woocommerce-german-market' ) . ' ' . $label_string, $label_string );

			}

		} else {
			$lieferzeit_output = apply_filters( 'wgm_deliverytime_loop', __( 'Delivery Time:', 'woocommerce-german-market' ) . ' ' . $label_string, $label_string );
		}

		// if product is out of stock, don't show delivery time
		if ( ! self::show_delivery_time_if_product_is_not_in_stock( $product ) ) {
			$lieferzeit_output = apply_filters( 'gm_delivery_time_message_if_out_of_stock', '', $product );
		}

		// Output delivery time
		if ( ! empty( $lieferzeit_output ) ) {
			
			$lieferzeit_and_markup = '<div class="wgm-info shipping_de shipping_de_string">
				<small>
					<span>' . $lieferzeit_output . '</span>
				</small>
			</div>';
			
			echo apply_filters( 'gm_lieferzeit_output_lieferzeit_and_markup', $lieferzeit_and_markup, $lieferzeit_output );
			
		}

	} // end function

	/**
	 * Retrieves the delivery time string from its term ID
	 *
	 * @param WC_Product $product
	 * @return string|void
	 */
	public static function get_deliverytime_string( $product ) {
		$term_id = self::get_term_id_from_product_meta( '_lieferzeit', $product );

		if ( (int) $term_id == - 1 || empty( $term_id ) ) {
			$term_id = get_option( WGM_Helper::get_wgm_option( 'global_lieferzeit' ) );
		}

		$label_term = get_term( $term_id, 'product_delivery_times' );

		if ( function_exists( 'icl_object_id' ) ) {
			$label_term = get_term( icl_object_id( $term_id, 'product_delivery_times' ), 'product_delivery_times' );
		}
		
		if ( is_wp_error( $label_term ) || ! isset( $label_term ) ) {
			$label_string = __( 'not specified', 'woocommerce-german-market' );
		} else {
			$label_string = $label_term->name;
		}

		return apply_filters( 'woocommerce_de_get_deliverytime_string_label_string', $label_string, $product );
	}

	/**
	 * If the product is of of stock, do we want to show delivery time?
	 *
	 * @param WC_Product $product
	 * @return Bollean
	 */
	public static function show_delivery_time_if_product_is_not_in_stock( $product ) {

		$return = true;

		if ( ( $product ) && ( ! $product->is_in_stock() ) ) {
			if ( apply_filters( 'woocommerce_de_do_not_show_delivery_time_if_out_of_stock', true, $product ) ) {
				$return = false;
			}
		}

		return $return;

	}

	/**
	 * Retrieves a term ID from a given product meta key.
	 * Recursively fetches the parent term ID from a variation if not set
	 *
	 * @param $product
	 * @return int
	 */
	public static function get_term_id_from_product_meta( $meta_key, $product = NULL ) {

		if ( ! function_exists( 'wc_get_product' ) ) {
			return $meta_key;
		}
		
		$variation = FALSE;
		if ( ! ( $product instanceof WC_Product ) ) {
			if ( ! isset ( $GLOBALS['post'] ) ) { // if something went wrong
				return '';
			}
			$product = wc_get_product();
			if ( ! $product || is_null( $product ) ) {
				return '';
			}
			$product_id = $product->get_id();
		} else {
			$variation = ( $product instanceof WC_Product_Variation );
			if ( $variation ) {
				$product_id = $product->get_id();
			} else {
				$product_id = $product->get_id();

			}
		}
		$data = get_post_meta( $product_id, $meta_key, TRUE );

		// If data = default value or "use the default" or is not set
		if ( $variation && ( (int) $data == - 1 || empty( $data ) ) ) {

			/**
			 * Use 'same as parent' when nothing is set
			 */
			$parent_product = wc_get_product( $product->get_parent_id() );
			$lieferzeit = self::get_term_id_from_product_meta( $meta_key, $parent_product );

		} else {
			$lieferzeit = $data;
		}

		return $lieferzeit;
	}
	/**
	*  add shipping costs and and discalmer to cart before the buttons
	* @access public
	* @static
	* @author jj, ap
	* @return void
	* @hook woocommerce_widget_shopping_cart_before_buttons
	*/
	public static function add_shopping_cart() {

		if( get_option( WGM_Helper::get_wgm_option( 'woocommerce_de_disclaimer_cart' ) ) == 'off' )
				return;
		?>

		<p class="jde_hint">
				<?php echo WGM_Template::disclaimer_line(); ?>
			</p>
		<?php
	}

	/**
	* add shipping costs and and discalmer to cart
	* @access public
	* @static
	* @author jj, ap
	* @return void
	* @hook woocommerce_cart_contents
	*/
	public static function add_shop_table_cart() {

		if( get_option( WGM_Helper::get_wgm_option( 'woocommerce_de_disclaimer_cart' ) ) == 'off' )
			return;

		?>
		<tr class="jde_hint">
			<td colspan="<?php echo apply_filters( 'wgm_colspan_add_shop_table_cart', WGM_Tax::is_kur() ? 6 : 7 ); ?>" class="actions">
				<?php echo WGM_Template::disclaimer_line(); ?>
			</td>
		</tr>
		<?php
	}


	/**
	* admin field string template
	*
	* @access public
	* @static
	* @param string $value
	* @return void
	* @hook woocommerce_admin_field_string
	*/
	public static function add_admin_field_string_template( $value ) {
		?>
		<tr valign="top">
			<th scope="row" class="titledesc">
				<?php echo $value[ 'name' ]; ?>
			</th>
			<td class="forminp">
				<?php echo esc_attr( $value[ 'desc' ] ); ?>
			</td>
		</tr>
		<?php
	}

	/**
	 * Returns the Small Trading Exemption string based on the Shop location
	 *
	 * @param bool $country @depcrcated
	 * @return String
	 */
	public static function get_ste_string( $country = FALSE ) {
		$str = get_option( 'gm_small_trading_exemption_notice', self::get_default_ste_string() );
		return apply_filters( 'woocommerce_de_small_business_regulation_text', $str );
	}

	/**
	 * Returns the default Small Trading Exemption string based on the Shop location
	 *
	 * @since 3.5
	 * @return String
	 */
	public static function get_default_ste_string() {

		$country = get_option( 'woocommerce_default_country' );

		switch ( $country ) {
			case 'DE':
				$str = apply_filters( 'woocommerce_de_small_business_regulation_text',
				                      __( 'VAT exempted according to UStG §19',
				                          'woocommerce-german-market' ) );
				break;
			case 'AT':
				$str = apply_filters( 'woocommerce_de_small_business_regulation_text',
				                      __( 'VAT exempted according to UStG §6',
				                          'woocommerce-german-market' ) );
				break;
			default:
				$str = '';
		}

		return $str;

	}

	/**
	 * Returns the Small Trading Exemption string based on the Shop location
	 *
	 * @param bool $country
	 *
	 * @return mixed|void
	 */
	public static function get_ste_string_invoice( $country = FALSE ) {
		$str = self::get_ste_string();
		return apply_filters( 'woocommerce_de_small_business_regulation_text_for_invoice', $str );
	}

	/**
	* Outputs readonly checkout fields
	*
	* @author jj
	* @access public
	* @static
	* @param string $key key of field
	* @param array $args	contains a list of args for showing the field, merged with defaults (below
	* @return string
	*/
	public static function checkout_readonly_field( $key, $args = array() ) {

		WC()->checkout();

		$defaults = array(
			'type'        => 'input',
			'name'        => '',
			'label'       => '',
			'placeholder' => '',
			'required'    => FALSE,
			'class'       => array(),
			'label_class' => array(),
			'rel'         => '',
			'return'      => TRUE
		);

		$args = wp_parse_args( $args, $defaults );

		$field = '';

		if ( ! WGM_Session::is_set( $key, 'first_checkout_post_array' ) ) {
			return FALSE;
		}

		$value = WGM_Session::get( $key, 'first_checkout_post_array' );

		if ( empty( $value ) ) {
			return FALSE;
		}

		switch ( $args[ 'type' ] ) {
			case "textarea" :
				$field  = '<span class="wgm-field-label">' . $args[ 'label' ] . '</span>' . '<span class="wgm-break"></span>' . $value;
				$hidden = sprintf( '<input type="hidden" name="%s" value="%s" /> ', $key, $value );
				break;
			case 'country':
				$countries = WC()->countries->countries;
				if ( isset( $countries[ $value ] ) ) {
					$display_value = $countries[ $value ];
				} else {
					$display_value = $value;
				}
				$field  = '<tr><td><span class="wgm-field-label">' . $args[ 'label' ] . '</span></td><td>' . stripcslashes( $display_value ) . '</td></tr>';
				$hidden = sprintf( '<input type="hidden" name="%s" value="%s" /> ', $key, $value );
				break;
			default :
				$field  = '<tr><td><span class="wgm-field-label">' . $args[ 'label' ] . '</span></td><td>' . stripcslashes( $value ) . '</td></tr>';
				$hidden = sprintf( '<input type="hidden" name="%s" value="%s" /> ', $key, $value );
				break;
		}

		if ( $args[ 'return' ] ) {
			return array( $field, $hidden );
		} else {
			printf( '%s,%s\n', $field, $hidden );
		}
	}

	/**
	 * Get last checkout Hints
	 *
	 * Likely not used anymore!
	 * @static
	 * @author jj
	 * @return string
	 */
	public static function get_last_checkout_hints( ){
		return WGM_Template::checkout_readonly_field( 'woocommerce_de_last_checkout_hints' );
	}


	/**
	 * Set a field of an fields array to a specific value, by passing the fieldtype
	 * @param array $fields
	 * @param string $field_type
	 * @param string $changefield
	 * @param mixed $value
	 * @return array
	 * @access public
	 * @static
	 * @author jj, ap
	 */
	public static function set_field_type( $fields, $field_type, $changefield, $value = FALSE ) {

		foreach ( $fields as $key => $field ) {
			if ( isset( $field[ 'type' ] ) && $field_type == $field[ 'type' ] )
				$fields[ $key ][ $changefield ] = $value;
		}

		return $fields;
	}


	/**
	* returns shipping costs and withdraw disclaimer as html with links
	*
	* @access public
	* @static
	* @author jj, ap
	* @uses get_option
	* @return string html
	*/
	public static function disclaimer_line(){

		// Shipping
		$shipping_url = get_permalink( get_option( WGM_Helper::get_wgm_option( 'versandkosten__lieferung' ) ) );
		if ( function_exists( 'icl_object_id' ) ) {
			$shipping_url = get_permalink( icl_object_id( get_option( WGM_Helper::get_wgm_option( 'versandkosten__lieferung' ) ) ) );
		}
		$shipping   = array(
			'url'  => $shipping_url,
			'atts' => ''
		);
		$shipping[ 'atts' ] = sprintf(
			'href="%s" class="wgm-versandkosten" target="_blank"',
			esc_url( $shipping[ 'url' ] )
		);

		// Payment
		$payment_url = get_permalink( get_option( WGM_Helper::get_wgm_option( 'zahlungsarten' ) ) );
		if ( function_exists( 'icl_object_id' ) ) {
			$payment_url = get_permalink( icl_object_id( get_option( WGM_Helper::get_wgm_option( 'zahlungsarten' ) ) ) );
		}
		$payment   = array(
			'url'  => $payment_url,
			'atts' => ''
		);
		$payment[ 'atts' ] = sprintf(
			'href="%s" class="wgm-zahlungsarten" target="_blank"',
			esc_url( $payment[ 'url' ] )
		);

		// Revocation
		$revocation_url = get_permalink( get_option( WGM_Helper::get_wgm_option( 'widerruf' ) ) );
		if ( function_exists( 'icl_object_id' ) ) {
			$revocation_url = get_permalink( icl_object_id( get_option( WGM_Helper::get_wgm_option( 'widerruf' ) ) ) );
		}
		$revocation   = array(
			'url'  => $revocation_url,
			'atts' => ''
		);
		$revocation[ 'atts' ] = sprintf(
			'href="%s" class="wgm-widerruf" target="_blank"',
			esc_url( $revocation[ 'url' ] )
		);

		// Privacy
		$privacy_url = get_permalink( get_option( 'woocommerce_datenschutz_page_id' ) );
		if ( function_exists( 'icl_object_id' ) ) {
			$privacy_url = get_permalink( icl_object_id( 'woocommerce_datenschutz_page_id' ) );
		}
		$privacy = array(
			'url' 	=> $privacy_url,
			'atts' 	=> ''
		);
		$privacy[ 'atts' ] = sprintf(
			'href="%s" class="wgm-privacy" target="_blank"',
			esc_url( $privacy[ 'url' ] )
		);

		// Build string
		$html  = '';
		if ( 'on' === get_option(  WGM_Helper::get_wgm_option( 'woocommerce_de_show_shipping_fee_overview_single' ) ) ) {

			$default_option = __( 'Learn more about [link-shipping]shipping costs[/link-shipping], [link-payment]payment methods[/link-payment] and our [link-revocation]revocation policy[/link-revocation].', 'woocommerce-german-market' );
			$option = get_option( 'woocommerce_de_learn_more_about_shipping_payment_revocation', $default_option );

			$text = str_replace( array( '[link-shipping]', '[link-payment]', '[link-revocation]', '[link-privacy]', '[/link-shipping]', '[/link-payment]', '[/link-revocation]', '[/link-privacy]' ), array( '<a %1$s>', '<a %2$s>', '<a %3$s>', '<a %4$s>', '</a>', '</a>', '</a>', '</a>' ), $option );
			$text = apply_filters( 'woocommerce_de_learn_more_about_string_1', $text );

			$html .= sprintf(
				$text,
				$shipping[ 'atts' ],
				$payment[ 'atts' ],
				$revocation[ 'atts' ],
				$privacy[ 'atts' ]
			);

		} else {

			// deprecated
			$default_option = __( 'Learn more about [link-payment]payment methods[/link-payment] and our [link-revocation]revocation policy[/link-revocation].', 'woocommerce-german-market' );
			$option = get_option( 'woocommerce_de_learn_more_about_payment_revocation', $default_option );

			$html .= sprintf(
				apply_filters( 'woocommerce_de_learn_more_about_string_2', __( 'Learn more about <a %1$s>payment methods</a> and our <a %2$s>revocation policy</a>.', 'woocommerce-german-market' ) ),
				$payment[ 'atts' ],
				$revocation[ 'atts' ]
			);
		}

		/**
		 * Filter legacy
		 */
		if ( has_filter( 'wgm_disclaimer_line' ) ) {
			$versandkosten      = '<a class="wgm-versandkosten" href="' . $shipping[ 'url' ] . '" target="_blank">' . __( 'shipping costs', 'woocommerce-german-market' ) . '</a>';
			$versandkosten_text = sprintf( ' ' . __( 'Infomation regarding %1$s,', 'woocommerce-german-market' ), $versandkosten );
			$widerrufsrecht     = '<a class="wgm-widerruf" href="' . $revocation[ 'url' ] . '" target="_blank">' . __( 'revocation policy', 'woocommerce-german-market' ) . '</a>';
			$widerrufsrecht_text= sprintf( ' ' . __( 'details regarding our %1$s', 'woocommerce-german-market' ), $widerrufsrecht );
			$zahlungsarten     = '<a class="wgm-zahlungsarten" href="' . $payment[ 'url' ] . '"  target="_blank">' . __( 'Payment Methods', 'woocommerce-german-market' ) . '</a>';
			$zahlungsarten_text= sprintf( ' ' . __( 'and %s', 'woocommerce-german-market' ), $zahlungsarten );
			$disclaimer_line_prefix = __( 'Here you find', 'woocommerce-german-market' );

			// Legacy output
			$disclaimer_line = apply_filters(
				'wgm_disclaimer_line',
				$html,
				$shipping[ 'url' ],
				$versandkosten_text,
				$versandkosten,
				$widerrufsrecht,
				$revocation[ 'url' ],
				$widerrufsrecht_text,
				$zahlungsarten,
				$payment[ 'url' ],
				$zahlungsarten_text,
				$disclaimer_line_prefix
			);
		} else {

			// Regular output
			$disclaimer_line = apply_filters(
				'wgm_disclaimer_line',
				$html,
				$shipping,
				$payment,
				$revocation
			);
		}

		return $disclaimer_line;
	}


	/**
	* get string from texttemplate directory, if filename is given, else it returns the parameter
	*
	* @access	public
	* @static
	* @author	et, ap
	* @param	string $name template filename
	* @param	array $args
	* @return	void
	*/
	public static function include_template( $name, $args = array() ) {
		_deprecated_function( 'WGM_Template::include_template', "v2.3", "WGM_Template::load_template" );
		WGM_Template::load_template( $name, $args );
	}


	/**
	* get string from texttemplate directory, if filename is given, else it returns the parameter
	*
	* @access public
	* @param string $name template filename
	* @author jj, ap
	* @return string
	*/
	public static function get_text_template( $name ) {
		$path = dirname( __FILE__ ) . '/../text-templates/' . $name;
		if ( file_exists( $path )  ) {
			return file_get_contents( $path );
		} else {
			return $name;
		}
	}

	/**
	 * Adds payment information to the mails
	 * @deprecated
	 * @param WC_Order $order The Woocommerce order object
	 * @access public
	 * @author ap
	 * @since 2.0
	 * @hook woocommerce_email_after_order_table
	 */
	public static function add_paymentmethod_to_mails( $order ){
		_deprecated_function(__FUNCTION__, '2.6.5' );
		$html = '<h3>' . __( 'Payment Method', 'woocommerce-german-market' ) . ': ' . $order->get_payment_method_title() . '</h3>';
		echo apply_filters( 'wgm_add_paymentmethod_to_mails_html', $html, $order );
	}


	/**
	 * adds the product short description to the checkout
	 * @param string $title
	 * @param string $item
	 * @return string
	 * @author ap
	 * @access public
	 * @static
	 * @hook woocommerce_checkout_item_quantity
	 */
	public static function add_product_short_desc_to_checkout_title( $title, $item ){
		if ( get_option( 'woocommerce_de_show_show_short_desc' ) !== 'on' )
			return $title;

		$product_id = $item[ 'data' ]->get_id();
		$product_post = get_post( $product_id );
		$product_short_desc = $product_post->post_excerpt;
		
		$html = '<span class="wgm-break"></span> <span class="product-desc">'  . $product_short_desc . '</span>';
		$title .= apply_filters( 'wgm_add_product_short_desc_to_checkout_title', $html, $title, $item, $product_short_desc );

		return $title;
	}

	/**
	 * adds the product short description to the oder listing
	 * @access public
	 * @static
	 * @author ap
	 * @param string $title
	 * @param string $item
	 * @return string
	 * @hook woocommerce_checkout_item_quantity
	 */
	public static function add_product_short_desc_to_order_title( $title, $item ){

		if ( get_option( 'woocommerce_de_show_show_short_desc' ) !== 'on' )
			return $title;

		$product_id = $item->get_product_id();
		$product_post = get_post( $product_id );
		$product_short_desc = $product_post->post_excerpt;
		$html = '<span class="wgm-break"></span> <span class="product-desc">'  . $product_short_desc . '</span>';

		$title .= apply_filters( 'wgm_add_product_short_desc_to_order_title_html', $html, $title, $item, $product_short_desc );

		return $title;
	}

	/**
	 * adds the product short description to the oder listing
	 * @access public
	 * @static
	 * @param int $item_id
	 * @param WC_Order_Item $item
	 * @param WC_Order $order
	 * @param bool $plain_text
	 * @return void
	 * @hook woocommerce_order_item_meta_start
	 */
	public static function woocommerce_order_item_meta_start_short_desc( $item_id, $item, $order, $plain_text = false ){
		
		$short_desc = self::add_product_short_desc_to_order_title( '', $item );
		if ( $short_desc !== '' ) {
			echo $plain_text ? "\n" : '';
			echo $short_desc;
		}

	}

	// Not in use
	public static function add_product_short_desc_to_order_title_mail( $item ){
		if ( get_option( 'woocommerce_de_show_show_short_desc' ) !== 'on' )
			return;

		if ( ! empty( $item[ 'variation_id' ] ) ) {
			$id = $item[ 'variation_id' ];
			$requirements_key = '_variation_requirements';
			$is_downloadable = get_post_meta( $id, '_downloadable', true ) == 'yes';
		} else {
			$id = $item[ 'product_id' ];
			$requirements_key = 'product_function_desc_textarea';
			$is_downloadable = $_product->is_downloadable();
		}

		$_product = wc_get_product( $item[ 'item_meta'][ '_product_id' ][0] );
		$product_short_desc = $_product->get_post()->post_excerpt;

		$prerequisites = get_post_meta( $id, $requirements_key, true );

		$html = '<span class="wgm-break"></span> <span class="product-desc">'  . $product_short_desc . '</span>';

		if ( ( WGM_Helper::is_digital( $id ) || $is_downloadable ) && $prerequisites != false ){
			$html .= ' <span class="wgm-break"></span><span class="wgm-product-prerequisites">' . $prerequisites . '</span>';
		}

		echo apply_filters( 'wgm_add_product_short_desc_to_order_title_html', $html, $item[ 'name' ], $item, $product_short_desc );
	}

	/**
	 * adds the requirements to the order listing
	 * @access public
	 * @static
	 * @param int $item_id
	 * @param WC_Order_Item $item
	 * @param WC_Order $order
	 * @param bool $plain_text
	 * @return void
	 * @hook woocommerce_order_item_meta_start
	 */
	public static function woocommerce_order_item_meta_requirements( $item_id, $item, $order, $plain_text = false ){
		
		$requirements =  self::add_product_function_desc( '', $item );
		if ( $requirements !== '' ) {
			echo $plain_text ? "\n" : '';
			echo $requirements;
		}

	}

	/**
	 * adds the product short description to the oder listing
	 * @access public
	 * @static
	 * @param int $item_id
	 * @param WC_Order_Item $item
	 * @param WC_Order $order
	 * @param $plain_text
	 * @return void
	 * @hook woocommerce_order_item_meta_start
	 */
	public static function add_product_function_desc( $title, $item ){

		$prerequisites = false;

		if ( ! empty( $item[ 'variation_id' ] ) ) {
			$id = $item[ 'variation_id' ];
			$requirements_key = '_variation_requirements';
			$is_downloadable = get_post_meta( $id, '_downloadable', true ) == 'yes';
		} else {
			
			$is_downloadable = false;
 
 			if ( isset( $item[ 'product_id' ] ) ) {
 				
 				$id = $item[ 'product_id' ];
 				$requirements_key = 'product_function_desc_textarea';
 				$_product = wc_get_product( $item[ 'product_id'] );
 				if ( $_product ) {
 					$is_downloadable = $_product->is_downloadable();
 				}

 				$prerequisites = get_post_meta( $id, $requirements_key, true );
 				
 			}
		}
       
		if ( ( WGM_Helper::is_digital( $id ) || $is_downloadable ) && $prerequisites != false ){

			$prerequisites_label = apply_filters( 'wgm_product_prerequisites_label', __( 'Requirements', 'woocommerce-german-market' ) );
			$prerequisites_label_markup = apply_filters( 'wgm_product_prerequisites_label_markup', '<span class="wgm-prerequisites-label">' . $prerequisites_label . ': </span>', $prerequisites_label );

			$html = ' <span class="wgm-break"></span><span class="wgm-product-prerequisites">' . $prerequisites_label_markup . esc_attr( $prerequisites ) . '</span>';
			$title .= apply_filters( 'wgm_add_product_prerequisites_to_order_title_html', $html, $title, $item, $prerequisites );
		}

		return $title;
	}

	/**
	 * @param WC_Product $_product
	 */
	public static function add_digital_product_prerequisits( $_product ) {

		echo self::get_digital_product_prerequisits( $_product );
	}

	/**
	 * @param $_product
	 *
	 * @return string|void
	 */
	public static function get_digital_product_prerequisits( $_product ) {

		if ( ! $_product ) {
			return;
		}

		// there is at least one theme where $_product is a WP_Post instead of a WC_Product
		if ( is_a( $_product, 'WP_Post' ) ) {
			$_product = wc_get_product( $_product );
		}

		$return = '';

		if ( $_product->get_type() == 'variation' ) {
			$_digital = ( get_post_meta( $_product->get_id(), '_digital', TRUE ) == 'yes' );
		} else {
			$_digital = WGM_Helper::is_digital( $_product->get_id() );
		}

		$prerequisites = get_post_meta( $_product->get_id(), 'product_function_desc_textarea', TRUE );

		if ( ( $_digital || $_product->is_downloadable() ) && $prerequisites != FALSE ) {

			ob_start();

			do_action( 'wgm_before_product_prerequists' );

			$html = '';

			if ( $_product->get_product_type() == 'variable' ) {

				$notice = '<span class="wgm-digital-variation-notice">';
				$notice .= apply_filters( 'wgm_variation_prerequists_notice_label',
				                          __( 'The following product configurations are digital:',
				                              'woocommerce-german-market' ) );
				$notice .= '</span>';

				$html .= apply_filters( 'wgm_digital_variation_notice_html', $notice );

				$list = '<ul class="wgm-digital-attribute-list">';

				$child_filter = array();

				foreach ( $_product->get_children( TRUE ) as $child ) {

					$c_product = wc_get_product( $child );

					if ( WGM_Helper::is_digital( $c_product->get_id() ) ) {
						$child_filter[] = $c_product;

						$list .= '<li>';

						$data = array_values( $c_product->variation_data );

						for ( $i = 0; $i <= count( $data ) - 1; $i ++ ) {
							if ( empty( $data[ $i ] ) ) {
								continue;
							}
							$list .= $data[ $i ];

							if ( isset( $data[ $i + 1 ] ) && ! empty( $data[ $i + 1 ] ) ) {
								$list .= apply_filters( 'wgm_digital_variation_notice_attribute_separator', ' & ' );
							}
						}
					}

					$list .= '</li>';
				}

				$list .= '</ul>';

				$html .= apply_filters( 'wgm_digital_variation_notice_attribues_list', $list, $child_filter );

				$html .= apply_filters(
					'wgm_variation_prerequists_label',
					sprintf( '<span class="wgm-product-prerequisites-label">%s</span>',
					         __( 'Digital configurations of this product have the following requirements:',
					             'woocommerce-german-market' ) )
				);
			}

			$prerequisites_html = '<div class="wgm-info wgm-product-prerequisites">' . $prerequisites . '</div>';
			$html .= apply_filters( 'wgm_add_product_prerequisites', $prerequisites_html, $prerequisites );

			echo $html;

			do_action( 'wgm_after_product_prerequists' );

			$return = ob_get_clean();

		}

		return $return;
	}

	/**
	* returns the extra cost for non eu countries to the product description
	* still used in class WGM_Embed
	*
	* @access public
	* @static
	* @return String
	*/
	public static function get_extra_costs_eu() {

		if ( get_option( WGM_Helper::get_wgm_option( 'woocommerce_de_show_extra_cost_hint_eu' ) ) !== 'on' ) {
			return '';
		}

		return apply_filters(
			'wgm_show_extra_costs_eu_html',
			sprintf(
				'<small class="wgm-info wgm-extra-costs-eu">%s</small>',
				__( 'Additional costs (e.g. for customs or taxes) may occur when shipping to non-EU countries.',
				    'woocommerce-german-market' )
			)
		);
	}

	/**
	* returns the extra cost for non eu countries to the product description
	*
	* @since GM 3.4.2
	* @wp-hook wgm_product_summary_parts
	* @access public
	* @static
	* @return String
	*/
	public static function add_extra_costs_non_eu( $parts, WC_Product $product, $hook ) {

		if ( get_option( WGM_Helper::get_wgm_option( 'woocommerce_de_show_extra_cost_hint_eu' ) ) == 'on' ) {
			
			$parts[ 'extra_costs_non_eu' ] = apply_filters( 'wgm_show_extra_costs_eu_html',
				
				sprintf(
					'<small class="wgm-info wgm-extra-costs-eu">%s</small>',
					__( 'Additional costs (e.g. for customs or taxes) may occur when shipping to non-EU countries.',
					    'woocommerce-german-market' )
				)

			);


		}

		return $parts;
	}

	/**
	 * hide flat rate shipping if free shipping is available
	 * @access public
	 * @static
	 * @param Array $rates
 	 * @param Array $package
	 * @return array
	 * @hook woocommerce_package_rates
	 */

	public static function hide_flat_rate_shipping_when_free_is_available( $rates, $package ) {

		if ( get_option( 'wgm_dual_shipping_option', 'off' ) == 'on' ) {
	    	
	    	// is there frees shipping?
	    	$free_shipping_is_available = false;
	    	foreach ( $rates as $rate ) {
	    		if ( $rate->method_id == 'free_shipping' ) {
	    			$free_shipping_is_available = true;
	    			break;
	    		}
	    	}

	    	if ( $free_shipping_is_available ) {
		    	
		    	$new_rates = $rates;
		    	// unset all other rates (there can be more than 1 rate of free shipping (e.g.: free local pickup, free delivery) )
		    	foreach( $rates as $key => $rate ) {
		    		
		    		if ( $rate->method_id == 'flat_rate' ) {
		    			unset( $new_rates[ $key ] );
		    		}

		    	}

		    	$rates = $new_rates;

		    }

	    }
     
    	return $rates;

	}

	public static function kur_notice() {

		if ( ! WGM_Tax::is_kur() ) {
			return;
		}
		echo apply_filters(
				'wgm_kur_notice_html',
				'<tr><td colspan="2"><div class="wgm-kur-notice">' .	self::get_ste_string() .'</div></td></tr>'
		);
	}

	public static function kur_review_order_notice() {
		if( get_option( WGM_Helper::get_wgm_option( 'woocommerce_de_kleinunternehmerregelung' ) ) == 'on' ){
			echo apply_filters(
				'wgm_kur_review_order_notice_html',
				'<tr>
				<td></td>
				<td>
				<div class="wgm-kur-notice-review">' .self::get_ste_string() .	'</div>
				</td>
				</tr>'
			);
		}
	}

	public static function kur_review_order_item( $formatted_total ){

		if( get_option( WGM_Helper::get_wgm_option( 'woocommerce_de_kleinunternehmerregelung' ) ) !== 'on' )
			return $formatted_total;

		$html = ' <small>' . self::get_ste_string() . '</small>';
		$html = apply_filters( 'wgm_kur_review_order_item_html', $html );

		return $formatted_total . $html;
	}

	/**
	 * Adds a [Digital] notice to the product title of a digital product
	 *
	 * @wp-hook woocommerce_product_title
	 *
	 * @param $name
	 * @param $_product
	 *
	 * @return mixed|void
	 */
	public static function add_virtual_product_notice( $name, $_product ) {

		/**
		 * Unfortunately, using Markup in the product title causes all kinds of problems with escaped markup showing up.
		 * (External Payment Gateways, REST API requests and so on)
		 *
		 *Disable the use of markup for now. Maybe a solution pops up eventually
		 */
		$use_markup = FALSE;

		// before WC 3.0.0 we checked if it's a variation or not, get_id always returns the correct id
		$id = $_product->get_id();

		if ( WGM_Helper::is_digital( $id ) ) {
			$digital_keyword = apply_filters( 'wgm_product_name_virtual_notice_keyword', '[Digital]' );
			if ( $use_markup ) {
				$string = sprintf( '%s <span class="wgm-virtual-notice">%s</span>', $name, $digital_keyword );
			} else {
				$string = sprintf( '%s %s', $name, $digital_keyword );
			}

			return apply_filters(
					'wgm_product_name_virtual_notice',
					$string,
					$name,
					$digital_keyword
			);
		}

		return $name;
	}

	/**
	 * Add the "[Digital]" to product name
	 *
	 * @author  ChriCo
	 *
	 * @wp-hook woocommerce_order_get_items
	 *
	 * @param   array $items
	 * @return  array $items
	 */
	public static function filter_order_item_name( $items ){

		$keyword= apply_filters( 'wgm_product_name_virtual_notice_keyword', '[Digital]' );
		$html   = sprintf( '<span class="wgm-virtual-notice">%s</span>', $keyword );

		foreach( $items as $key => $item ){
			$search = apply_filters(
				'wgm_product_name_virtual_notice',
				$html,
				$item[ 'name' ],
				$keyword
			);

			if( strpos( $item[ 'name' ], $search ) !== FALSE ){
				$item[ 'name' ] = str_replace( $search, $keyword, $item[ 'name' ] );
			}

			/**
			 * re-assign the value
			 * @issue #421
			 */
			$items[ $key ] = $item;

		}

		return $items;
	}

	/**
	 * Caches the Order Button HTML and removes it form checkout
	 * @param $button_html
	 * @author ap
	 * @singe 2.4.13
	 * @wp-hook woocommerce_order_button_html
	 * @return string
	 */
	public static function remove_order_button_html( $button_html ){
		self::$button_html = $button_html;
		return '';
	}

	/**
	 * Prints the cahced Order Button HTML at the absolute end of the checkout page
	 *
	 * @author ap
	 * @singe 2.4.13
	 * @wp-hook woocommerce_review_order_after_submit
	 * @return void
	 */
	public static function print_order_button_html(){
		echo self::$button_html;
	}

	/**
	 * Adds excluding tax notice after cart and checkout total
	 * @param string $value
	 * @author ap, ch
	 * @hook woocommerce_cart_totals_order_total_html
	 * @since 2.5
	 * @return string
	 */
	public static function woocommerce_cart_totals_excl_tax_string( $value ) {

		// Default total without any taxes.
		$value = '<strong>' . WC()->cart->get_total() . '</strong> ';
		if ( ! wc_tax_enabled() ) {
			return $value;
		}

		$tax_total_string = WGM_Template::get_totals_tax_string( WC()->cart->get_tax_totals(),
		                                                         get_option( 'woocommerce_tax_display_cart' ) );

		// Append tax line to total.
		$value .= $tax_total_string;

		return apply_filters( 'wgm_cart_totals_order_total_html', $value, $tax_total_string );
	}

	/**
	 * Adds excluding tax notice after cart and checkout total
	 * @param string $value
	 * @author ap, ch
	 * @hook woocommerce_cart_totals_order_total_html
	 * @since 2.5
	 * @return string
	 */
	public static function woocommerce_tax_totals_excl_tax_string( $value ) {

		// Default total without any taxes.
		$value = '<strong>' . $value . '</strong> ';

		if ( ! wc_tax_enabled() || get_option( 'woocommerce_tax_total_display' ) === 'itemized' ) {
			return $value;
		}
		//TODO: It would probably be necessary to force an itemized tax display in WGM_Template::get_totals_tax_string(). This is currently not possible

		// Append tax line to total.
		$tax_total_string = WGM_Template::get_totals_tax_string( WC()->cart->get_tax_totals(),
		                                                         'incl' );
		$value .= $tax_total_string;

		return apply_filters( 'wgm_tax_totals_order_total_html', $value, $tax_total_string );
	}

	/**
	 * Get totals for display on pages and in emails.
	 *
	 * @param  array  $total_rows [description]
	 * @param  object $order      [description]
	 * @return array             [description]
	 */
	public static function get_order_item_totals( $total_rows, $order ) {

		if ( is_a( $order, 'WC_Order_Refund' ) ) {
			$parent_id = $order->get_parent_id();
			$order = wc_get_order( $parent_id );
		}
		
		$tax_display = get_option( 'woocommerce_tax_display_cart' );

		$total_rows[ 'order_total' ] = array(
				'label' => __( 'Total:', 'woocommerce-german-market' ),
				'value' => $order->get_formatted_order_total()
		);

		// Tax for inclusive prices
		if ( wc_tax_enabled() && 'incl' == $tax_display ) {

			$tax_total_string = WGM_Template::get_totals_tax_string( $order->get_tax_totals(), $tax_display, $order );
			$tax_total_strng .= $order->get_tax_totals();

			$total_rows[ 'order_total' ][ 'value' ] .= sprintf( ' %s', $tax_total_string );
		}

		return $total_rows;
	}

	/**
	 * Hide WGMs Order Item Meta form showing up in backend order view
	 * @param array $meta Order Item Meta keys
	 * @author ap
	 * @since 2.5
	 * @wp-hook woocommerce_hidden_order_itemmeta
	 * @return array
	 */
	public static function add_hidden_order_itemmeta( array $meta ){
		$meta[] = '_price';
		$meta[] = '_deliverytime';

		return $meta;
	}

	/**
	 * Show Copoun html without removal link
	 * @param WC_Coupon|string $coupon
	 * @author ap
	 * @since 2.5
	 * @return void
	 */
	public static function checkout_totals_coupon_html( $coupon ) {
		if ( is_string( $coupon ) ) {
			$coupon = new WC_Coupon( $coupon );
		}

		$value  = array();

		if ( $amount = WC()->cart->get_coupon_discount_amount( $coupon->get_code(), WC()->cart->display_cart_ex_tax ) ) {
			$discount_html = '-' . wc_price( $amount );
		} else {
			$discount_html = '';
		}

		$value[] = apply_filters( 'woocommerce_coupon_discount_amount_html', $discount_html, $coupon );

		if ( $coupon->get_free_shipping() ) {
			$value[] = __( 'Free shipping coupon', 'woocommerce-german-market' );
		}

		// get rid of empty array elements
		$value = array_filter( $value );
		$value = implode( ', ', $value );

		echo apply_filters( 'wgm_checkout_totals_coupon_html', $value, $coupon );
	}



	/**
	 * Get tax string for order total.
	 *
	 * @param  array    $tax_totals  Array of tax rate objects
	 * @param  string   $tax_display incl|excl
	 * @param  WC_Order $order
	 * @param  string  $tax_total_display
	 *
	 * @return string              String indicating taxes included|excluded
	 */
	public static function get_totals_tax_string( $tax_totals, $tax_display, $order = NULL, $tax_total_display = NULL ) {

		if ( empty( $tax_total_display ) ) {
			$tax_total_display = get_option( 'woocommerce_tax_total_display' );
		}

		if ( wc_tax_enabled() && 'excl' === get_option( 'woocommerce_tax_display_cart' ) ) {
			return '';
		}

		$tax_string_array = array();
		$tax_total        = 0;

		$is_tax_free = false;

		// Collect applicable taxes.
		foreach ( $tax_totals as $code => $tax ) {

			/**
			 * If this method is called from an order (as opposed to the cart),
			 * it is possible that refunds have taken place and that we're currently rendering the
			 * refund notification email.
			 * In this case, we need to take the refunded amount of taxes into account and substract it from the
			 * actual total tax amount.
			 *
			 * Otherwise, we would still end up showing the inital amount of taxes without refunds
			 */
			if ( ! is_null( $order ) && ( $refunded = $order->get_total_tax_refunded_by_rate_id( $tax->rate_id ) ) > 0 ) {
				$tax_total += $tax->amount - $refunded;
				$formatted_amount = '<del>' . strip_tags( $tax->formatted_amount ) . '</del> <ins>' . wc_price( $tax->amount - $refunded, array( 'currency' => $order->get_currency() ) ) . '</ins>';

			} else {
				$tax_total += $tax->amount;
				$formatted_amount = $tax->formatted_amount;
			}

			// Tax label
			$tax_label = $tax->label;

			// Append percentage to label when in order review or e-mail.
			if ( isset( $tax->rate_id ) ) {
				// %s: ({percentage}%)
				$tax_label .= sprintf( ' (%s)', str_replace( '.', wc_get_price_decimal_separator(), WC_Tax::get_rate_percent( $tax->rate_id ) ) );
			}

			// %s: €{amount} {label}
			$tax_string_array[] = sprintf(
				'<span class="amount">%s</span> %s',
				$formatted_amount,
				$tax_label // here: percentage rate already included
			);
		}

		// Safety first.
		if ( empty( $tax_string_array ) ) {

			$free_of_taxes = false;
			
			if ( $order ) {

				$free_of_taxes = ! ( $order->get_taxes > 0.0 );

			} else {

				$free_of_taxes = ! ( floatval( WC()->cart->get_taxes() ) > 0.0 );

			} 

			if ( $free_of_taxes ) {
				
				return apply_filters( 'wgm_zero_tax_rate_message', '', 'total_tax' );

			} else {
				
				$default_vat_label = get_option( WGM_Helper::get_wgm_option( 'wgm_default_tax_label' ), __( 'VAT', 'woocommerce-german-market' ) );
				
				if ( $default_vat_label == '' ) {
					$default_vat_label = __( 'VAT', 'woocommerce-german-market' );
				}

				$tax_string_array[] = sprintf(
					'<span class="amount">%s</span> %s',
					wc_price( 0.0 ),
					$default_vat_label
				);

			}

		}

		$tax_line         = '';
		$tax_string       = '';
		$tax_line_class   = '';
		$tax_total_string = '';

		/**
		 * Itemized displaying of taxes.
		 */
		if ( $tax_total_display == 'itemized' ) {

			foreach ( $tax_string_array as $tax_string ) {

				// Tax included.
				if ( $tax_display == 'incl' ) {

					$tax_line       = sprintf(
					/* translators: %s: tax included */
						__( 'Includes %s', 'woocommerce-german-market' ),
						$tax_string
					);
					$tax_line_class = 'wgm-tax includes_tax';

				} else { // Tax to be added.

					$tax_line       = sprintf(
					/* translators: %s: tax to be added */
						__( 'Plus %s', 'woocommerce-german-market' ),
						$tax_string
					);
					$tax_line_class = 'wgm-tax excludes_tax';
				}

				// Append tax line to total.
				$tax_total_string .= sprintf( '<br class="wgm-break" /><span class="%s">', $tax_line_class );
				$tax_total_string .= $tax_line;
				$tax_total_string .= '</span>';
			}

			/**
			 * Single displaying of taxes.
			 */
		} else {

			$tax_string = sprintf(
				'<span class="amount">%s</span> %s',
				wc_price( $tax_total ),
				WGM_Helper::get_default_tax_label()
			);

			// Tax included.
			if ( $tax_display == 'incl' ) {

				$tax_line       = sprintf(
				/* translators: %s: tax included */
					__( 'Includes %s', 'woocommerce-german-market' ),
					$tax_string
				);
				$tax_line_class = 'wgm-tax includes_tax';

			} else { // Tax to be added.

				$tax_line       = sprintf(
				/* translators: %s: tax to be added */
					__( 'Plus %s', 'woocommerce-german-market' ),
					$tax_string
				);
				$tax_line_class = 'wgm-tax excludes_tax';
			}

			// Append tax line to total.
			$tax_total_string .= sprintf( '<span class="%s">', $tax_line_class );
			$tax_total_string .= $tax_line;
			$tax_total_string .= '</span>';

		}

		/**
		 * Output
		 */
		return apply_filters( 'wgm_get_totals_tax_string', $tax_total_string, $tax_string_array, $tax_totals, $tax_display );
	}

	/**
	 * @param $product
	 *
	 * @deprecated
	 * @return array
	 */
	public static function get_price_per_unit_data( $product ) {
		_doing_it_wrong( __CLASS__.'::'.__FUNCTION__, 'This method was moved. Use WGM_Price_Per_Unit::get_price_per_unit_data instead', 'WGM 2.6.7' );

		return WGM_Price_Per_Unit::get_price_per_unit_data( $product );
	}

	/**
	 * @deprecated
	 *
	 * @param $product
	 *
	 * @return string
	 */
	public static function text_including_tax( $product ) {
		_doing_it_wrong( __CLASS__.'::'.__FUNCTION__, 'This method was moved. Use WGM_Tax::text_including_tax instead', 'WGM 2.6.7' );

		return WGM_Tax::text_including_tax( $product );
	}

	/**
	 * @param $product
	 * @deprecated
	 * @return string
	 */
	public static function shipping_page_link( $product ) {
		_doing_it_wrong( __CLASS__.'::'.__FUNCTION__, 'This method was moved. Use WGM_Shipping::shipping_page_link instead', 'WGM 2.6.7' );

		return WGM_Shipping::shipping_page_link($product);
	}

	/**
	 * Avoid products that are for free in checkout
	 *
	 * @version 3.0.2
	 * @wp-hook woocommerce_checkout_process
	 * @return void
	 */
	public static function avoid_free_items_in_cart() {

		if ( get_option( 'woocommerce_de_avoid_free_items_in_cart', 'off' ) == 'on' ) {
			$cart = WC()->cart->get_cart();
			$has_free_items = false;
			foreach ( $cart as $item ) {
				$line_total = $item[ 'line_subtotal' ];
				if ( ! floatval( $line_total ) > floatval( apply_filters( 'woocommerce_de_avoid_free_items_limit', 0.0 ) ) ) {
					$has_free_items = true;
					break;
				}

			}

			if ( $has_free_items ) {
				wc_add_notice( get_option( 'woocommerce_de_avoid_free_items_in_cart_message' ), 'error' );
			}
		}

	}

	/**
	 * VC Compability
	 *
	 * @since GM v3.2
	 * @param String $content
	 * @return $content
	 */
	public static function remove_vc_shortcodes( $content ) {

		if ( apply_filters( 'wgm_vc_remove_shortcodes', true ) ) {
            
            $regexes = array(
                "^\[(\/|)vc_(.*)\]^",
                "^\[(\/|)av_(.*)\]^",
            );

            $regexes = apply_filters( 'wgm_vc_regexes', $regexes );

            foreach ( $regexes as $regex ) {
                $content = preg_replace( $regex, '', $content );
            }

        }

        return $content;
	}

	/**
	 * Deactivate Phone as a required field during checkout
	 *
	 * @since GM v3.2
	 * @wp-hook woocommerce_billing_fields
	 * @param Array $address_fields
	 * @return $address_fields
	 */
	public static function required_tel_number( $address_fields ) {
		
		if ( get_option( 'woocommerce_de_phone_is_required_field_in_checkout', 'on' ) != 'on' ) {
			$address_fields[ 'billing_phone' ][ 'required' ] = false;
		}

		return $address_fields;
	}

	/**
	 * Get Terms And Conditions Checkbox
	 *
	 * @since GM v3.2
	 * @wp-hook woocommerce_de_add_review_order
	 * @return void
	 */
	public static function terms_and_conditions() {
		wc_get_template( 'checkout/terms.php' );
	}

	/**
	 * German Market Product Informations in Widgets
	 *
	 * @since GM v3.2
	 * @wp-hook woocommerce_after_template_part
	 * @param String $template_name
	 * @return void
	 */
	public static function widget_after_content_product( $template_name, $template_path, $located, $args ) {
			
		if ( $template_name == 'content-widget-product.php' ) {
			
			global $product;
			$hook = 'loop';
			$output_parts = array();
			
			$output_parts = apply_filters( 'wgm_product_summary_parts', $output_parts, $product, $hook );
			$output_parts = WGM_Shipping::add_shipping_part( $output_parts, $product ); // this class inits the filter with less parameters, so call it manually
			
			// unset price in widgets, because it's already there
			if ( isset( $output_parts[ 'price' ] ) ) {
				unset( $output_parts[ 'price' ] );
			}

			$output_html  = implode( $output_parts );
			
			$output_html = apply_filters( 'wgm_product_summary_html', $output_html, $output_parts, $product, $hook );
			$output_html = apply_filters( 'wgm_product_summary_html_in_widget', $output_html, $output_parts, $product, $hook );

			echo $output_html;

		}

	}

	/**
	 * German Market Product Informations in Mini Cart
	 *
	 * @since GM v3.2
	 * @wp-hook woocommerce_widget_cart_item_quantity
	 * @param String $html_string
	 * @param Array $cart_item
	 * @param String $cart_item_key
	 * @return void
	 */
	public static function mini_cart_price( $html_string, $cart_item, $cart_item_key ) {

		$product      = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
		$hook = 'loop';
		$output_parts = array();

		$output_parts = apply_filters( 'wgm_product_summary_parts', $output_parts, $product, $hook );

		// unset price in widgets, because it's already there
		if ( isset( $output_parts[ 'price' ] ) ) {
			unset( $output_parts[ 'price' ] );
		}

		// unset shipping in mini cart, because it's already there
		if ( isset( $output_parts[ 'shipping' ] ) ) {
			unset( $output_parts[ 'shipping' ] );
		}

		// unset ppu in mini cart, because it's already there
		if ( isset( $output_parts[ 'ppu' ] ) ) {
			unset( $output_parts[ 'ppu' ] );
		}

		$output_html = implode( $output_parts );
		$output_html = apply_filters( 'wgm_product_summary_html', $output_html, $output_parts, $product, $hook );
		$output_html = apply_filters( 'wgm_product_summary_html_in_widget', $output_html, $output_parts, $product, $hook );

		return $html_string . $output_html;

	}

	/**
	* Delivery Time in Checkout: Add item meta
	*
	* @wp-hook woocommerce_add_cart_item_data
	* @since GM v3.2
	* @static
	* @access public
	* @param Array $cart_item_data
	* @param Integer $product_id
	* @param Integer $variation_id
	* @return Array
	**/
	public static function delivery_time_co_woocommerce_add_cart_item_data( $cart_item_data, $product_id, $variation_id ) {

		$id = ( $variation_id && $variation_id > 0 ) ? $variation_id : $product_id;
		$product = wc_get_product( $id  );
		
		$delivery_time_string = self::get_deliverytime_string( $product );

		if ( $delivery_time_string != '' ) {
			$cart_item_data[ 'gm_delivery_time' ] = $delivery_time_string;
		}

		return $cart_item_data;
	}

	/**
	* Delivery Time in Checkout: Add item meta from session
	*
	* @wp-hook woocommerce_add_cart_item_data
	* @since GM v3.2
	* @static
	* @access public
	* @param Array $cart_item_data
	* @param Array $cart_item_session_data
	* @param String $cart_item_key
	* @return Array
	**/
	public static function delivery_time_co_woocommerce_get_cart_item_from_session( $cart_item_data, $cart_item_session_data, $cart_item_key ) {

		if ( isset( $cart_item_session_data[ 'gm_delivery_time' ] ) ) {
	        $cart_item_data[ 'gm_delivery_time' ] = $cart_item_session_data[ 'gm_delivery_time' ];
	    }

		return $cart_item_data;
	}

	/**
	* Delivery Time in Checkout: Show Item Meta in Checkout
	*
	* @wp-hook woocommerce_add_cart_item_data
	* @since GM v3.2
	* @static
	* @access public
	* @param Array $data
	* @param Array $cart_item
	* @return Array
	**/
	public static function delivery_time_co_woocommerce_get_item_data( $data, $cart_item ) {
		
		if ( isset( $cart_item[ 'gm_delivery_time' ] ) ) {

			$label = apply_filters( 'gm_delivery_time_label_in_checkout', __( 'Delivery Time:', 'woocommerce-german-market' ) );

			// no ':' at the end of the string, this will be added by woocommerce itself
			if ( substr( $label, -1 ) == ':' ) {
				$label = substr( $label, 0, -1 );
			}

	        $data[] = array(
	            'name' => $label,
	            'value' => $cart_item[ 'gm_delivery_time' ]
	        );
	    }

		return $data;
	}

	/**
	* Attributes in product names: Cart, Checkout
	*
	* @wp-hook woocommerce_cart_item_name
	* @since GM v3.4
	* @static
	* @access public
	* @param String $name
	* @param Array $cart_item
	* @param String $cart_item_key
	* @return String
	**/
	public static function attribute_in_product_name( $name, $cart_item, $cart_item_key ) {

		if ( get_option( 'german_market_attribute_in_product_name', 'off' ) == 'off' ) {

			$_product = $cart_item[ 'data' ];
			$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );

			if ( is_object( $_product ) && $_product->is_type( 'variation' ) ) {
				if ( ! $product_permalink ) {
					$name = $_product->get_title();
				} else {

					if ( is_cart() ) {
						$name =  sprintf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $_product->get_title() );
					} else {
						$name = $_product->get_title();
					}


				}
			}

		}

		return $name;
	}

	/**
	* Attributes in product names: Order name
	*
	* @wp-hook woocommerce_order_item_name
	* @since GM v3.4
	* @static
	* @access public
	* @param String $name
	* @param WC_Order_Item_Product $item
	* @param Boolean $is_visible
	* @return String
	**/
	public static function attribute_in_product_name_order( $name, $item, $is_visible = false ) {

		if ( get_option( 'german_market_attribute_in_product_name', 'off' ) == 'off' ) {

			$_product 			= $item->get_product();
			$order 				= $item->get_order();

			$product_permalink 	= apply_filters( 'woocommerce_order_item_permalink', $is_visible ? $_product->get_permalink( $item ) : '', $item, $order );

			if ( is_object( $_product ) && $_product->is_type( 'variation' ) ) {
				if ( ! $product_permalink ) {
					$name = $_product->get_title();
				} else {
					$name =  sprintf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $_product->get_title() );
				}
			}

		}

		return $name;
		
	}

	/**
	* No user login in second checkout
	*
	* @wp-hook option_woocommerce_enable_checkout_login_reminder
	* @since GM v3.4.1
	* @static
	* @access public
	* @param String $value
	* @param String $option
	* @return String
	**/
	public static function remove_login_from_second_checkout( $value, $option ) {

		if ( defined( 'WGM_CHECKOUT' ) && WGM_CHECKOUT === true ) {
			$value = 'no';
		}

		return $value;
	}

}
