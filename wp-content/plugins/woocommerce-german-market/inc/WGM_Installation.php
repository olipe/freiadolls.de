<?php
/**
 * Installation
 *
 * @author jj,ap
 */
Class WGM_Installation {

	/**
	* Activation of the plugin, and create the default pages and Prefereces
	* @access	public
	* @static
	* @uses		version_compare, deactivate_plugins, wp_sprintf, update_option
	* @return	void
	*/
	public static function on_activate() {
		global $wpdb;

		// check wp version
		if ( ! version_compare( $GLOBALS[ 'wp_version' ], '4.7', '>=' ) ) {
			deactivate_plugins( Woocommerce_German_Market::$plugin_filename );
			die(
				__( 'German Market requires WordPress 4.7+. Please update your WordPress installation to a recent version first, then try activating German Market again.', 'woocommerce-german-market' )
			);
		}

		// check php version
		if ( version_compare( PHP_VERSION, '5.6.0', '<' ) ) {
			deactivate_plugins( Woocommerce_German_Market::$plugin_filename ); // Deactivate ourself
			die(
				sprintf(
					__( 'WooCommerce German Market requires PHP 5.6+. Your server is currently running PHP %s. Please ask your web host to upgrade to a recent, more stable version of PHP.', 'woocommerce-german-market' ),
					PHP_VERSION
				)
			);
		}

        // test if woocommerce is installed
        if ( Woocommerce_German_Market::is_wc_3_0() !== true ) {
            deactivate_plugins( Woocommerce_German_Market::$plugin_filename ); // Deactivate ourself
            die(
			    __( 'German Market requires WooCommerce 3.0.0+. Please install a recent version of WooCommerce first, then try activating German Market again.', 'woocommerce-german-market' )
            );
        }

        // set the status to installed
		update_option( WGM_Helper::get_wgm_option( 'woocommerce_options_installed' ), 0 );

		// install the default options
		WGM_Installation::install_default_options();

		// Activate Add-ons
		$all_add_ons = WGM_Add_Ons::get_all_add_ons();

		foreach ( $all_add_ons as $add_on_id => $add_on_file ) {
			
			// include files
			require_once( $add_on_file );

			// get class name
			$add_on_class = WGM_Add_ons::get_class_name( $add_on_id );
			
			// if method 'active' exists in add-on
			if ( method_exists( $add_on_class, 'activate' ) ) {

				// run this method
				call_user_func( array( $add_on_class, 'activate' ) );
			}
		}
		
	}

	/**
	* Deactivation of the plugin
	* @static
	* @return	void
	*/
	public static function on_deactivate() {

		// Dectivate Add-ons
		$all_add_ons = WGM_Add_Ons::get_all_add_ons();

		foreach ( $all_add_ons as $add_on_id => $add_on_file ) {
			
			// include files
			require_once( $add_on_file );

			// get class name
			$add_on_class = WGM_Add_ons::get_class_name( $add_on_id );
			
			// if method 'active' exists in add-on
			if ( method_exists( $add_on_class, 'deactivate' ) ) {

				// run this method
				call_user_func( array( $add_on_class, 'deactivate' ) );
			}
		}

	}

	/**
	* Handle install notice
	*
	* @access	public
	* @static
	* @uses		is_plugin_inactive, deactivate_plugins, get_option, wp_verify_nonce, update_option
	* @return	void
	*/
	public static function install_notice() {

		if( 1 == get_option( WGM_Helper::get_wgm_option( 'woocommerce_options_installed' ) ) )
			return;

		if ( get_option( 'woocommerce_de_previous_installed' ) == 1 )
			return;

		if( isset( $_REQUEST[ 'woocommerce_de_install' ] ) && wp_verify_nonce( $_REQUEST[ '_wpnonce' ] ) ) {

			$overwrite = isset( $_REQUEST[ 'woocommerce_de_install_de_pages_overwrite' ] );

			if ( isset( $_REQUEST[ 'woocommerce_de_install_de_options' ] ) )
				WGM_Installation::install_de_options();

			if ( isset( $_REQUEST[ 'woocommerce_de_install_de_pages' ] ) )
				WGM_Installation::install_default_pages( $overwrite );

			// activate add-ons for legal texts
			if ( isset( $_REQUEST[ 'woocommerce_de_activate_protected_shops' ] ) ) {
				update_option( 'wgm_add_on_protected_shops', 'on' );
			}

			if ( isset( $_REQUEST[ 'woocommerce_de_activate_it_recht_kanzlei' ] ) ) {
				update_option( 'wgm_add_on_it_recht_kanzlei', 'on' );
			}

			// set woocommerce de installed
			update_option( WGM_Helper::get_wgm_option( 'woocommerce_options_installed' ), 1 );

			?>
			<div class="updated">
				<p><?php _e( 'Congratulations, you have successfully installed WooCommerce German Market.', 'woocommerce-german-market' ); ?></p>
			</div>
			<?php

			WGM_Helper::update_option_if_not_exist( 'woocommerce_de_previous_installed', 1 );

			return;
		}

		WGM_Template::load_template( 'install_notice.php' );
	}

	/**
	* Install default options
	*
	* @uses update_option
	* @static
	* @author jj, ap
	* @return void
	*/
	public static function install_default_options() {

		WGM_Helper::update_option_if_not_exist( WGM_Helper::get_wgm_option( 'woocommerce_de_append_imprint_to_mail' ), 'on' );
		WGM_Helper::update_option_if_not_exist( WGM_Helper::get_wgm_option( 'woocommerce_de_append_withdraw_to_mail' ), 'on' );
		WGM_Helper::update_option_if_not_exist( WGM_Helper::get_wgm_option( 'woocommerce_de_append_terms_to_mail' ), 'on' );
		WGM_Helper::update_option_if_not_exist( WGM_Helper::get_wgm_option( 'load_woocommerce-de_standard_css' ), 'on' );
		WGM_Helper::update_option_if_not_exist( WGM_Helper::get_wgm_option( 'woocommerce_de_show_Widerrufsbelehrung' ), 'on' );
		WGM_Helper::update_option_if_not_exist( WGM_Helper::get_wgm_option( 'wgm_display_digital_revocation' ), 'on' );
		WGM_Helper::update_option_if_not_exist( WGM_Helper::get_wgm_option( 'wgm_use_split_tax' ), 'on' );

		update_option( 'woocommerce_price_thousand_sep', '.' );
		update_option( 'woocommerce_price_decimal_sep', ',' );
	}

	/**
	* Install german specific options
	*
	* @access public
	* @author jj, ap
	* @static
	* @return void
	*/
	public static function install_de_options() {

		// set currency and country to EUR and germany
		update_option( 'woocommerce_currency', apply_filters( 'woocommerce_de_currency', WGM_Defaults::$woocommerce_de_currency ) );
		
		$base_country = get_option( 'woocommerce_default_country', 'DE' );
		if ( $base_country != 'DE' && $base_country != 'AT' ) {
			update_option( 'woocommerce_default_country', apply_filters( 'woocommerce_de_default_country', WGM_Defaults::$woocommerce_de_default_country ) );
		}
		
		// When having a fresh woocommerce2 installation, we should copy into the new database

		WGM_Installation::set_default_woocommerce2_tax_rates( WGM_Defaults::get_default_tax_rates() );

		update_option( 'woocommerce_prices_include_tax', apply_filters( 'woocommerce_de_prices_include_tax', 'yes' ) );

		// tax default settings
		// make tax calculation default
		update_option( 'woocommerce_calc_taxes', 'yes' );
		// was deleted in woocommerce2 see: https://github.com/woothemes/woocommerce/commit/9eb63a8518448fe0e99820ba924f2bee850e9ddc#L0R1031
		//update_option( 'woocommerce_display_cart_prices_excluding_tax', apply_filters( 'woocommerce_de_display_cart_prices_excluding_tax', 'no' ) );
		update_option( 'woocommerce_tax_display_cart', apply_filters( 'woocommerce_de_woocommerce_tax_display_cart', 'incl' ) );
		// was also deleted and not used by WC German Market
		//update_option( 'woocommerce_display_totals_excluding_tax', apply_filters( 'woocommerce_de_display_totals_excluding_tax', 'no' ) );

		// install product attribues
		WGM_Installation::install_default_attributes();
	}

	/**
	 * Set the default tax rates for Woocommerce
	 * Copied from Woocommerce2 Core file admin/includes/updates/woocommerce-update-2.0.php
	 *
	 * @access	public
	 * @static
	 * @global	$wpdb
	 * @since	1.1.5
	 * @param	$tax_rates
	 * @return	array
	 */
	public static function set_default_woocommerce2_tax_rates( $tax_rates ) {

        $name = "tax-rates-en.csv";

        if ( get_locale() == 'de_DE' ) {
            $name = "tax-rates-de.csv";
        }

        $base_country = get_option( 'woocommerce_default_country', 'DE' );
		if ( $base_country == 'AT' ) {
			 $name = "tax-rates-at.csv";
		}

		$file = dirname( plugin_dir_path( __FILE__ ) ) . '/import/' . $name;

		if ( ! is_file( $file ) ) {
			die( __( 'WooCommerce German Market tax rates (csv) not found. Please contact our support staff to help you get going with this.', 'woocommerce-german-market' ) );
		}

		self::import_csv( $file );
	}


    /**
     * Import rates form CSV file.
     *
     * * WC_Tax_Rate_Importer::import uses WP_Importer as a hard
     *  dependency and does various outputs. This method prevents
     *  those circumstances.
     *
     * @author dw
     * @access public
     * @static
     * @param string $file
     * @return void
     */
    public static function import_csv( $file ) {

		global $wpdb;

		$new_rates = array();

		ini_set( 'auto_detect_line_endings', '1' );

		if ( ( $handle = fopen( $file, "r" ) ) !== FALSE ) {

			$header = fgetcsv( $handle, 0 );

			if ( sizeof( $header ) == 10 ) {

				$loop = 0;

				while ( ( $row = fgetcsv( $handle, 0 ) ) !== FALSE ) {

					list( $country, $state, $postcode, $city, $rate, $name, $priority, $compound, $shipping, $class ) = $row;

					$country = trim( strtoupper( $country ) );
					$state   = trim( strtoupper( $state ) );

					if ( $country == '*' )
						$country = '';
					if ( $state == '*' )
						$state = '';
					if ( $class == 'standard' )
						$class = '';

					$wpdb->insert(
						$wpdb->prefix . "woocommerce_tax_rates",
						array(
							'tax_rate_country'  => $country,
							'tax_rate_state'    => $state,
							'tax_rate'          => wc_format_decimal( $rate, 4 ),
							'tax_rate_name'     => trim( $name ),
							'tax_rate_priority' => absint( $priority ),
							'tax_rate_compound' => $compound ? 1 : 0,
							'tax_rate_shipping' => $shipping ? 1 : 0,
							'tax_rate_order'    => $loop,
							'tax_rate_class'    => sanitize_title( $class )
						)
					);

					$tax_rate_id = $wpdb->insert_id;

					$postcode  = wc_clean( $postcode );
					$postcodes = explode( ';', $postcode );
					$postcodes = array_map( 'strtoupper', array_map( 'wc_clean', $postcodes ) );
					foreach( $postcodes as $postcode ) {
						if ( ! empty( $postcode ) && $postcode != '*' ) {
							$wpdb->insert(
								$wpdb->prefix . "woocommerce_tax_rate_locations",
								array(
									'location_code' => $postcode,
									'tax_rate_id'   => $tax_rate_id,
									'location_type' => 'postcode',
								)
							);
						}
					}

					$city   = wc_clean( $city );
					$cities = explode( ';', $city );
					$cities = array_map( 'strtoupper', array_map( 'wc_clean', $cities ) );
					foreach( $cities as $city ) {
						if ( ! empty( $city ) && $city != '*' ) {
							$wpdb->insert(
							$wpdb->prefix . "woocommerce_tax_rate_locations",
								array(
									'location_code' => $city,
									'tax_rate_id'   => $tax_rate_id,
									'location_type' => 'city',
								)
							);
						}
					}

					$loop ++;
			    }

			}

		    fclose( $handle );
		}
	}

	/**
	* install default product attributes
	*
	* @author jj, ap
	* @access public
	* @static
	* @uses globals $woocommerce, $wpdb, register_taxonomy, taxonomy_exists, get_option, sanitize_title, term_exists, wp_insert_term
	* @return void
	*/
	public static function install_default_attributes() {

		global $woocommerce, $wpdb;

		foreach( WGM_Defaults::get_default_product_attributes() as $attr )
			$new_tax_name = wc_attribute_taxonomy_name( $attr[ 'attribute_name' ] );
			if ( !taxonomy_exists( $new_tax_name ) ) {
				// insert attributes
				$wpdb->insert(
					$wpdb->prefix . "woocommerce_attribute_taxonomies",
					array(
						'attribute_name'  => $attr[ 'attribute_name' ],
						'attribute_label' => $attr[ 'attribute_label' ],
						'attribute_type'  => $attr[ 'attribute_type' ]
					),
					array( '%s', '%s', '%s' )
				);

				$category_base = '';
				$label = $attr[ 'attribute_label' ];
				$hierarchical = true;

				register_taxonomy( $new_tax_name,
						array( 'product' ),
						array(
							'hierarchical'          => $hierarchical,
							'labels' => array(
								'name'              => $label,
								'singular_name'     => $label,
								'search_items'      => sprintf( __( 'Search %s', 'woocommerce-german-market' ), $label ),
								'all_items'         => sprintf( __( 'All %s', 'woocommerce-german-market' ), $label ),
								'parent_item'       => sprintf( __( 'Parent %s', 'woocommerce-german-market' ), $label ),
								'parent_item_colon' => sprintf( __( 'Parent %s:', 'woocommerce-german-market' ), $label ),
								'edit_item'         => sprintf( __( 'Edit %s', 'woocommerce-german-market' ), $label ),
								'update_item'       => sprintf( __( 'Update %s', 'woocommerce-german-market' ), $label ),
								'add_new_item'      => sprintf( __( 'Add New %s', 'woocommerce-german-market' ), $label ),
								'new_item_name'     => sprintf( __( 'New %s', 'woocommerce-german-market' ), $label )
							),
							'show_ui'               => false,
							'query_var'             => true,
							'show_in_nav_menus'     => false,
							'rewrite'               => array( 'slug' => $category_base . strtolower( sanitize_title( $attr[ 'attribute_name' ] ) ), 'with_front' => false, 'hierarchical' => $hierarchical ),
						)
				);
			}

			foreach( $attr[ 'elements' ] as $element ) {

				$params = array_merge( $element , array( 'post_type' => 'product' ) );
				if( ! term_exists( $element[ 'tag-name' ], $new_tax_name ) )
					wp_insert_term( $element[ 'tag-name' ], $new_tax_name , $params );
			}

		// remove the transient to renew the cache
		delete_transient( 'wc_attribute_taxonomies' );
	}

	/**
	* insert the default pages, and overwrite existing pages, if wanted.
	*
	* @author jj, ap
	* @access public
	* @static
	* @uses globals $wpdb, apply_filters, wp_insert_post, wp_update_post, update_option
	* @param bool $overwrite overwrite existing pages
	* @return void
	*/
	public static function install_default_pages( $overwrite = FALSE ) {
		global $wpdb;
		// filter for change/add pages on auto insert on activation
		$pages = apply_filters( 'woocommerce_de_insert_pages', WGM_Helper::get_default_pages() );
	
		foreach ( $pages as $page ) {
			$check_sql = "SELECT ID, post_name FROM $wpdb->posts WHERE post_name = %s LIMIT 1";

			$post_name_db = str_replace( '&', '', $page[ 'post_name' ] );
			$post_name_check = $wpdb->get_row( $wpdb->prepare( $check_sql, $post_name_db ), ARRAY_A );

			$post_id = NULL;

			// only if not page exist, add page
			if ( $post_name_db !== $post_name_check[ 'post_name' ] ) {
                $post_id = wp_insert_post( $page );
			} else {
               // overwrite the content of the old pages
               $post_id = $post_name_check[ 'ID' ];
				if( $overwrite ) {
					$page[ 'ID' ] = $post_id;
					wp_update_post( $page );
				}
			}

			// insert default option
			if( $post_id && in_array( WGM_Defaults::get_german_option_name( $page[ 'post_name' ] ), array_keys( WGM_Defaults::get_options() ) ) ) {
				update_option( WGM_Helper::get_wgm_option( $page[ 'post_name' ] ), $post_id );

			}
		}

	}

	/**
	* delete options on uninstall
	*
	* @author fb
	* @static
	* @access public
	* @uses delete_option
	*/
	public static function on_uninstall() {
		
		// unistall add_ons
		define( 'WGM_UNINSTALL_ADD_ONS', TRUE );
		WGM_Add_Ons::uninstall();

		// uninstall WGM options
		foreach ( WGM_Defaults::get_options() as $key => $option ) {
			delete_option( $option );
		}

		// clean all
		$prefixes = array(
			'wgm_',
			'wgm-',
			'woocommerce_de_',
		);

		$all_wordpress_options = wp_load_alloptions();

		foreach ( $prefixes as $prefix ) {

			$length_of_prefix = strlen( $prefix );

			foreach ( $all_wordpress_options as $option_key => $option_value ) {
				
				if ( substr( $option_key, 0, $length_of_prefix ) == $prefix ) {
					delete_option( $option_key );
				}

			}

		}
	}

    /**
     * Update routine to imigrate old deliverytime to new format. Used for upgrade form version 2.2.3 to 2.2.4
     * @access public
     * @static
     * @author ap
     * @reutrn void
     */
    public static function upgrade_deliverytimes(){

		$option = 'wgm_upgrade_deliverytimes';

		if( !get_option( $option, false ) ) {

			add_option( $option, true );

			$terms = get_terms( 'product_delivery_times', array( 'orderby' => 'id', 'hide_empty' => 0 ) );
			$old_terms = WGM_Defaults::get_lieferzeit_strings();

			if( count( $old_terms ) > count( $terms ) ) {
				$missing = new stdClass();
				$missing->term_id = -1;
				array_unshift( $terms, $missing );
			}

			$products = get_posts( array( 'post_type' => 'product', 'posts_per_page' => -1 ) );

			foreach( $products as $product ) {

				$deliverytime_index = get_post_meta( $product->ID, '_lieferzeit', TRUE );

				// Don't change the default delivery time
				if( (int) $deliverytime_index == -1 ) continue;

				if( ! array_key_exists( $deliverytime_index, $terms ) )
					$term_id = -1;
				else
					$term_id = $terms[ $deliverytime_index ]->term_id;

				update_post_meta( $product->ID, '_lieferzeit', $term_id );
			}

			$global_delivery = get_option( WGM_Helper::get_wgm_option( 'global_lieferzeit' ) );
			$term_id = $terms[ $global_delivery ]->term_id;

			update_option( WGM_Helper::get_wgm_option( 'global_lieferzeit' ), $term_id );
		}
	}

    /**
     * shows deliverytimes upgrade notice need for upgrade form 2.2.3 to 2.2.4
     * @access public
     * @static
     * @author ap
     * @return mixed
     */
    public static function upgrade_deliverytimes_notice() {

		if ( array_key_exists( 'woocommerce_de_upgrade_deliverytimes' , $_POST ) )
			update_option( 'wgm_upgrade_deliverytimes_notice_off', 1 );

		if ( get_option( 'woocommerce_de_previous_installed' ) === FALSE ) {
			update_option( 'wgm_upgrade_deliverytimes_notice_off', 1 );
			return false;
		}

		if( get_option( 'wgm_upgrade_deliverytimes_notice_off' ) )
			return false;


		$screen = get_current_screen();

		if( $screen->id != 'woocommerce_page_woocommerce_settings' )
			WGM_Template::load_template( 'deliverytimes_upgrade_notice.php' );
	}


    /**
     * upgrades for new v2.4
     */
    public static function upgrade_system(){
        if( ! get_option( 'wgm_upgrade_24', false ) ){

            //Shipping fees now have to be allways displayed
            update_option( WGM_Helper::get_wgm_option( 'woocommerce_de_show_shipping_fee_overview_single' ), 'on' );
            update_option( WGM_Helper::get_wgm_option( 'woocommerce_de_show_shipping_fee_overview' ), 'on' );

            update_option( 'wgm_upgrade_24', true );
        }

	    // Exclude checkout page from cache when updating to 2.4.10
	    if( ! get_option( 'wgm_upgrade_2410', false ) ) {
		    $wc_page_uris       = get_transient( 'woocommerce_cache_excluded_uris' );
		    $wgm_checkout_2     = absint( get_option( 'woocommerce_check_page_id' ) );
		    $wgm_checkout_uri   = 'p=' . $wgm_checkout_2;

		    $wc_page_uris[] = $wgm_checkout_uri;
		    $page = get_post( $wgm_checkout_2 );

		    if ( ! is_null( $page ) ) {
			    $wc_page_uris[] = '/' . $page->post_name;
		    }

		    set_transient( 'woocommerce_cache_excluded_uris', $wc_page_uris );
		    update_option( 'wgm_upgrade_2410', true );
	    }
    }

     /**
     * Update notice for GM 3.2: legal texts changed
     *
     * @access public
     * @static
     * @wp-hook admin_notices
     * @return void
     */
    public static function legal_texts_version_three_two() {

    	$class = 'notice notice-success is-dismissible gm-3-2-update-notice-legal-texts';
		$message = sprintf( __( '<b>Update Notice for German Market 3.2: Attention!</b> With the German Market Update 3.2 the legal texts were updated. Please check the templates and customize your shop. For more information about the update of the legal texts, please visit our <a href="%s">site</a>.', 'woocommerce-german-market' ), 'https://marketpress.de/documentation/german-market/rechtstexteupdate/' );
		printf( '<div class="%1$s"><p>%2$s</p></div>', $class, $message ); 

    }

    /**
     * Dismiss Update Notice
     *
     * @access public
     * @static
     * @wp-hook wp_ajax_woocommerce_de_dismiss_update_notice_legal_texts
     * @return void
     */
    public static function legal_texts_version_dismiss() {
    	update_option( 'woocommerce_de_update_legal_texts', 'off' );
    	exit();
    }

    /**
     * Licence Information
     *
     * @access public
     * @static
     * @wp-hook admin_notices
     * @return void
     */
    public static function licence_notices() {
    	
    	$licence_expires = get_option( 'german_market_access_expires' );
    	$today = new DateTime();
    	$licence_expires_date = new DateTime( $licence_expires );
    	$interval = $today->diff( $licence_expires_date );
		$days =  intval( $interval->format( '%R%a' ) );

		if ( $days >= 0 && $days <= 30 ) {
		
			if ( get_option( 'german_market_access_expires_first' ) != $licence_expires ) {

				$class = 'notice notice-warning is-dismissible gm-3-2-expires-one';
				
				if ( $days > 1 ) {
					$message = sprintf( __( '<b>Your German Market licence is going to expire in %s days.', 'woocommerce-german-market' ), $days );
				} else if ( $days == 1 ) {
					$message = sprintf( __( '<b>Your German Market licence is going to expire in one day.', 'woocommerce-german-market' ), $days );
				} else if ( $days == 0 ) {
					$message = sprintf( __( '<b>Your German Market licence is going to expire today.', 'woocommerce-german-market' ), $days );
				}
				
				$message .= ' ' . __( 'Please renew your German Market license immediately, so that your shop keeps to be legally safe.</b>', 'woocommerce-german-market' );
				printf( '<div class="%1$s"><p>%2$s</p></div>', $class, $message ); 

			} 

		} else if ( $days < 0 ) {

			if ( get_option( 'german_market_access_expires_second' ) != $licence_expires ) {
				
				$class = 'notice notice-error is-dismissible gm-3-2-expires-two';
				$message = __( '<b>Warnung! Your German Market licence has been expired. You run the risk of being warned with your shop. Please renew your German Market license immediately.</b>', 'woocommerce-german-market' );
				printf( '<div class="%1$s"><p>%2$s</p></div>', $class, $message );

			}

		}

    }

     /**
     * Load JavaScript so you can dismiss the update notice
     *
     * @access public
     * @static
     * @wp-hook admin_enqueue_scripts
     * @return void
     */
    public static function licence_notices_scripts() {
    	wp_enqueue_script( 'woocommerce_de_licence', plugins_url( '/js/WooCommerce-German-Market-Licence.js', Woocommerce_German_Market::$plugin_base_name ), array( 'jquery' ) );
    	wp_localize_script( 'woocommerce_de_licence', 'gm_licence_ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
    }

     /**
     * Dismiss Licence One Notice
     *
     * @access public
     * @static
     * @wp-hook woocommerce_de_dismiss_licence_notice_one
     * @return void
     */
    public static function licence_notices_one_dismiss() {
    	$licence_expires = get_option( 'german_market_access_expires' );
    	update_option( 'german_market_access_expires_first', $licence_expires );
    	exit();
    }

     /**
     * Dismiss Licence Two Notice
     *
     * @access public
     * @static
     * @wp-hook woocommerce_de_dismiss_licence_notice_two
     * @return void
     */
    public static function licence_notices_two_dismiss() {
    	$licence_expires = get_option( 'german_market_access_expires' );
    	update_option( 'german_market_access_expires_second', $licence_expires );
    	exit();
    }

     /**
     * WC 3.0.0+ Notice
     *
     * @access public
     * @static
     * @wp-hook admin_notices
     * @return void
     */
    public static function wc_3_0_0_notice() {
		$class = 'notice notice-error gm-3-2-2-wc-3-0-0';
		$message = __( '<b>German Market is activated, but not effective.</b> German Market requires WooCommerce 3.2.0+. Please install a recent version of WooCommerce first.', 'woocommerce-german-market' );
		printf( '<div class="%1$s"><p>%2$s</p></div>', $class, $message ); 
    }

    /**
     * PHP 5.6 Notice
     *
     * @access public
     * @static
     * @wp-hook admin_notices
     * @return void
     */
    public static function php_5_6_notice() {
		$class = 'notice notice-error gm-3-2-3-php-5-6';
		$message = sprintf( __( '<b>German Market is activated, but not effective.</b> German Market requires PHP 5.6+. Your server is currently running PHP %s. Please ask your web host to upgrade to a recent, more stable version of PHP.', 'woocommerce-german-market' ), PHP_VERSION );
		printf( '<div class="%1$s"><p>%2$s</p></div>', $class, $message ); 
    }

}
