<?php
/**
 * Helper Functions
 *
 * @author jj, ap
 */
class WGM_Helper {

	/**
	 * Returns the decimal length of any scalar value.
	 *
	 * @param   int|float|string|bool $value
	 * @return  int
	 */
	public static function get_decimal_length( $value ) {

		if ( ! is_scalar( $value ) ) {
			return 0;
		}

		$value = (float) $value;

		$value = strrchr( $value, "." );
		$value = substr( $value, 1 );
		$value = strlen( $value );

		return $value;
	}

	/**
	 * Tests against a specific version of Woocommerce
	 *
	 * @param string $min_version
	 *
	 * @return bool
	 */
	public static function woocommerce_version_check( $min_version = '2.5.0-beta' ) {
		return ( version_compare( WC()->version, $min_version === 0 ) );
	}

	/**
	 * Replaces umlauts etc.
	 *
	 * @param string $name
	 * @return mixed
	 */
	public static function get_page_slug( $name ){
		return str_replace( array( ' ', 'ä', 'ö', 'ü', 'Ä', 'Ö', 'Ü', 'ß', '&rarr;', '__', '&amp;' ) , array( '_', 'ae', 'oe', 'ue', 'ae', 'oe', 'ue', 'ss', '_', '', ''), strtolower( $name ) );
	}

	/**
	* get the page_id from db by name of page
	*
	* @access	public
	* @static
	* @param	string $page_name
	* @return	int page_id the page id
	*/
	public static function get_page_id( $page_name ) {
		global $wpdb;

		$page_id = $wpdb->get_var( 'SELECT
										ID
									FROM
										' . $wpdb->posts . '
									WHERE
										post_name = "' . $page_name . '"
									AND
										post_status = "publish"
									AND
										post_type = "page"'
								 );

		return (int) $page_id;
	}

	/**
	* Gets the url to the check page and then to checkout form the core plugin
	*
	* @access	public
	* @uses		get_option, get_permalink, is_ssl
	* @static
	* @return	string link to checkout page
	*/
	public static function get_check_url() {

		$check_page_id = get_option( WGM_Helper::get_wgm_option( 'check' ) );

		//WPML Support
		if( function_exists( 'icl_object_id' ) ) {
			$check_page_id = icl_object_id( $check_page_id, 'page', true );
		}

		$permalink     = get_permalink( $check_page_id );


		if ( is_ssl() )
			$permalink = str_replace( 'http:', 'https:', $permalink );

		return $permalink;
	}

	/**
	* gets the checkout page_id
	*
	* @access	public
	* @uses		get_option
	* @static
	* @return 	int checkout poge id
	*/
	public static function get_checkout_redirect_page_id() {
		return get_option( WGM_Helper::get_wgm_option( 'check' ) );
	}

	/**
	* get checkout page id via filter
	*
	* @param int $checkout_redirect_page_id checkout poge id
	* @return int checkout poge id
	*/
	public static function change_checkout_redirect_page_id ( $checkout_redirect_page_id ) {
		return apply_filters( 'woocommerce_de_get_checkout_redirect_page_id', WGM_Helper::get_checkout_redirect_page_id() );
	}

	/**
	* get the default pages
	*
	* @access public
	* @static
	* @author jj, ap
	* @return array default pages
	*/
	public static function get_default_pages() {

		// get data from current user for add pages with his ID
		$user_data = wp_get_current_user();

		foreach( WGM_Defaults::get_default_page_objects() as $page ){

			$default_pages[ $page->slug ] = array(
									'post_status'       => $page->status,
									'post_type'         => 'page',
									'post_author'       => (int) $user_data->data->ID,
									'post_name'         => $page->slug,
									'post_title'        => $page->name,
									'post_content'      => apply_filters( 'woocommerce_de_' . $page->slug . '_content', WGM_Template::get_text_template( $page->content ) ),
									'comment_status'    => 'closed'
								);
		}

		return $default_pages;
	}

	/**
	 * Deactivates Pay for order button
	 *
	 * @param  int $post_id The Post ID
	 * @return void
	 */
	public static function deactivate_pay_for_order_button( $post_id ) {
		?>
		<script type="text/javascript">
		jQuery(document).ready(function($) {
			$(".order_actions .button[name=invoice]").parent().hide();
		});
		</script>
		<?php
	}

	/**
	 * Determines wether to show shipping address or not
	 * @author jj, ap
	 * @static
	 * @return bool should show seperate shipping address or not
	 */
	public static function ship_to_billing() {
		if( ! WGM_Session::is_set( 'ship_to_different_address', 'first_checkout_post_array' ) || wc_ship_to_billing_address_only() ) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	/**
	 * Get an Option name for WooCommerce German Market
	 *
	 * @since	1.1.5
	 * @static
	 * @access	public
	 * @param	string $option_index
	 * @return	mixed string of option, when not exist FALSE
	 */
	public static function get_wgm_option( $option_index ) {

		// geht the default option array
		$options = WGM_Defaults::get_options();

		if( isset( $options[ $option_index ] ) )
			return  $options[ $option_index ];
		else
			return FALSE;
	}

	/**
	* update option if not exists
	*
	* @access	public
	* @static
	* @uses		update_option, get_option
	* @param 	string $option
	* @param 	string $value
	* @return	void
	*/
	public static function update_option_if_not_exist( $option, $value ) {
		if( ! get_option( $option ) )
			update_option( $option, $value );
	}

	/**
	 * Checks if the shop is configured to only ship to its base location
	 *
	 * ATTENTION: This is backported from WGM3 and is not currently in use.
	 * It's here for safekeeping as me might need this check here as well
	 *
	 * @return bool
	 */
	//public static function is_domestic_shop() {
	//
	//	if ( get_option( 'woocommerce_allowed_countries' ) !== 'specific' ) {
	//		return FALSE;
	//	}
	//	$base_location = get_option( 'woocommerce_default_country' );
	//
	//	$wc_countries = WC()->countries;
	//	if ( is_null( $wc_countries ) ) {
	//		$wc_countries = new WC_Countries();
	//	}
	//
	//	$allowed = $wc_countries->get_allowed_countries();
	//	if ( count( $allowed ) === 1 && isset( $allowed[ $base_location ] ) ) {
	//		return TRUE;
	//	}
	//
	//	return FALSE;
	//
	//}

	/**
	* inserts a given element before key into given array
	*
	* @access public
	* @author jj, ap
	* @param array $array
	* @param string $key
	* @param string $element
	* @return array items
	*/
	public function insert_array_before_key( $array, $key, $element ) {

		if( in_array( key( $element ), array_keys( $array ) ) )
			return $array;

		$position = array_search( $key ,array_keys( $array ) );
		$before   = array_slice( $array, 0, $position );
		$after    = array_slice( $array, $position );

		return array_merge( $before, $element, $after );
	}

	/**
	 * Adds bodyclass to second checkout
	 * @param array $classes
	 * @return array
	 * @author ap
	 */
	public static function add_checkout_body_classes( $classes) {

		global $woocommerce;

		$classes = ( array ) $classes;

		// id of the second checkout page
		$check_page_id = absint( get_option( WGM_Helper::get_wgm_option( 'check' ) ) );

		// current page id
		$current_id =  @get_the_ID();

		if( ! empty( $woocommerce ) && is_object( $woocommerce ) && $current_id == $check_page_id ) {
			$classes[] = 'woocommerce';
			$classes[] = 'woocommerce-checkout';
			$classes[] = 'woocommerce-page';
			$classes[] = 'wgm-second-checkout';
		}

		return $classes;
	}

	/**
	 * Enforced certain settings for the small business regulation setting.
	 * @author ap
	 * @return void
	 */
	public static function check_kleinunternehmerregelung(){

		if( get_option( WGM_Helper::get_wgm_option( 'woocommerce_de_kleinunternehmerregelung' ) ) == 'on' ){
			// Enforce that all prices do not include tax
			update_option( 'woocommerce_prices_include_tax', 'no' );
			// Don't calc the taxes
			update_option( 'woocommerce_calc_taxes', 'no' );
			// Display prices excluding taxes
			update_option( 'woocommerce_tax_display_shop', 'excl' );
			update_option( 'woocommerce_tax_display_cart', 'excl' );
		}
	}


	/**
	 * Filters and replaces deliveryimes
	 * @param string $string
	 * @param string $deliverytime
	 * @author ap
	 * @return string
	 */
	public static function filter_deliverytimes( $string, $deliverytime ){
		if( $deliverytime == __( 'available for immediate delivery', 'woocommerce-german-market' ) ) {
			/* translators: This is placed in the middle of a longer string, therefore lowercase in English. Should be merged into 1 longer string in a future version. */
			$string = ', ' . __( 'delivery time:', 'woocommerce-german-market' ) . ' ' . $deliverytime;
		}
		return $string;
	}

	/**
	 * Removes postcount on deliverytimes backend page
	 * @author ap
	 * @param array $cols
	 * @return array
	 */
	public static function remove_deliverytime_postcount_columns( $cols ){
		unset( $cols['posts'] );
		return $cols;
	}


	/**
	 * @since 3.0.2: filter does nothing if woocommerce-subscriptions is activated and we are in checkout or in cart 
	 *
	 *
	 * @author unknown
	 * @wp-hook woocommerce_countries_inc_tax_or_vat
	 * @wp-hook woocommerce_countries_ex_tax_or_vat
	 * @param String $return
	 * @return return
	 */
	public static function remove_woo_vat_notice( $return ){
		
		include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

		if ( apply_filters( 'gm_remove_woo_vat_notice_return_original_param', false ) ) {
			return $return;
		}
		
		if ( is_plugin_active( 'woocommerce-subscriptions/woocommerce-subscriptions.php' ) || is_plugin_active_for_network( 'woocommerce-subscriptions/woocommerce-subscriptions.php' ) ) {
			if ( ! ( is_cart() || is_checkout() ) ) {
				$return = '';
			}
		} else {
			$return = '';
		}

		return $return;
	}

	/**
	 * Adds additional info to the variation data used by the add-to-cart form
	 *
	 * @wp_hook woocommerce_available_variation
	 *
	 * @param $data
	 * @param $product
	 * @param $variation
	 *
	 * @return array
	 */
	public static function prepare_variation_data( $data, $product, $variation ) {

		remove_filter( 'wgm_product_summary_parts', array( 'WGM_Template', 'add_product_summary_price_part' ), 0 );
		//WGM_Template::add_template_loop_shop( $variation );
		$price_html = WGM_Template::get_wgm_product_summary( $variation );
		add_filter( 'wgm_product_summary_parts', array( 'WGM_Template', 'add_product_summary_price_part' ), 0, 3 );

		$data[ 'price_html' ] .= $price_html;

		// meta data
		ob_start();

		if ( apply_filters( 'gm_show_itemprop', false ) ) { ?>
			
			<div class="legacy-itemprop-offers" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
				<meta itemprop="price" content="<?php echo esc_attr( $variation->get_price() ); ?>" />
				<meta itemprop="priceCurrency" content="<?php echo esc_attr( get_woocommerce_currency() ); ?>" />
				<link itemprop="availability" href="http://schema.org/<?php echo $variation->is_in_stock() ? 'InStock' : 'OutOfStock'; ?>" />
			</div> 

			<?php

		} 

		$data[ 'price_html' ] .= ob_get_clean();

		return $data;
	}


	public static function is_digital( $product_id = 0 ){

		//When the product_id is an array get the first entry as the product id
		if( is_array($product_id) )
			$product_id = current($product_id);

		if( $product_id == 0 ){
			$product = wc_get_product();
		} else {
			$product = wc_get_product( $product_id );
		}

		$is_digital = null;

		if ( ! method_exists( $product, 'get_id' ) ) {
			return $is_digital;
		}

		if ( is_a( 'WC_Product_Variable', $product ) ){
			$digital = get_post_meta( $product->get_id(), '_digital', true );
			$is_digital = ( $digital == 'yes' );
		} else {
			$digital = get_post_meta( $product->get_id(), '_digital', true );
			$is_digital = ( $digital == 'yes' );
		}

		return apply_filters( 'wgm_digital_product', $is_digital, $product );
	}

	public static function paypal_fix( $args ){
		$args[ 'return' ] = urldecode( $args[ 'return' ] );
		$args[ 'cancel_return' ] = urldecode( $args[ 'cancel_return' ] );

		return $args;
	}

	/**
	 * Change the checkout button of gateways to "next"
	 *
	 * @param $template_name
	 * @param $template_path
	 * @param $located
	 * @param $args
	 */
	public static function change_payment_gateway_order_button_text( $template_name, $template_path, $located, $args ) {

		if ( $template_name == 'checkout/payment-method.php' && is_object( $args[ 'gateway' ] ) && ! empty( $args[ 'gateway' ]->order_button_text ) ) {
			/* translators: Button during checkout, will lead either to the final order confirmation page, or to an external payment provider. */
			$button_text = WGM_Template::change_order_button_text( $args[ 'gateway' ]->order_button_text );
			$button_text = apply_filters( 'woocommerce_de_buy_button_text_gateway_' . $args[ 'gateway' ]->id, $button_text, $args[ 'gateway' ]->order_button_text );
			$args[ 'gateway' ] ->order_button_text = $button_text;
		}
	}

	/**
	 * Disable shipping for virtual products
	 * @deprecated since 2.6.9
	 * @param bool $need_shipping
	 * @access public
	 * @since 2.4.10
	 * @author ap
	 * @wp-hook woocommerce_cart_needs_shipping
	 * @return bool $need_shipping
	 */
	public static function virtual_needs_shipping( $need_shipping ){
		_deprecated_function(__FUNCTION__, '2.6.9' );
		return $need_shipping;
	}

	public static function get_default_tax_label(){
		$tax_label = get_option(WGM_Helper::get_wgm_option( 'wgm_default_tax_label' ), __( 'VAT', 'woocommerce-german-market' ) );
		if( $tax_label && trim( $tax_label ) != ''){
			return $tax_label;
		} else {
			/* translators: fallback for default tax label */
			return __( 'VAT', 'woocommerce-german-market' );
		}
	}

	public static function only_digital( WC_Order $order ) {

		$cart = $order->get_items();

		$dcount = 0;
		foreach( $cart as $item ){
			if( empty( $item[ 'variation_id' ] ) )
				$product = wc_get_product( $item['product_id'] );
			else
				$product = wc_get_product( $item[ 'variation_id' ] );


			if( WGM_Helper::is_digital( $product->get_id() ) ){
				$dcount++;
			}
		}

		$only_digital = false;
		if( $dcount == count( $cart ) ) {
			$only_digital = true;
		}

		return $only_digital;
	}

	public static function order_has_digital_product( WC_Order $order ) {

		$has_digital = false;
		foreach( $order->get_items() as $item ){
			if( empty( $item[ 'variation_id' ] ) )
				$product = wc_get_product( $item['product_id'] );
			else
				$product = wc_get_product( $item[ 'variation_id' ] );


			if( WGM_Helper::is_digital( $product->get_id() ) ){
				$has_digital = true;
				break;
			}
		}

		return $has_digital;
	}
}
?>
