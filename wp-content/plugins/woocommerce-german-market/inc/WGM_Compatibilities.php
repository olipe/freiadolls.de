<?php

/**
 * Class WGM_Compatibilities
 *
 * German Market Userinterface
 *
 * @author MarketPress
 */
class WGM_Compatibilities {

	/**
	 * @var WGM_Compatibilities
	 * @since v3.1.2
	 */
	private static $instance = null;
	
	/**
	* Subscriptions WGM_Compatibility Variables
	**/
	private $_new_order_create;
    private $_wp_wc_running_invoice_number;
    private $_wp_wc_running_invoice_number_date;

	/**
	* Singletone get_instance
	*
	* @static
	* @return WGM_Compatibilities
	*/
	public static function get_instance() {
		if ( self::$instance == NULL) {
			self::$instance = new WGM_Compatibilities();	
		}
		return self::$instance;
	}

	/**
	* Singletone constructor
	*
	* @access private
	*/
	private function __construct() {

		// Checkout Strings
		$options = array(
			'woocommerce_de_avoid_free_items_in_cart_message',
			'woocommerce_de_order_button_text',
			'woocommerce_de_checkbox_text_terms_and_conditions',
			'woocommerce_de_checkbox_text_revocation',
			'woocommerce_de_checkbox_text_revocation_digital',
			'woocommerce_de_checkbox_text_digital_content',
			'woocommerce_de_checkbox_text_digital_content_notice',
			'woocommerce_de_learn_more_about_shipping_payment_revocation',
			'vat_options_notice',
			'vat_options_non_eu_notice',
			'vat_options_label',
			'gm_small_trading_exemption_notice',
			'wgm_default_tax_label',
		);

		$add_ons = WGM_Add_Ons::get_activated_add_ons();

		// Invoice PDF
		if ( isset( $add_ons[ 'woocommerce-invoice-pdf' ] ) ) {
			
			$options[] = 'wp_wc_invoice_pdf_file_name_frontend';
			$options[] = 'wp_wc_invoice_pdf_file_name_backend';
			$options[] = 'wp_wc_invoice_pdf_billing_address_additional_notation';
			$options[] = 'wp_wc_invoice_pdf_invoice_start_subject';
			$options[] = 'wp_wc_invoice_pdf_invoice_start_welcome_text';
			$options[] = 'wp_wc_invoice_pdf_text_after_content';
			$options[] = 'wp_wc_invoice_pdf_page_numbers_text';
			$options[] = 'wp_wc_invoice_pdf_fine_print_custom_content';
			$options[] = 'wp_wc_invoice_pdf_refund_file_name_frontend';
			$options[] = 'wp_wc_invoice_pdf_refund_file_name_backend';
			$options[] = 'wp_wc_invoice_pdf_refund_start_subject_big';
			$options[] = 'wp_wc_invoice_pdf_refund_start_subject_small';
			$options[] = 'wp_wc_invoice_pdf_refund_start_refund_date';
			$options[] = 'wp_wc_invoice_pdf_view_order_button_text';

			$header_columns = get_option( 'wp_wc_invoice_pdf_header_number_of_columns', 1 );
			for ( $i = 1; $i <= $header_columns; $i++ ) {
				$options[] = 'wp_wc_invoice_pdf_header_column_' . $i . '_text';
			}

			$footer_columns = get_option( 'wp_wc_invoice_pdf_footer_number_of_columns', 1 );
			for ( $i = 1; $i <= $footer_columns; $i++ ) {
				$options[] = 'wp_wc_invoice_pdf_footer_column_' . $i . '_text';
			}

		}

		// Invoice Numbers
		if ( isset( $add_ons[ 'woocommerce-running-invoice-number' ] ) ) {
			
			$options[] = 'wp_wc_running_invoice_completed_order_email_subject';
			$options[] = 'wp_wc_running_invoice_completed_order_email_header';
			$options[] = 'wp_wc_running_invoice_email_subject';
			$options[] = 'wp_wc_running_invoice_email_header';
			$options[] = 'wp_wc_running_invoice_email_subject_paid';
			$options[] = 'wp_wc_running_invoice_email_header_paid';
			$options[] = 'wp_wc_running_invoice_email_subject_refunded';
			$options[] = 'wp_wc_running_invoice_email_header_refunded';

			$options[] = 'wp_wc_running_invoice_pdf_file_name_backend';
			$options[] = 'wp_wc_running_invoice_pdf_file_name_frontend';
			$options[] = 'wp_wc_running_invoice_pdf_subject';
			$options[] = 'wp_wc_running_invoice_pdf_date';

			$options[] = 'wp_wc_running_invoice_pdf_file_name_backend_refund';
			$options[] = 'wp_wc_running_invoice_pdf_file_name_frontend_refund';
			$options[] = 'wp_wc_running_invoice_pdf_refund_start_subject_big';
			$options[] = 'wp_wc_running_invoice_pdf_refund_start_subject_small';

		}

		// Return Delivery
		if ( isset( $add_ons[ 'woocommerce-return-delivery-pdf' ] ) ) {
			
			$options[] = 'woocomerce_wcreapdf_wgm_pdf_file_name';
			$options[] = 'woocomerce_wcreapdf_wgm_pdf_author';
			$options[] = 'woocomerce_wcreapdf_wgm_pdf_title';
			$options[] = 'woocomerce_wcreapdf_wgm_pdf_shop_name';
			$options[] = 'woocomerce_wcreapdf_wgm_pdf_address';
			$options[] = 'woocomerce_wcreapdf_wgm_pdf_shop_small_headline';
			$options[] = 'woocomerce_wcreapdf_wgm_pdf_remark';
			$options[] = 'woocomerce_wcreapdf_wgm_pdf_reasons';
			$options[] = 'woocomerce_wcreapdf_wgm_pdf_footer';

			$options[] = 'woocomerce_wcreapdf_wgm_pdf_file_name_delivery';
			$options[] = 'woocomerce_wcreapdf_wgm_pdf_author_delivery';
			$options[] = 'woocomerce_wcreapdf_wgm_pdf_title_delivery';
			$options[] = 'woocomerce_wcreapdf_wgm_pdf_shop_name_delivery';
			$options[] = 'woocomerce_wcreapdf_wgm_pdf_address_delivery';
			$options[] = 'woocomerce_wcreapdf_wgm_pdf_shop_small_headline_delivery';
			$options[] = 'woocomerce_wcreapdf_wgm_pdf_remark_delivery';
			$options[] = 'woocomerce_wcreapdf_wgm_pdf_reasons_delivery';
			$options[] = 'woocomerce_wcreapdf_wgm_pdf_footer_delivery';

			$options[] = 'woocomerce_wcreapdf_wgm_view-order-button-text';
		}

		// Due Date
		if ( get_option( 'woocommerce_de_due_date', 'off' ) == 'on' ) {

			add_action( 'admin_init', array( $this, 'due_date' ) );

		}

		/******************************
		// Compatibility with WPML
		******************************/
		
		if ( function_exists( 'icl_register_string' ) && function_exists( 'icl_t' ) && function_exists( 'icl_st_is_registered_string' ) ) {
			
			add_filter( 'woocommerce_find_rates', array( $this, 'translate_woocommerce_find_rates' ), 10 );
			add_filter( 'wgm_translate_tax_label', array( $this, 'translate_tax_label' ) );

			// Register Strings
			if ( is_admin() ) {
				
				$tax_classes = WC_Tax::get_tax_classes();
				 
				$tax_classes[] = 'standard';

				foreach ( $tax_classes as $tax_class ) {
				 	
				 	$rates = WC_Tax::get_rates_for_tax_class( $tax_class );
				 	foreach ( $rates as $rate ) {
				 		$label = $rate->tax_rate_name;
				 		if ( ! icl_st_is_registered_string( 'German Market: WooCommerce Tax Rate', 'tax rate label: ' . $label ) ) {
	                        icl_register_string( 'German Market: WooCommerce Tax Rate', 'tax rate label: ' . $label, $label );
	                    }
				 	}
				}

			}

			foreach ( $options as $option ) {

				if ( is_admin() ) {
					if ( ! ( icl_st_is_registered_string( 'German Market: Checkout Option', $option ) || icl_st_is_registered_string( 'German Market: Invoice PDF', $option ) || icl_st_is_registered_string( 'German Market: Running Invoice Number', $option ) || icl_st_is_registered_string( 'German Market: Return Delivery Note', $option ) ) ) {
	                    
						if ( str_replace( 'wp_wc_invoice_pdf_', '', $option ) != $option ) {
							icl_register_string( 'German Market: Invoice PDF', $option, get_option( $option ) );

						} else if ( str_replace( 'wp_wc_running_invoice_', '', $option ) != $option ) {
							icl_register_string( 'German Market: Running Invoice Number', $option, get_option( $option ) );

						} else if ( ( str_replace( 'woocomerce_wcreapdf_wgm_', '', $option ) != $option ) || ( str_replace( 'woocommerce_wcreapdf_wgm_', '', $option ) != $option  ) ) {
							icl_register_string( 'German Market: Return Delivery Note', $option, get_option( $option ) );

						} else {
							icl_register_string( 'German Market: Checkout Option', $option, get_option( $option ) );
						}
	                    
	                }

	            }

                if ( ( ! ( is_admin() && isset( $_REQUEST[ 'page' ] ) && $_REQUEST[ 'page' ] == 'german-market' ) ) || ( ! is_admin() ) ) {
                	add_filter( 'option_' . $option, array( $this, 'translate_woocommerce_checkout_options' ), 10, 2 );
                }

			}

		}

		/******************************
		// Compatibility with WPML: WooCommerce Multilingual 
		******************************/

		// Onliy if WooCommerce Multilingual && WPML && GM Invoice PDF ADd-On
		if ( is_admin() && class_exists( 'WCML_Admin_Menus' ) && class_exists( 'SitePress' ) && get_option( 'wgm_add_on_woocommerce_invoice_pdf', 'off' ) == 'on' ) {
			add_action( 'current_screen', array( $this, 'woocommerece_multilingual_add_language_switcher' ) );
		}


		/******************************
		// Compatibility with polylang
		******************************/
		if ( function_exists( 'pll_register_string' ) && function_exists( 'pll__' ) ){

			foreach ( $options as $option ) {
				pll_register_string( $option, get_option( $option ), 'German Market: Checkout Option', true );
				add_filter( 'option_' . $option, array( $this, 'translate_woocommerce_checkout_options_polylang' ), 10, 2 );
			}

			// Delivery Times
			add_filter( 'woocommerce_de_get_deliverytime_string_label_string', array( $this, 'polylang_woocommerce_de_get_deliverytime_string_label_string' ), 10, 2 );
		}
		
		/******************************
		// Compatibility with WooCommerce Composite Products
		******************************/
		if ( class_exists( 'WC_Product_Composite' ) ) {
			add_filter( 'gm_compatibility_is_variable_wgm_template', '__return_false' );
			add_filter( 'gm_cart_template_force_woocommerce_template', '__return_true' );
			add_filter( 'gm_remove_woo_vat_notice_return_original_param', '__return_true' );
		}

		/******************************
		// Compatibility WooCommerce Subscriptions
		******************************/
		if ( function_exists( 'wcs_cart_totals_order_total_html' ) ) {
			add_action( 'german_market_after_frontend_init', array( $this, 'subscriptions' ) );
		}

		if ( class_exists( 'WC_Subscriptions' ) ) {
			add_filter( 'gm_invoice_pdf_email_settings', 					array( $this, 'subscriptions_gm_invoice_pdf_email_settings' ) );
			add_filter( 'gm_invoice_pdf_email_settings_additonal_pdfs', 	array( $this, 'subscriptions_gm_invoice_pdf_email_settings' ) );
			add_filter( 'wp_wc_inovice_pdf_allowed_stati', 					array( $this, 'subscriptions_gm_allowed_stati_additional_mals' ) );
			add_filter( 'wp_wc_inovice_pdf_allowed_stati_additional_mals', 	array( $this, 'subscriptions_gm_allowed_stati_additional_mals' ) );
			add_filter( 'gm_emails_in_add_ons', 							array( $this, 'subscriptions_gm_emails_in_add_ons' ) );

			if ( has_filter( 'wcs_new_order_created' ) ) {
	            add_action( 'woocommerce_new_order', array( $this, 'action_woocommerce_new_order' ), 70, 1 );
	            add_filter( 'wcs_new_order_created', array( $this, 'filter_wcs_new_order_created' ), 70, 1) ;
	        }

	        add_filter( 'woocommerce_countries_inc_tax_or_vat', array( $this, 'dummy_remove_woo_vat_notice' ), 70, 1 );
	        add_filter( 'woocommerce_countries_ex_tax_or_vat', array( $this, 'dummy_remove_woo_vat_notice' ), 70, 1 );
			
		}

		/******************************
		// Theme Compabilities
		******************************/

		$the_theme = wp_get_theme();

		// Theme aurum
		if ( $the_theme->get( 'TextDomain' ) == 'aurum' ) {
			add_action( 'after_setup_theme', array( $this, 'theme_support_aurum' ) );
		}

		// Theme avada
		if ( $the_theme->get( 'TextDomain' ) == 'Avada' ) {
			add_action( 'after_setup_theme', array( $this, 'theme_support_avada' ) );
		}

		// Theme Superba
		if ( $the_theme->get( 'TextDomain' ) == 'thb_text_domain' || ( $the_theme->get_template() == 'superba' ) ) {
			add_action( 'init', array( $this, 'theme_support_superba' ), 20 );
		}

		// Theme woodance
		if ( ( ! is_admin() ) && ( $the_theme->get( 'TextDomain' ) == 'woodance' || ( $the_theme->get_template() == 'woodance' ) ) ) {
			add_action( 'wp', array( $this, 'theme_support_woodance' ) );
		}

		// Theme Envision
		if ( $the_theme->get_template() == 'envision' || $the_theme->get_stylesheet() == 'envision' ) {
			add_action( 'german_market_after_frontend_init', array( $this, 'theme_support_envision' ) );
		}

		// Theme Fluent
		if ( $the_theme->get_template() == 'fluent' || $the_theme->get_stylesheet() == 'fluent' || $the_theme->get_stylesheet() == 'fluent-child' ) {
			add_action( 'wp', array( $this, 'theme_support_fluent' ) );
		}

		/******************************
		// Plugin Klarna Compabilities
		******************************/
		if ( function_exists( 'init_klarna_gateway' ) ) {
			add_action( 'german_market_after_frontend_init', array( $this, 'plugin_support_klarna' ) );
		}
	
	}

	/**
	* Theme Envision: "Cart Estimate Notice" is shown twice, because the theme brings exactly the same notice
	*
	* @since v3.5.1
	* @wp-hook german_market_after_frontend_init
	* @return void
	*/
	function theme_support_envision() {
		remove_filter( 'woocommerce_proceed_to_checkout', array( 'WGM_Template', 'add_cart_estimate_notice' ), 0 );
	}

	/**
	* Plugin Polylang Support: Show delivery times in the correct way
	*
	* @since v3.5.1
	* @wp-hook woocommerce_de_get_deliverytime_string_label_string
	* @param String $string
	* @param WC_Product $product
	* @return String
	*/
	function polylang_woocommerce_de_get_deliverytime_string_label_string( $string, $product ) {

		error_log( 'hey' );
		
	    $term_id = WGM_Template::get_term_id_from_product_meta( '_lieferzeit', $product );

	    if ( (int) $term_id == - 1 || empty( $term_id ) ) {
	        $term_id = get_option( WGM_Helper::get_wgm_option( 'global_lieferzeit' ) );
	    }

	    $label_term = get_term( $term_id, 'product_delivery_times' );
	    
	    if ( is_wp_error( $label_term ) || ! isset( $label_term ) ) {
	        $label_string = __( 'not specified', 'woocommerce-german-market' );
	    } else {
	        $label_string = $label_term->name;
	    }

	    return $label_string;

	}

	/**
	* Plugin Klarna Support: Change behaviour how to send confirmation email
	*	
	* @since v3.5.1
	* @wp-hook german_market_after_frontend_init
	* @return void
	*/
	function plugin_support_klarna() {

		add_filter( 'gm_email_confirm_order_send_it', array( $this, 'klarna_email_confirm_order_send_it' ), 10, 2 );
		add_action( 'woocommerce_thankyou', array( $this, 'klarna_woocommerce_thankyou_send_email_confirm_order' ) );
	}

	/**
	* Plugin Klarna Support: Do not send order confirmation email at processed order
	*
	* @since v3.5.1
	* @wp-hook gm_email_confirm_order_send_it
	* @return void
	*/
	function klarna_email_confirm_order_send_it( $boolean, $order ) {

		$payment_method = $order->get_payment_method();
		if ( str_replace( 'klarna', '', $payment_method ) != $payment_method ) {
			$boolean = false;
		}

		return $boolean;
	}

	/**
	* Plugin Klarna Support: Send order confirmation email on thankyou page
	*
	* @since v3.5.1
	* @wp-hook woocommerce_thankyou
	* @return void
	*/
	function klarna_woocommerce_thankyou_send_email_confirm_order( $order_id ) {

		$order = wc_get_order( $order_id );
		$payment_method = $order->get_payment_method();
		
		if ( str_replace( 'klarna', '', $payment_method ) != $payment_method ) {

			if ( empty( $order->get_meta( '_gm_email_confirm_order_send' ) ) ) {
				
				WGM_Email::send_order_confirmation_mail( $order_id );
				$order->update_meta_data( '_gm_email_confirm_order_send', 'yes' );
				$order->save_meta_data();

			}

		}

	}

	/**
	* Theme Support woodance: Display prices correctly (not twice) on single products and variable product pages
	*
	* @wp-hook wp
	* @return void
	*/
	function theme_support_woodance() {
		
		if ( is_product() ) {

			$product = wc_get_product();

			if ( $product->get_type() == 'simple' ) {
				
				remove_action( 'woocommerce_single_product_summary', array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_single' ), 7 );
				add_action( 'woocommerce_before_add_to_cart_button', array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_single' ), 21 );
				remove_filter( 'wgm_product_summary_parts', array( 'WGM_Template', 'add_product_summary_price_part' ), 0, 3 );
			
			} else if ( $product->get_type() == 'variable' ) {

				remove_action( 'woocommerce_single_product_summary', array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_single' ), 7 );
				add_action( 'woocommerce_before_variations_form', array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_single' ), 21 );
				add_action( 'woocommerce_before_add_to_cart_form', array( $this, 'theme_support_woodance_no_price' ) );
				add_action( 'woocommerce_before_variations_form', array( $this, 'theme_support_woodance_no_price_add_again' ) );

			}

		}

	}

	/**
	* Theme Support fluent: Display prices correctly (not twice) on single products and variable product pages
	*
	* @wp-hook wp
	* @return void
	*/
	function theme_support_fluent() {
		
		if ( is_product() ) {

			$product = wc_get_product();

			if ( $product->get_type() == 'simple' ) {
				
				remove_action( 'woocommerce_single_product_summary', array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_single' ), 7 );
				add_action( 'woocommerce_single_product_summary', array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_single' ), 8 );
				remove_filter( 'wgm_product_summary_parts', array( 'WGM_Template', 'add_product_summary_price_part' ), 0, 3 );
			
			} else if ( $product->get_type() == 'variable' ) {

				remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 7 );

			}

		}

	}

	/**
	* Theme Support woodance: Display prices correctly (not twice) on single products and variable product pages
	* Remove Price html for variable products
	*
	* @wp-hook woocommerce_before_add_to_cart_form
	* @return void
	*/
	function theme_support_woodance_no_price() {
		add_filter( 'woocommerce_get_price_html', array( $this, 'theme_support_woodance_variable_price' ) );
	}

	/**
	* Theme Support woodance: Display prices correctly (not twice) on single products and variable product pages
	* Add Price html filter for variable products again
	*
	* @wp-hook woocommerce_before_variations_form
	* @return void
	*/
	function theme_support_woodance_no_price_add_again() {
		remove_filter( 'woocommerce_get_price_html', array( $this, 'theme_support_woodance_variable_price' ) );
	}

	/**
	* Theme Support woodance: Removes price html for variable product
	*
	* @wp-hook woocommerce_get_price_html
	* @return void
	*/
	function theme_support_woodance_variable_price( $price, $product ) {
		return '';
	}

	/**
	* Translateable Due Date Options
	*
	* @access public
	* @wp-hook init
	* @return void
	*/
	function due_date() {

		if ( ! is_admin() ) {
			return;
		}

		$gateways = WC()->payment_gateways()->payment_gateways();
		$strings  = array();

		foreach ( $gateways as $payment_method_id => $gateway ) {

			if ( isset( $gateway->settings[ 'wgm_due_date_text' ] ) ) {
				$due_date_text 	= $gateway->settings[ 'wgm_due_date_text' ];
			} else {
				$due_date_text 	= apply_filters( 'woocommerce_de_due_date_text_' . $payment_method_id, __( 'Due Date: {{due-date}}', 'woocommerce-german-market' ) );
			}

			if ( function_exists( 'icl_register_string' ) && function_exists( 'icl_t' ) && function_exists( 'icl_st_is_registered_string' ) ) {

				if ( ! ( icl_st_is_registered_string( 'German Market: Due Date Option', $due_date_text ) ) ) {

					icl_register_string( 'German Market: Due Date Option', $due_date_text, $due_date_text );

				}

			} else if( function_exists( 'pll_register_string' ) && function_exists( 'pll__' ) ) {

					pll_register_string( $due_date_text, $due_date_text, 'German Market: Due Date Option', true );

			}

		}

	}

	/**
	* Theme Superba Support: Double price in loop and single product pages
	*
	* @access public
	* @wp-hook after_setup_theme
	* @return void
	*/
	function theme_support_superba() {
		
		// avoid double price in loop
		remove_action( 'woocommerce_after_shop_loop_item', 			'thb_loop_product_end', 999 );
		remove_action( 'woocommerce_after_shop_loop_item_title', 	array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_loop' ), 5 );
		add_action( 'woocommerce_after_shop_loop_item', 			array( $this, 'superba_woocommerce_after_shop_loop_item' ), 999 );
		
		// avoid douple price in single product
		remove_action( 'woocommerce_single_product_summary',		array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_single' ), 7 );
		remove_action('woocommerce_single_product_summary', 'thb_single_product_summary');
		add_action( 'woocommerce_single_product_summary', array( $this, 'superba_woocommerce_single_product_summary' ), 999 );
	}

	/**
	* Theme Superba Support: Double price in loop
	*
	* @access public
	* @wp-hook woocommerce_after_shop_loop_item
	* @return void
	*/
	function superba_woocommerce_after_shop_loop_item() {

		global $post, $product;
		$size = sizeof( get_the_terms( $post->ID, 'product_cat' ) );
		echo $product->get_categories( ', ', '<span class="posted_in">', '</span>' );

		woocommerce_template_loop_rating();

		echo '<div class="thb-add-to-cart-wrapper">';
			WGM_Template::woocommerce_de_price_with_tax_hint_loop();
		echo "</div>";

		woocommerce_template_loop_add_to_cart();
		echo "</div>";

	}

	/**
	* Theme Superba Support: Double price on single product pages
	*
	* @access public
	* @wp-hook woocommerce_single_product_summary
	* @return void
	*/
	function superba_woocommerce_single_product_summary() {

		?>
		<div class="thb-product-header">
			<?php
				thb_pagination();
				woocommerce_breadcrumb();
				woocommerce_template_single_title();
				woocommerce_template_loop_rating();
				WGM_Template::woocommerce_de_price_with_tax_hint_single();
			?>
		</div>
		<div class="thb-product-description">
			<?php
				woocommerce_template_single_excerpt();
				woocommerce_template_single_add_to_cart();
			?>
		</div>
		<?php

	}

	/**
	* Woocommerce Subscriiption active filter wcs_new_order_created:filter_wcs_new_order_created each new Order, this new Order its same Subscription-Order, (Copy-Parent) 
	* we have to edit_wp_wc_running_invoice_number, _wp_wc_running_invoice_number_date in the new Order
	*
	* @since GM 3.4.3
	* @wp-hook wcs_new_order_created
	* @param WC_Order $order
	* @return WC_Order
	*/
    public function filter_wcs_new_order_created( $order ){

        if ( ( $order->get_id() != null ) && ( $order->get_id() == $this->_new_order_create ) ) {
            
            $date_process= get_post_meta( $order->get_id(), '_wp_wc_running_invoice_number_date', true );
            
            if ( (int)$this->_wp_wc_running_invoice_number_date > (int)$date_process ) {                
                $order->update_meta_data( '_wp_wc_running_invoice_number', $this->_wp_wc_running_invoice_number );
                $order->update_meta_data( '_wp_wc_running_invoice_number_date', $this->_wp_wc_running_invoice_number_date );                
            }            
        } 

        return $order;
    }

	/**
	* Wenn new Order is created, New_order = Parent_order_subscrition, then we save data and number, (this process with German-Marker its Okay, but this will replace in the new Filter).
	*
	* @since GM 3.4.3
	* @wp-hook woocommerce_new_order
	* @param WC_Order $order
	* @return WC_Order
	*/
    public function action_woocommerce_new_order($order){

        $this->_new_order_create = $order;
        $this->_wp_wc_running_invoice_number = get_post_meta( $order, '_wp_wc_running_invoice_number', true );
        $this->_wp_wc_running_invoice_number_date = get_post_meta( $order, '_wp_wc_running_invoice_number_date', true );
       
       return $order;
    }

	/**
	* Wenn new Order is created, New_order Parent_order_subscrition,
	* @since GM 3.4.3
	* @wp-hook woocommerce_countries_inc_tax_or_vat, woocommerce_countries_ex_tax_or_vat
	* @param String $return
	* @return String
	*/
	public function dummy_remove_woo_vat_notice( $return = "" ){

        if ( $return == "" ){
            $return = " ";
        }

        return $return;
    }

	/**
	* Wocommerece Multilingual removes the languages switcher on "post_type=shop_order" but 
	* we need it to make invoice pdfs translatable
	*
	* @access public
	* @wp-hook current_screen
	* @return void
	*/
	function woocommerece_multilingual_add_language_switcher() {

		$screen = get_current_screen();
		
		if ( $screen->id == 'edit-shop_order' && $screen->base == 'edit' ) {
			
			global $sitepress;
			add_action( 'wp_before_admin_bar_render', array( $sitepress, 'admin_language_switcher' ), 20 );
			
		}

	}

	/**
	* Theme aurom Support: Double price in loop and single product pages
	*
	* @access public
	* @wp-hook after_setup_theme
	* @return void
	*/
	function theme_support_aurum() {
		
		// avoid double price in loop
		remove_action( 'woocommerce_after_shop_loop_item_title', array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_loop' ), 5 );
		remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_price', 20 );
		add_action( 'woocommerce_after_shop_loop_item',		array( 'WGM_Template', 'woocommerce_de_price_with_tax_hint_single' ), 20 );

		// avoid douple price in single product
		remove_filter( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 25 );
	}

	/**
	* Theme Avada Support
	*
	* @access public
	* @wp-hook after_setup_theme
	* @return void
	*/
	function theme_support_avada() {
		add_filter( 'woocommerce_cart_item_name', array( $this, 'theme_support_avada_remove_double_digital' ), 99, 3 );
		add_filter( 'woocommerce_pay_order_button_html', array( $this, 'theme_support_avada_pay_for_order_page_checkboxes' ) );
	}

	/**
	* Theme Avada Support: Checkboxes on pay for order page
	*
	* @access public
	* @wp-hook woocommerce_pay_order_button_html
	* @param String $markup
	* @return String
	*/
	function theme_support_avada_pay_for_order_page_checkboxes( $markup ) {

		if ( is_wc_endpoint_url( 'order-pay' ) ) {

			$markup = WGM_Template::add_review_order() . $markup;

		}

		return $markup;

	}

	/**
	* Theme Avada Support: Double "[Digital]" during checkout, very simple solution
	*
	* @access public
	* @wp-hook woocommerce_cart_item_name
	* @return void
	*/
	function theme_support_avada_remove_double_digital( $title, $cart_item, $cart_item_key ) {
		return str_replace( '[Digital] [Digital]', '[Digital]', $title );
	}

	/**
	* WooCommerce Subscriptions Support: Email Attachments
	*
	* @access public
	* @wp-hook gm_invoice_pdf_email_settings
	* @wp-hook gm_invoice_pdf_email_settings_additonal_pdfs
	* @param Array $options
	* @return Array
	*/
	function subscriptions_gm_invoice_pdf_email_settings( $options ) {

		$prefix = current_filter() == 'gm_invoice_pdf_email_settings_additonal_pdfs' ? '_add_pdfs' : '';

		$options[] = array( 'title' => __( 'WooCommerce Subscriptions Support', 'woocommerce-german-market' ), 'type' => 'title','desc' => '', 'id' => 'wp_wc_invoice_pdf_emails_subcriptions' . $prefix );

		$options[] = array(
			'name'		=> __( 'New Renewal Order', 'woocommerce-german-market' ),
			'id'   		=> 'wp_wc_invoice_pdf_emails_new_renewal_order' . $prefix,
			'type' 		=> 'wgm_ui_checkbox',
			'default'  	=> 'off',
		);

		$options[] = array(
			'name'		=> __( 'Processing Renewal Order', 'woocommerce-german-market' ),
			'id'   		=> 'wp_wc_invoice_pdf_emails_customer_processing_renewal_order' . $prefix,
			'type' 		=> 'wgm_ui_checkbox',
			'default'  	=> 'off',
		);

		$options[] = array(
			'name'		=> __( 'Complete Renewal Order', 'woocommerce-german-market' ),
			'id'   		=> 'wp_wc_invoice_pdf_emails_customer_completed_renewal_order' . $prefix,
			'type' 		=> 'wgm_ui_checkbox',
			'default'  	=> 'off',
		);

		$options[] = array( 'type' => 'sectionend', 'id' => 'wp_wc_invoice_pdf_emails_subcriptions' . $prefix );

		return $options;
	}

	/**
	* WooCommerce Subscriptions Support: Email Attachments
	*
	* @access public
	* @wp-hook wp_wc_inovice_pdf_allowed_stati
	* @wp-hook wp_wc_inovice_pdf_allowed_stati_additional_mals
	* @param Array allowed_stati
	* @return Array
	*/
	function subscriptions_gm_allowed_stati_additional_mals( $allowed_stati ) {

		$allowed_stati[] = 'new_renewal_order';
		$allowed_stati[] = 'customer_processing_renewal_order';
		$allowed_stati[] = 'customer_completed_renewal_order';

		return $allowed_stati;
	}

	/**
	* WooCommerce Subscriptions Support: Email Attachments in Add-Ons
	*
	* @access public
	* @wp-hook emails
	* @param Array allowed_stati
	* @return Array
	*/
	function subscriptions_gm_emails_in_add_ons( $emails ) {

		$emails[ 'new_renewal_order' ] 					= __( 'New Renewal Order', 'woocommerce-german-market' );
		$emails[ 'customer_processing_renewal_order' ]	= __( 'Processing Renewal Order', 'woocommerce-german-market' );
		$emails[ 'customer_completed_renewal_order' ]	= __( 'Complete Renewal Order', 'woocommerce-german-market' );

		return $emails;
	}

	/**
	* WooCommerce Subscriptions Support: Recurring Totals
	*
	* @access public
	* @wp-hook german_market_after_frontend_init
	*/
	function subscriptions() {
		remove_filter( 'woocommerce_cart_totals_order_total_html',	array( 'WGM_Template', 'woocommerce_cart_totals_excl_tax_string' ) );
	}
	
	/**
	* WPML Support: Translate WooCommerce Tax Rates for WPML
	*
	* @access public
	* @wp-hook woocommerce_find_rates
	* @param Array $matched_tax_rates
	* @return Array
	*/
	function translate_woocommerce_find_rates( $matched_tax_rates ) {

        foreach( $matched_tax_rates as &$rate ) {
 				
                if ( $rate[ 'label' ] ) {
                    $rate[ 'label' ] = icl_t( 'German Market: WooCommerce Tax Rate', 'tax rate label: ' . $rate[ 'label' ], $rate[ 'label' ] );  
                }

                unset($rate);

        }

        reset($matched_tax_rates);
 
        return $matched_tax_rates;
 
	}

	/**
	* WPML Support: Translate WooCommerce Checkout Strings
	*
	* @access public
	* @wp-hook option_{option}
	* @param String $value
	* @param String $option
	* @return String
	*/
	function translate_woocommerce_checkout_options( $value, $option ) {
		
		global $sitepress;
		$lang = $sitepress->get_current_language();
		$default_lang = $sitepress->get_default_language();

		if ( $lang == $default_lang ) {
			return $value;
		}

		if ( str_replace( 'wp_wc_invoice_pdf_', '', $option ) != $option ) {
			$value = apply_filters( 'wpml_translate_single_string', $value, 'German Market: Invoice PDF', $option, $lang );

		} else if ( str_replace( 'wp_wc_running_invoice_', '', $option ) != $option ) {
			$value = apply_filters( 'wpml_translate_single_string', $value, 'German Market: Running Invoice Number', $option, $lang );

		} else if ( ( str_replace( 'woocomerce_wcreapdf_wgm_', '', $option ) != $option ) || ( str_replace( 'woocommerce_wcreapdf_wgm_', '', $option ) != $option  ) ) {
			$value = apply_filters( 'wpml_translate_single_string', $value, 'German Market: Return Delivery Note', $option, $lang );

		} else {

			$value = apply_filters( 'wpml_translate_single_string', $value, 'German Market: Checkout Option', $option, $lang );
		}

		return $value;

	}

	/**
	* Polylang Support: Translate WooCommerce Checkout Strings
	*
	* @access public
	* @wp-hook option_{option}
	* @param String $value
	* @param String $option
	* @return String
	*/
	function translate_woocommerce_checkout_options_polylang( $value, $option ) {

		$value = pll__( $value );
		return $value;

	}

	/**
	* WPMP Support: Translate Tax Labels for order items
	*
	* @access public
	* @wp-hook option_{wgm_translate_tax_label}
	* @param String $tax_label
	* @return String
	*/
	function translate_tax_label( $tax_label ) {

		// WPML
		if ( function_exists( 'icl_register_string' ) && function_exists( 'icl_t' ) && function_exists( 'icl_st_is_registered_string' ) ) {
			$tax_label = icl_t( 'German Market: WooCommerce Tax Rate', 'tax rate label: ' . $tax_label, $tax_label );
		}

		return $tax_label;
	}

}
