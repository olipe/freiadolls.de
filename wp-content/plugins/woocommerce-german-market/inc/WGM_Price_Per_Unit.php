<?php

class WGM_Price_Per_Unit {

	public static function init() {

		add_filter( 'wgm_product_summary_parts', array( __CLASS__, 'add_ppu_part' ), 10, 2 );
		add_action( 'woocommerce_product_data_panels', array( __CLASS__, 'add_product_write_panel' ) );

	}

	public static function add_ppu_part( $parts, $product ) {

		$parts[ 'ppu' ] = self::get_price_per_unit_string( $product );

		return $parts;
	}

	public static function get_price_per_unit_string( $product ) {

		$result              = '';
		$price_per_unit_data = self::get_price_per_unit_data( $product );

		if ( empty( $price_per_unit_data ) ) {
			return $result;
		}

		apply_filters( 'wgm_before_price_per_unit_loop', $result );

		$result .= apply_filters(
			'wmg_price_per_unit_loop',
			sprintf( '<span class="wgm-info price-per-unit price-per-unit-loop ppu-variation-wrap">(%s / %s %s)</span>',
			         wc_price( str_replace( ',', '.', $price_per_unit_data[ 'price_per_unit' ] ) ),
			         $price_per_unit_data[ 'mult' ],
			         $price_per_unit_data[ 'unit' ]
			),
			wc_price( str_replace( ',', '.', $price_per_unit_data[ 'price_per_unit' ] ) ),
			$price_per_unit_data[ 'mult' ],
			$price_per_unit_data[ 'unit' ]
		);

		apply_filters( 'wgm_after_price_per_unit_loop', $result );

		return $result;
	}

	/**
	 * Retrives price per unit data
	 *
	 * @param WC_Product $_product
	 *
	 * @access public
	 * @static
	 * @author ap
	 * @return array
	 */
	public static function get_price_per_unit_data( $_product ) {

		$id = $_product->get_id();

		$price          = ( $_product->is_on_sale() ) ? 'sale' : 'regular';
		$price_per_unit = str_replace( ',', '.', get_post_meta( $id, '_' . $price . '_price_per_unit', TRUE ) );
		$unit           = get_post_meta( $id, '_unit_' . $price . '_price_per_unit', TRUE );
		$mult           = get_post_meta( $id, '_unit_' . $price . '_price_per_unit_mult', TRUE );

		if ( $price_per_unit && $unit && $mult ) {
			return compact( 'price_per_unit', 'unit', 'mult' );
		} else {
			return array();
		}

	}

	/**
	 * add delivery time control and shipping control to products
	 *
	 * @access public
	 * @author jj, ap
	 * @uses   maybe_unserialize, get_the_ID, get_post_meta, selected, woocommerce_wp_text_input,
	 *         get_woocommerce_currency_symbol
	 * @static
	 * @return void
	 */
	public static function add_product_write_panel() {

		?>
		<div id="price_per_unit_options" class="panel woocommerce_options_panel" style="display: block; ">
			<?php
			$smalltax                         = '<br /><small> ' . __( 'VAT included',
			                                                           'woocommerce-german-market' ) . ' </small>';
			$regular_price_per_unit_selection = array( 'id' => '_unit_regular_price_per_unit' );

			$mult_field = '<span style="float: left;">&nbsp;&#47; &nbsp;</span> <input type="text" style="width: 40px;" name="_unit_regular_price_per_unit_mult" value="' . get_post_meta( get_the_ID(),
			                                                                                                                                                                               '_unit_regular_price_per_unit_mult',
			                                                                                                                                                                               TRUE ) . '" />';

			// Price
			WGM_Settings::extended_woocommerce_text_input(
				array(
					'id'                             => '_regular_price_per_unit',
					'label'                          => __( 'Default Price',
					                                        'woocommerce-german-market' ) . ' (' . get_woocommerce_currency_symbol() . ')' . $smalltax,
					'between_input_and_desscription' => $mult_field . self::select_scale_units( $regular_price_per_unit_selection )
				)
			);

			$sale_price_per_unit_selection = array( 'id' => '_unit_sale_price_per_unit' );

			$mult_field = '<span style="float: left;">&nbsp;&#47; &nbsp;</span> <input type="text" style="width: 40px;" name="_unit_sale_price_per_unit_mult" value="' . get_post_meta( get_the_ID(),
			                                                                                                                                                                            '_unit_sale_price_per_unit_mult',
			                                                                                                                                                                            TRUE ) . '" />';

			// Special Price
			WGM_Settings::extended_woocommerce_text_input(
				array(
					'id'                             => '_sale_price_per_unit',
					'label'                          => __( 'Sale Price',
					                                        'woocommerce-german-market' ) . ' (' . get_woocommerce_currency_symbol() . ')' . $smalltax,
					'between_input_and_desscription' => $mult_field . self::select_scale_units( $sale_price_per_unit_selection )
				)
			);
			?>
		</div>
		<?php
	}

	/**
	 * Make a select field for scale_units
	 *
	 * @access      public
	 *
	 * @param    array $field
	 *
	 * @uses        get_post_meta, get_terms, selected
	 * @global         $thepostid , $post, $woocommerce
	 * @static
	 * @return    string html
	 */
	public static function select_scale_units( $field ) {

		global $thepostid, $post, $woocommerce;

		if ( ! $thepostid ) {
			$thepostid = $post->ID;
		}

		if ( ! isset( $field[ 'class' ] ) ) {
			$field[ 'class' ] = 'select short';
		}

		if ( ! isset( $field[ 'value' ] ) ) {
			$field[ 'value' ] = get_post_meta( $thepostid, $field[ 'id' ], TRUE );
		}

		$default_product_attributes = WGM_Defaults::get_default_product_attributes();
		$attribute_taxonomy_name    = wc_attribute_taxonomy_name( $default_product_attributes[ 0 ][ 'attribute_name' ] );
		$terms                      = get_terms( $attribute_taxonomy_name, 'orderby=name&hide_empty=0' );

		// Select field output
		$select = sprintf( '<select name="%s">', esc_attr( $field[ 'id' ] ) );
		if ( is_array( $terms ) && ! empty( $terms ) ) {
			foreach ( $terms as $value ) {

				$select .= sprintf(
					'<option value="%1$s" %2$s>%3$s</option>',
					esc_attr( $value->name ),
					selected( $field[ 'value' ], $value->name, FALSE ),
					! empty( $value->description ) ? esc_attr( $value->description )
						: esc_attr( __( 'Fill in attribute description!', 'woocommerce-german-market' ) )
				);
			}
		}
		$select .= '</select>';

		return $select;
	}

	/**
	* Price Per Unit in Checkout: Add item meta
	*
	* @wp-hook woocommerce_add_cart_item_data
	* @since GM v3.2
	* @static
	* @access public
	* @param Array $cart_item_data
	* @param Integer $product_id
	* @param Integer $variation_id
	* @return Array
	**/
	public static function ppu_co_woocommerce_add_cart_item_data( $cart_item_data, $product_id, $variation_id ) {

		$ppu_string = '';

		if ( $variation_id && $variation_id > 0 ) {
			
			$product = new WC_Product_Variation( $variation_id );
			$ppu_string = wcppufv_get_price_per_unit_string_by_product( $product );
			
			if ( $ppu_string == '' ) {
				$product = wc_get_product( $product_id );
				$ppu_string = self::get_price_per_unit_string( $product );
			}

		} else {
			
			$product = wc_get_product( $product_id  );
			$ppu_string = self::get_price_per_unit_string( $product );

		}

		if ( $ppu_string != '' ) {
			$cart_item_data[ '_gm_ppu' ] = $ppu_string;
		}

		return $cart_item_data;
	}

	/**
	* Price Per Unit in Checkout: Add item meta from session
	*
	* @wp-hook woocommerce_add_cart_item_data
	* @since GM v3.2
	* @static
	* @access public
	* @param Array $cart_item_data
	* @param Array $cart_item_session_data
	* @param String $cart_item_key
	* @return Array
	**/
	public static function ppu_co_woocommerce_get_cart_item_from_session( $cart_item_data, $cart_item_session_data, $cart_item_key ) {

		if ( isset( $cart_item_session_data[ '_gm_ppu' ] ) ) {
	        $cart_item_data[ '_gm_ppu' ] = $cart_item_session_data[ '_gm_ppu' ];
	    }

		return $cart_item_data;
	}

	/**
	* Price Per Unit in Checkout: Show PPU in Cart and Checkout
	*
	* @wp-hook woocommerce_cart_item_price
	* @wp-hook woocommerce_cart_item_subtotal
	* @since GM v3.2
	* @static
	* @access public
	* @param String $price
	* @param Array $cart_item_session_data
	* @param String $cart_item_key
	* @return String
	**/
	public static function ppu_co_woocommerce_cart_item_price( $price, $cart_item, $cart_item_key ) {
		
		if ( current_filter() == 'woocommerce_cart_item_subtotal' && ! is_checkout() ) {
			return $price;
		}

		$ppu = isset( $cart_item[ '_gm_ppu' ] ) && $cart_item[ '_gm_ppu' ] != '' ? $cart_item[ '_gm_ppu' ] : '';
		return $price . $ppu;
	}

	/**
	* Store into order
	*
	* @wp-hook woocommerce_new_order_item
	* @since GM v3.2.2
	* @static
	* @access public
	* @param Integer $item_id
	* @param Object $item
	* @param Integer $order_id
	* @return void
	**/
	public static function ppu_co_woocommerce_add_order_item_meta_wc_3( $item_id, $item, $order_id  ) {

		if ( is_a( $item, 'WC_Order_Item_Product' ) ) {

			$product = $item->get_product();

			if ( $product->get_type() == 'variation' ) {

				$ppu_string = wcppufv_get_price_per_unit_string_by_product( $product );
			
				if ( $ppu_string == '' ) {
					$parent_product = wc_get_product( $product->get_parent_id() );
					$ppu_string = self::get_price_per_unit_string( $parent_product );
				}

			} else {

				$ppu_string = self::get_price_per_unit_string( $product );

			}

			if ( $ppu_string != '' ) {
				wc_add_order_item_meta( $item_id, '_gm_ppu' , $ppu_string );
			}

		}
		
	}

	/**
	* Price Per Unit in Checkout: Show PPU in Order
	*
	* @wp-hook woocommerce_order_formatted_line_subtotal
	* @since GM v3.2
	* @static
	* @access public
	* @param String $subtotal
	* @param Array $cart_item_session_data
	* @param String $cart_item_key
	* @return String
	**/
	public static function ppu_co_woocommerce_order_formatted_line_subtotal( $subtotal, $item, $order ) {

		if ( $item->get_meta( '_gm_ppu' ) != '' ) {
			$subtotal .= '<br />' . $item->get_meta( '_gm_ppu' ); 
		}

		return $subtotal;
	}
}