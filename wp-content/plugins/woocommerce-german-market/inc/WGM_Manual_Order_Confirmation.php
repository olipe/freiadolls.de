<?php

/**
 * Class WGM_Manual_Order_Confirmation
 *
 * German Market Userinterface
 *
 * @author MarketPress
 */
class WGM_Manual_Order_Confirmation {

	/**
	 * @var WGM_Manual_Order_Confirmation
	 * @since v3.2
	 */
	private static $instance = null;
	
	/**
	* Singletone get_instance
	*
	* @static
	* @return WGM_Compatibilities
	*/
	public static function get_instance() {
		if ( self::$instance == NULL) {
			self::$instance = new WGM_Manual_Order_Confirmation();	
		}
		return self::$instance;
	}

	/**
	* Singletone constructor
	*
	* @access private
	*/
	private function __construct() {

		if ( get_option( 'woocommerce_de_manual_order_confirmation' ) == 'on' ) {

			// Order Status has to be 'on-hold'
			add_filter( 'woocommerce_create_order', array( $this, 'set_order_status' ), 100, 2 );

			// Do not show payment information to customer
			add_action( 'woocommerce_before_template_part', array( $this, 'disable_payment_info' ), 1, 4 );

			// Remove Redirecting for payment
			add_action( 'woocommerce_checkout_order_processed', array( $this, 'remove_redirect' ) );

			// Confirm Order E-Mail
			add_action( 'woocommerce_email_before_order_table', array( $this, 'woocommerce_email_before_order_table_confirm' ), 1, 3 );

			// Remove other payment gateways
			add_filter( 'woocommerce_available_payment_gateways', array( $this, 'woocommerce_available_payment_gateways' ) );

			// Send Admin Email
			add_action( 'woocommerce_checkout_update_order_meta', array( $this, 'send_admin_email' ), 10, 2 );

			// View Order
			add_action( 'woocommerce_view_order', array( $this, 'view_order_info' ), 1 );

			if ( is_admin() ) {

				// Small notice if order is not confirmed yet
				add_action( 'manage_shop_order_posts_custom_column', array( $this, 'admin_confirmation_notice' ) );

				// Admin Order Action (new icon to conirm order)
 				add_filter( 'woocommerce_admin_order_actions', array( $this, 'admin_icon_confirm' ), 100, 2 ); 

 				// Add the aciton for woocommerce_admin_order_actions
 				add_filter( 'wp_ajax_german_market_manual_order_confirmation', array( __CLASS__, 'admin_icon_confirm_action' ) );

 				// Order Confirmation button next to "save" button
 				add_action( 'woocommerce_order_actions_end', array( $this, 'woocommerce_order_actions_end_confirm_button' ) );

 				// Confirm Order if Order Confirmation button has been pressed
 				add_action( 'woocommerce_process_shop_order_meta', array( $this, 'woocommerce_process_shop_order_meta_save' ), 100, 2 );

 				// Add Bulk Action
 				add_action( 'admin_footer', array( $this, 'bulk_admin_footer' ) );

 				// Do Bulk Action
 				add_action( 'load-edit.php', array( $this, 'bulk_action' ) );

			}

			////////
			// Exception Handling if Manual Order Confirmation is activated
			////////

			// remove additional sepa fields from checkout
			add_filter( 'gm_sepa_fields_in_checkout', array( $this, 'sepa_fields_in_checkout' ) );

			// add notice in checkout
			add_filter( 'gm_sepa_description_in_checkout', array( $this, 'sepa_description_in_checkout' ) );

			// no sepa mandate checkbox in checkout
			add_filter( 'gm_sepa_checkout_field_checkbox', array( $this, 'sepa_checkout_field_checkbox' ) );

			// do not send sepa mandate yet
			add_filter( 'gm_sepa_send_sepa_email', array( $this, 'sepa_send_sepa_email' ) );

		}

	}

	function sepa_send_sepa_email( $rtn ) {

		if ( ! is_wc_endpoint_url( 'order-pay' ) ) {
			$rtn = false;
		}

		return $rtn;

	}

	/**
	* SEPA: no sepa mandate checkbox in checkout
	*
	* @wp-hook gm_sepa_checkout_field_checkbox
	* @param Boolean $rtn
	* @return rtn
	**/
	function sepa_checkout_field_checkbox( $rtn ) {

		if ( ! is_wc_endpoint_url( 'order-pay' ) ) {
			$rtn = true;
		}

		return $rtn;
	}

	/**
	* SEPA: Payment Method Descrition
	*
	* @wp-hook gm_sepa_description_in_checkout
	* @param String $description
	* @return String
	**/
	function sepa_description_in_checkout( $description ) {

		if ( ! is_wc_endpoint_url( 'order-pay' ) ) {
			
			if ( trim( $description ) != '' ) {
				$description .= '<br />';
			}

			$notice = apply_filters( 'gm_manual_order_confirmation_description_notice_sepa', __( 'Your order must be confirmed manually. You enter your SEPA data after the manual confirmation.', 'woocommerce-german-market' ) );
			$description .= $notice;

		}

		return $description;

	}

	/**
	* SEPA: No checkout fields in checkout
	*
	* @wp-hook gm_sepa_fields_in_checkout
	* @param Array $fields
	* @return Array
	**/
	function sepa_fields_in_checkout( $fields ) {

		if ( ! is_wc_endpoint_url( 'order-pay' ) ) {
			return array();
		}

		return $fields;
	}

	/**
	* Send Admin Email New Order
	*
	* @access public
	* @wp-hook woocommerce_checkout_update_order_meta
	* @param Integer $order_id
	* @return Integer
	*/
	public function send_admin_email( $order_id, $checkout ) {
		$mailer = WC()->mailer();
		$mails = $mailer->get_emails();
		if ( ! empty( $mails ) ) {
		    foreach ( $mails as $mail ) {
		        if ( $mail->id == 'new_order' ) {
		           $mail->trigger( $order_id);
		           return;
		        }
		     }
		}
	}

	/**
	* Order Status has to be 'on-hold'
	*
	* @access public
	* @wp-hook woocommerce_create_order
	* @param Integer $order_id
	* @return Integer
	*/
	public function set_order_status( $order_id, $instance ) {
		add_filter( 'woocommerce_default_order_status', array( $this, 'return_order_status' ) );
		return $order_id;
	}

	/**
	* Order Status has to be 'on-hold'
	*
	* @access public
	* @wp-hook woocommerce_default_order_status
	* @param String $status
	* @return String
	*/
	public function return_order_status( $status ) {
		return 'on-hold';
	}

	/**
	* Do not show payment information to customer
	*
	* @access public
	* @wp-hook woocommerce_before_template_part
	* @param String $template_name
	* @param String $template_path
	* @param String $located
	* @param Array $args
	* @return void
	*/
	public function disable_payment_info( $template_name, $template_path, $located, $args ) {
		
		if ( $template_name == 'checkout/thankyou.php' ) {
			
			if ( isset( $args[ 'order' ] ) ) {
				
				$order = $args[ 'order' ];
				if ( ! self::confirmation_needed( $order ) ) {
					return;
				}

			}

			$gateways = WC()->payment_gateways()->payment_gateways();

			foreach ( $gateways as $key => $method ) {
				remove_all_actions( 'woocommerce_thankyou_' . $key );
			}

		}
	}

	/**
	* Remove Redirecting for payment
	*
	* @access public
	* wp-hook woocommerce_checkout_order_processed and set post_meta '_gm_needs_conirmation' to 'yes'
	* @return bool
	*/
	public function remove_redirect( $order_id ) {

		update_post_meta( $order_id, '_gm_needs_conirmation', 'yes' );
		
		add_filter( 'woocommerce_cart_needs_payment', '__return_false' );
		add_filter( 'woocommerce_valid_order_statuses_for_payment_complete', array( $this, 'no_payment' ), 100 );
	}

	/**
	* No Payment
	*
	* @access public
	* wp-hook woocommerce_valid_order_statuses_for_payment_complete
	* @param Array $stati
	* @return Array
	*/
	public function no_payment( $stati ) {
		return array();
	}

	/**
	* Don'ty show any other information in order confirmation email
	*
	* @access public
	* wp-hook woocommerce_email_before_order_table
	* @param WC_Order $order
	* @param Boolean $sent_to_admin
	* @param Boolean $plain_text
	* @return void
	*/
	public function woocommerce_email_before_order_table_confirm( $order, $sent_to_admin, $plain_text ) {

		if ( self::confirmation_needed( $order ) ) {
			
			if ( $order->get_status() != 'cancelled' ) {

				$text = apply_filters( 'gm_manual_order_confirmation_notice_in_email', __( 'Your order will be manually checked. You will get another email after your order has been confirmed.', 'woocommerce-german-market' ) );

				if ( $plain_text ) {
					echo $text . '\n\n';
				} else {
					echo '<p>' . $text . '</p>';
				}

				remove_all_actions( 'woocommerce_email_before_order_table' );

			}

		}

	}

	/**
	* Add a small confirmation notice to admin order table if order is not confirmed
	*
	* @access public
	* @wp-hook manage_shop_order_posts_custom_column
	* @param String Column
	* @return void
	*/
	public function admin_confirmation_notice( $column ) {
		
		switch ( $column ) {
			
			case 'order_title' :
				
				global $post, $woocommerce, $the_order;

				if ( empty( $the_order ) || $the_order->get_id() != $post->ID ) {
					$the_order = wc_get_order( $post->ID );
				}

				if ( self::confirmation_needed( $the_order ) ) {
					echo '<small class="gm-manual-confirmation-notice">' . __( 'Not confirmed', 'woocommerce-german-market' ) . '</small>';
				}
			
			break;
		}

	}

	/**
	* Remove all payment gateways but that one that customer has chosen during checkout
	*
	* @access public
	* @wp-hook woocommerce_available_payment_gateways
	* @param Array gateways
	* @return Array
	*/
	public function woocommerce_available_payment_gateways( $gateways ) {

		if ( is_wc_endpoint_url( 'order-pay' ) ) {
			
			// get order
			global $wp;
	    	$order_id = $wp->query_vars[ 'order-pay' ];
	    	$order = wc_get_order( $order_id );

	    	if ( get_post_meta( $order_id, '_gm_needs_conirmation', true ) == 'confirmed' ) {
	    		
	    		$payment_method = get_post_meta( $order_id, '_payment_method', true );

	    		foreach ( $gateways as $key => $gateway ) {
	    			
	    			if ( $key != $payment_method ) {
	    				unset( $gateways[ $key ] );
	    			}
	    		}

	    	}


	    }

		return $gateways;
	}

	/**
	* Admin Order Action (new icon to conirm order)
	*
	* @access public
	* @wp-hook woocommerce_admin_order_actions
	* @param Array $actions
	* @param WC_Order $order
	* @return Array
	*/	
	public function admin_icon_confirm( $actions, $order ) {

		if ( self::confirmation_needed( $order ) ) {
			
			// unset all other actions
			$actions = array();

			$actions[ 'complete' ] = array(
				'url'       => wp_nonce_url( admin_url( 'admin-ajax.php?action=german_market_manual_order_confirmation&order_id=' . $order->get_id() ), 'german-market-manual-order-confirmation' ),
				'name'      => __( 'Manual order confirmation', 'woocommerce-german-market' ),
				'action'    => 'complete'
			);

		}

		return $actions;

	}

	/**
	* Order Confirmation button next to "save" button
	*
	* @access public
	* @wp-hook woocommerce_order_actions_end
	* @param Integer $post_id
	* @return void
	*/	
	public function woocommerce_order_actions_end_confirm_button( $post_id ) {

		if ( self::confirmation_needed( $post_id ) ) { ?>
			
			<li style="width: 100%">
				<input type="submit" style="float: right;" class="button button-primary" name="gm_manual_order_confirmation" value="<?php echo __( 'Manual Order Confirmation', 'woocommerce-german-market' ); ?>">
			</li>
			
		<?php }

	}

	/**
	* Confirm order if button is pressed in backend
	*
	* @access public
	* @wp-hook woocommerce_process_shop_order_meta
	* @param WP_Post $post
	* @param Integer $post_id
	* @return void
	*/
	public function woocommerce_process_shop_order_meta_save( $post_id, $post ) {

		if ( isset( $_REQUEST[ 'gm_manual_order_confirmation' ] ) ) {
			self::confirm( $post_id );
		}

	}

	/**
	* Add bulk action
	*
	* @access public
	* @hook admin_footer
	* @return void
	*/
	public function bulk_admin_footer() {
		
		global $post_type;

		if ( 'shop_order' == $post_type ) {
			?>
			<script type="text/javascript">
			jQuery(function() {
				jQuery('<option>').val('gm_manual_order_confirmation').text('<?php _e( 'Manual Order Confirmation', 'woocommerce-german-market' )?>').appendTo('select[name="action"]');
				jQuery('<option>').val('gm_manual_order_confirmation').text('<?php _e( 'Manual Order Confirmation', 'woocommerce-german-market' )?>').appendTo('select[name="action2"]');
			});
			</script>
			<?php
		}
	}

	/**
	* Do bulk action
	*
	* @access public
	* @hook load-edit.php
	* @return void
	*/
	public function bulk_action() {

		$wp_list_table = _get_list_table( 'WP_Posts_List_Table' );
		$action        = $wp_list_table->current_action();

		// return if it's not the zip download action
		if ( $action != 'gm_manual_order_confirmation' ) {
			return;
		}

		// return if no orders are checked
		if ( ! isset( $_REQUEST[ 'post' ] ) ) {
			return;
		}

		$post_ids = array_map( 'absint', (array) $_REQUEST[ 'post' ] );

		// return if no order is checked
		if ( empty( $post_ids ) ) {
			return;
		}

		foreach ( $post_ids as $post_id ) {
			self::confirm( $post_id );
		}

	}

	/**
	* Ajax Action to confirm order
	*
	* @access public
	* @static
	* @hook wp_ajax_german_market_manual_order_confirmation
	* @return void
	*/
	public static function admin_icon_confirm_action() {

		if ( ! check_ajax_referer( 'german-market-manual-order-confirmation', 'security', false ) ) {
			wp_die( __( 'You have taken too long. Please go back and retry.', 'woocommerce-german-market' ), '', array( 'response' => 403 ) );
		}

		$order_id = isset( $_REQUEST[ 'order_id' ] ) ? $_REQUEST[ 'order_id' ] : null;

		if ( $order_id ) {
			self::confirm( $order_id );
		}

		wp_safe_redirect( wp_get_referer() );

		exit();

	}

	/**
	* Returns true if post meta '_gm_needs_conirmation' is set to 'yes'
	*
	* @access public
	* @static
	* @param WC_Order || Integer $order
	* @return Boolean
	*/
	public static function confirmation_needed( $order ) {

		if ( is_object( $order ) ) {
			$order_id = $order->get_id();
		} else {
			$order_id = $order;
		}

		return get_post_meta( $order_id, '_gm_needs_conirmation', true ) == 'yes';
	}

	/**
	* Ajax Action to confirm order
	*
	* @access public
	* @static
	* @hook wp_ajax_german_market_manual_order_confirmation
	* @return void
	*/
	public static function confirm( $order_id ) {

		if ( self::confirmation_needed( $order_id ) ) {

			// init to avoid errors
			if ( WC()->cart == null ) {
				WC()->cart = new WC_Cart();
			}

			// Delete Post Meta
			update_post_meta( $order_id, '_gm_needs_conirmation', 'confirmed' );

			// Update Order Status
			$status_after_confirmation = apply_filters( 'gm_order_status_after_confirmation', 'pending' );
			$order = wc_get_order( $order_id );
			
			$mailer = WC()->mailer();
			$mails = $mailer->get_emails();

			$stati = array(
				'woocommerce_order_status_' . $status_after_confirmation . '_to_processing',
				'woocommerce_order_status_' . $status_after_confirmation . '_to_completed',
				'woocommerce_order_status_' . $status_after_confirmation . '_to_cancelled',
				'woocommerce_order_status_' . $status_after_confirmation . '_to_on-hold',
				'woocommerce_order_status_on-hold_' . $status_after_confirmation,
			);
		
			// Remove now all actions so we have not 2 emails send!	
			foreach ( $stati as $status ){
				remove_all_actions( $status . '_notification' );
			}

			$order->update_status( $status_after_confirmation );

			// Process Order Payment (e.g. stock management)
			
			if ( $order->get_payment_method() != 'german_market_sepa_direct_debit' ) {

				$gateways = WC()->payment_gateways()->payment_gateways();

				if ( isset( $gateways[ $order->get_payment_method() ] ) ) {

					$gateway = $gateways[ $order->get_payment_method() ];
					
					if ( is_object( $gateway ) ) {
						$result = @$gateway->process_payment( $order_id );
					}

				}

			}
			
			// Send Email to Customer
			// Filter processing order string translation
			add_filter( 'gettext', array( __CLASS__, 'new_text_in_order_processing_mail' ), 10, 3 );
			add_action( 'woocommerce_email_before_order_table', array( __CLASS__, 'woocommerce_email_before_order_table_process' ), 10, 3 );

			$wc_mails = WC_Emails::instance();
			$mail = new WC_Email_Customer_Processing_Order();
			$mail->trigger( $order_id );

		}

	}

	/**
	* Change text in order processing mail
	*
	* @access public
	* @static
	* @wp-hook gettext
	* @param String $translated
	* @param String $original
	* @param String $domain
	* @return String
	*/
	public static function new_text_in_order_processing_mail( $translated, $original, $domain ) {
		
		$search = 'Your order has been received and is now being processed. Your order details are shown below for your reference:';
		
		if ( $domain == 'woocommerce' && $original == $search ) {
			
			$translated =  __( 'Your order has been confirmed. Your order details are shown below for your reference.', 'woocommerce-german-market' );

		}
		
		return $translated;

	}

	/**
	* Show Payment link in new processing mail
	*
	* @access public
	* wp-hook woocommerce_email_before_order_table
	* @param WC_Order $order
	* @param Boolean $sent_to_admin
	* @param Boolean $plain_text
	* @return void
	*/
	public static function woocommerce_email_before_order_table_process( $order, $sent_to_admin, $plain_text ) {

		if ( $order->get_status() == 'pending' ) {

			$pay_link = $order->get_checkout_payment_url();

			if ( $plain_text ) {
			
				$text = __( 'Pay now', 'woocommerce-german-market' ) . ': ' . $pay_link . '\n\n'; 

			} else {

				$text = '<p><a href="' . $pay_link . '">' . __( 'Pay Now', 'woocommerce-german-market' ) . '</a></p>';

			}

			echo $text;
		}

	}

	/**
	* Show info on my_account
	*
	* @access public
	* wp-hook woocommerce_view_order
	* @param Integer $order_id
	* @return void
	*/
	public static function view_order_info( $order_id ) {
		
		if ( get_post_meta( $order_id, '_gm_needs_conirmation', true ) == 'yes' ) {
		
			?><p><?php
				echo apply_filters( 'gm_manual_order_confirmation_notice_in_email', __( 'Your order will be manually checked. You will get another email after your order has been confirmed.', 'woocommerce-german-market' ) );
			?></p><?php

		}
			
	}

}
