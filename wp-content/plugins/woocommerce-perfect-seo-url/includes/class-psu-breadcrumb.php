<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( ! class_exists( 'PSU_Breadcrumb' ) ) :

/**
 * PSU_Breadcrumb based on WC_Breadcrumb class
 */
class PSU_Breadcrumb {

    /**
     * PSU_Breadcrumb instance
     */
    protected static $_instance = null;

    /**
     * Breadcrumb trail
     *
     * @var array
     */
    private $crumbs = array();

    /**
     * Main PSU_Breadcrumb instance
     * Ensures only one instance of PSU_Breadcrumb is loaded or can be loaded.
     *
     * @static
     */
    public static function instance() {
        if ( is_null( self::$_instance ) ) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * Cloning is forbidden.
     *
     * @since 2.3
     */
    public function __clone() {
        _doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'psu' ), '2.3' );
    }

    /**
     * Unserializing instances of this class is forbidden.
     *
     * @since 2.3
     */
    public function __wakeup() {
        _doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'psu' ), '2.3' );
    }

    public function __construct() {
        if ( PSU()->is_activated( true ) ) {
            add_filter( 'woocommerce_get_breadcrumb', array( $this, 'woocommerce_get_breadcrumb' ), 10, 2 );
        }
    }

    /**
     * Add a crumb so we don't get lost
     *
     * @param string $name
     * @param string $link
     */
    public function add_crumb( $name, $link = '' ) {
        $this->crumbs[] = array(
            $name,
            $link
        );
    }

    /**
     * Breadcrumbs based on url
     *
     * @param array $breadcrumbs
     * @param WC_Breadcrumb $wc_breadcrumbs
     *
     * @return array
     */
    public function woocommerce_get_breadcrumb( $breadcrumbs, $wc_breadcrumbs ) {
        global $wp_the_query, $wp;

        $categories = explode( '/', $wp->request );
        array_pop( $categories );

        if ( ! $id = $wp_the_query->get_queried_object_id() )
            return $breadcrumbs;

        if ( ! is_single( $id ) || $wp_the_query->query['post_type'] !== PSU()->woocommerce_post_type )
            return $breadcrumbs;

        // Reset WooCommerce breadcrumbs
        $wc_breadcrumbs->reset();
        
        // Begin PSU breadcrumb trail
        $this->add_crumb( _x( 'Home', 'breadcrumb', 'woocommerce' ), apply_filters( 'woocommerce_breadcrumb_home_url', home_url() ) );

        foreach ( $categories as $category_slug ) {
            $term = get_term_by( 'slug', $category_slug, PSU()->woocommerce_taxonomy );
            if ( $term ) {
                $this->add_crumb( $term->name, get_term_link( $term ) );
            }
        }

        $last_breadcrumb = end( $breadcrumbs );
        $this->add_crumb( $last_breadcrumb[0], '' );

        return $this->crumbs;
    }

}

endif;