<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( ! class_exists( 'PSU_Compatibility' ) ) :

/**
 * PSU_Compatibility Class
 */
class PSU_Compatibility {

    /**
     *  Constructor
     */
    public function __construct() {
        add_action( 'admin_init', array( $this, 'check' ) );

        if ( ! is_admin() ) {
            add_action( 'init', array( $this, 'check' ) );
        }
    }

    /**
     * Check for compatible plugins
     *
     * @return void
     */
    public function check() {
        $plugins = array(
            'sitepress-multilingual-cms/sitepress.php',
            'amp/amp.php',
            'wordpress-seo/wp-seo.php'
        );

        if ( ! is_admin() ) {
            require_once ABSPATH . 'wp-admin/includes/plugin.php';
        }

        foreach ( $plugins as $plugin ) {
            if ( is_plugin_active( $plugin ) ) {
                require_once PSU_PLUGIN_PATH . 'includes/compatibility/' . basename( $plugin );
            }
        }
    }

}

endif;

return new PSU_Compatibility();