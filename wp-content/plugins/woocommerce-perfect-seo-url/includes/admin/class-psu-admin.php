<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( ! class_exists( 'PSU_Admin' ) ) :

class PSU_Admin {

    /**
     * PSU_Admin instance
     */
    protected static $_instance = null;

    /**
     * Main PSU_Admin instance
     * Ensures only one instance of PSU_Admin is loaded or can be loaded.
     *
     * @static
     */
    public static function instance() {
        if ( is_null( self::$_instance ) ) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * Cloning is forbidden.
     *
     * @since 2.1.1
     */
    public function __clone() {
        _doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'psu' ), '2.1.1' );
    }

    /**
     * Unserializing instances of this class is forbidden.
     *
     * @since 2.1.1
     */
    public function __wakeup() {
        _doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'psu' ), '2.1.1' );
    }

    public function __construct() {
        add_action( 'admin_menu', array( $this, 'add_options_page' ), 99 );
        add_action( 'admin_init', array( $this, 'settings' ) );
        add_action( 'admin_init', array( $this, 'redirect_to_correct_tab' ) );
        add_action( 'admin_init', array( $this, 'enqueue_scripts' ) );
        add_action( 'admin_notices', array( $this, 'admin_notices' ) );

        add_action( 'tab_general', array( $this, 'tab_general' ) );
        add_action( 'tab_license', array( $this, 'tab_license' ) );
        add_action( 'tab_install', array( $this, 'tab_install' ) );

        if ( PSU()->is_activated( true ) ) {
            //add_action( 'admin_init', array( PSU(), 'flush' ), 999 );

            $this->update_check(
                PSU()->upgrade_url,
                PSU()->plugin_name,
                PSU()->product_id,
                get_option( 'psu_key' ),
                get_option( 'psu_email' ),
                'https://www.perfectseourl.com/account/licenses/',
                get_option( 'psu_instance' ),
                str_ireplace( array( 'http://', 'https://' ), '', home_url() ),
                PSU()->version,
                'psu'
            );
        }
    }

    /**
     * Update check Class.
     *
     * @return PSU_Admin_Update_API_Check
     */
    public function update_check( $upgrade_url, $plugin_name, $product_id, $api_key, $activation_email, $renew_license_url, $instance, $domain, $software_version, $text_domain, $extra = '' ) {
        return PSU_Admin_Update_API_Check::instance( $upgrade_url, $plugin_name, $product_id, $api_key, $activation_email, $renew_license_url, $instance, $domain, $software_version, $text_domain, $extra );
    }

    /**
     * Admin notices
     */
    public function admin_notices() {
        global $hook_suffix, $wp_version;

        if ( ! PSU()->is_activated() ) {
            if ( isset( $_GET['page'] ) && 'wc-psu' === $_GET['page'] ) return;

            echo '<div class="error"><p>' . sprintf( __( 'Perfect SEO Url license key has not been activated, so the plugin is inactive! %sClick here%s to activate the license key and the plugin.', 'psu' ), '<a href="' . esc_url( admin_url( 'admin.php?page=wc-psu&tab=license' ) ) . '">', '</a>' ) . '</p></div>';
        } else {
            if ( ! PSU()->is_installed() ) {
                if ( isset( $_GET['page'] ) && 'wc-psu' === $_GET['page'] ) return;

                echo '<div class="error"><p>' . sprintf( __( 'Perfect SEO Url has not been installed, so the plugin is inactive! %sClick here%s to install the plugin.', 'psu' ), '<a href="' . esc_url( admin_url( 'admin.php?page=wc-psu&tab=install' ) ) . '">', '</a>' ) . '</p></div>';
            }
        }

        if ( version_compare( $wp_version, '4.4', '<' ) ) {
            echo '<div class="error"><p>' . sprintf( __( 'Perfect SEO Url requires Wordpress version %s or greater to work properly.', 'psu' ), '4.4' ) . '</p></div>';
        }
    }

    public function add_options_page() {
        add_submenu_page(
            'woocommerce',
            'Perfect SEO url',
            'Perfect SEO url',
            'manage_options',
            'wc-psu',
            array( $this, 'sections' )
        );
    }

    public function sections() {
        $current_tab = empty( $_GET['tab'] ) ? 'general' : sanitize_title( $_GET['tab'] );

        $tabs = array(
            'general' => __( 'General', 'psu' ),
            'license' => __( 'License', 'psu' )
        );

        if ( ! PSU()->is_installed() ) {
            $tabs['install'] = __( 'Installation', 'psu' );
        }

        if ( ! array_key_exists( $current_tab, $tabs ) ) {
            exit;
        }
        ?>
        <div class="wrap woocommerce">
            <form method="post" id="mainform" action="options.php">
                <div class="icon32 icon32-woocommerce-settings" id="icon-woocommerce"><br /></div>
                <h2 class="nav-tab-wrapper woo-nav-tab-wrapper">
                    <?php
                    foreach ( $tabs as $name => $label ) {
                        echo '<a href="' . admin_url( 'admin.php?page=wc-psu&tab=' . $name ) . '" class="nav-tab ' . ( $current_tab == $name ? 'nav-tab-active' : '' ) . '">' . $label . '</a>';
                    }
                    ?>
                </h2>

                <?php
                settings_errors();
                do_action( 'tab_' . $current_tab );
                ?>
            </form>
        </div>
        <?php
    }

    public function tab_general() {
        settings_fields( 'psu_general_group' );
        do_settings_sections( 'psu-general-product' );
        do_settings_sections( 'psu-general-category' );
        do_settings_sections( 'psu-general-canonical' );
        do_settings_sections( 'psu-general-breadcrumbs' );
        do_settings_sections( 'psu-general-url-suffix' );
        submit_button();
    }

    public function tab_license() {
        if ( PSU()->is_activated() ) {
            settings_fields( 'psu_license_deactivation_group' );
            do_settings_sections( 'psu-license-deactivation' );

            $psu_key = get_option( 'psu_key' );
            $psu_key_count = strlen( $psu_key ) - 4;
            $psu_key_last_chars = substr( $psu_key, -4 );
            $psu_key = '';
            for ( $i = 0; $i < $psu_key_count; $i++ ) {
                $psu_key .= '*';
            }
            $psu_key .= $psu_key_last_chars;

            echo '<p>License email: <strong>' . get_option( 'psu_email' ) . '</strong></p>';
            echo '<p>License key: <strong>' . $psu_key . '</strong></p>';

            submit_button( __( 'Deactivate License', 'psu' ), 'delete' );
        } else {
            settings_fields( 'psu_license_group' );
            do_settings_sections( 'psu-license' );
            submit_button( __( 'Activate License', 'psu' ) );
        }
    }

    public function tab_install() {
        $current_section = empty( $_GET['section'] ) ? null : sanitize_title( $_GET['section'] );

        if ( $current_section === 'run' ) {
            
            global $wpdb;

            $product_count = $wpdb->query( $wpdb->prepare( 'SELECT ID FROM ' . $wpdb->posts . ' WHERE post_type = %s AND post_status = %s', PSU()->woocommerce_post_type, 'publish' ) );
            $chunck_size = 100;
            $productChuncks = ceil( $product_count / $chunck_size );

            $categoryChuncks = count(get_terms( PSU()->woocommerce_taxonomy, array(
                'parent' => 0,
                'hide_empty' => false
            )));

            echo '<h3 id="psu-install-title">Installation is running</h3>';
            echo '<h2>Installing may take some time. Please do not close your browser or refresh the page until the process is complete.</h2>';
            echo '<div id="psu-install-status"><div class="loading"><div class="spinner" style="float:left; visibility:visible;"></div></div><div class="status" style="clear:both; max-height:300px; overflow-y:auto;"></div></div>';
            echo '<script type="text/javascript">jQuery(document).ready(function() {
                jQuery(this).runInstall(' . $productChuncks . ', ' . $categoryChuncks . ');
            });</script>';
        } else {
            echo '<h3>Perfect SEO url Installation</h3>';
            echo '<p>Save old product, product category and product tag urls so they can be redirected to the new urls.</p>';
            echo '<p class="submit"><input type="button" name="submit" id="psu-start-install" class="button button-primary" value="Start Installation" /></p>';
        }
    }

    public function save_general_settings() {
        if ( empty( $_POST ) ) {
            return false;
        }

        $input = $_POST;

        update_option( 'psu_flush_rewrite', 'yes' );

        update_option( 'psu_redirect_product', isset( $input['psu_redirect_product'] ) ? 'yes' : 'no' );
        update_option( 'psu_redirect_category', isset( $input['psu_redirect_category'] ) ? 'yes' : 'no' );
        update_option( 'psu_product_canonical_nc', isset( $input['psu_product_canonical_nc'] ) ? 'yes' : 'no' );
        update_option( 'psu_product_multiple_urls', isset( $input['psu_product_multiple_urls'] ) ? 'yes' : 'no' );
        update_option( 'psu_breadcrumb_rewrite_enabled', isset( $input['psu_breadcrumb_rewrite_enabled'] ) ? 'yes' : 'no' );
        update_option( 'psu_rewrite_yoast_canonical', isset( $input['psu_rewrite_yoast_canonical'] ) ? 'yes' : 'no' );
        update_option( 'psu_url_suffix_enabled', isset( $input['psu_url_suffix_enabled'] ) ? 'yes' : 'no' );
        update_option( 'psu_url_suffix', $input['psu_url_suffix'] );

        $psu_product_hierarchical_slugs = isset( $input['psu_product_hierarchical_slugs'] ) ? 'yes' : 'no';

        update_option( 'psu_product_hierarchical_slugs', $psu_product_hierarchical_slugs );

        if ( $psu_product_hierarchical_slugs === 'yes' ) {
            $permalinks = get_option( 'woocommerce_permalinks' );

            if ( $permalinks['product_base'] === '/' . PSU()->woocommerce_product_slug ) {
                $permalinks['product_base'] = '/' . PSU()->woocommerce_product_slug . '/%product_cat%';
            }

            if ( $permalinks['product_base'] === '/' . _x( 'shop', 'default-slug', 'woocommerce' ) ) {
                $permalinks['product_base'] = '/' . _x( 'shop', 'default-slug', 'woocommerce' ) . '/%product_cat%';
            }

            update_option( 'woocommerce_permalinks', $permalinks );
        }

        add_settings_error( 'save_general_settings', 'save_general_settings', __( 'Your settings have been saved.' ), 'updated' );
    }

    public function save_license_settings() {
        if ( empty( $_POST ) ) {
            return false;
        }

        $input = $_POST;

        $api_email = sanitize_email( $input['psu_email'] );
        $api_key = sanitize_text_field( $input['psu_key'] );

        $activation_status = get_option( 'psu_activated' );
        $current_api_key = get_option( 'psu_key' );

        if ( $activation_status === 'Deactivated' || $activation_status === '' || $api_key === '' || $api_email === '' || $current_api_key !== $api_key  ) {
            $args = array(
                'email' => $api_email,
                'licence_key' => $api_key
            );

            $activate_results = PSU()->key()->activate( $args );

            if ( is_wp_error( $activate_results ) ) {
                $debug_error = '<hr /><h3>Debug data</h3>Code: ' . $activate_results->get_error_code() . '<br />Message: ' . $activate_results->get_error_message() . '<br />Data: ' . $activate_results->get_error_data();
                add_settings_error( 'api_key_wp_text', 'api_key_wp_error', __( 'Activation failed.', 'psu' ) . $debug_error, 'error' );
                return;
            }

            $activate_results = json_decode( $activate_results, true );
			$activate_results['activated'] = true;

            if ( $activate_results['activated'] === true ) {
                update_option( 'psu_email', $api_email );
                update_option( 'psu_key', $api_key );
                update_option( 'psu_activated', 'Activated' );
                update_option( 'psu_flush_rewrite', 'yes' );

                add_settings_error( 'activate_text', 'activate_msg', __( 'License activated.', 'psu' ) . "{$activate_results['message']}.", 'updated' );
                return;
            }

            if ( $activate_results == false ) {
                add_settings_error( 'api_key_check_text', 'api_key_check_error', __( 'Connection failed to the License Key server. Please try again later.', 'psu' ), 'error' );
            }

            if ( isset( $activate_results['code'] ) ) {
                switch ( $activate_results['code'] ) {
                    case '100':
                    case '101':
                    case '102':
                    case '103':
                    case '104':
                    case '105':
                    case '106':
                        add_settings_error( 'api_key_error', 'api_key_error', "{$activate_results['error']}. {$activate_results['additional info']}", 'error' );
                        break;
                }
            }
        }
    }

    public function save_deactivation_license_settings() {
        if ( empty( $_POST ) ) {
            return false;
        }

        $email = get_option( 'psu_email' );
        $key = get_option( 'psu_key' );

        $this->deactivate_license_key( $email, $key );
    }

    public function license_key_status( $email, $key ) {
        $args = array(
            'email' => $email,
            'licence_key' => $key
        );

        return json_decode( PSU()->key()->status( $args ), true );
    }

    public function deactivate_license_key( $email, $key ) {
        $args = array(
            'email' => $email,
            'licence_key' => $key
        );

        $reset = PSU()->key()->deactivate( $args );

        if ( $reset === false ) {
            add_settings_error( 'not_deactivated_text', 'not_deactivated_error', __( 'The license could not be deactivated on the License Key server.', 'psu' ), 'error' );
        } else {
            update_option( 'psu_email', '' );
            update_option( 'psu_key', '' );
            update_option( 'psu_activated', 'Deactivated' );
            update_option( 'psu_flush_rewrite', 'yes' );

            add_settings_error( 'deactivated_text', 'deactivated_error', __( 'The license has been deactivated.', 'psu' ), 'updated' );
        }
    }

    public static function ajax() {
        $function = sanitize_title( $_GET['func'] );

        if ( $function === 'install_product_redirects' ) {
            PSU_Install::install_product_redirects();
        }

        if ( $function === 'install_category_redirects' ) {
            PSU_Install::install_category_redirects();
        }

        if ( $function === 'install_category_groups' ) {
            PSU_Install::install_category_groups();
        }

        if ( $function === 'install_tag_redirects' ) {
            PSU_Install::install_tag_redirects();
        }

        if ( $function === 'install_finish' ) {
            PSU_Install::install_finish();
        }
    }

    public function redirect_to_correct_tab() {
        $current_page = ! empty( $_GET['page'] ) ? sanitize_title( $_GET['page'] ) : null;
        $current_tab = ! empty( $_GET['tab'] ) ? sanitize_title( $_GET['tab'] ) : null;

        if ( $current_page === 'wc-psu' ) {

            if ( $current_tab !== 'license' && ! PSU()->is_activated() ) {
                wp_redirect( admin_url( 'admin.php?page=wc-psu&tab=license' ) );
                exit;
            }

            if ( $current_tab !== 'install' && $current_tab !== 'license' && PSU()->is_activated() && ! PSU()->is_installed() ) {
                wp_redirect( admin_url( 'admin.php?page=wc-psu&tab=install' ) );
                exit;
            }

            if ( $current_tab === 'install' && PSU()->is_activated() && PSU()->is_installed() ) {
                wp_redirect( admin_url( 'admin.php?page=wc-psu' ) );
                exit;
            }

        }
    }

    public function setting_input_field( $args ) {
        $option = $args['label_for'];
        $style = isset( $args['style'] ) ? ' style="' . $args['style'] . '"' : '';
        $description = isset( $args['description'] ) ? '<span class="description">' . __( $args['description'], 'psu' ) . '</span>' : '';

        printf('<input type="text" name="%s" id="%s" value="%s"%s />%s',
            $option,
            $option,
            get_option( $option ),
            $style,
            $description
        );
    }

    public function setting_checkbox_field( $args ) {
        $option = $args['label_for'];
        $description = __( $args['description'], 'psu' );

        printf('<label for="%s"><input type="checkbox" name="%s" id="%s" value="1" %s/>%s</label>',
            $option,
            $option,
            $option,
            get_option( $option ) === 'yes' ? 'checked="checked"' : '',
            $description
        );
    }

    public function settings() {
        // General settings

        register_setting(
            'psu_general_group',
            'psu_general',
            array( $this, 'save_general_settings' )
        );

        add_settings_section(
            'psu_general_product_section',
            'Product Options',
            '',
            'psu-general-product'
        );

        add_settings_section(
            'psu_general_category_section',
            'Category Options',
            '',
            'psu-general-category'
        );

        add_settings_section(
            'psu_general_canonical_section',
            'Canonical Options',
            '',
            'psu-general-canonical'
        );

        add_settings_section(
            'psu_general_breadcrumbs_section',
            'Breadcrumb Options',
            '',
            'psu-general-breadcrumbs'
        );

        add_settings_section(
            'psu_general_url_suffix_section',
            'Url suffix',
            '',
            'psu-general-url-suffix'
        );

        add_settings_field(
            'psu_redirect_product',
            'Redirect products',
            array( $this, 'setting_checkbox_field' ),
            'psu-general-product',
            'psu_general_product_section',
            array(
                'label_for' => 'psu_redirect_product',
                'description' => 'Redirect old product urls.'
            )
        );

        add_settings_field(
            'psu_product_hierarchical_slugs',
            'Hierarchical slugs',
            array( $this, 'setting_checkbox_field' ),
            'psu-general-product',
            'psu_general_product_section',
            array(
                'label_for' => 'psu_product_hierarchical_slugs',
                'description' => 'Include all categories in hierarchical order in the url for products and categories.'
            )
        );

        add_settings_field(
            'psu_redirect_category',
            'Redirect categories',
            array( $this, 'setting_checkbox_field' ),
            'psu-general-category',
            'psu_general_category_section',
            array(
                'label_for' => 'psu_redirect_category',
                'description' => 'Redirect old category urls.'
            )
        );

        add_settings_field(
            'psu_product_canonical_nc',
            'Product canonical',
            array( $this, 'setting_checkbox_field' ),
            'psu-general-canonical',
            'psu_general_canonical_section',
            array(
                'label_for' => 'psu_product_canonical_nc',
                'description' => 'Remove categories from product canonical url. Example: ' . trim( get_option( 'home' ), '/' ) . '/my-product/'
            )
        );

        add_settings_field(
            'psu_product_multiple_urls',
            'Product multiple urls',
            array( $this, 'setting_checkbox_field' ),
            'psu-general-product',
            'psu_general_product_section',
            array(
                'label_for' => 'psu_product_multiple_urls',
                'description' => 'Allow a product to have multiple urls when linked to multiple categories.'
            )
        );

        add_settings_field(
            'psu_breadcrumb_rewrite_enabled',
            'Rewrite breadcrumbs',
            array( $this, 'setting_checkbox_field' ),
            'psu-general-breadcrumbs',
            'psu_general_breadcrumbs_section',
            array(
                'label_for' => 'psu_breadcrumb_rewrite_enabled',
                'description' => 'Rewrite default WooCommerce breadcrumbs. Breadcrumbs are based on the product url.'
            )
        );

        add_settings_field(
            'psu_rewrite_yoast_canonical',
            'Rewrite Yoast SEO canonical',
            array( $this, 'setting_checkbox_field' ),
            'psu-general-canonical',
            'psu_general_canonical_section',
            array(
                'label_for' => 'psu_rewrite_yoast_canonical',
                'description' => 'Prefer Perfect SEO Url canonical over Yoast SEO canonical urls for categories and products.'
            )
        );

        add_settings_field(
            'psu_url_suffix_enabled',
            'Add url suffix',
            array( $this, 'setting_checkbox_field' ),
            'psu-general-url-suffix',
            'psu_general_url_suffix_section',
            array(
                'label_for' => 'psu_url_suffix_enabled',
                'description' => 'Add a suffix to product and product categories.'
            )
        );

        add_settings_field(
            'psu_url_suffix',
            'Url suffix',
            array( $this, 'setting_input_field' ),
            'psu-general-url-suffix',
            'psu_general_url_suffix_section',
            array(
                'label_for' => 'psu_url_suffix'
            )
        );

        // License settings

        register_setting(
            'psu_license_group',
            'psu_license',
            array( $this, 'save_license_settings' )
        );
        
        add_settings_section(
            'psu_license_section',
            'Perfect SEO url License',
            '',
            'psu-license'
        );

        add_settings_field(
            'psu_email',
            'License Email',
            array( $this, 'setting_input_field' ),
            'psu-license',
            'psu_license_section',
            array(
                'label_for' => 'psu_email',
                'style' => 'min-width:300px;'
            )
        );

        add_settings_field(
            'psu_key',
            'License Key',
            array( $this, 'setting_input_field' ),
            'psu-license',
            'psu_license_section',
            array(
                'label_for' => 'psu_key',
                'style' => 'min-width:300px;'
            )
        );

        // License deactivation settings

        register_setting(
            'psu_license_deactivation_group',
            'psu_license_deactivation',
            array( $this, 'save_deactivation_license_settings' )
        );

        add_settings_section(
            'psu_license_deactivation_section',
            'Perfect SEO url License',
            '',
            'psu-license-deactivation'
        );
    }

    /**
     * Enqueue admin scripts
     *
     * @return void
     */
    public function enqueue_scripts() {
        wp_register_script( 'psu-functions', PSU_PLUGIN_DIR . 'assets/js/functions.js', false, PSU()->version, true );
        wp_enqueue_script( 'psu-functions' );
        wp_register_style( 'psu-style', PSU_PLUGIN_DIR . 'assets/css/style.css', false, PSU()->version );
        wp_enqueue_style( 'psu-style' );
    }

}

endif;