<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( ! class_exists( 'PSU_Compatibility_AMP' ) ) :

class PSU_Compatibility_AMP {

    public function __construct() {
        add_filter( 'psu_rewrite_product_rewrites', array( $this, 'product_amp_rewrite_rule' ), 10, 5 );
    }

    /**
     * Add rewrite rule for AMP products.
     *
     * @return array
     */
    public function product_amp_rewrite_rule( $product_rewrites, $category, $product_prefix, $category_nicename, $url_suffix ) {
        $product_rewrites[$product_prefix . $category_nicename . '/([^/]+)/amp(/(.*))?/?$'] = 'index.php?product=$matches[1]&=$matches[3]';

        return $product_rewrites;
    }

}

endif;

new PSU_Compatibility_AMP();