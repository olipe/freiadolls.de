<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( ! class_exists( 'PSU_Compatibility_WPSEO' ) ) :

class PSU_Compatibility_WPSEO {

    /**
     * Breadcrumb trail
     *
     * @var array
     */
    private $crumbs = array();

    /**
     * Construct
     */
    public function __construct() {
        if ( get_option( 'psu_breadcrumb_rewrite_enabled' ) === 'yes' ) {
            add_filter( 'wpseo_breadcrumb_links', array( $this, 'wpseo_breadcrumb_links' ) );
        }
    }

    /**
     * Add a crumb so we don't get lost
     *
     * @param array $data
     */
    public function add_crumb( $data ) {
        $this->crumbs[] = $data;
    }

    /**
     * Breadcrumbs based on url
     *
     * @param array $breadcrumbs
     * @return array
     */
    public function wpseo_breadcrumb_links( $breadcrumbs ) {
        global $wp_the_query, $wp;

        $categories = explode( '/', $wp->request );
        array_pop( $categories );

        if ( ! $id = $wp_the_query->get_queried_object_id() )
            return $breadcrumbs;

        if ( ! is_single( $id ) || $wp_the_query->query['post_type'] !== PSU()->woocommerce_post_type )
            return $breadcrumbs;

        // Begin PSU breadcrumb trail
        $this->add_crumb( $breadcrumbs[0] );

        foreach ( $categories as $category_slug ) {
            $term = get_term_by( 'slug', $category_slug, PSU()->woocommerce_taxonomy );
            if ( $term ) {
                $this->add_crumb( array( 'term' => $term ) );
            }
        }

        $last_breadcrumb = end( $breadcrumbs );
        $this->add_crumb( $last_breadcrumb );

        return $this->crumbs;
    }

}

endif;

new PSU_Compatibility_WPSEO();