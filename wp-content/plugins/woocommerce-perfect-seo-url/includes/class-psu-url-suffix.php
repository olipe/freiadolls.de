<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( ! class_exists( 'PSU_Url_Suffix' ) ) :

class PSU_Url_Suffix {

    /**
     * PSU_Url_Suffix instance
     */
    protected static $_instance = null;

    /**
     * The url suffix
     */
    protected $_suffix;

    /**
     * Main PSU_Url_Suffix instance
     * Ensures only one instance of PSU_Url_Suffix is loaded or can be loaded.
     *
     * @static
     */
    public static function instance() {
        if ( is_null( self::$_instance ) ) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * Cloning is forbidden.
     *
     * @since 2.4
     */
    public function __clone() {
        _doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'psu' ), '2.4' );
    }

    /**
     * Unserializing instances of this class is forbidden.
     *
     * @since 2.4
     */
    public function __wakeup() {
        _doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'psu' ), '2.4' );
    }

    /**
     * Constructor
     */
    public function __construct() {
        if ( PSU()->is_activated( true ) ) {
            $this->_set_suffix();

            add_filter( 'psu_term_link_after', array( $this, 'add_suffix' ) );
            add_filter( 'psu_post_type_link_after', array( $this, 'add_suffix' ) );

            add_filter( 'psu_request_filter_url_before', array( $this, 'delete_suffix' ) );

            add_filter( 'psu_product_cat_canonical_page_link', array( $this, 'page_link' ), 10, 3 );
            add_filter( 'post_comments_feed_link', array( $this, 'feed_link' ) );
            add_filter( 'paginate_links', array( $this, 'paginate_links' ) );
            add_filter( 'redirect_canonical', array( $this, 'redirect_canonical' ), 10, 2 );

            add_filter( 'wpseo_next_rel_link', array( $this, 'wpseo_next_rel_link' ) );
            add_filter( 'wpseo_prev_rel_link', array( $this, 'wpseo_prev_rel_link' ) );

            add_filter( 'psu_rewrite_url_suffix', array( $this, 'get_suffix' ) );
        }
    }

    /**
     * Set suffix
     */
    protected function _set_suffix() {
        $this->_suffix = get_option( 'psu_url_suffix' );
    }

    /**
     * Return suffix
     */
    public function get_suffix() {
        return $this->_suffix;
    }

    /**
     * Add suffix to link
     */
    public function add_suffix( $link ) {
        $link = rtrim( $link, '/' );
        $link = $link . $this->_suffix;

        return $link;
    }

    /**
     * Delete suffix from link
     */
    public function delete_suffix( $link ) {
        if ( ! is_wp_error( $link ) ) {
            $link = preg_replace( '/' . $this->_suffix . '/', '', $link );
        }

        return $link;
    }

    /**
     * Set feed link
     */
    public function feed_link( $link ) {
        return $this->delete_suffix( $link );
    }

    /**
     * Set pagination links
     */
    public function paginate_links( $link ) {
        if ( is_admin() || is_search() ) {
            return $link;
        }

        $page = explode( '/', trim( $link, '/' ) );
        $page = end( $page );

        $term = PSU()->get_current_category();
        if ( is_null( $term ) ) {
            return $this->delete_suffix( $link );
        }

        $link = $this->page_link( $link, $page, $term->term_id );

        return $link;
    }

    /**
     * Set page link for category archive
     */
    public function page_link( $link, $page, $term_id ) {
        if ( is_search() ) {
            return $link;
        }

        global $wp_rewrite;

        $link = get_term_link( $term_id, PSU()->woocommerce_taxonomy );
        $link = $this->delete_suffix( $link );

        if ( $page == 1 ) {
            return $link . $this->_suffix;
        }
        
        $link = trailingslashit( $link ) . $wp_rewrite->pagination_base . '/' . $page . $this->_suffix;

        return $link;
    }

    /**
     * Prevent redirecting url
     *
     * @param string $redirect_url The redirect URL.
     * @param string $requested_url The requested URL.
     *
     * @return string|boolean
     */
    public function redirect_canonical( $redirect_url, $requested_url ) {
        return false;
    }

    public function wpseo_next_rel_link( $output ) {
        if ( is_search() || ! is_tax( PSU()->woocommerce_taxonomy ) ) {
            return $output;
        }

        global $wp_rewrite;

        $term = PSU()->get_current_category();
        $page = get_query_var('paged') + 1;
        if ( $page === 1 ) {
            $page = 2;
        }

        $link = get_term_link( $term->term_id, PSU()->woocommerce_taxonomy );
        $link = $this->delete_suffix( $link );
        $link = trailingslashit( $link ) . $wp_rewrite->pagination_base . '/' . $page . $this->_suffix;

        $output = '<link rel="next" href="' . esc_url( $link ) . "\" />\n";

        return $output;
    }

    public function wpseo_prev_rel_link( $output ) {
        if ( is_search() || ! is_tax( PSU()->woocommerce_taxonomy ) ) {
            return $output;
        }

        global $wp_rewrite;

        $term = PSU()->get_current_category();
        $page = get_query_var('paged') - 1;

        $link = get_term_link( $term->term_id, PSU()->woocommerce_taxonomy );
        $link = $this->delete_suffix( $link );

        if ( $page === 1 ) {
            $link = $link . $this->_suffix;
        } else {
            $link = trailingslashit( $link ) . $wp_rewrite->pagination_base . '/' . $page . $this->_suffix;
        }

        $output = '<link rel="prev" href="' . esc_url( $link ) . "\" />\n";

        return $output;
    }

}

endif;