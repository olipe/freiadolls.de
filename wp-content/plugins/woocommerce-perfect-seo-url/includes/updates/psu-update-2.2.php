<?php
/**
 * Update PSU to 2.2
 *
 * @author      PSU
 * @category    Admin
 * @version     2.2
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

global $wpdb;

// Create term group table
$sql = "CREATE TABLE " . $wpdb->prefix . PSU()->table_terms . " (
    term_id bigint(20) NOT NULL,
    term_group bigint(20) NOT NULL,
    term_level smallint(3) NOT NULL,
    PRIMARY KEY  (term_id),
    KEY term_group (term_group)
);";

require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
dbDelta( $sql );

// Add term groups
$terms = get_terms( PSU()->woocommerce_taxonomy, array(
    'parent' => 0,
    'hide_empty' => false
));

foreach ($terms as $term) {
	$term_children = get_term_children( $term->term_id, PSU()->woocommerce_taxonomy );
	$term_children[] = $term->term_id;
	
	foreach ( $term_children as $term_id ) {
    	PSU()->term->insert( $term_id, array(
            'term_group' => $term->term_id,
            'term_level' => PSU()->term->get_term_level( $term_id )
        ));
	}
}

// Update options
update_option( 'psu_product_canonical_nc', 'no' );