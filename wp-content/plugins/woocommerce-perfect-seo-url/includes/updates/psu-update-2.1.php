<?php
/**
 * Update PSU to 2.1
 *
 * @author      PSU
 * @category    Admin
 * @version     2.1
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

// Flush rewrite rules
update_option( 'psu_flush_rewrite', 'yes' );