<?php
/**
 * Update PSU to 2.4
 *
 * @author      PSU
 * @category    Admin
 * @version     2.4
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

// Update options
update_option( 'psu_url_suffix_enabled', 'no' );
update_option( 'psu_url_suffix', '.html' );