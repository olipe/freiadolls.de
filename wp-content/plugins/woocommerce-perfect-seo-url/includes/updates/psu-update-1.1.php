<?php
/**
 * Update PSU to 1.1
 *
 * @author 		PSU
 * @category 	Admin
 * @version     1.1
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $wpdb;

// Update table
$wpdb->query( "ALTER TABLE `" . $wpdb->prefix . PSU()->table_redirects . "` CHANGE `redirect_type` `redirect_type` ENUM( 'post', 'term', 'tag' ) NOT NULL DEFAULT 'post'" );

// Update options with the correct values
$wpdb->query( "UPDATE `" . $wpdb->options . "` SET `option_value` = 'no' WHERE `option_name` LIKE 'psu_%' AND `option_value` = '0'" );
$wpdb->query( "UPDATE `" . $wpdb->options . "` SET `option_value` = 'yes' WHERE `option_name` LIKE 'psu_%' AND `option_value` = '1'" );

// // Get all product tags
// $product_tags = get_terms( PSU()->woocommerce_tag_taxonomy, array(
// 	'get' => 'all'
// ) );

// // Add product tag url's to redirect table
// foreach ( $product_tags as $tag ) {
// 	$wpdb->insert( $wpdb->prefix . PSU()->table_redirects, array(
// 		'redirect_entity_id' => $tag->term_id,
// 		'redirect_type' => 'tag',
// 		'redirect_url' => trim( get_term_link( $tag, PSU()->woocommerce_tag_taxonomy ), '/' )
// 	));
// }

// Add options
add_option( 'psu_redirect_tag', 'no' );
add_option( 'psu_tag_support', 'no' );
add_option( 'psu_category_linked_to_product', 'yes' );