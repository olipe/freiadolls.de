<?php
/**
 * Update PSU to 2.5
 *
 * @author      PSU
 * @category    Admin
 * @version     2.5
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

// Update options
update_option( 'psu_flush_rewrite', 'yes' );