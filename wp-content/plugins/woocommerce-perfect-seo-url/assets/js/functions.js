jQuery(document).ready(function($) {

    $('#psu-start-install').click(function() {
        window.location = window.location.href + '&section=run';
        return false;
    });

    $('#add-redirect').live('click', function(event) {
        event.preventDefault();
        $(this).hide();
        $('#add-redirect-form').slideDown();
    });

    $('#add-redirect-form .cancel').live('click', function(event) {
        event.preventDefault();
        $('#add-redirect-form').slideUp();
        $('#add-redirect').slideDown();
    });

    $('#add-redirect-form .save').live('click', function(event) {
        event.preventDefault();

        $('#redirects-table .spinner').css('visibility', 'visible');

        var data = {
            action: 'psu-add-redirect',
            id: $('#psu_redirect_entity_id').val(),
            url: $('#psu_redirect_url').val(),
            status: $('#psu_redirect_status').val(),
            nonce: $('#psu_redirect_nonce').val()
        };

        $.ajax({
            method: 'post',
            url: ajaxurl,
            dataType: 'json',
            data: data
        }).done(function( response ) {
            if ( response.status === 'success' ) {
                $('#redirects-table').html( response.html );
            } else {
                $('#redirects-table .spinner').css('visibility', 'hidden');
                alert( response.message );
            }
        });
    });

    $('#redirects-table .edit').live('click', function(event) {
        event.preventDefault();
        var parent = $(this).parent().parent();
        $(this).hide();
        $(parent).find('span').hide();
        $(parent).find('input').show();
        $(parent).find('select').show();
        $(parent).find('.save').show();
    });

    $('#redirects-table .save').live('click', function(event) {
        event.preventDefault();
        
        var parent = $(this).parent().parent();
        $('#redirects-table .spinner').css('visibility', 'visible');

        var data = {
            action: 'psu-edit-redirect',
            id: $('#psu_redirect_entity_id').val(),
            redirect_id: $(parent).data('redirect-id'),
            url: $(parent).find('input').val(),
            status: $(parent).find('select').val(),
            nonce: $('#psu_redirect_nonce').val()
        };

        $.ajax({
            method: 'post',
            url: ajaxurl,
            dataType: 'json',
            data: data
        }).done(function( response ) {
            if ( response.status === 'success' ) {
                $('#redirects-table').html( response.html );
            } else {
                $('#redirects-table .spinner').css('visibility', 'hidden');
                alert( response.message );
            }
        });
    });

    $('#redirects-table .delete').live('click', function(event) {
        event.preventDefault();

        if ( ! confirm( 'Are you sure you want to remove this redirect?' ) ) {
            return;
        }

        $('#redirects-table .spinner').css('visibility', 'visible');

        var data = {
            action: 'psu-delete-redirect',
            id: $('#psu_redirect_entity_id').val(),
            redirect_id: $(this).parent().parent().data('redirect-id'),
            nonce: $('#psu_redirect_nonce').val()
        };

        $.ajax({
            method: 'post',
            url: ajaxurl,
            dataType: 'json',
            data: data
        }).done(function( response ) {
            if ( response.status === 'success' ) {
                $('#redirects-table').html( response.html );
            } else {
                $('#redirects-table .spinner').css('visibility', 'hidden');
                alert( response.message );
            }
        });
    });

});

jQuery.fn.runInstall = function( productChuncks, categoryChuncks ) {
    var status_container = jQuery('#psu-install-status');
    var status = jQuery(status_container).find('.status');
    var loading = jQuery(status_container).find('.loading');
    var title = jQuery('#psu-install-title');
    var queue = new jQuery.AjaxQueue();

    for ( var i = 0; i < productChuncks; i++ ) {
        queue.add({
            url: ajaxurl,
            data: {
                action: 'psu_ajax',
                func: 'install_product_redirects',
                offset: i
            },
            complete: function( response ) {
                jQuery(status).append( response.responseText + ' product redirects created and saved.<br />' );
                jQuery(status).animate({
                    scrollTop: jQuery(status)[0].scrollHeight
                });
            }
        });
    }

    queue.add({
        url: ajaxurl,
        data: {
            action: 'psu_ajax',
            func: 'install_category_redirects'
        },
        complete: function() {
            jQuery(status).append( 'Category redirects created and saved.<br />' );
            jQuery(status).animate({
                scrollTop: jQuery(status)[0].scrollHeight
            });
        }
    });

    for ( var i = 0; i < categoryChuncks; i++ ) {
        queue.add({
            url: ajaxurl,
            data: {
                action: 'psu_ajax',
                func: 'install_category_groups',
                offset: i
            },
            complete: function( response ) {
                jQuery(status).append( 'Category group ' + response.responseText + ' created and saved.<br />' );
                jQuery(status).animate({
                    scrollTop: jQuery(status)[0].scrollHeight
                });
            }
        });
    }

    queue.add({
        url: ajaxurl,
        data: {
            action: 'psu_ajax',
            func: 'install_tag_redirects'
        },
        complete: function() {
            jQuery(status).append( 'Tag redirects created and saved.<br />' );
            jQuery(status).animate({
                scrollTop: jQuery(status)[0].scrollHeight
            });
        }
    });

    queue.add({
        url: ajaxurl,
        data: {
            action: 'psu_ajax',
            func: 'install_finish'
        },
        complete: function() {
            jQuery(status).append( '<b>Installation is finished</b><br />' );
            jQuery(status).animate({
                scrollTop: jQuery(status)[0].scrollHeight
            });
            jQuery(loading).html('');
            jQuery(title).html('Installation is finished');
        }
    });
}

jQuery.AjaxQueue = function() {
  this.reqs = [];
  this.requesting = false;
};

jQuery.AjaxQueue.prototype = {
  add: function(req) {
    this.reqs.push(req);
    this.next();
  },
  next: function() {
    if (this.reqs.length == 0)
      return;

    if (this.requesting == true)
      return;

    var req = this.reqs.splice(0, 1)[0];
    var complete = req.complete;
    var self = this;
    if (req._run)
      req._run(req);
    req.complete = function() {
      if (complete)
        complete.apply(this, arguments);
      self.requesting = false;
      self.next();
    }

    this.requesting = true;
    jQuery.ajax(req);
  }
};